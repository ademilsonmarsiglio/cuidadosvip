package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.ItemProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.MedicamentoAplicacao;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.ProdutoJson;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAMedicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAProduto;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAMedicamentoAplicacao;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class MedicamentoService {

    @Autowired
    private JPAMedicamento jpa;
    @Autowired
    private JPAEstoque estoqueJPA;
    @Autowired
    private JPAProduto produtoJPA;
    @Autowired
    private JPAMedicamentoAplicacao medicamentoAplicacaoJPA;
    
    public String msgGravar;
    
    
    public boolean salvar(List<Medicamento> medicamentos, Paciente paciente, HttpSession httpSession) {
        
        List<Medicamento> listOriginal = list(paciente.getIdPaciente(), paciente.getPessoa().getNmPessoa());
        
        if(medicamentos != null){
            for (Medicamento medicamento : medicamentos) {
                if(!Verify.nullsOrEmpty(medicamento.getEstoque()) && !Verify.nullsOrEmpty(medicamento.getEstoque().getProduto().getDsProduto())){
                    medicamento.setPaciente(paciente);
                    if(!salvar(medicamento, httpSession)){
                        return false;
                    }
                    
                    if(medicamento.getIdMedicamento() != null){
                        for (Medicamento medOriginal : listOriginal) {
                            if(medOriginal.getIdMedicamento().equals(medicamento.getIdMedicamento())){
                                listOriginal.remove(medOriginal);
                                break;
                            }
                        }
                    }
                }
            }
        } 
        
        for (Medicamento medicamento : listOriginal) {
            if(!excluir(medicamento))
                return false;
        }
        
        return true;
    }
    
    public boolean salvar(Medicamento medicamento, HttpSession httpSession) {
        
        msgGravar = "";
        
        boolean inserindo = medicamento.getIdMedicamento() == null;

        if(inserindo){
            Produto produto = medicamento.getEstoque().getProduto();
            boolean verificaProduto_Paciente = true;
            
            //Produto Novo
            if(produto.getIdProduto() == null){
                
                Produto localizadoPeloNome = produtoJPA.localizaProdutoPeloNome(produto.getDsProduto());
                
                if(localizadoPeloNome != null){
                    produto = localizadoPeloNome;
                } else {
                    produtoJPA.incluir();
                    produtoJPA.setObjetoCorrente(produto);
                    produto.setEstabelecimento(EstabelecimentoService.getEstabelecimento(httpSession));
                    if(!produtoJPA.gravar()){
                        msgGravar = "Erro ao gravar o produto.";
                    } else {
                        verificaProduto_Paciente = false;
                    }

                    produto = produtoJPA.getObjeto();
                }
            }
            
            if(verificaProduto_Paciente && pacienteContemProduto(produto.getIdProduto(), medicamento.getPaciente().getIdPaciente())){
                msgGravar = "O produto informado já existe para este paciente.";
                return false;
            }
            
            //Estoque Novo
            produto.setQtdEstoqueInicial(medicamento.getEstoque().getQtd());
            
            Login login = (Login)httpSession.getAttribute(Key.LOGIN_SESSION);
            medicamento.setEstoque(estoqueJPA.insereEstoqueZerado(produto, 'P', login));
            
        }/* else { não altera mais o estoque, por causa do historico estoque.
            if(!estoqueJPA.save(medicamento.getEstoque())){
                msgGravar = "Erro ao gravar o estoque.";
                return false;
            }
        }*/
        
        boolean salvou = jpa.save(medicamento);
        
        if (salvou) {
            gravarAplicacaoMedicamentos(medicamento);
        }
        
        return salvou;
    }
    
    public Medicamento get(Long codigo){
        try {
            jpa.guardaConfiguracoes();
            
            jpa.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estoque.produto", JoinType.LEFT_OUTER_JOIN},
            });
            return jpa.get(codigo);
        } finally {
            jpa.restauraConfiguracoes();
        }
    }

    public boolean excluir(Medicamento medicamento) {
        msgGravar = "";
        Estoque estoque = medicamento.getEstoque();
        
        jpa.setObjeto(medicamento);
        if(!jpa.excluir()){
            msgGravar = "Erro ao excluir o medicamento.";
            return false;
        }
        
        GenericController<ItemProntuario> itemProntuarioCon = new GenericController(ItemProntuario.class);
        itemProntuarioCon.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("estoque", estoque)}});
        itemProntuarioCon.setProjections(new ProjectionParc[]{new ProjectionParc(Projections.id())});
        if(Verify.nullsOrEmpty(itemProntuarioCon.getLista())){
            if(!estoqueJPA.excluirEstoqueByProduto(estoque.getProduto().getIdProduto())){
                msgGravar = "Erro ao excluir o estoque do medicamento.";
            }
        }
        
        return true;
    }
    
    public List<Medicamento> list(Long idPaciente, String nmPaciente){
        
        try {
            jpa.guardaConfiguracoes();
            
            List arrCriterion = new ArrayList();
            arrCriterion.add(new Object[]{"paciente", Restrictions.eq("idPaciente", idPaciente)});

            if(nmPaciente != null && !nmPaciente.trim().isEmpty()){
                arrCriterion.add(new Object[]{"paciente.pessoa", Restrictions.like("nmPessoa", nmPaciente.replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()});
            }

            jpa.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estoque.produto", JoinType.LEFT_OUTER_JOIN},
            });

            jpa.setCriterions(arrCriterion.toArray());
            jpa.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.asc("dsProduto"), "estoque.produto"),
            });

            jpa.setResultTransformer(new AliasToBeanNestedResultTransformer(Medicamento.class));
            jpa.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("idMedicamento").as("idMedicamento")),
                new ProjectionParc(Projections.property("estoque.qtd").as("estoque.qtd")),
                new ProjectionParc(Projections.property("estoque.idEstoque").as("estoque.idEstoque")),
                new ProjectionParc(Projections.property("produto.dsProduto").as("estoque.produto.dsProduto")),
//                new ProjectionParc(Projections.property("posologia").as("posologia")),
//                new ProjectionParc(Projections.property("hrManha").as("hrManha")),
//                new ProjectionParc(Projections.property("hrTarde").as("hrTarde")),
//                new ProjectionParc(Projections.property("hrNoite").as("hrNoite")),
//                new ProjectionParc(Projections.property("aplicacaoManha").as("aplicacaoManha")),
//                new ProjectionParc(Projections.property("aplicacaoTarde").as("aplicacaoTarde")),
//                new ProjectionParc(Projections.property("aplicacaoNoite").as("aplicacaoNoite")),
                new ProjectionParc(Projections.property("seNecessario").as("seNecessario")),
                new ProjectionParc(Projections.property("dias").as("dias")),
                new ProjectionParc(Projections.property("observacao").as("observacao")),
            });

            return jpa.getLista();
        } finally {
            jpa.restauraConfiguracoes();
        }
    }
    
    public List<Medicamento> listar(Long idPaciente){
        
        String sql = 
                ""
                + "select "
                + "produto.dsProduto as \"estoque__produto__dsProduto\", "
                + "estoque.idEstoque as \"estoque__idEstoque\", "
                + "paciente.idPaciente as \"paciente__idPaciente\", "
                + "estoque.qtd as \"estoque__qtd\", "
                + "medicamento.idMedicamento as \"idMedicamento\", "
                + "medicamentoaplicacao.hora as \"hora\", " 
                + "medicamentoaplicacao.aplicacao as \"aplicacao\", "
                + "medicamentoaplicacao.id as \"idAplicacao\", "
                + "medicamento.observacao as \"observacao\","
                + "medicamento.seNecessario as \"seNecessario\", "
                + "cast(medicamento.dias as varchar) as \"str_dias\" "
                + "from estoque  "
                + "left outer join medicamento on medicamento.idEstoque=estoque .idEstoque  "
                + "left outer join produto on estoque.idProduto=produto.idProduto  "
                + "left outer join paciente on medicamento.idPaciente=paciente.idPaciente  "
                + "left outer join medicamentoaplicacao on medicamentoaplicacao.idMedicamento=medicamento.idMedicamento "
                + "where "
                + " produto.status = true AND "
                + " ((paciente.idPaciente=" + idPaciente + " ) or "
                + "   (estoque.tpestoque = 'G' "
                + "    and paciente.idPaciente is null "
                + "    and estoque.qtd > 0 "
                + "    and produto.idproduto not in (select idproduto from medicamento inner join estoque using (idestoque) where idpaciente = " + idPaciente + ")"
                + "   ) "
                + " ) "
                + "order by paciente.idPaciente, produto.dsProduto  ";
        
        JPAEstoque con = new JPAEstoque();
        con.setResultTransformer(new AliasToBeanNestedResultTransformer(Medicamento.class));
        return con.execQuerySQL(sql);
    }
    
    
    public List<ProdutoJson> getMedicamentos(Long idPaciente, String pesquisa){
        
        //(CASE WHEN paciente.idPaciente IS NULL THEN 'Estoque Geral' ELSE 'Estoque Paciente' END)  || ' (' || (CASE WHEN estoque.qtd is null THEN ' 0' ELSE to_char(estoque.qtd, '9G999G990D99') END) || ' )' as aux
        String sql = ""
                + "select produto.dsProduto as descricao, estoque.idEstoque as codigo, estoque.qtd as qtd "
                + "from estoque "
                + "left outer join medicamento on medicamento.idEstoque=estoque .idEstoque "
                + "left outer join produto on estoque.idProduto=produto.idProduto "
                + "left outer join paciente on medicamento.idPaciente=paciente.idPaciente "
                + "where produto.status = true "
                
                + (idPaciente != null 
                
                    ? 
                        "AND paciente.idPaciente=" + idPaciente 
                    : 
                        "  AND (  estoque.tpestoque = 'G' "
                        + "   and paciente.idPaciente is null)"
                )
                
                + (Verify.nullsOrEmpty(pesquisa) ? "" : "and lower(produto.dsProduto) like '%" +pesquisa.replace(" ", "%").toLowerCase()+ "%'  ")
                + "order by produto.dsProduto  "
                + "limit 6";
        
        JPAEstoque con = new JPAEstoque();
        con.setResultTransformer(new AliasToBeanNestedResultTransformer(ProdutoJson.class));
        return con.execQuerySQL(sql);
    }
    
    private boolean pacienteContemProduto(Long idProduto, Long idPaciente){
        JPAMedicamento medicamentoJPA = new JPAMedicamento();
        medicamentoJPA.setProjections(new ProjectionParc[]{new ProjectionParc(Projections.property("idMedicamento"))});
        medicamentoJPA.setCriterions(new Object[]{
            new Object[]{"estoque", Restrictions.eq("produto.idProduto", idProduto)},
            new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)},
        });
        List list = medicamentoJPA.getLista();
        
        if(list != null){
            return !list.isEmpty();
        }
        
        //Problema
        return true;
    }
    
//    public List<Medicamento> filtrar(Long idPaciente, Filter filtro) {
//        String descricao = filtro == null ? null : filtro.getDescricao();
//        
//        ArrayList arrCriterion = new ArrayList();
//        if(descricao != null && !descricao.trim().isEmpty()){
//            arrCriterion.add(new Object[]{"this", Restrictions.like("nmArquivo", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
//        }
//        
//        medicamentoJPA.setCriterions(arrCriterion.toArray());
//        medicamentoJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.desc("dtModificacao"))});
//        medicamentoJPA.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)}});
//        
//        return medicamentoJPA.getLista();
//    }
    
    public Medicamento getMedicamentoByIdEstoque(Long idEstoque) {
        try {
            jpa.guardaConfiguracoes();
            jpa.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("estoque.idEstoque", idEstoque)}});
            jpa.setJoin(new Object[]{
                new Object[]{"estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            jpa.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("paciente.idPaciente").as("paciente.idPaciente")),
                new ProjectionParc(Projections.property("pessoa.nmPessoa").as("paciente.pessoa.nmPessoa")),
                new ProjectionParc(Projections.property("estoque.qtd").as("estoque.qtd")),
            });
            jpa.setResultTransformer(new AliasToBeanNestedResultTransformer(Medicamento.class));
            jpa.setMaxFetchSize(1);
            List<Medicamento> lista = jpa.getLista();
            if (Verify.nullsOrEmpty(lista)) {
                return null;
            }
            
            return lista.get(0);
        } finally {
            jpa.restauraConfiguracoes();
        }
    }
    
    
    
    /////////////////////////////////////////////
    //
    // SERVIÇOS RELACIONADOS A APLICAÇÃO DE MEDICAMENTOS
    //
    ////////////////////////////////////////////
    
    
    /**
     * Busca as aplicações de um medicamento.
     * 
     * @param list_idMedicamento
     * @return 
     */
    public List<MedicamentoAplicacao> getAplicacoesMedicamentos(List<Long> list_idMedicamento) {
        try {
            medicamentoAplicacaoJPA.guardaConfiguracoes();
            medicamentoAplicacaoJPA.setJoin(new Object[]{new Object[]{"medicamento", JoinType.LEFT_OUTER_JOIN}});
            medicamentoAplicacaoJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("hora"), "this")});
            
            List<ProjectionParc> projs = Util.projectionAllFields(MedicamentoAplicacao.class);
            projs.add(new ProjectionParc(Projections.property("medicamento.idMedicamento").as("medicamento.idMedicamento")));
            medicamentoAplicacaoJPA.setProjections(projs.toArray(new ProjectionParc[0]));
            medicamentoAplicacaoJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(MedicamentoAplicacao.class));
            medicamentoAplicacaoJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.in("medicamento.idMedicamento", list_idMedicamento)}});
            return medicamentoAplicacaoJPA.getLista();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar a aplicação do medicamento.", e);
            return null;
        } finally {
            medicamentoAplicacaoJPA.restauraConfiguracoes();
        }
    }
    
    public List<Medicamento> getListaMedicamentosComAplicacoes(List<Medicamento> listaMedicamento) {
        
        if (Verify.nullsOrEmpty(listaMedicamento)) {
            return listaMedicamento;
        }
        
        //CRIA LISTA, PARA FAZER UM "IN"
        Set<Long> list_idMedicamento = new HashSet();
        for (Medicamento medicamento : listaMedicamento) {
            list_idMedicamento.add(medicamento.getIdMedicamento());
        }
        
        //BUSCA A APLICAÇÃO DOS MEDICAMENTOS
        List<MedicamentoAplicacao> aplicacoes = getAplicacoesMedicamentos(new ArrayList(list_idMedicamento));
        
        
        if (!Verify.nullsOrEmpty(aplicacoes)) {
            
            //ADICIONA O MEDICAMENTO A UM HASH PARA LOCALIZAR RAPIDAMENTE
            HashMap<Long, Medicamento> hmMedicamentos = new HashMap();
            for (Medicamento medicamento : listaMedicamento) {
                hmMedicamentos.put(medicamento.getIdMedicamento(), medicamento);
            }
            
            for (MedicamentoAplicacao aplicacao : aplicacoes) {
                //SETA AS APLICAÇÕES AO SEU RESPECTIVO MEDICAMENTO
                Medicamento medicamento = hmMedicamentos.get(aplicacao.getMedicamento().getIdMedicamento());
                if (medicamento != null){
                    medicamento.addAplicacao(aplicacao);
                }
                
            }
        }
        
        return listaMedicamento;
    }
    
    public List<Medicamento> getListaMedicamentosSeparadoPorTurno(List<Medicamento> listaMedicamento) {
        
        
        return listaMedicamento;
    }
    
    private boolean gravarAplicacaoMedicamentos(Medicamento medicamento) {
        
        boolean ok = true;
        
        List<Long> listaNaoExcluir = new ArrayList();

        if (!Verify.nullsOrEmpty(medicamento.getAplicacoes())) {
            for (MedicamentoAplicacao aplicacaoWEB : medicamento.getAplicacoes()) {
                //PARA NAO EXCUIR, DEVE TER ID, QUANTIDADE OU HORARIO
                if (!Verify.nullsOrEmpty(aplicacaoWEB.getId()) && (!Verify.nullsOrEmpty(aplicacaoWEB.getHora()) || !Verify.nullsOrEmpty(aplicacaoWEB.getAplicacao()))) {
                    listaNaoExcluir.add(aplicacaoWEB.getId());
                }
            }
        }
        
        String WHERE = "";
        if (!Verify.nullsOrEmpty(listaNaoExcluir)) {
            WHERE = " AND id NOT IN " + listaNaoExcluir.toString().replace("[", "(").replace("]", ")");
        }
        
        medicamentoAplicacaoJPA.execQuerySQL("DELETE FROM medicamentoaplicacao WHERE idMedicamento = " + medicamento.getIdMedicamento() + WHERE, true);
        
        if (!Verify.nullsOrEmpty(medicamento.getAplicacoes())) {
            for (MedicamentoAplicacao aplicacao : medicamento.getAplicacoes()) {
                //SÓ GRAVA SE TIVER HORA OU APLICACAO
                if ((!Verify.nullsOrEmpty(aplicacao.getHora()) || !Verify.nullsOrEmpty(aplicacao.getAplicacao()) )) {
                    aplicacao.setMedicamento(medicamento);
                    ok = medicamentoAplicacaoJPA.save(aplicacao);

                    if (!ok) {
                        break;
                    }
                }
            }
        }
        
        return ok;
    }
}
