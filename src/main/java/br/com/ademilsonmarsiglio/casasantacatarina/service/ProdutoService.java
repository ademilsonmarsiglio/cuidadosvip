package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.BasicJson;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAMedicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAProduto;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class ProdutoService {
    @Autowired
    private JPAProduto produtoJPA;
    
    @Autowired
    private JPAEstoque estoqueJPA;
    
    public void salvar(Produto produto, HttpSession httpSession) {
        produto.setAssuser(Util.getAssuser(httpSession));
        boolean produtoNovo = produto.getIdProduto() == null;
        produtoJPA.save(produto, httpSession);
        
        Login login = (Login)httpSession.getAttribute(Key.LOGIN_SESSION);
        
        if(produtoNovo){
            estoqueJPA.insereEstoqueZerado(produto, 'G', login);
        } else {
            Estoque estoque = getEstoqueProduto(produto.getIdProduto());
            
            if(estoque == null){
                estoqueJPA.insereEstoqueZerado(produto, 'G', login);
            } /*else { não altera mais o estoque, por causa do historico estoque.
                estoqueJPA.save(estoque.setQtd(produto.getQtdEstoqueInicial()));
            }*/
        }
    }

    public boolean excluir(Long codigo) {
        
        JPAMedicamento jPAMedicamento = new JPAMedicamento();
        jPAMedicamento.setJoin(new Object[]{
            new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
            new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
            new Object[]{"estoque", JoinType.LEFT_OUTER_JOIN},
            new Object[]{"estoque.produto", JoinType.LEFT_OUTER_JOIN},
        });
        jPAMedicamento.setCriterions(new Object[]{
            new Object[]{"estoque.produto", Restrictions.eq("idProduto", codigo)},
            new Object[]{"paciente", Restrictions.eq("stPaciente", 'A')},
        });
        jPAMedicamento.setProjections(new ProjectionParc[]{new ProjectionParc(Projections.property("pessoa.nmPessoa"))});
        jPAMedicamento.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("nmPessoa"), "paciente.pessoa")});
        List listPacientes = jPAMedicamento.getLista();
        
        if(!Verify.nullsOrEmpty(listPacientes)){
            String pacientes = "Produto usado pelo(s) paciente(s): ";
            for (int i = 0; i < listPacientes.size(); i++) {
                String paciente = listPacientes.get(i).toString();
                
                pacientes += paciente;
                
                if(i != listPacientes.size() -1){
                    pacientes += ", ";
                }
            }
            
            produtoJPA.setMensagem(pacientes);
            return false;
        }
        
        Produto produto = getProduto(codigo);
        produto.setStatus(false);
        return produtoJPA.save(produto);
        
        
//        if(estoqueJPA.excluirEstoqueByProduto(codigo)){
//            return produtoJPA.delete(codigo);
//        } else {
//            produtoJPA.setMensagem(estoqueJPA.getMensagem());
//        }
//        return false;
    }
    
    public String getMessageJPA(){
        return produtoJPA.getMensagem();
    }
    
    public Produto getProduto(Long id){
        try {
            produtoJPA.guardaConfiguracoes();
            
            if(produtoJPA.localizar(id))
                return produtoJPA.getObjeto();

            return null;
        } finally {
            produtoJPA.restauraConfiguracoes();
        }
    }

    public List<Produto> filtrar(Filter filtro, HttpSession httpSession) {
        Login login = (Login)httpSession.getAttribute(Key.LOGIN_SESSION);
        String descricao = filtro == null ? null : filtro.getDescricao();
        
        ArrayList arrCriterion = new ArrayList();
        if(descricao != null && !descricao.trim().isEmpty()){
            arrCriterion.add(new Object[]{"this", Restrictions.like("dsProduto", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        List<Produto> listProduto = null;
        
        try {
            produtoJPA.guardaConfiguracoes();
            produtoJPA.setDefaultCriterions(new Object[]{
                new Object[]{"this", Restrictions.eq("estabelecimento", login.getEstabelecimento())},
                new Object[]{"this", Restrictions.eq("status", true)},
            });
            produtoJPA.setCriterions(arrCriterion.toArray());
            produtoJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("dsProduto"))});

            Integer offset = -1;
            Integer limit = -1;

            if (filtro != null && filtro.getLimit() != null){
                limit = filtro.getLimit();
            }

            if (filtro != null && filtro.getOffset()!= null){
                offset = filtro.getOffset();
            }
            
            listProduto = produtoJPA.getLista(offset, limit);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar produtos", e);
        } finally {
            produtoJPA.restauraConfiguracoes();
        }
        
        HashMap<Long, Produto> hash_produto = new HashMap();
        
        if (listProduto != null)
            for (Produto produto : listProduto) {
                hash_produto.put(produto.getIdProduto(), produto);
            }
        
        if(!hash_produto.isEmpty()){
            JPAEstoque estoqueJPA = new JPAEstoque();
            estoqueJPA.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            List<HashMap<String, Object>> lista = estoqueJPA.execQuerySQL(""
                    + "SELECT "
                    + "idProduto as \"idProduto\", "
                    + "SUM(CASE WHEN estoque.tpEstoque = 'P' THEN estoque.qtd ELSE 0 END) as \"qtdPaciente\", "
                    + "SUM(CASE WHEN estoque.tpEstoque = 'G' THEN estoque.qtd ELSE 0 END) as \"qtdGeral\", "
                    + "MAX(CASE WHEN estoque.tpEstoque = 'G' THEN estoque.idEstoque ELSE 0 END) as \"idEstoqueGeral\" "
                    + "FROM estoque "
                    + "WHERE estoque.idProduto in " + hash_produto.keySet().toString().replace("[", "(").replace("]", ")")
                    + "GROUP BY estoque.idProduto ", false);
            
            for (HashMap<String, Object> hashMap : lista) {
                BigInteger idProduto = (BigInteger) hashMap.get("idProduto");
                Double qtdPaciente = (Double) hashMap.get("qtdPaciente");
                Double qtdGeral = (Double) hashMap.get("qtdGeral");
                BigInteger idEstoqueGeral = (BigInteger) hashMap.get("idEstoqueGeral");
                
                Produto produto = hash_produto.get(idProduto.longValue());
                produto.setQtdGeral(qtdGeral);
                produto.setQtdPacientes(qtdPaciente);
                produto.setIdEstoqueGeral(Verify.coalesce(idEstoqueGeral, new BigInteger("0")).longValue());
            }
        }
        
        return listProduto;
    }
    
    public List<BasicJson> getProdutosJson(String frase){
        if(frase == null || frase.trim().isEmpty()){
            return Arrays.asList();
        }
        
        JPAProduto produtoCon = new JPAProduto();
        produtoCon.setProjections(new ProjectionParc[]{
            new ProjectionParc(Projections.property("dsProduto").as("descricao")),
            new ProjectionParc(Projections.property("idProduto").as("codigo")),
        });
        produtoCon.setMaxFetchSize(6);
        produtoCon.setResultTransformer(new AliasToBeanNestedResultTransformer(BasicJson.class));
        produtoCon.setCriterions(new Object[]{
            new Object[]{"this", Restrictions.eq("status", true)},
            new Object[]{"this", Restrictions.like("dsProduto", frase.replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()}
        });
        produtoCon.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("dsProduto"))});
        
        return (List) produtoCon.getLista();
    }
    
    public Estoque getEstoqueProduto(Long idProduto){
        try {
            estoqueJPA.guardaConfiguracoes();
            estoqueJPA.setMaxFetchSize(1);
            estoqueJPA.setCriterions(new Object[]{
                new Object[]{"this", Restrictions.eq("produto.idProduto", idProduto)},
                new Object[]{"this", Restrictions.eq("tpEstoque", 'G')},
            });
            List<Estoque> listEstoque = estoqueJPA.getLista();

            if(listEstoque != null && !listEstoque.isEmpty()){
                return listEstoque.get(0);
            }
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar o estoque do produto", e);
        } finally {
            estoqueJPA.restauraConfiguracoes();
        }
        
        return null;
    }
    
    public Estoque insereEstoqueProduto(Produto produto, Login login){
        return estoqueJPA.insereEstoqueZerado(produto, 'G', login);
    }
    
    public Estoque getEstoqueById(Long idEstoque) {
        try {
            estoqueJPA.guardaConfiguracoes();
            estoqueJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idEstoque", idEstoque)}});
            estoqueJPA.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("qtd").as("qtd")),
            });
            estoqueJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(Estoque.class));
            estoqueJPA.setMaxFetchSize(1);
            List<Estoque> lista = estoqueJPA.getLista();
            if (Verify.nullsOrEmpty(lista)) {
                return null;
            }
            
            return lista.get(0);
        } finally {
            estoqueJPA.restauraConfiguracoes();
        }
    }
}
