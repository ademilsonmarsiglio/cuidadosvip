package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAEstabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAPessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class EstabelecimentoService {

    @Autowired
    private JPAEstabelecimento estabelecimentoJPA;
    
    
    public void salvar(Estabelecimento paciente) {
        estabelecimentoJPA.save(paciente);
    }

    public void excluir(Long codigo) {
        estabelecimentoJPA.delete(codigo);
    }
    
    public Estabelecimento getEstabelecimento(Long id){
        if(estabelecimentoJPA.localizar(id))
            return estabelecimentoJPA.getObjeto();
        
        return null;
    }

    public List<Estabelecimento> filtrar(Filter filtro) {
        String descricao = filtro.getDescricao() == null ? "%" : filtro.getDescricao();
        estabelecimentoJPA.setJoin(new Object[]{new Object[]{"pessoa", JoinType.INNER_JOIN}});
        return estabelecimentoJPA.getLista();
    }
    
    public static Estabelecimento getEstabelecimento(HttpSession httpSession){
        Login loginSession = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        Estabelecimento estabelecimento = loginSession.getEstabelecimento();

        if(estabelecimento == null){
            JPAEstabelecimento estabelecimentoJPA = new JPAEstabelecimento();
            estabelecimentoJPA.setMaxFetchSize(1);
            estabelecimentoJPA.setJoin(new Object[]{new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN}});
            List<Estabelecimento> listEstabelecimento = estabelecimentoJPA.getLista();
            if(listEstabelecimento != null && !listEstabelecimento.isEmpty()){
                estabelecimento = listEstabelecimento.get(0);
            }
        }
        
        return estabelecimento;
    }
    
    public static Pessoa getPessoaEstabelecimento(Estabelecimento estabelecimento){
        
        if(estabelecimento.getPessoa() != null){
            if(estabelecimento.getPessoa() instanceof HibernateProxy || estabelecimento.getPessoa().getNmPessoa() == null){
                
                Long idPessoa;
                
                if (estabelecimento.getPessoa() instanceof HibernateProxy) {
                    idPessoa = (Long) ((HibernateProxy)estabelecimento.getPessoa()).getHibernateLazyInitializer().getIdentifier();
                } else {
                    idPessoa = estabelecimento.getPessoa().getIdPessoa();
                }
                
                Pessoa pessoa = null;

                JPAPessoa con = new JPAPessoa();
                if(con.localizar(idPessoa)){
                    pessoa = con.getObjeto();
                }
                
                estabelecimento.setPessoa(pessoa);

                return pessoa;
            } else {
                return estabelecimento.getPessoa();
            }
        } else {
            return null;
        }
    }
}
