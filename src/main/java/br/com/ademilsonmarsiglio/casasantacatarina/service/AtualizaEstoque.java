package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.ItemProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAProntuario;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;
import org.springframework.stereotype.Service;

/**
 * @deprecated 
 * @author ademilson
 */
@Service
public class AtualizaEstoque {
    
    private JPAEstoque estoqueJPA;
    private JPAProntuario prontuarioJPA;
    private HashMap<Long, Estoque> hmEstoque;

    public AtualizaEstoque() {
        estoqueJPA = new JPAEstoque();
        prontuarioJPA = new JPAProntuario();
        hmEstoque = new HashMap();
    }
    
    private void atualizaEstoque(Long idEstoque, Double qtdAnterior, Double qtdAlterada){
        if (qtdAlterada == null){
            qtdAlterada = 0D;
        }
        
        if (qtdAnterior == null){
            qtdAnterior = 0D;
        }
        
        Double diferencaMovimentoEstoque = qtdAlterada - qtdAnterior;
        
        if (diferencaMovimentoEstoque.equals(0D)){
            return;
        }
        
        Estoque estoque = null;
        if (hmEstoque.containsKey(idEstoque)){
            estoque = hmEstoque.get(idEstoque);
        } else {
            if(estoqueJPA.localizar(idEstoque)){
                estoque = estoqueJPA.getObjeto();
                estoqueJPA.alterar();
                
                hmEstoque.put(idEstoque, estoque);
            } else {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "Estoque {0} não localizado.", idEstoque);
                return;
            }
        }
        
        if(estoque != null){
            Double qtd = estoque.getQtd();
            if (qtd == null) {
                qtd = 0D;
            }
            
            estoque.setQtd(qtd - diferencaMovimentoEstoque);
        }
    }
    
    private void removeProntuario(Long idProntuario){
        atualizaEstoque(new Prontuario().setIdProntuario(idProntuario));
    }
    
    private void atualizaEstoque(Prontuario prontuario){
        Object[] join = prontuarioJPA.getJoin();
        Object[] criterions = prontuarioJPA.getCriterions();
        ProjectionParc[] projections = prontuarioJPA.getProjections();
        ResultTransformer resultTransformer = prontuarioJPA.getResultTransformer();
        
        try {
            
            List<ItemProntuario> listaItemProntuarioBanco = null;
            
            if(prontuario.getIdProntuario() != null){ //Verifica se não está inserindo o prontuário
                prontuarioJPA.setJoin(new Object[]{
                    new Object[]{"itens", JoinType.INNER_JOIN},
                    new Object[]{"itens.estoque", JoinType.INNER_JOIN},
                });
                prontuarioJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idProntuario", prontuario.getIdProntuario())}});
                prontuarioJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(ItemProntuario.class));
                prontuarioJPA.setProjections(new ProjectionParc[]{
                    new ProjectionParc(Projections.property("itens.idItemProntuario").as("idItemProntuario")),
                    new ProjectionParc(Projections.property("estoque.idEstoque").as("estoque.idEstoque")),
                    new ProjectionParc(Projections.property("itens.qtd").as("qtd")),
                });

                listaItemProntuarioBanco = (List) prontuarioJPA._getLista();
            }
            
            
            List<ItemProntuario> listItemProntuario = prontuario.getItens();
            if(listItemProntuario != null){
                for (ItemProntuario itemProntuario : listItemProntuario) {
                    Long idProntuario = itemProntuario.getIdItemProntuario();
                    
                    if (itemProntuario.getEstoque() != null && itemProntuario.getEstoque().getIdEstoque() != null) {
                        Long idEstoque = itemProntuario.getEstoque().getIdEstoque();
                        Double qtd = itemProntuario.getQtd();

                        if(idProntuario == null){ //Item é novo;
                            atualizaEstoque(idEstoque, 0D, qtd);
                        } else {                  //Está editando
                            if(listaItemProntuarioBanco != null){
                                for (ItemProntuario itemBanco : listaItemProntuarioBanco) {
                                    if(itemBanco.getIdItemProntuario().equals(idProntuario)){
                                        atualizaEstoque(idEstoque, itemBanco.getQtd(), qtd);
                                        listaItemProntuarioBanco.remove(itemBanco);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(listaItemProntuarioBanco != null){
                //verifica se removeu algum item
                for (ItemProntuario itemBanco : listaItemProntuarioBanco) {
                    atualizaEstoque(itemBanco.getEstoque().getIdEstoque(), itemBanco.getQtd(), 0D);
                }
            }

            estoqueJPA.gravar();
        } finally {
            
            prontuarioJPA.setJoin(join);
            prontuarioJPA.setCriterions(criterions);
            prontuarioJPA.setProjections(projections);
            prontuarioJPA.setResultTransformer(resultTransformer);
        }
    }
}
