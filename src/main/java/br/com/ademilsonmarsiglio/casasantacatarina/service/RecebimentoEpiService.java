package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.model.ControleEPI;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAControleEPI;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import lombok.NonNull;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ademilson
 */
@Service
public class RecebimentoEpiService {
    @Autowired
    private JPAControleEPI repository;
    
    public List<ControleEPI> list(Filter filtro, boolean isReport){
        
        String dsEpi = filtro.getDescricao();
        Date date1 = filtro.getDate1();
        Date date2 = filtro.getDate2();
        
        if(date1 != null){
            date1 = Util.getLimiteInicial(date1);
        }
        
        if(date2 != null){
            date2 = Util.getLimiteFinal(date2);
        }
        
        List arrCriterion = new ArrayList();
        
        if(!Verify.nullsOrEmpty(dsEpi)){
            arrCriterion.add(new Object[]{"this", Restrictions.like("dsEpi", dsEpi.replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        if(!Verify.nullsOrEmpty(filtro.getId())){
            arrCriterion.add(new Object[]{"this", Restrictions.like("ca", filtro.getId().toString().replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        
        if(date1 != null && date2 == null){ //Maior que data 1
            arrCriterion.add(new Object[]{"this", Restrictions.ge("dtEntrega", date1)});
        } else  if (date1 == null && date2 != null){ //Menor que a data 2
            arrCriterion.add(new Object[]{"this", Restrictions.le("dtEntrega", date2)});
        } else  if (date1 != null && date2 != null){ 
            arrCriterion.add(new Object[]{"this", Restrictions.between("dtEntrega", date1, date2)});
        }

        if (!Verify.nullsOrEmpty(filtro.getDescricao2())) {
            arrCriterion.add(new Object[]{"login.pessoa", Restrictions.like("nmPessoa", filtro.getDescricao2().replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        try {
            repository.guardaConfiguracoes();
            repository.setJoin(new Object[]{
                new Object[]{"login", JoinType.LEFT_OUTER_JOIN}});
            repository.setCriterions(arrCriterion.toArray());
            if (isReport) {
                repository.setDefaultOrderBy(new OrderBy[]{
                        new OrderBy(Order.asc("nmPessoa"), "login.pessoa")
                });
            }
            repository.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.desc("dtEntrega")),
                new OrderBy(Order.asc("dsEpi")),
            });
            repository.setUsePaginator(true);
            return repository.getLista();
        } finally {
            repository.restauraConfiguracoes();
        }
    }
    
    public boolean incluirOuEditar(ControleEPI recebimentoEPI, @NonNull Login login){
        if (recebimentoEPI != null){
            if (recebimentoEPI.getId() == null)
                recebimentoEPI.setLogin(login);
            
            ControleEPI bd = recebimentoEPI;
            
            if (recebimentoEPI.getId() != null) {
                bd = repository.get(recebimentoEPI.getId());
                BeanUtils.copyProperties(recebimentoEPI, bd, "login");
            }
            
            return repository.save(bd);
        } else {
            return false;
        }
    }
    
    public ControleEPI get(Long id){
        try {
            repository.guardaConfiguracoes();
            repository.setJoin(new Object[]{
                new Object[]{"login", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            repository.setCriterions(new Object[]{
                new Object[]{"this", Restrictions.eq("id", id)}
            });
            return repository.getFirstResult();
        } finally {
            repository.restauraConfiguracoes();
        }
    }
    
    public boolean delete(Long id){
        return repository.delete(id);
    }
}
