package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.model.DiagnosticoPaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SaeProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPASaeProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilsonmarsiglio
 */
@Service
public class SaeProntuarioService {
    
    @Autowired private DiagnosticoPacienteService diagnosticoService;
    @Autowired private JPASaeProntuario jpa;
    
    public void salvar(Prontuario prontuario) {
        
        if (prontuario != null) {
            
            if (!Verify.nullsOrEmpty(prontuario.getSaes())) {
                
                boolean salvaAlgumaCoisa = false;
                
                verificaSePreencheuAlgumaCoisa:
                for (SaeProntuario sae : prontuario.getSaes()) {
                    if (!Verify.nullsOrEmpty(sae.getFilhos())) {
                        for (SaeProntuario filho : sae.getFilhos()) {
                            if (filho.getMarcado()) {
                                salvaAlgumaCoisa = true;
                                break verificaSePreencheuAlgumaCoisa;
                            }
                            
                            if (!Verify.nullsOrEmpty(filho.getJustificativa())) {
                                salvaAlgumaCoisa = true;
                                break verificaSePreencheuAlgumaCoisa;
                            }
                        }
                    }   
                }
                
                if (salvaAlgumaCoisa)
                    for (SaeProntuario sae : prontuario.getSaes()) {
                        sae.setProntuario(prontuario);
                        if (jpa.save(sae)) {
                            if (!Verify.nullsOrEmpty(sae.getFilhos())) {
                                for (SaeProntuario filho : sae.getFilhos()) {
                                    filho.setProntuario(prontuario);
                                    jpa.save(filho);
                                }
                            }   
                        }
                    }
            }
            
            
            
        } else {
            System.out.println("Prontuario está null");
        }
        
    }
    
    
    
    
    
    
    
    public List<SaeProntuario> getListaNovoProntuario(Long idPaciente) {
        
        List<DiagnosticoPaciente> lista = diagnosticoService.getLista(idPaciente, false);
        
        List<SaeProntuario> listSAE = new ArrayList();
        
        //popula a lista de saes
        for (DiagnosticoPaciente d : lista) {
            
            SaeProntuario s = new SaeProntuario();
            s.setDescricao(d.getDescricao());
            s.setMarcado(Boolean.FALSE);
            s.setDiagnosticoPaciente(d);
            
            listSAE.add(s);
        }
        
        
        
        LinkedHashMap<Integer, SaeProntuario> map = new LinkedHashMap<>();
        
        //adiciona o pai
        for (SaeProntuario s : listSAE) {
            DiagnosticoPaciente d = s.getDiagnosticoPaciente();
            if (d.getIdParent() == null) {
                map.put(d.getId(), s);
            }
        }

        //adiciona os filhos
        for (SaeProntuario s : listSAE) {
            DiagnosticoPaciente d = s.getDiagnosticoPaciente();
            if (d.getIdParent() != null) {
                SaeProntuario master = map.get(d.getIdParent());

                if (master != null) {
                    master.addFilho(s);
                }
            }
        }
        
        
        return new ArrayList(map.values());
    }
    
    
    
    
    
    
    
    public void getListaByProntuario(List<Prontuario> prontuarios) {
        try {
            
            if (Verify.nullsOrEmpty(prontuarios))
                return;
            
            jpa.guardaConfiguracoes();
        
            List<Long> list_idProntuario = new ArrayList();

            for (Prontuario prontuario : prontuarios) {
                list_idProntuario.add(prontuario.getIdProntuario());
            }

            jpa.setResultTransformer(new AliasToBeanNestedResultTransformer(SaeProntuario.class));

            //busca a lista de saes por prontuario
            List<SaeProntuario> lista = jpa.execQuerySQL(""
                    + "SELECT  "
                    + "	sae.id as \"id\", "
                    + "	prontuario.idProntuario as \"prontuario__idProntuario\", "
                    + "	sae.descricao as \"descricao\", "
                    + "	sae.justificativa as \"justificativa\", "
                    + "	sae.marcado as \"marcado\", "
                    + "	diagnostico.id as \"diagnosticoPaciente__id\", "
                    + "	diagnostico.tipo as \"diagnosticoPaciente__tipo\", "
                    + "	diagnostico.idparent as \"diagnosticoPaciente__idParent\" "
                    + "FROM paciente "
                    + "INNER JOIN diagnosticopaciente diagnostico USING (idPaciente) "
                    + "INNER JOIN prontuario USING (idPaciente) "
                    + "INNER JOIN sae_prontuario sae ON sae.idprontuario = prontuario.idprontuario AND sae.iddiagnosticopaciente = diagnostico.id "
                    + "WHERE prontuario.idProntuario IN " + list_idProntuario.toString().replace("[", "(").replace("]", ")")
                    + " ORDER BY diagnostico.ordem");

            // primeiro separa a lista por prontuarios
            HashMap<Long, List<SaeProntuario>> map_prontuario_saes = new HashMap();
            for (SaeProntuario saeProntuario : lista) {
                List<SaeProntuario> saesProntuario = map_prontuario_saes.get(saeProntuario.getProntuario().getIdProntuario());

                if (Verify.nullsOrEmpty(saesProntuario)) {
                    saesProntuario = new ArrayList();
                }

                saesProntuario.add(saeProntuario);
                map_prontuario_saes.put(saeProntuario.getProntuario().getIdProntuario(), saesProntuario);
            }



            // agora, depois da lista estar separada, agrupa os itens filhos dentro dos pais.
            for (Long idProntuario : map_prontuario_saes.keySet()) {

                LinkedHashMap<Integer /*diagnostico.id ou idParent*/, SaeProntuario> map_parents = new LinkedHashMap<>();
                List<SaeProntuario> saeProntuario_list = map_prontuario_saes.get(idProntuario);

                //adiciona o pai
                for (SaeProntuario s : saeProntuario_list) {
                    DiagnosticoPaciente d = s.getDiagnosticoPaciente();
                    if (d.getIdParent() == null) {
                        map_parents.put(d.getId(), s);
                    }
                }



                //adiciona os filhos
                for (SaeProntuario s : saeProntuario_list) {
                    DiagnosticoPaciente d = s.getDiagnosticoPaciente();
                    if (d.getIdParent() != null) {
                        SaeProntuario master = map_parents.get(d.getIdParent());

                        if (master != null) {
                            master.addFilho(s);
                        }
                    }
                }


                List<Prontuario> listaProntuarioById = prontuarios
                        .stream()
                        .filter((Prontuario t) -> t.getIdProntuario().equals(idProntuario))
                        .collect(Collectors.toList());
                //vincula os pais aos respectivos prontuarios
                for (Prontuario prontuario : listaProntuarioById) {
                    prontuario.setSaes(new ArrayList(map_parents.values()));
                }
            }
            
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar lista de sae", e);
        } finally {
            jpa.restauraConfiguracoes();
        }
    }
    
    
    public void removerSaeByProntuario(Long idProntuario) {
        jpa.execQuerySQL("DELETE FROM sae_prontuario WHERE idProntuario = " + idProntuario, true);
    }
    
}
