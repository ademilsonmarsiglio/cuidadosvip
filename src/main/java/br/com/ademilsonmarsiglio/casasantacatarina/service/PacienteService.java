package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.BasicJson;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAMedicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAPaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAResponsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class PacienteService {

    @Autowired
    private JPAPaciente pacienteJPA;

    @Autowired
    private JPAResponsavel responsavelJPA;
    
    @Autowired
    private JPAMedicamento medicamentoJPA;
    
    public boolean  salvar(Paciente paciente, HttpSession httpSession) {
        return pacienteJPA.save(paciente, httpSession);
    }

    public boolean excluir(Long codigo) {
        return pacienteJPA.delete(codigo);
    }
    
    public String getMessageJPA(){
        return pacienteJPA.getMensagem();
    }
    
    public Paciente getPaciente(Long id){
        try {
            pacienteJPA.guardaConfiguracoes();
            pacienteJPA.setJoin(new Object[]{new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN}});
            pacienteJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idPaciente", id)}});
            return pacienteJPA.getUniqueResult();
        } finally {
            pacienteJPA.restauraConfiguracoes();
        }
    }
    
    public Paciente getOnlyPropertiesPaciente(Long id){
        try {
            pacienteJPA.guardaConfiguracoes();
            List<ProjectionParc> projectionAllFields = Util.projectionAllFields(Paciente.class);
            pacienteJPA.setProjections(projectionAllFields.toArray(new ProjectionParc[0]));
            pacienteJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(Paciente.class));
            pacienteJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idPaciente", id)}});
            return pacienteJPA.getUniqueResult();
        } finally {
            pacienteJPA.restauraConfiguracoes();
        }
    }

    public List<Paciente> filtrar(Filter filtro, HttpSession httpSession) {
        Login login = (Login)httpSession.getAttribute(Key.LOGIN_SESSION);
        String descricao = filtro == null ? null : filtro.getDescricao();
        Character situacao = filtro == null ? null : filtro.getSituacao();
        
        ArrayList arrCriterion = new ArrayList();
        if(descricao != null && !descricao.trim().isEmpty()){
            arrCriterion.add(new Object[]{"pessoa", Restrictions.like("nmPessoa", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        if(situacao != null && !situacao.equals(' ')){
            arrCriterion.add(new Object[]{"this", Restrictions.eq("stPaciente", situacao)});
        }
        
        try {
            pacienteJPA.guardaConfiguracoes();
            pacienteJPA.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("estabelecimento.idEstabelecimento", login.getEstabelecimento().getIdEstabelecimento())}});
            pacienteJPA.setCriterions(arrCriterion.toArray());
            pacienteJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("nmPessoa"), "pessoa")});
            
            pacienteJPA.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("idPaciente").as("idPaciente")),
                new ProjectionParc(Projections.property("pessoa.IdPessoa").as("pessoa.IdPessoa")),
                new ProjectionParc(Projections.property("pessoa.nmPessoa").as("pessoa.nmPessoa")),
                new ProjectionParc(Projections.property("pessoa.dtNascimento").as("pessoa.dtNascimento")),
                new ProjectionParc(Projections.property("pessoa.cpf").as("pessoa.cpf")),
                new ProjectionParc(Projections.property("pessoa.fone").as("pessoa.fone")),
            });
            pacienteJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(Paciente.class));
            
            Integer offset = -1;
            Integer limit = -1;

            if (filtro != null && filtro.getLimit() != null){
                limit = filtro.getLimit();
            }

            if (filtro != null && filtro.getOffset()!= null){
                offset = filtro.getOffset();
            }
            
            return pacienteJPA.getLista(offset, limit);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar lista de clientes", e);
        } finally {
            pacienteJPA.restauraConfiguracoes();
        }
        
        return null;
    }
    
    public List<Paciente> relatorio(Long[] idPacientes, HttpSession httpSession) {
        Login login = (Login)httpSession.getAttribute(Key.LOGIN_SESSION);
        
        ArrayList arrCriterion = new ArrayList();
        if(idPacientes != null && idPacientes.length > 0){
            arrCriterion.add(new Object[]{"this", Restrictions.in("idPaciente", idPacientes)});
        }
        
        List<Paciente> list = null;
        List<Responsavel> listResponsavel = null;
            
        try {
            pacienteJPA.guardaConfiguracoes();
            pacienteJPA.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("estabelecimento", login.getEstabelecimento())}});
            pacienteJPA.setCriterions(arrCriterion.toArray());
            pacienteJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("nmPessoa"), "pessoa")});
            list = pacienteJPA.getLista();
        } finally {
            pacienteJPA.restauraConfiguracoes();
        }
        
        
        { //Busca os responsáveis:
            
            try {
                responsavelJPA.guardaConfiguracoes();
                responsavelJPA.setJoin(new Object[]{new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN}});
                if(idPacientes != null && idPacientes.length > 0){
                    responsavelJPA.setCriterions(new Object[]{new Object[]{"paciente", Restrictions.in("idPaciente", idPacientes)}});
                }

                responsavelJPA.setOrderByClause(new OrderBy[]{
                    new OrderBy(Order.asc("idPaciente"), "paciente"),
                    new OrderBy(Order.desc("principal"))
                });
                listResponsavel = responsavelJPA.getLista();
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar responsáveis", e);
            } finally {
                responsavelJPA.restauraConfiguracoes();
            }
        }
        
        
        { //Busca os medicamentos:
            try { 
                medicamentoJPA.guardaConfiguracoes();
                
                if(idPacientes != null && idPacientes.length > 0){
                    medicamentoJPA.setCriterions(new Object[]{new Object[]{"paciente", Restrictions.in("idPaciente", idPacientes)}});
                }
                medicamentoJPA.setJoin(new Object[]{
                    new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                    new Object[]{"estoque", JoinType.LEFT_OUTER_JOIN},
                    new Object[]{"estoque.produto", JoinType.LEFT_OUTER_JOIN},
                });
                medicamentoJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(Medicamento.class));
                medicamentoJPA.setProjections(new ProjectionParc[]{
                    new ProjectionParc(Projections.property("paciente.idPaciente").as("paciente.idPaciente")),
                    new ProjectionParc(Projections.property("produto.dsProduto").as("estoque.produto.dsProduto")),
                });
                List<Medicamento> listMedicamentos = medicamentoJPA.getLista();

                for (Paciente paciente : list) {

                    if (!Verify.nullsOrEmpty(listResponsavel)){
                        for (Responsavel responsavel : listResponsavel) {
                            if(paciente.getIdPaciente().equals(responsavel.getPaciente().getIdPaciente())){
                                if(paciente.getResponsavelPrincipal() == null){
                                    paciente.setResponsavelPrincipal(responsavel);
                                } else if(paciente.getResponsavel() == null){
                                    paciente.setResponsavel(responsavel);
                                } else {
                                    break;
                                }
                            }
                        }
                    }

                    for (Medicamento medicamento : listMedicamentos) {
                        if(paciente.getIdPaciente().equals(medicamento.getPaciente().getIdPaciente())){
                            paciente.addMedicamento(medicamento.getEstoque().getProduto().getDsProduto());
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar os medicamentos.", ex);
            } finally {
                medicamentoJPA.restauraConfiguracoes();
            }
        }
        
        return list;
    }
    
    public static String getNomePaciente(Long idPaciente){
        try {
            List list = new JPAPaciente().execQuerySQL("SELECT pess.nmPessoa FROM paciente pac INNER JOIN pessoa pess ON pess.idPessoa = pac.idPessoa WHERE pac.idPaciente = " + idPaciente);

            if(list != null && !list.isEmpty()){
                return (String) list.get(0);
            }
        } catch (Exception e) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Erro ao buscar o nome do paciente", e);
        }
        
        return "";
    }
    
    public List<BasicJson> getPacienteJson(String phrase){
        
        if(phrase == null || phrase.trim().isEmpty()){
            return Arrays.asList();
        }
        JPAPaciente pacienteCon = new JPAPaciente();
        pacienteCon.setJoin(new Object[]{new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN}});
        pacienteCon.setProjections(new ProjectionParc[]{
            new ProjectionParc(Projections.property("pessoa.nmPessoa").as("descricao")),
            new ProjectionParc(Projections.property("idPaciente").as("codigo")),
        });
        pacienteCon.setMaxFetchSize(6);
        pacienteCon.setResultTransformer(new AliasToBeanNestedResultTransformer(BasicJson.class));
        pacienteCon.setCriterions(new Object[]{new Object[]{"pessoa", Restrictions.like("nmPessoa", phrase.replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()}});
        pacienteCon.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("stPaciente", 'A')}});
        pacienteCon.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("nmPessoa"), "pessoa")});
        
        return (List) pacienteCon.getLista();
    }
    
    public Paciente getPacienteMedicamento(String phrase, Long id) {
        if(id == null){
            return null;
        }
        
        JPAPaciente pacienteCon = new JPAPaciente();
        pacienteCon.setMaxFetchSize(1);
        pacienteCon.setJoin(new Object[]{new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN}});
        pacienteCon.setProjections(new ProjectionParc[]{
            new ProjectionParc(Projections.property("idPaciente").as("idPaciente")),
            new ProjectionParc(Projections.property("pessoa.nmPessoa").as("pessoa.nmPessoa")),
            new ProjectionParc(Projections.property("medicamentosEmUso").as("medicamentosEmUso")),
        });
        pacienteCon.setResultTransformer(new AliasToBeanNestedResultTransformer(Paciente.class));
        pacienteCon.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idPaciente", id)},});
        
        if(phrase != null && !phrase.trim().isEmpty()){
            pacienteCon.setCriterions(new Object[]{new Object[]{"pessoa", Restrictions.like("nmPessoa", phrase.replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()},});
        }
        
        List<Paciente> list = pacienteCon.getLista();
        
        if(list == null || list.isEmpty()){
            return null;
        }
        
        return list.get(0);
    }    
}
