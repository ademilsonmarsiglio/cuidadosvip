package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class ProntuarioService {

    @Autowired private JPAProntuario prontuarioJPA;
    
    @Autowired SaeProntuarioService saeService;
    
    public void salvar(Prontuario prontuario, HttpSession session) {
        
        prontuario.setAssuser(Util.getAssuser(session));
        
        if(prontuario.getIdProntuario() == null){
            prontuario.setDtCriacao(new Date());
        }
        
        prontuario.setDtAlteracao(new Date());
        
//        try {
//            new AtualizaEstoque().atualizaEstoque(prontuario);
//        } catch (Exception e) {
//            Logger.getLogger(Prontuario.class.getName()).log(Level.SEVERE, "Erro ao atualizar o estoque.", e);
//        }
        
        prontuarioJPA.save(prontuario);
        
        saeService.salvar(prontuario);
    }
    
    public Prontuario get(Long codigo){
        try {
            prontuarioJPA.guardaConfiguracoes();
            prontuarioJPA.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens.estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens.estoque.produto", JoinType.LEFT_OUTER_JOIN},
            });
            prontuarioJPA.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.asc("dsProduto"), "itens.estoque.produto"),
            });
            return prontuarioJPA.get(codigo);
        } finally {
            prontuarioJPA.restauraConfiguracoes();
        }
    }
    
    public Prontuario getItens(Long codigo){
        
        try {
            prontuarioJPA.guardaConfiguracoes();
            prontuarioJPA.setJoin(new Object[]{
                new Object[]{"itens", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens.estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens.estoque.produto", JoinType.LEFT_OUTER_JOIN},
            });
            prontuarioJPA.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.asc("dsProduto"), "itens.estoque.produto"),
            });
            prontuarioJPA.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("idProntuario").as("idProntuario")),
                new ProjectionParc(Projections.property("produto.dsProduto").as("itens.estoque.produto.dsProduto")),
                new ProjectionParc(Projections.property("itens.qtd").as("itens.qtd")),
            });
            prontuarioJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(Prontuario.class));
            prontuarioJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("idProntuario", codigo)}});

            List<Prontuario> lista = prontuarioJPA._getLista();
            
            Prontuario p = new Prontuario();
            for (Prontuario prontuario : lista) {
                if (Verify.nullsOrEmpty(p.getItens())){
                    p.setItens(prontuario.getItens());
                } else {
                    p.getItens().addAll(prontuario.getItens());
                }
            }
            
            if (!Verify.nullsOrEmpty(lista)){
                return p;
            }
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao buscar json itens", e);
        } finally {
            prontuarioJPA.restauraConfiguracoes();
        }
        return null;
    }

    public void excluir(Long codigo) {
//        new AtualizaEstoque().removeProntuario(codigo);

        saeService.removerSaeByProntuario(codigo);
        
        prontuarioJPA.setJoin(new Object[]{
            new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
            new Object[]{"itens", JoinType.LEFT_OUTER_JOIN},
        });
        prontuarioJPA.delete(codigo);
    }
    
    public List<Prontuario> filtrar(Long idPaciente, Filter filtro) {
        try {
            prontuarioJPA.guardaConfiguracoes();
            ArrayList arrCriterion = new ArrayList();
        
            if (filtro != null) {
                String descricao = filtro.getDescricao();
                Date date1 = filtro.getDate1();
                Date date2 = filtro.getDate2();

                if(!Verify.nullsOrEmpty(descricao)){
                    arrCriterion.add(new Object[]{"this", Restrictions.like("descricao", descricao.replace(" ", "%").toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
                }

                if (date1 != null){
                    arrCriterion.add(new Object[]{"this", Restrictions.ge("dtProntuario", Util.getLimiteInicial(date1))});
                }

                if (date2 != null){
                    arrCriterion.add(new Object[]{"this", Restrictions.le("dtProntuario", Util.getLimiteInicial(date2))});
                }
            }

            arrCriterion.add(new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)});
            
            prontuarioJPA.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            prontuarioJPA.setCriterions(arrCriterion.toArray());
            prontuarioJPA.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.desc("dtProntuario")),
            });
            prontuarioJPA.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("idProntuario").as("idProntuario")),
                new ProjectionParc(Projections.property("dtProntuario").as("dtProntuario")),
                new ProjectionParc(Projections.property("dtCriacao").as("dtCriacao")),
                new ProjectionParc(Projections.property("dtAlteracao").as("dtAlteracao")),
                new ProjectionParc(Projections.property("turno").as("turno")),
                new ProjectionParc(Projections.property("descricao").as("descricao")),
                new ProjectionParc(Projections.property("responsavel").as("responsavel")),
                new ProjectionParc(Projections.property("paciente.idPaciente").as("paciente.idPaciente")),
                new ProjectionParc(Projections.property("pessoa.nmPessoa").as("paciente.pessoa.nmPessoa")),
                new ProjectionParc(Projections.property("pessoa1.nmPessoa").as("login.pessoa.nmPessoa")),
            });
            prontuarioJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(Prontuario.class));

            Integer offset = -1;
            Integer limit = -1;

            if (filtro != null && filtro.getLimit() != null){
                limit = filtro.getLimit();
            }

            if (filtro != null && filtro.getOffset()!= null){
                offset = filtro.getOffset();
            }

            return prontuarioJPA.getLista(offset, limit);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao bucar lista", e);
            return null;
        } finally {
            prontuarioJPA.restauraConfiguracoes();
        }
    }
    
    public List<Prontuario> relatorio(Long idPaciente,
                                      Long[] arrCodigosProntuario,
                                      Long[] arrCodigosPacientes,
                                      boolean enfermeiro,
                                      boolean medico,
                                      boolean tecEnfermeiro,
                                      boolean cuidador,
                                      boolean assSocial,
                                      boolean fisioterapeuta,
                                      boolean nutricionista,
                                      boolean outros,
                                      String dataStr1, 
                                      String dataStr2,
                                      String descricao,
                                      Long login,
                                      boolean imprimirMedicamentos,
                                      boolean imprimirSae,
                                      boolean imprimirDescricao) {
        
        ArrayList arrCriterion = new ArrayList();

        ArrayList<String> arrTipoLogin = new ArrayList();
            if (enfermeiro) {
                arrTipoLogin.add("E");
            }
            if (medico) {
                arrTipoLogin.add("M");
            }
            if (tecEnfermeiro) {
                arrTipoLogin.add("T");
            }
            if (outros) {
                arrTipoLogin.add("O");
            }
            if (cuidador) {
                arrTipoLogin.add("C");
            }
            if (fisioterapeuta) {
                arrTipoLogin.add("F");
            }
            if (nutricionista) {
                arrTipoLogin.add("N");
            }
            if (assSocial) {
                arrTipoLogin.add("A");
            }
        
        if(!arrTipoLogin.isEmpty()){
            arrCriterion.add(new Object[]{"login", Restrictions.in("tpLogin", arrTipoLogin.toArray())});
        }
        
        if(arrCodigosProntuario != null && arrCodigosProntuario.length > 0){
            arrCriterion.add(new Object[]{"this", Restrictions.in("idProntuario", arrCodigosProntuario)});
        }
        
        if(arrCodigosPacientes != null && arrCodigosPacientes.length > 0){
            arrCriterion.add(new Object[]{"paciente", Restrictions.in("idPaciente", arrCodigosPacientes)});
        }
        
        Date date1 = Util.getLimiteInicial(Util.parseDate(dataStr1));
        Date date2 = Util.getLimiteFinal(Util.parseDate(dataStr2));
        
        if(!Verify.nullsOrEmpty(descricao)){
            arrCriterion.add(new Object[]{"this", Restrictions.like("descricao", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        if (date1 != null){
            arrCriterion.add(new Object[]{"this", Restrictions.ge("dtProntuario", Util.getLimiteInicial(date1))});
        }

        if (date2 != null){
            arrCriterion.add(new Object[]{"this", Restrictions.le("dtProntuario", Util.getLimiteInicial(date2))});
        }
        
        if (idPaciente != null)
            arrCriterion.add(new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)});
        
        if (login != null)
            arrCriterion.add(new Object[]{"this", Restrictions.eq("login.idLogin", login)});
        
        JPAProntuario con = new JPAProntuario();
        
        if (imprimirMedicamentos) {
            con.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens.estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"itens.estoque.produto", JoinType.LEFT_OUTER_JOIN},
            });
            con.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.asc("nmPessoa"), "paciente.pessoa"),
                new OrderBy(Order.desc("dtProntuario")),
                new OrderBy(Order.asc("dsProduto"), "itens.estoque.produto"),
            });
        } else {
            con.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
            });
            con.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.asc("nmPessoa"), "paciente.pessoa"),
                new OrderBy(Order.desc("dtProntuario")),
            });
        }
        
        con.setCriterions(arrCriterion.toArray());
        
        
        List<Prontuario> lista = con.getLista();
        
        if (!imprimirMedicamentos) {
            for (Prontuario prontuario : lista) {
                prontuario.setItens(null);
            }
        }
        
        if (imprimirSae) {
            saeService.getListaByProntuario(lista);
        }
        
        if (!imprimirDescricao) {
            for (Prontuario prontuario : lista) {
                prontuario.setDescricao(null);
            }
        }
        
        return lista;
    }
    
    public String getTurno(){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int hora = c.get(Calendar.HOUR_OF_DAY);
        
        if(hora >= 7 && hora <= 13){
            return "M";
        } else if (hora > 13 && hora <= 19){
            return "T";
        } else {
            return "N";
        }
    }
}
