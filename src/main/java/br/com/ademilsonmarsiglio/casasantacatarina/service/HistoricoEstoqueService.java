package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.model.HistoricoEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAHistoricoEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.HashMap;
import java.util.List;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 * @since 05/07/2018
 */
@Service
public class HistoricoEstoqueService {

    @Autowired 
    private JPAHistoricoEstoque jPAHistoricoEstoque;
    
    public List<HistoricoEstoque> getLista(HistoricoEstoque.Filter filtro) {
        
        try {
            jPAHistoricoEstoque.guardaConfiguracoes();
            jPAHistoricoEstoque.setResultTransformer(new AliasToBeanNestedResultTransformer(HistoricoEstoque.class));
            List<HistoricoEstoque> lista = jPAHistoricoEstoque.execQuerySQL(""
                    + "SELECT a.\"data\", "
                    + "       a.\"login__pessoa__nmPessoa\", "
                    + "       a.\"tipo\", "
                    + "       a.\"observacao\", "
                    + "       a.\"qtd\", "
                    + "       a.\"id\", "
                    + "       a.\"idPaciente\" "
                    + "FROM "
                    + "  ( (SELECT data AS \"data\", "
                    + "           pessoa.nmPessoa AS \"login__pessoa__nmPessoa\", "
                    + "           'MANUAL' AS \"tipo\", "
                    + "           historicoestoque.observacao AS \"observacao\", "
                    + "           qtd AS \"qtd\", "
                    + "           id AS \"id\", "
                    + "           0 AS \"idPaciente\" "
                    + "   FROM historicoestoque "
                    + "   INNER JOIN login USING (idlogin) "
                    + "   INNER JOIN pessoa USING (idPessoa) "
//                    + "   INNER JOIN medicamento USING (idEstoque) "
                    + "   WHERE historicoestoque.idEstoque = " + filtro.getIdEstoque() + " "
                    + "   ORDER BY data DESC) "
                    + "   UNION ALL "
                            + ""
                            + "   (SELECT  dtmovimentacao AS \"data\", "
                            + "           pessoa.nmPessoa AS \"login__pessoa__nmPessoa\", "
                            + "           'PRONTUARIO' AS \"tipo\", "
                            + "           '' AS \"observacao\", "
                            + "           (qtd * -1) AS \"qtd\", "
                            + "           prontuario.idProntuario AS \"id\", "
                            + "           prontuario.idPaciente AS \"idPaciente\" "
                            + "   FROM itemprontuario "
                            + "   INNER JOIN prontuario_itemprontuario ON item_iditemprontuario = itemprontuario.iditemprontuario "
                            + "   INNER JOIN prontuario ON prontuario_idprontuario = prontuario.idprontuario "
                            + "   INNER JOIN login USING (idlogin) "
                            + "   INNER JOIN pessoa USING (idPessoa) "
                            + "   WHERE itemprontuario.idEstoque = " + filtro.getIdEstoque() + " "
                            + "   ORDER BY dtmovimentacao DESC) "
                            + ") a "
                            + "ORDER BY \"data\" DESC "
                            + "LIMIT " + filtro.getLimit() + " "
                                    + "OFFSET " + filtro.getOffset()
                    + "");
            
            if (!Verify.nullsOrEmpty(lista)) {
                jPAHistoricoEstoque.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                List listaQtd = //new ArrayList();
                        jPAHistoricoEstoque.execQuerySQL(""
                        + "SELECT "
                        + "       a.\"qtd\", "
                        + "       a.\"data\" "
                        + "FROM "
                        + "  ( (SELECT "
                        + "           qtd AS \"qtd\", "
                        + "           data as data "
                        + "   FROM historicoestoque "
                        + "   INNER JOIN login USING (idlogin) "
                        + "   INNER JOIN pessoa USING (idPessoa) "
                        + "   WHERE historicoestoque.idEstoque = " + filtro.getIdEstoque() + " "
                        + "   ORDER BY data DESC) "
                        + "   UNION ALL "
                        + ""
                        + "   (SELECT  "
                        + "           (qtd * -1) AS \"qtd\","
                        + "           dtmovimentacao as data "
                        + "   FROM itemprontuario "
                        + "   INNER JOIN prontuario_itemprontuario ON item_iditemprontuario = itemprontuario.iditemprontuario "
                        + "   INNER JOIN prontuario ON prontuario_idprontuario = prontuario.idprontuario "
                        + "   INNER JOIN login USING (idlogin) "
                        + "   INNER JOIN pessoa USING (idPessoa) "
                        + "   WHERE itemprontuario.idEstoque = " + filtro.getIdEstoque() + " "
                        + "   ORDER BY dtmovimentacao DESC) "
                        + ") a "
                        + " ORDER BY data DESC "
                        + " OFFSET " + (filtro.getOffset() + filtro.getLimit())
                        + "");

                Double saldoAnterior = 0D;
                if (!Verify.nullsOrEmpty(listaQtd)) {
                    for (HashMap object : (List<HashMap>)listaQtd) {
                        Double qtd = (Double) object.get("qtd");
                        if (!Verify.nullsOrEmpty(qtd)) 
                            saldoAnterior += qtd;
                    }
                }
                
                //calcula o saldo com a lista ao contrário
                for (int i = lista.size() -1; i >= 0; i--) {
                    Double qtd = lista.get(i).getQtd();
                    
                    if (Verify.nullsOrEmpty(qtd))
                        qtd = 0D;
                    
                    saldoAnterior = saldoAnterior + qtd;
                    lista.get(i).setSaldo(saldoAnterior);
                }
            }
            
            return lista;
            
        } finally {
            jPAHistoricoEstoque.restauraConfiguracoes();
        }
    }
    
    public boolean gravar(HistoricoEstoque historicoEstoque) {
        return jPAHistoricoEstoque.save(historicoEstoque);
    }
    
    public boolean excluir(Long id) {
        return jPAHistoricoEstoque.delete(id);
    }
    
    public HistoricoEstoque get(Long id) {
        try {
            jPAHistoricoEstoque.guardaConfiguracoes();
            jPAHistoricoEstoque.setJoin(new Object[]{
                new Object[]{"login", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estoque", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estoque.produto", JoinType.LEFT_OUTER_JOIN},
            });
            return jPAHistoricoEstoque.get(id);
        } finally {
            jPAHistoricoEstoque.restauraConfiguracoes();
        }
    }
}
