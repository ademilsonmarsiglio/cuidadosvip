package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.AnamnesePaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAAnamnese;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilsonmarsiglio
 */
@Service
public class AnamneseService {
    
    @Autowired private JPAAnamnese con;
    
    public boolean salvar(AnamnesePaciente obj, HttpSession session) {
        obj.setAssuser(Util.getAssuser(session));
        
        if (obj.getId() == null) { //INSERCAO
            obj.setData(new Date());
        } else {
            AnamnesePaciente q_bd = get(obj.getId());
            BeanUtils.copyProperties(obj, q_bd, "data");
            obj = q_bd;          
        }
        
        return con.save(obj);
    }
    
    public List<AnamnesePaciente> lista(Filter filtro) {
        try {
            con.guardaConfiguracoes();
            
            
            if (filtro != null) {
                if (filtro.getId() != null) {
                    con.setCriterions(new Object[]{
                        new Object[]{"this", Restrictions.eq("paciente.idPaciente", filtro.getId())}
                    });
                }
            }
            con.setOrderByClause(new OrderBy[]{new OrderBy(Order.desc("data"), "this")});
            List<ProjectionParc> projectionAllFields = Util.projectionAllFields(AnamnesePaciente.class);
            con.setProjections(projectionAllFields.toArray(new ProjectionParc[0]));
            con.setResultTransformer(new AliasToBeanNestedResultTransformer(AnamnesePaciente.class));
            
            return con.getLista(filtro.getOffset(), filtro.getLimit());
        } finally {
            con.limpaConfiguracoes();
        }
    }
    
    
    public AnamnesePaciente get(Long id) {
        try {
            con.guardaConfiguracoes();
            
            
            con.setCriterions(new Object[]{
                new Object[]{"this", Restrictions.eq("id", id)}
            });
            con.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            
//            List<ProjectionParc> projectionAllFields = Util.projectionAllFields(AnamnesePaciente.class);
//            con.setProjections(projectionAllFields.toArray(new ProjectionParc[0]));
//            con.setResultTransformer(new AliasToBeanNestedResultTransformer(AnamnesePaciente.class));
            
            return con.getUniqueResult();
        } finally {
            con.limpaConfiguracoes();
        }
    }

    public boolean excluir(AnamnesePaciente anamnesePaciente) {
        return con.delete(anamnesePaciente);
    }

    public List<AnamnesePaciente> relatorio(Long[] arrCodigos) {
        if (Verify.nullsOrEmpty(arrCodigos))
            return new ArrayList();
        
        
        try {
            con.guardaConfiguracoes();
            con.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            con.setCriterions(new Object[]{
                new Object[]{"this", Restrictions.in("id", arrCodigos)}
            });
            
            return con.getLista();
        } finally {
            con.limpaConfiguracoes();
        }
    }
    
}
