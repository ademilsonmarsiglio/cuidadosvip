package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.BasicJson;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPALogin;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.CryptoUtil;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ademilson
 */
@Service
public class LoginService {

    @Autowired
    private JPALogin loginJPA;
    
    public void salvar(Login login, HttpSession httpSession) {
        
        if(login.getEstabelecimento() == null){
            Estabelecimento estabelecimento = EstabelecimentoService.getEstabelecimento(httpSession);
            login.setEstabelecimento(estabelecimento);
        }
        
        login.setSenha(CryptoUtil.MD5(login.getSenha()));
        
        loginJPA.save(login);
    }

    public void excluir(Long codigo) {
        loginJPA.delete(codigo);
    }
    
    public Login getLogin(Long id){
        loginJPA.setJoin(new Object[]{
            new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN},
            new Object[]{"estabelecimento", JoinType.LEFT_OUTER_JOIN},
        });
        return loginJPA.get(id);
    }

    public List<Login> filtrar(Filter filtro) {
        String descricao = filtro == null ? null : filtro.getDescricao();
        Estabelecimento estabelecimento = filtro == null ? null : filtro.getEstabelecimento();
        
        ArrayList arrCriterion = new ArrayList();
        if(descricao != null && !descricao.trim().isEmpty()){
            arrCriterion.add(new Object[]{"this", Restrictions.like("login", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        if(estabelecimento != null){
            arrCriterion.add(new Object[]{"this", Restrictions.eq("estabelecimento", estabelecimento)});
        }
        
        try {
            loginJPA.guardaConfiguracoes();
            loginJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("login"), "this")});
            loginJPA.setJoin(new Object[]{
                new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estabelecimento", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estabelecimento.pessoa", JoinType.LEFT_OUTER_JOIN},
            });

            loginJPA.setCriterions(arrCriterion.toArray());

            return loginJPA.getLista();
        } finally {
            loginJPA.restauraConfiguracoes();
        }
    }
    
    public Login validarLogin(Login login){
        JPALogin loginJPAValidar = new JPALogin();
        if(login != null){
            String login_ = login.getLogin();
            String senha_ = login.getSenha();
            
            senha_ = CryptoUtil.MD5(senha_);
            
            loginJPAValidar.setMaxFetchSize(1);
            loginJPAValidar.setJoin(new Object[]{
                new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estabelecimento", JoinType.LEFT_OUTER_JOIN},
            });
            loginJPAValidar.setJoin(new Object[]{
                new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estabelecimento", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"estabelecimento.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            loginJPAValidar.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("estabelecimento.idEstabelecimento").as("estabelecimento.idEstabelecimento")),
                new ProjectionParc(Projections.property("estabelecimento.localArquivos").as("estabelecimento.localArquivos")),
                new ProjectionParc(Projections.property("pessoa1.IdPessoa").as("estabelecimento.pessoa.IdPessoa")),
                new ProjectionParc(Projections.property("pessoa.nmPessoa").as("pessoa.nmPessoa")),
                new ProjectionParc(Projections.property("idLogin").as("idLogin")),
                new ProjectionParc(Projections.property("admin").as("admin")),
                new ProjectionParc(Projections.property("tpLogin").as("tpLogin")),
                new ProjectionParc(Projections.property("superUser").as("superUser")),
                new ProjectionParc(Projections.property("qtdAcessos").as("qtdAcessos")),
            });
            loginJPAValidar.setCriterions(new Object[]{
                new Object[]{"this", Restrictions.eq("login", login_).ignoreCase()},
                new Object[]{"this", Restrictions.eq("senha", senha_)},
            });
            loginJPAValidar.setResultTransformer(new AliasToBeanNestedResultTransformer(Login.class));
            loginJPAValidar.setMaxFetchSize(1);
            List<Login> lista = loginJPAValidar.getLista();
            
            if(lista != null && !lista.isEmpty()){
                atualizaAcessos(lista.get(0));
                return lista.get(0);
            }
        }
        
        return null;
    }
    
    public List<BasicJson> getListaResumida(Estabelecimento estabelecimento) {
        ArrayList arrCriterion = new ArrayList();
        
        if(estabelecimento != null){
            arrCriterion.add(new Object[]{"this", Restrictions.eq("estabelecimento.idEstabelecimento", estabelecimento.getIdEstabelecimento())});
        }
        
        try {
            loginJPA.guardaConfiguracoes();
            loginJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.asc("login"), "this")});
            loginJPA.setJoin(new Object[]{
                new Object[]{"pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            loginJPA.setProjections(new ProjectionParc[]{
                new ProjectionParc(Projections.property("pessoa.nmPessoa").as("descricao")),
                new ProjectionParc(Projections.property("idLogin").as("codigo")),
            });
            loginJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(BasicJson.class));
            loginJPA.setCriterions(arrCriterion.toArray());

            return (List) loginJPA.getLista();
        } finally {
            loginJPA.restauraConfiguracoes();
        }
    }
    
    @Async
    public void atualizaAcessos(Login login){
        try {
            loginJPA.execQuerySQL("UPDATE login SET qtdAcessos = " + ((login.getQtdAcessos() == null ? 0 : login.getQtdAcessos()) + 1) + " WHERE idLogin = " + login.getIdLogin(), true);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao atualiza qtdAcessos", e);
        }
    }
}
