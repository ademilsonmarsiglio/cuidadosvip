package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.transients.Relatorios;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAProduto;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */

public class RelatoriosService {
    
    @Service
    public static class ProdutoPorPacienteSer {
        public List<Relatorios.ProdutoPorPaciente> getLista(Relatorios.ProdutoPorPaciente.Filter filter) {
            
            if (filter == null) {
                return new ArrayList();
            } 
            
            Long id = filter.getId();
            Date date1 = filter.getDate1();
            Date date2 = filter.getDate2();
            String tipoEstoque = filter.getTipoEstoque();
            
            String whereDate = "";
            
            if (date1 != null && date2 == null) {
                whereDate = " and itemprontuario.dtmovimentacao > '" + Util.getLimiteInicial(date1) + "'";
            } else if (date1 == null && date2 != null) {
                whereDate = " and itemprontuario.dtmovimentacao < '" + Util.getLimiteFinal(date2) + "'";
            } else if (date1 != null && date2 != null) {
                whereDate = " and itemprontuario.dtmovimentacao between  '" + Util.getLimiteInicial(date1) + "' and '" + Util.getLimiteFinal(date2) + "'";
            }
            
            String sql = ""
                    + "SELECT "
                    + "produto.dsproduto  as \"dsProduto\", "
                    + "sum(itemprontuario.qtd) as \"qtd\", "
                    + "estoque.tpEstoque as \"tpEstoque\" "
                    + "FROM itemprontuario "
                    + "left join prontuario_itemprontuario on item_iditemprontuario  = iditemprontuario "
                    + "left join prontuario on idprontuario = prontuario_idprontuario "
                    + "left join paciente on paciente.idpaciente = prontuario.idpaciente "
                    + "left join estoque on estoque.idestoque = itemprontuario.idestoque "
                    + "left join produto on estoque.idproduto = produto.idproduto "
                    + "WHERE paciente.idpaciente = " + id + " "
                    + whereDate
                    + (tipoEstoque != null && !tipoEstoque.equals("T") ?  " and estoque.tpEstoque = '" + tipoEstoque + "' " : "")
                    + " group by produto.dsproduto, estoque.tpEstoque "
                    + "order by produto.dsproduto";
            
            JPAEstoque jpaEstoque = new JPAEstoque();
            jpaEstoque.setResultTransformer(new AliasToBeanNestedResultTransformer(Relatorios.ProdutoPorPaciente.class));
            return jpaEstoque.execQuerySQL(sql);
        }
    }
    
    @Service
    public static class ProntuarioPorPaciente {
        
        @Autowired
        private JPAProntuario jpaProntuario;
        
        @Autowired
        ProntuarioService prontuarioService;
        
        public List<Paciente> getLista(Filter filtro) {
            
            Date date1 = null, date2 = null;
            
            Integer offset = -1;
            Integer limit = -1;
            
            if (filtro != null && filtro.getLimit() != null){
                limit = filtro.getLimit();
            }

            if (filtro != null && filtro.getOffset()!= null){
                offset = filtro.getOffset();
            }

            if (filtro != null) {
                if (!Verify.nullsOrEmpty(filtro.getDateStr1())) filtro.setDate1(Util.parseDate(filtro.getDateStr1()));
                if (!Verify.nullsOrEmpty(filtro.getDateStr2())) filtro.setDate2(Util.parseDate(filtro.getDateStr2()));
                    
                date1 = filtro.getDate1();
                date2 = filtro.getDate2();
            }
            
            date1 = Util.getLimiteInicial(date1);
            date2 = Util.getLimiteFinal(date2);
            
            try {
                jpaProntuario.guardaConfiguracoes();
                
                if (date1 != null && date2 != null) {
                    jpaProntuario.setCriterions(new Object[]{new Object[]{"this", Restrictions.between("dtProntuario", date1, date2)}});
                } else if (date1 != null) {
                    jpaProntuario.setCriterions(new Object[]{new Object[]{"this", Restrictions.ge("dtProntuario", date1)}});
                } else if (date2 != null) {
                    jpaProntuario.setCriterions(new Object[]{new Object[]{"this", Restrictions.le("dtProntuario", date2)}});
                }
                jpaProntuario.setOrderByClause(new OrderBy[]{
                    new OrderBy(Order.asc("nmPessoa"), "paciente.pessoa")
                });
                jpaProntuario.setJoin(new Object[]{
                    new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                    new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
                });
                jpaProntuario.setProjections(new ProjectionParc[]{
                    new ProjectionParc(Projections.groupProperty("paciente.idPaciente").as("idPaciente")),
                    new ProjectionParc(Projections.groupProperty("pessoa.nmPessoa").as("pessoa.nmPessoa")),
                });
                jpaProntuario.setResultTransformer(new AliasToBeanNestedResultTransformer(Paciente.class));
                return (List) jpaProntuario._getLista(offset, limit);
            } finally {
                jpaProntuario.restauraConfiguracoes();
            }
        }
        
        public List<Prontuario> relatorio(Long[] arrCodigosPacientes,
                                boolean enfermeiro,
                                boolean medico,
                                boolean tecEnfermeiro,
                                boolean cuidador,
                                boolean assSocial,
                                boolean fisioterapeuta,
                                boolean nutricionista,
                                boolean outros,
                                boolean imprimirDescricao,
                                boolean imprimirMedicamentos,
                                boolean imprimirSae,
                                String data1, 
                                String data2,
                                Long login) {
            return prontuarioService.relatorio(null, null, arrCodigosPacientes, enfermeiro, medico, tecEnfermeiro, cuidador, assSocial, fisioterapeuta, nutricionista, outros, data1, data2, null, login, imprimirMedicamentos, imprimirSae, imprimirDescricao);
        }
    }
    
    
    @Service
    public static class Produtos {
        
        @Autowired
        private JPAProduto jpaProdutos
;        
        @Autowired
        ProntuarioService prontuarioService;
        
        public List<Produto> getLista(Produto.FilterRelatorio filtro) {
            
            
            String where = "";
            String limitOffSet = "";
            
            if (filtro != null) {
                
                if (!Verify.nullsOrEmpty(filtro.getProduto())) {
                    where += " AND dsProduto ilike '%" + filtro.getProduto().replace(" ", "%") + "%'";
                }
                
                if (!Verify.nullsOrEmpty(filtro.getTipoEstoque()) && !Verify.equals(filtro.getTipoEstoque(), Produto.FilterRelatorio.EnumTipoEstoque.TODOS)) {
                    where += String.format(" AND tpEstoque = '%s'", Verify.equals(filtro.getTipoEstoque(), Produto.FilterRelatorio.EnumTipoEstoque.MEDICAMENTOS) ? "P" : "G");
                }
                
                if (!Verify.nullsOrEmpty(filtro.getSituacaoEstoque()) && !Verify.equals(filtro.getSituacaoEstoque(), Produto.FilterRelatorio.EnumSituacaoEstoque.TODOS)) {
                    where += String.format(" AND qtd %s", Verify.equals(filtro.getSituacaoEstoque(), Produto.FilterRelatorio.EnumSituacaoEstoque.NEGATIVOS) ? "<= 0" : "> 0");
                }
                
                if (filtro.getLimit() != null) {
                    limitOffSet += " LIMIT " + filtro.getLimit();
                }
                
                if (filtro.getOffset() != null) {
                    limitOffSet += " OFFSET " + filtro.getOffset();
                }
            }
            
            
            
            String sql = ""
                    + "SELECT dsProduto as \"dsProduto\", tpEstoque as \"tpEstoque\", SUM(qtd) as \"qtd\" "
                    + "FROM produto "
                    + "LEFT JOIN estoque on produto.idProduto = estoque.idProduto "
                    + "WHERE produto.status = true " + where
                    + "GROUP BY produto.idProduto, tpEstoque "
                    + "ORDER BY 1, 2 "
                    + limitOffSet ;
            

            
            try {
                jpaProdutos.guardaConfiguracoes();
                jpaProdutos.setResultTransformer(new AliasToBeanNestedResultTransformer(Produto.class));
                return jpaProdutos.execQuerySQL(sql);
            } finally {
                jpaProdutos.restauraConfiguracoes();
            }
        }
    }
}
