package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericCON;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Resumo;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class IndexService {
    private static final Logger LOG = Logger.getLogger(IndexService.class.getName());
    
    public Resumo getDadosResumo(){
        GenericCON sqlCon = new GenericCON(Estoque.class);
        
        Resumo resumo = new Resumo();
        
        //Verifica os pacientes documentado hoje
            String sql_documentadosHoje = "SELECT "
                    + "paciente.idPaciente, "
                    + "pessoa.nmPessoa "
                    + "FROM paciente "
                    + "LEFT JOIN prontuario "
                    + "ON prontuario.idpaciente = paciente.idpaciente "
                    + "LEFT JOIN pessoa "
                    + "ON pessoa.idpessoa = paciente.idpessoa "
                    + "WHERE "
                    + "date_trunc('day', prontuario.dtCriacao) = date_trunc('day', now()) "
                    + "and paciente.stPaciente = 'A' "
                    + "and (CASE "
                    + "	WHEN date_part('hour', now()) > 19 THEN prontuario.turno in('M','T', 'N') "
                    + "	WHEN date_part('hour', now()) > 12 THEN prontuario.turno in('M','T') "
                    + "	WHEN date_part('hour', now()) > 5 THEN prontuario.turno in('M') "
                    + "END)     "
                    + "GROUP BY paciente.idPaciente, pessoa.idpessoa "
                    + "order by pessoa.nmPessoa limit 11";


            List list_documentadosHoje  =sqlCon.execQuerySQL(sql_documentadosHoje);
            if(list_documentadosHoje != null && !list_documentadosHoje.isEmpty()){

                int size = list_documentadosHoje.size();

                resumo.setQtdDocumetadosHoje(size > 10 ? "10+" : String.valueOf(size));
                resumo.setPacientesDocumentados(populaListaPaciente(list_documentadosHoje));
            }
        
        
        //Verifica os clientes não documentado hoje
            String sql_naoDocumentadosHoje = "SELECT paciente.idPaciente, pessoa.nmPessoa "
                    + "FROM paciente "
                    + "LEFT JOIN prontuario ON prontuario.idpaciente = paciente.idpaciente "
                    + "LEFT JOIN pessoa ON pessoa.idpessoa = paciente.idpessoa "
                    + "WHERE "
                    + "paciente.stPaciente = 'A' "
                    + "and (prontuario.idProntuario is null "
                    + "or "
                    + "prontuario.idPaciente not in (SELECT distinct prontuario.idPaciente FROM prontuario WHERE date_trunc('day', now()) = date_trunc('day', prontuario.dtCriacao))) "
                    + "GROUP BY paciente.idPaciente, pessoa.idPessoa "
                    + "ORDER BY pessoa.nmPessoa limit 11";

            List list_naoDocumentadosHoje  =sqlCon.execQuerySQL(sql_naoDocumentadosHoje);

            if(list_naoDocumentadosHoje != null && !list_naoDocumentadosHoje.isEmpty()){
                int size = list_naoDocumentadosHoje.size();
                resumo.setQtdNaoDocumetadosHoje(size > 10 ? "10+" : String.valueOf(size));
                resumo.setPacientesNaoDocumentados(populaListaPaciente(list_naoDocumentadosHoje));
            }
            
        List<Resumo.ProdutoEstoqueBaixo> produtosEstoqueBaixo = getProdutosEstoqueBaixo();
        if (!Verify.nullsOrEmpty(produtosEstoqueBaixo)){
            resumo.setQtdProdutosEstoqueBaixo(produtosEstoqueBaixo.size() + "");
            resumo.setProdutoEstoqueBaixo(produtosEstoqueBaixo);
        } else {
            resumo.setQtdProdutosEstoqueBaixo("Nenhum");
        }
        
        return resumo;
    }
    
    private List<Paciente> populaListaPaciente(List listObj){
        List<Paciente> pacientes = new ArrayList();
            
        try {
            for (Object object : listObj) {
                Object[] row = (Object[]) object;
                Number idPaciente = (Number) row[0];
                String nmPaciente = (String) row[1];

                pacientes.add(
                        new Paciente()
                        .setIdPaciente(idPaciente.longValue())
                        .setPessoa(new Pessoa().setNmPessoa(nmPaciente)));
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Erro ao popular a lista de pacientes", e);
        }
        
        
        return pacientes;
    }
    
    private List<Resumo.ProdutoEstoqueBaixo> getProdutosEstoqueBaixo(){
        String sql = ""
                + "select "
                + "dsProduto as \"dsProduto\", "
                + "qtd as \"qtd\", "
                + "idEstoque as \"idEstoque\", "
                + "(CASE WHEN medicamento.idMedicamento IS NULL THEN 'Estoque Geral' ELSE nmPessoa END) as \"nmPessoa\" "
                + "from estoque "
                + "left join medicamento using (idestoque) "
                + "left join produto using (idproduto) "
                + "left join paciente using (idpaciente) "
                + "left join pessoa using (idpessoa) "
                + "where produto.status = true "
                + "and qtd <= 5 "
                + "and paciente.stPaciente = 'A' "
                + "order by nmPessoa nulls first, dsproduto ";
        
        GenericCON sqlCon = new GenericCON(Estoque.class);
        sqlCon.setResultTransformer(new AliasToBeanNestedResultTransformer(Resumo.ProdutoEstoqueBaixo.class));
        List lista = sqlCon.execQuerySQL(sql);
        return lista;
    }
}
