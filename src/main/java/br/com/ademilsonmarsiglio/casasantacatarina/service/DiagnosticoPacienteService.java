package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.DiagnosticoPaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPADiagnosticoPaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilsonmarsiglio
 */
@Service
public class DiagnosticoPacienteService {
    
    @Autowired
    private JPADiagnosticoPaciente jpa;
    
    public List<DiagnosticoPaciente> getLista(Long idPaciente, boolean organizaLista) {
        
        try {
            
            List<ProjectionParc> projectionAllFields = Util.projectionAllFields(DiagnosticoPaciente.class);
            
            jpa.guardaConfiguracoes();
            jpa.setProjections(projectionAllFields.toArray(new ProjectionParc[0]));
            jpa.setResultTransformer(new AliasToBeanNestedResultTransformer(DiagnosticoPaciente.class));
            
            jpa.setCriterions(new Object[]{
                new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)},
                new Object[]{"this", Restrictions.eq("ativo", true)}
            });
            
            jpa.setOrderByClause(new OrderBy[]{
                new OrderBy(Order.asc("tipo")),
                new OrderBy(Order.asc("ordem")),
            });
                       
            List<DiagnosticoPaciente> listBD = jpa.getLista();
            
            if (!organizaLista) {
                return listBD;
            }
            
            LinkedHashMap<Integer, DiagnosticoPaciente> map = new LinkedHashMap();
            
            //adiciona o pai
            for (DiagnosticoPaciente d : listBD) {
                if (d.getIdParent() == null) {
                    map.put(d.getId(), d);
                }
            }
            
            //adiciona os filhos
            for (DiagnosticoPaciente d : listBD) {
                if (d.getIdParent() != null) {
                    DiagnosticoPaciente master = map.get(d.getIdParent());
                    
                    if (master != null) {
                        master.addFilho(d);
                    }
                    
                }
            }
            
            return new ArrayList(map.values());
            
        } finally {
            jpa.restauraConfiguracoes();
        }
    }
    
    public DiagnosticoPaciente get(Integer id) {
        try {
            jpa.guardaConfiguracoes();
            return jpa.get(id);
        } finally {
            jpa.restauraConfiguracoes();
        }
    }
    
    public boolean salvar (DiagnosticoPaciente d, HttpSession session) {
        
        d.setAssuser(Util.getAssuser(session));
        
        try {
            jpa.guardaConfiguracoes();
            
            if (d.getId() == null) { //is insert

                if (d.getIdParent() != null)
                    jpa.setCriterions(new Object[]{
                        new Object[]{"this", Restrictions.eq("idParent", d.getIdParent())},
                    });
                else {
                    jpa.setCriterions(new Object[]{
                        new Object[]{"this", Restrictions.isNull("idParent")},
                    });
                }

                jpa.setDefaultCriterions(new Object[]{
                    new Object[]{"this", Restrictions.eq("ativo", Boolean.TRUE)},
                    new Object[]{"this", Restrictions.eq("paciente.idPaciente", d.getPaciente().getIdPaciente())},
                });            
                jpa.setOrderByClause(new OrderBy[]{
                    new OrderBy(Order.desc("ordem")),
                });

                DiagnosticoPaciente ultimo = jpa.getUniqueResult();

                if (ultimo != null) {
                    d.setOrdem(ultimo.getOrdem() + 1);            
                }
            }

            return jpa.save(d);
        } finally {
            jpa.restauraConfiguracoes();
        }
    }
    
    public boolean excluir(Long id, HttpSession session) {
        return jpa.execQuerySQL("UPDATE diagnosticopaciente SET ativo = false, assuser = '" + Util.getAssuser(session) +  "' WHERE id = " + id + " OR idParent = " + id, true) != null;
    }
    
    public boolean ordenar(Integer id, String pos) {
        try {
            jpa.guardaConfiguracoes();
            
            jpa.setJoin(new Object[]{new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN}});
            if (jpa.localizar(id)) {
                int oldOrdem = jpa.getObjeto().getOrdem();
                int newOrdem = oldOrdem + ( pos.equals("UP") ? -1 : 1);

                if (jpa.getObjeto().getIdParent() != null)
                    jpa.setDefaultCriterions(new Object[]{
                        new Object[]{"this", Restrictions.eq("idParent", jpa.getObjeto().getIdParent())},
                    });
                else {
                    jpa.setDefaultCriterions(new Object[]{
                        new Object[]{"this", Restrictions.isNull("idParent")},
                    });
                }


                jpa.setCriterions(new Object[]{
                    new Object[]{"this", Restrictions.eq("paciente.idPaciente", jpa.getObjeto().getPaciente().getIdPaciente())},
                    new Object[]{"this", Restrictions.eq("ativo", Boolean.TRUE)},
                    new Object[]{"this", pos.equals("UP") 
                                            ? Restrictions.lt("ordem", oldOrdem)
                                            : Restrictions.gt("ordem", oldOrdem)},
                });

                jpa.setOrderByClause(new OrderBy[] {
                    new OrderBy(pos.equals("UP") 
                                            ? Order.desc("ordem")
                                            : Order.asc("ordem")
                            )
                });



                DiagnosticoPaciente anterior = jpa.getUniqueResult();
                if (anterior != null) {
                    jpa.getObjeto().setOrdem(newOrdem);
                    jpa.alterar();

                    anterior.setOrdem(oldOrdem);
                    jpa.setObjeto(anterior);
                } 

                return jpa.gravar();
            }

            return false;
        
        } finally {
            jpa.restauraConfiguracoes();
        }  
    }
    
}
