package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.AliasToBeanNestedResultTransformer;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SinalVital;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPASinalVital;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class SinalVitalService {

    @Autowired
    private JPASinalVital sinalVitalJPA;
    
    public boolean salvar(SinalVital sinalVital, HttpSession httpSession) {
        sinalVital.setAssuser(Util.getAssuser(httpSession));
        return sinalVitalJPA.save(sinalVital);
    }
    
    public SinalVital get(Long codigo){
        try {
            sinalVitalJPA.guardaConfiguracoes();
            sinalVitalJPA.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            return sinalVitalJPA.get(codigo);
        } finally {
            sinalVitalJPA.restauraConfiguracoes();
        }
    }

    public boolean excluir(SinalVital sinalVital) {
        sinalVitalJPA.setObjeto(sinalVital);
        return sinalVitalJPA.excluir();
    }
    
    public List<SinalVital> list(Filter filtro){
        
        Long idPaciente = filtro.getId();
        String nmPaciente = filtro.getDescricao();
        
        //se tem informação na variavel datestr popula o date.
        if (!Verify.nullsOrEmpty(filtro.getDateStr1())) filtro.setDate1(Util.parseDate(filtro.getDateStr1()));
        if (!Verify.nullsOrEmpty(filtro.getDateStr2())) filtro.setDate2(Util.parseDate(filtro.getDateStr2()));
        
        Date date1 = Util.getLimiteInicial(filtro.getDate1());
        Date date2 = Util.getLimiteFinal(filtro.getDate2());
        
        Integer offset = -1;
        Integer limit = -1;

        if (filtro != null && filtro.getLimit() != null){
            limit = filtro.getLimit();
        }

        if (filtro != null && filtro.getOffset()!= null){
            offset = filtro.getOffset();
        }
        
            
        
        List arrCriterion = new ArrayList();
        arrCriterion.add(new Object[]{"paciente", Restrictions.eq("idPaciente", idPaciente)});
        
        if(nmPaciente != null && !nmPaciente.trim().isEmpty()){
            arrCriterion.add(new Object[]{"paciente.pessoa", Restrictions.like("nmPessoa", nmPaciente.replace(" ", "%").toUpperCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        
        if(date1 != null && date2 == null){ //Maior que data 1
            arrCriterion.add(new Object[]{"this", Restrictions.ge("data", date1)});
        } else  if (date1 == null && date2 != null){ //Menor que a data 2
            arrCriterion.add(new Object[]{"this", Restrictions.le("data", date2)});
        } else  if (date1 != null && date2 != null){ 
            arrCriterion.add(new Object[]{"this", Restrictions.between("data", date1, date2)});
        }
        
        List<ProjectionParc> projections = Util.projectionAllFields(SinalVital.class);
        projections.add(new ProjectionParc(Projections.property("login.identificador").as("login.identificador")));
        projections.add(new ProjectionParc(Projections.property("pessoa1.nmPessoa").as("login.pessoa.nmPessoa")));
        projections.add(new ProjectionParc(Projections.property("pessoa.nmPessoa").as("paciente.pessoa.nmPessoa")));
        
        try {
            sinalVitalJPA.guardaConfiguracoes();
            sinalVitalJPA.setProjections(projections.toArray(new ProjectionParc[0]));
            sinalVitalJPA.setResultTransformer(new AliasToBeanNestedResultTransformer(SinalVital.class));

            sinalVitalJPA.setJoin(new Object[]{
                new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"paciente.pessoa", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login", JoinType.LEFT_OUTER_JOIN},
                new Object[]{"login.pessoa", JoinType.LEFT_OUTER_JOIN},
            });
            sinalVitalJPA.setCriterions(arrCriterion.toArray());
            sinalVitalJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.desc("data")),});
            sinalVitalJPA.setUsePaginator(true);

            if (offset == -1 && limit == -1) {
                return sinalVitalJPA.getLista();
            }

            return sinalVitalJPA.getLista(offset, limit);
        } finally {
            sinalVitalJPA.restauraConfiguracoes();
        }
    }

    public String getMensagem() {
        return sinalVitalJPA.getMensagem();
    }
}
