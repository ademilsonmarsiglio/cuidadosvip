package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.OrderBy;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Arquivo;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAArquivo;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ademilson
 */
@Service
public class ArquivoService {

    @Autowired
    private JPAArquivo arquivoJPA;
    
    public void salvar(Arquivo prontuario) {
        prontuario.setDtModificacao(new Date());
        arquivoJPA.save(prontuario);
    }
    
    public Arquivo get(Long codigo){
        try {
            arquivoJPA.guardaConfiguracoes();
            arquivoJPA.setJoin(new Object[]{new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN}});
            return arquivoJPA.get(codigo);
        } finally {
            arquivoJPA.restauraConfiguracoes();
        }
    }

    public boolean excluir(Long codigo) {
        return arquivoJPA.delete(codigo);
    }
    
    public Arquivo getArquivo(Long id){
        try {
            arquivoJPA.guardaConfiguracoes();
            
            if(arquivoJPA.localizar(id))
                return arquivoJPA.getObjeto();

            return null;
        } finally {
            arquivoJPA.restauraConfiguracoes();
        }
    }

    public List<Arquivo> filtrar(Long idPaciente, Filter filtro) {
        String descricao = filtro == null ? null : filtro.getDescricao();
        
        ArrayList arrCriterion = new ArrayList();
        if(descricao != null && !descricao.trim().isEmpty()){
            arrCriterion.add(new Object[]{"this", Restrictions.like("nmArquivo", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        try {
            arquivoJPA.guardaConfiguracoes();
            arquivoJPA.setCriterions(arrCriterion.toArray());
            arquivoJPA.setOrderByClause(new OrderBy[]{new OrderBy(Order.desc("dtModificacao"))});
            arquivoJPA.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)}});

            return arquivoJPA.getLista();
        } finally {
            arquivoJPA.restauraConfiguracoes();
        }
    }
    
    public Arquivo gravar(MultipartFile file, Login login, Long idPaciente){
        
        try {
            String localArquivos = login.getEstabelecimento().getLocalArquivos();
        
            if(localArquivos == null || localArquivos.trim().isEmpty()){
                localArquivos = System.getProperty("java.io.tmpdir");
            }

            if(!localArquivos.endsWith(File.separator)){
                localArquivos = localArquivos.concat(File.separator);
            }

            String originalFileName = file.getOriginalFilename();
            String extensao = "";
            
            if(originalFileName.contains(".")){
                extensao = originalFileName.substring(originalFileName.lastIndexOf("."));
            }
            
            arquivoJPA.setConsiderarAcaoAoGravar(true);
            arquivoJPA.incluir();
            arquivoJPA.getObjeto()
                    .setNmArquivo(originalFileName)
                    .setDtModificacao(new Date())
                    .setTamanho(file.getSize())
                    .setPaciente(new Paciente().setIdPaciente(idPaciente))
                    .setLogin(login);
            if(arquivoJPA.gravar()){
                arquivoJPA.setObjeto(arquivoJPA.getObjeto());
                arquivoJPA.alterar();
                arquivoJPA.getObjeto().setLocalArquivo(localArquivos.concat(arquivoJPA.getObjeto().getIdArquivo().toString()).concat(extensao));
                if(arquivoJPA.gravar()){
                    return arquivoJPA.getObjeto();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao gravar arquivo no banco.", e);
        }
        
        return null;
    }
}
