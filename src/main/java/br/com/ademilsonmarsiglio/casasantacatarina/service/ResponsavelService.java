package br.com.ademilsonmarsiglio.casasantacatarina.service;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAResponsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ademilson
 */
@Service
public class ResponsavelService {

    @Autowired
    private JPAResponsavel responsavelJPA;
    
    public void salvar(Responsavel responsavel, HttpSession httpSession) {
        if(responsavel.getEstabelecimento() == null){
            Estabelecimento estabelecimento = EstabelecimentoService.getEstabelecimento(httpSession);
            responsavel.setEstabelecimento(estabelecimento);
        }
        responsavelJPA.save(responsavel);
    }
    
    public Responsavel get(Long codigo){
        try {
            responsavelJPA.guardaConfiguracoes();
            responsavelJPA.setJoin(new Object[]{new Object[]{"paciente", JoinType.LEFT_OUTER_JOIN}});
            return responsavelJPA.get(codigo);
        } finally {
            responsavelJPA.restauraConfiguracoes();
        }
    }

    public void excluir(Long codigo) {
        responsavelJPA.delete(codigo);
    }
    
    public Responsavel getResponsavel(Long id){
        try {
            responsavelJPA.guardaConfiguracoes();
            if (responsavelJPA.localizar(id)) {
                return responsavelJPA.getObjeto();
            }

            return null;
        } finally {
            responsavelJPA.restauraConfiguracoes();
        }
    }

    public List<Responsavel> filtrar(Long idPaciente, Filter filtro) {
         String descricao = filtro == null ? null : filtro.getDescricao();
        
        ArrayList arrCriterion = new ArrayList();
        if(descricao != null && !descricao.trim().isEmpty()){
            arrCriterion.add(new Object[]{"pessoa", Restrictions.like("nmPessoa", descricao.toLowerCase(), MatchMode.ANYWHERE).ignoreCase()});
        }
        
        try {
            responsavelJPA.guardaConfiguracoes();
            responsavelJPA.setCriterions(arrCriterion.toArray());
            responsavelJPA.setDefaultCriterions(new Object[]{new Object[]{"this", Restrictions.eq("paciente.idPaciente", idPaciente)}});

            return responsavelJPA.getLista();
        } finally {
            responsavelJPA.restauraConfiguracoes();
        }
    }
}
