package br.com.ademilsonmarsiglio.casasantacatarina.util;

import java.beans.PropertyEditorSupport;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import org.apache.log4j.Logger;

/**
 *
 * @author ademilson
 * @param <T>
 */
public class MonetaryPropertyEditor<T extends Number> extends PropertyEditorSupport{
    private static final Logger LOG = Logger.getLogger(MonetaryPropertyEditor.class);

    private final DecimalFormat df;
    
    public MonetaryPropertyEditor() {
        df = new DecimalFormat();
        df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("pt", "BR")));
        df.setGroupingSize(3);
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        df.setMinimumIntegerDigits(1);
    }
    
    
    @Override
    public void setAsText(String text) {
        try {
            if(text == null || text.trim().isEmpty())
                setValue(null);
            else {
                T obj = (T) df.parse(text);
                setValue(obj);
            }
            
        } catch (ParseException e) {
            LOG.fatal("Erro ao formatar o valor: " + text, e);
        }
    }

    @Override
    public String getAsText() {
        T value = (T) getValue();
        return (value != null ? this.df.format(value) : "");
    }
}