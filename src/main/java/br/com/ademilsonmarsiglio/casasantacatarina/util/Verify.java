package br.com.ademilsonmarsiglio.casasantacatarina.util;

import java.util.Collection;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

/**
 *
 * @author ademilson
 */
public class Verify {
    
    public static boolean nullsOrEmpty(Object obj){
        if(obj == null)
            return true;
        
        if(obj instanceof String){
            return ((String)obj).trim().isEmpty();
        } else if(obj instanceof Character){
            return ((Character)obj).equals(' ');
        } else if(obj instanceof Double){
            return ((Double) obj).isInfinite() || ((Double) obj).isNaN();
        } else if(obj instanceof Float){
            return ((Float) obj).isInfinite() || ((Float) obj).isNaN();
        } else if(obj instanceof Collection){
            return ((Collection)obj).isEmpty();
        } else if (obj instanceof HibernateProxy) { //testa se está lazy...
            LazyInitializer lazyInitializer = ((HibernateProxy) obj).getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized()) {
                return false;
            }
        } else if(obj instanceof Object[]){
            return ((Object[]) obj).length <= 0;
        }
        
        return false;
    }
    
    public static boolean equals(Object o1, Object o2){
        if (o1 == null && o2 == null){
            return true;
        } else if ((o1 == null && o2 != null) || (o1 != null && o2 == null)){
            return false;
        } else {
            return o1.equals(o2);
        }
    }
    
    public static boolean startsWith(String o1, String o2){
        if (o1 == null && o2 == null){
            return true;
        } else if ((o1 == null && o2 != null) || (o1 != null && o2 == null)){
            return false;
        } else {
            return o1.startsWith(o2);
        }
    }
    
    /**
     * Se obj1 for null, assume o valor do obj2.
     * @param <T>
     * @param obj1
     * @param obj2
     * @return 
     */
    public static <T>T coalesce(T obj1, T obj2) {
        if (nullsOrEmpty(obj1))
            return obj2;
        
        return obj1;
    }
}
