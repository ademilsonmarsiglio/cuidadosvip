package br.com.ademilsonmarsiglio.casasantacatarina.util;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.ProjectionParc;
import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.service.EstabelecimentoService;
import org.hibernate.criterion.Projections;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

import javax.imageio.ImageIO;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ademilson
 */
public class Util {
    
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private static DecimalFormat df;
    
    public static String[] getStringVector(String text, String delim){
        List<String> list = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(text, delim, false);
        while (tokenizer.hasMoreElements()) {
            list.add(((String) tokenizer.nextElement()).trim());
        }
        
        return list.toArray(new String[0]);
    }
    
    public static Date getLimiteInicial(Date date) {
        if (date != null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
            gc.clear(GregorianCalendar.MINUTE);
            gc.clear(GregorianCalendar.SECOND);
            gc.clear(GregorianCalendar.MILLISECOND);

            return gc.getTime();
        }
        
        return null;
    }
    
    public static Date getLimiteFinal(Date date) {
        if (date != null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            gc.set(GregorianCalendar.HOUR_OF_DAY, 23);
            gc.set(GregorianCalendar.MINUTE, 59);
            gc.set(GregorianCalendar.SECOND, 59);
            gc.set(GregorianCalendar.MILLISECOND, 999);

            return gc.getTime();
        }
        
        return null;
    }
    
    public static Date addDays(Date date, int days) {
        if (date != null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            gc.add(Calendar.DAY_OF_MONTH, days);
            return gc.getTime();
        }
        
        return null;
    }
    
    public static void parametersToReport(JasperReportsPdfView report, HttpSession httpSession){
        Estabelecimento estabelecimento = EstabelecimentoService.getEstabelecimento(httpSession);
        Pessoa pessoa = EstabelecimentoService.getPessoaEstabelecimento(estabelecimento);
        
        String NM_EMPRESA = pessoa.getDsFantasia();
        String CNPJ = pessoa.getCnpj();
        String logradouro = pessoa.getLogradouro();
        String numero = pessoa.getNumero();
        String bairro = pessoa.getBairro();
        String cidade = pessoa.getCidade();
        String FONE = pessoa.getFone();
        String EMAIL = pessoa.getEmail();
        
        String ENDERECO = logradouro + ", " + numero + ", " + bairro + " - " + cidade;
        
        report.addStaticAttribute("NM_EMPRESA", NM_EMPRESA);
        report.addStaticAttribute("CNPJ", CNPJ);
        report.addStaticAttribute("FONE", FONE);
        report.addStaticAttribute("EMAIL", EMAIL);
        report.addStaticAttribute("ENDERECO", ENDERECO);
        report.addStaticAttribute("LOGO", addImageParameter());
    }

    private static Image addImageParameter() {
        try {
            BufferedImage mapmodifiable = ImageIO.read(new ClassPathResource("static/images/logo-relatorio.jpg").getInputStream());
            return mapmodifiable;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static String formatDate(Date d){
        if (d == null)
            return "";
        else 
            return dateFormat.format(d);
    }
    
    public static String formatTimestamp(Date d){
        if (d == null)
            return "";
        else 
            return timestampFormat.format(d);
    }
    
    public static String formatTime(Date d){
        if (d == null)
            return "";
        else 
            return timeFormat.format(d);
    }
    
    public static Date dateSetTime(Date date, String time) {
        date = dateSet(date, Calendar.HOUR_OF_DAY, Integer.parseInt(time.substring(0, time.indexOf(":"))));
        date = dateSet(date, Calendar.MINUTE, Integer.parseInt(time.substring(time.indexOf(":") + 1)));
        return date;
    }
    
    public static Date dateSet(Date date, int field, int valor) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.set(field, valor);
        return gc.getTime();
    }
    
    public static Date parseDate(String date){
        if (!Verify.nullsOrEmpty(date)){
            try {
                return dateFormat.parse(date);
            } catch (ParseException ex) {
                Logger.getLogger(Util.class.getName()).log(Level.SEVERE, "Erro ao fazer o parse data.", ex);
            }
        }
        
        return null;
    }
    
    public static String formatNumber(Number nbr) {
        if (df == null) {
            df = new DecimalFormat();
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("pt", "BR")));
            df.setGroupingSize(3);
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            df.setMinimumIntegerDigits(1);
        }
        
        if (nbr == null) {
            return null;
        }
        
        return df.format(nbr);
    }
    
    /**
     * Tratamento, 'o' for null, retorna string vazia.
     * 
     * @param o
     * @return 
     */
    public static String ifNullEmpty (Object o) {
        if (o == null){
            return "";
        } else {
            return o.toString();
        }
    }
    
    public static List<ProjectionParc> projectionAllFields(Class clazz) {
        List<ProjectionParc> listaProjection = new ArrayList();
        Field[] fields = clazz.getDeclaredFields();
        
        for (Field field : fields) {
            if (field.getAnnotation(JoinColumn.class) == null && field.getAnnotation(Transient.class) == null){
                listaProjection.add(new ProjectionParc(Projections.property(field.getName()).as(field.getName())));
            }
        }
        
        return listaProjection;
    }
    
    public static int timeToMinute(String time) {
        if (Verify.nullsOrEmpty(time)) {
            return 0;
        }
        
        try {
            int indiceDoisPontos = time.indexOf(":");
            
            if (indiceDoisPontos >= 0) {
                int hour = Integer.parseInt(time.substring(0, indiceDoisPontos));
                int minute = Integer.parseInt(time.substring(indiceDoisPontos + 1));
                
                return (hour * 60) + minute;
            }

            return 0;
        } catch (Exception e) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, "Erro ao converter timeToMinute", e);
            return 0;
        }
    }
    
    public static Long convertObjToLong(Object v) {
        if (v != null) {
            if (v instanceof Long) {
                return (Long) v;
            } else if (v instanceof BigInteger) {
                return ((BigInteger) v).longValue();
            } else if (v instanceof Integer) {
                return ((Integer) v).longValue();
            }
        }

        return null;
    }
    
    public static Double round(Double d, int decimais) {
        return new BigDecimal(d).setScale(decimais, RoundingMode.HALF_UP).doubleValue();
    }
    
    public static String getAssuser(HttpSession session) {
        if (session != null) {
            Login login = (Login) session.getAttribute(Key.LOGIN_SESSION);
            
            if (login != null) {
                if (login.getPessoa() != null) {
                    return login.getPessoa().getNmPessoa();
                } else {
                    return login.getLogin();
                }
            }
        }
        
        return "no...";
    }

    public static String encodeURL(String url) {
        try {
            return URLEncoder.encode( url, "UTF-8" );
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
