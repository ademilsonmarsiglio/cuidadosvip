/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ademilsonmarsiglio.casasantacatarina.util.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author ademilson
 * @since 17/07/2018
 */
@Data
@Component
@ConfigurationProperties(prefix = "project")
public class ProjectSettings {
    private String version;
    private String name;
    private String production;
}
