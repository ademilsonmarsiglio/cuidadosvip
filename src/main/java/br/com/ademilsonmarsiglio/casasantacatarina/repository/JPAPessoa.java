package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class JPAPessoa extends GenericController<Pessoa> {

    public JPAPessoa() {
        super(Pessoa.class);
    }
    
    public List<Pessoa> findByNmPessoaContainingIgnoreCase(String nome){
        return getLista();
    }
}
