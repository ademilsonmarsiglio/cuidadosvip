package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.service.EstabelecimentoService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class JPAProduto extends GenericController<Produto> {

    public JPAProduto() {
        super(Produto.class);
    }
    
    public void save(Produto produto, HttpSession httpSession) {
        
        Estabelecimento estabelecimento = produto.getEstabelecimento();
        if(estabelecimento == null){
            estabelecimento = EstabelecimentoService.getEstabelecimento(httpSession);
            produto.setEstabelecimento(estabelecimento);
        }
        
        super.save(produto); 
    }
    
    public Produto localizaProdutoPeloNome(String nmProduto){
        JPAProduto con = new JPAProduto();
        con.setCriterions(new Object[]{
            new Object[]{"this", 
                Restrictions.sqlRestriction("(TRANSLATE(UPPER(dsProduto) , 'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜ', 'ACEIOUAEIOUAEIOUAOEU')) = " 
                                           +"(TRANSLATE(UPPER('" + nmProduto.trim() + "') , 'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜ', 'ACEIOUAEIOUAEIOUAOEU'))")}
        });
        con.setMaxFetchSize(1);
        List<Produto> lista = con.getLista();
        
        if(!Verify.nullsOrEmpty(lista)){
            return lista.get(0);
        }
        
        
        return null;
    }
}
