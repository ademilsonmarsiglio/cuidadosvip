package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.AnamnesePaciente;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAAnamnese extends GenericController<AnamnesePaciente> {

    public JPAAnamnese() {
        super(AnamnesePaciente.class);
    }
}