package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Arquivo;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAArquivo extends GenericController<Arquivo> {

    public JPAArquivo() {
        super(Arquivo.class);
    }
}