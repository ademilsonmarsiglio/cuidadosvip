package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.HistoricoEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class JPAEstoque extends GenericController<Estoque> {
    
    @Autowired
    private JPAHistoricoEstoque historicoEstoqueJPA;
    
    public JPAEstoque() {
        super(Estoque.class);
    }
    
    public Estoque insereEstoqueZerado(Produto produto, Character tpEstoque, Login login){
        incluir();
        getObjeto().setProduto(produto).setTpEstoque(tpEstoque).setQtd(0D);
        
        if(gravar()){
            Estoque estoque = getObjeto();
            
            Double qtd = produto.getQtdEstoqueInicial();
            
            if (Verify.nullsOrEmpty(qtd))
                qtd = 0D;
            
            historicoEstoqueJPA.incluir();
            historicoEstoqueJPA.getObjeto().setEstoque(estoque);
            historicoEstoqueJPA.getObjeto().setQtd(qtd);
            historicoEstoqueJPA.getObjeto().setLogin(login);
            historicoEstoqueJPA.getObjeto().setObservacao("Estoque Inicial");
            historicoEstoqueJPA.gravar();
            
            return estoque;
        } else {
            return null;
        }
    }
    
    public boolean excluirEstoqueByProduto(Long idProduto){
        
        String sql = "";
        
        sql += "DELETE FROM historicoestoque WHERE idEstoque IN (SELECT idEstoque FROM estoque WHERE idProduto = " + idProduto + ");";
        sql += "DELETE FROM estoque WHERE idEstoque IN (SELECT idEstoque FROM estoque WHERE idProduto = " + idProduto + ");";
        
//        List<Estoque> estoques = getEstoquesByProduto(idProduto);
//        
////        for (Estoque estoque : estoques) {
////            try {
////                historicoEstoqueJPA.guardaConfiguracoes();
////                historicoEstoqueJPA.setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("estoque.idEstoque", estoque.getIdEstoque())}});
////                List<HistoricoEstoque> listaHistoricoEstoque = historicoEstoqueJPA.getLista();
////                
////                if (!Verify.nullsOrEmpty(listaHistoricoEstoque)) {
////                    historicoEstoqueJPA.excluir(listaHistoricoEstoque.toArray());
////                }
////            } finally {
////                historicoEstoqueJPA.restauraConfiguracoes();
////            }
////        }
//        
//        return excluir(estoques.toArray());

        return execQuerySQL(sql, true) != null;
    }
    
    public List<Estoque> getEstoquesByProduto(Long idProduto){
        setCriterions(new Object[]{new Object[]{"this", Restrictions.eq("produto.idProduto", idProduto)}});
        return getLista();
    }
}
