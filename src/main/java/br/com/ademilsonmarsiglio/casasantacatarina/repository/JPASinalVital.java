package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SinalVital;
import org.springframework.stereotype.Repository;

/**
 * @since 01/08/2017
 * @author ademilson
 */
@Repository
public class JPASinalVital extends GenericController<SinalVital> {
    public JPASinalVital() {
        super(SinalVital.class);
    }
}
