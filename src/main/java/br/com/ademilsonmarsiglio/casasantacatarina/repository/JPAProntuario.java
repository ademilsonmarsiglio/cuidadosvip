package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.ItemProntuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.hibernate.collection.internal.PersistentBag;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAProntuario extends GenericController<Prontuario> {

    public JPAProntuario() {
        super(Prontuario.class);
    }

    /**
     * Sobrescrito método para agrupar os itens do prontuario, que é uma lista, e ao retorno da consulta, ele duplica os registros.
     * 
     * @return 
     */
    @Override
    public List<Prontuario> getLista(int primeiro, int maximo) {
        List<Prontuario> lista = super.getLista(primeiro, maximo); 
        
        LinkedHashMap<Long, LinkedHashMap<Long, ItemProntuario>> hm_agrupaItemProntuario = new LinkedHashMap();
        
        for (Prontuario prontuario : lista) {
            Long key = prontuario.getIdProntuario();
            LinkedHashMap<Long, ItemProntuario> hm_agrupaItem;
            
            if(hm_agrupaItemProntuario.containsKey(key)){
                hm_agrupaItem = hm_agrupaItemProntuario.get(key);
            } else {
                hm_agrupaItem = new LinkedHashMap();
            }
            
            List<ItemProntuario> itens = prontuario.getItens();
            
            try {
                if (itens != null && !(itens instanceof HibernateProxy)) {
                    for (ItemProntuario item : itens) {
                        hm_agrupaItem.put(item.getIdItemProntuario(), item);
                    }
                }
            } catch (Exception e) {}
            
            hm_agrupaItemProntuario.put(key, hm_agrupaItem);
        }
        
        List<Prontuario> listaRetorno = new ArrayList();
        for (Long key : hm_agrupaItemProntuario.keySet()) {
            for (Prontuario prontuario : lista) {
                if(prontuario.getIdProntuario().equals(key)){
                    LinkedHashMap<Long, ItemProntuario> hm_agrupaItem = hm_agrupaItemProntuario.get(key);
                    if(!Verify.nullsOrEmpty(hm_agrupaItem)){
                        prontuario.setItens(new ArrayList(hm_agrupaItem.values()));
                    }
                    
                    listaRetorno.add(prontuario);
                    break;
                }
            }
        }
        
        return listaRetorno;
    }
    
    public List _getLista() {
        return _getLista(-1, -1);
    }
    
    public List _getLista(int primeiro, int maximo) {
        return super.getLista(primeiro, maximo);
    }
}