package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.DiagnosticoPaciente;
import org.springframework.stereotype.Repository;

/**
 * @since 12/02/2019
 * @author ademilsonmarsiglio
 */
@Repository
public class JPADiagnosticoPaciente extends GenericController<DiagnosticoPaciente> {
    public JPADiagnosticoPaciente() {
        super(DiagnosticoPaciente.class);
    }
}
