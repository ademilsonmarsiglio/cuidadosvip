package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAResponsavel extends GenericController<Responsavel> {

    public JPAResponsavel() {
        super(Responsavel.class);
    }

    @Override
    public boolean save(Responsavel responsavel) {
        JPAPessoa pessoaJPA = new JPAPessoa();
        pessoaJPA.save(responsavel.getPessoa());
        return super.save(responsavel); 
    }
}