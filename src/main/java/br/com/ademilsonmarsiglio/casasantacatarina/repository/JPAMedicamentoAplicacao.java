package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.MedicamentoAplicacao;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAMedicamentoAplicacao extends GenericController<MedicamentoAplicacao> {

    public JPAMedicamentoAplicacao() {
        super(MedicamentoAplicacao.class);
    }
}