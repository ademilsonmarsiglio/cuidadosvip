package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SaeProntuario;
import org.springframework.stereotype.Repository;

/**
 * @since 14/02/2019
 * @author ademilsonmarsiglio
 */
@Repository
public class JPASaeProntuario extends GenericController<SaeProntuario> {
    public JPASaeProntuario() {
        super(SaeProntuario.class);
    }
}
