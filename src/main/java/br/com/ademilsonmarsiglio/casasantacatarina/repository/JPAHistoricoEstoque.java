package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.HistoricoEstoque;
import org.springframework.stereotype.Repository;

@Repository
public class JPAHistoricoEstoque extends GenericController<HistoricoEstoque> {
    public JPAHistoricoEstoque() {
        super(HistoricoEstoque.class);
    }
}
