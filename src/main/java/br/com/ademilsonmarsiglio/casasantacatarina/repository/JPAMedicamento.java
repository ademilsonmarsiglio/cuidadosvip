package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAMedicamento extends GenericController<Medicamento> {

    public JPAMedicamento() {
        super(Medicamento.class);
    }
}