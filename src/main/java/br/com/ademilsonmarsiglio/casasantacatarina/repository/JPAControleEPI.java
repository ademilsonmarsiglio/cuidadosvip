package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.ControleEPI;
import org.springframework.stereotype.Repository;

/**
 * @since 09/11/2017
 * @author ademilson
 */
@Repository
public class JPAControleEPI extends GenericController<ControleEPI> {
    public JPAControleEPI() {
        super(ControleEPI.class);
    }
}
