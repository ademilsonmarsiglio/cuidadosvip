package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import org.springframework.stereotype.Repository;

@Repository
public class JPAEstabelecimento extends GenericController<Estabelecimento> {

    public JPAEstabelecimento() {
        super(Estabelecimento.class);
    }
    
    @Override
    public boolean save(Estabelecimento estabelecimento) {
        JPAPessoa pessoaJPA = new JPAPessoa();
        pessoaJPA.save(estabelecimento.getPessoa());
        return super.save(estabelecimento); 
    }
}
