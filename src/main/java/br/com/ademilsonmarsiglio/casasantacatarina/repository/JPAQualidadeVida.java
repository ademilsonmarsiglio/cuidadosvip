package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.QualidadeVida;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ademilson
 */
@Repository
public class JPAQualidadeVida extends GenericController<QualidadeVida> {

    public JPAQualidadeVida() {
        super(QualidadeVida.class);
    }
}