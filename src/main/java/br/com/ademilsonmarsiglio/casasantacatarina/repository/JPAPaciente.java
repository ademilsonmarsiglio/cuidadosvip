package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.service.EstabelecimentoService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Repository;

@Repository
public class JPAPaciente extends GenericController<Paciente> {

    public JPAPaciente() {
        super(Paciente.class);
    }
    
    public boolean save(Paciente paciente, HttpSession httpSession) {
        
        paciente.setAssuser(Util.getAssuser(httpSession));
        paciente.getPessoa().setAssuser(Util.getAssuser(httpSession));
        
        JPAPessoa pessoaJPA = new JPAPessoa();
        pessoaJPA.save(paciente.getPessoa());
        
        Estabelecimento estabelecimento = paciente.getEstabelecimento();
        if(estabelecimento == null){
            estabelecimento = EstabelecimentoService.getEstabelecimento(httpSession);
            paciente.setEstabelecimento(estabelecimento);
        }
        
        if(super.save(paciente)){
            JPAResponsavel responsavelJPA = new JPAResponsavel();
                if(paciente.getResponsavelPrincipal() != null 
                        && paciente.getResponsavelPrincipal().getPessoa().getNmPessoa() != null
                        && !paciente.getResponsavelPrincipal().getPessoa().getNmPessoa().trim().isEmpty()){
                    Responsavel responsavel = paciente.getResponsavelPrincipal(); 
                    responsavel.setPaciente(paciente);
                    responsavel.setEstabelecimento(estabelecimento);
                    responsavelJPA.save(responsavel);
                }


                if(paciente.getResponsavel() != null
                        && paciente.getResponsavel().getPessoa().getNmPessoa() != null
                        && !paciente.getResponsavel().getPessoa().getNmPessoa().trim().isEmpty()){

                    Responsavel responsavel = paciente.getResponsavel(); 
                    responsavel.setPaciente(paciente);
                    responsavel.setEstabelecimento(estabelecimento);
                    responsavelJPA.save(responsavel);
                }
                
            return true;
        }
        
        return false;
    }
}
