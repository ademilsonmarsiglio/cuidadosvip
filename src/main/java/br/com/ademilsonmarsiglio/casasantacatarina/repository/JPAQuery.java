package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import org.springframework.stereotype.Repository;

/**
 * Controller usado apenas para querys;
 * 
 * @author ademilson
 * @since 13/03/2018
 */
@Repository
public class JPAQuery extends GenericController {
    public JPAQuery() {
        super(null);
    }
}
