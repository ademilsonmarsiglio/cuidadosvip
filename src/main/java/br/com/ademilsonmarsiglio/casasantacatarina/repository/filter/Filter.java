package br.com.ademilsonmarsiglio.casasantacatarina.repository.filter;

import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ademilson
 */
@Data
@Accessors(chain = true)
public class Filter implements Serializable{
    private String descricao;
    private String descricao2;
    private Long id;
    private Character situacao;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Null
    private Date date1;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Null
    private Date date2;
    
    private String dateStr1;
    private String dateStr2;
//    
    private Estabelecimento estabelecimento;
    
    private Integer offset;
    private Integer limit;
    
    public Filter setSessionEstabelecimento(HttpSession httpSession){
        Login sessionLogin = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        estabelecimento = sessionLogin.getEstabelecimento();
        return this;
    }
}
