package br.com.ademilsonmarsiglio.casasantacatarina.repository;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.GenericController;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import org.springframework.stereotype.Repository;

@Repository
public class JPALogin extends GenericController<Login> {

    public JPALogin() {
        super(Login.class);
    }
    
    @Override
    public boolean save(Login login) {
        if(login.getPessoa() != null){
            JPAPessoa pessoaJPA = new JPAPessoa();
            pessoaJPA.save(login.getPessoa());
        }
        return super.save(login); 
    }
}
