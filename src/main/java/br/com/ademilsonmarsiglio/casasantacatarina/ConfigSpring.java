package br.com.ademilsonmarsiglio.casasantacatarina;

import br.com.ademilsonmarsiglio.casasantacatarina.config.InterceptorLogin;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.JPAEstabelecimento;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

import javax.servlet.MultipartConfigElement;
import javax.swing.*;

@SpringBootApplication(exclude = JpaRepositoriesAutoConfiguration.class)
@EnableAsync
public class ConfigSpring extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ConfigSpring.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ConfigSpring.class, args);
    }        

    @Configuration
    public static class MvcConfig extends WebMvcConfigurerAdapter {

        @Override
        public void addViewControllers(ViewControllerRegistry registry) {
//            registry.addRedirectViewController("/", "/index");
            
            //só para abrir a sessao:
            JPAEstabelecimento jpa = new JPAEstabelecimento();
            jpa.getUniqueResult();

            System.out.println("#####################################");
            System.out.println("######                       ########");
            System.out.println("###### ABRINDO SESSAO COM BD ########");
            System.out.println("######                       ########");
            System.out.println("#####################################");
        }

        @Override
        public void addInterceptors(InterceptorRegistry registry) {
            registry
                    .addInterceptor(new InterceptorLogin())
                    .addPathPatterns("/**")
                    .excludePathPatterns("/static/**")
            ;
        }
    }
    
    @Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages/messages_pt_BR.properties");
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    @Qualifier("report_paciente")
    public JasperReportsPdfView initRelPaciente() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/PacienteMainRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }

    @Bean
    @Qualifier("report_prontuario")
    public JasperReportsPdfView initRelProntuario() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/ProntuarioMainRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_sinalVital")
    public JasperReportsPdfView initRelSinalVital() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/SinaisVitaisMainRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    
    @Bean
    @Qualifier("report_medicamento")
    public JasperReportsPdfView initRelMedicamento() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/MedicamentoMainRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_produtoByPaciente")
    public JasperReportsPdfView initRelProdutoPorPaciente() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/ProdutosPorPacienteRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_controleEpi")
    public JasperReportsPdfView initRelControleEpi() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/ControleEpi.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_produtos")
    public JasperReportsPdfView initRelProdutos() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.addStaticAttribute("LOGO", new ImageIcon(ConfigSpring.class.getResource("/static/images/logo-relatorio.jpg").getFile()).getImage());
        v.setUrl("classpath:jasperreports/ProdutosRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_diagnosticos_condutas_paciente")
    public JasperReportsPdfView initRelDiagnosticosECondutas() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/DiagnosticosIntervencoesPorPacienteRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_anamnese_paciente")
    public JasperReportsPdfView initRelAnamnesePaciente() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/AnamnesePacienteRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_resultadoqualidadevida_paciente")
    public JasperReportsPdfView initRelResulQualidadeVida() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/ResultadoQualidadeVidaRelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    @Qualifier("report_formularioqualidadevida_paciente")
    public JasperReportsPdfView initRelFormQualidadeVida() {
        JasperReportsPdfView v = new JasperReportsPdfView();
        v.setUrl("classpath:jasperreports/FormularioQualidadeVida2RelJasper.jasper");
        v.setReportDataKey("datasource");
        return v;
    }
    
    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("250MB");
        factory.setMaxRequestSize("250MB");
        return factory.createMultipartConfig();
    }
}
