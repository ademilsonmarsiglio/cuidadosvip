package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "medicamentoaplicacao")
@Data
@EqualsAndHashCode(of = "id")
@ToString(exclude = "medicamento")
public class MedicamentoAplicacao implements Serializable{
    @Id
    @Generated(GenerationTime.INSERT)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;
    
    @Column(scale = 2, precision = 8, length = 10)
    private Double aplicacao;
    
    @Column(length = 5)
    private String hora;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idMedicamento", nullable = false)
    private Medicamento medicamento;
}
