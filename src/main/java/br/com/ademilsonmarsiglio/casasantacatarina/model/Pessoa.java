package br.com.ademilsonmarsiglio.casasantacatarina.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 */

@Entity()
@Table(name = "pessoa")
@Data
@SequenceGenerator(allocationSize = 1, name = "SEQ_PESSOA", sequenceName = "SEQ_PESSOA")
@Accessors(chain = true)
public class Pessoa implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_BASPESSOA")
    @GenericGenerator(name = "SEQ_BASPESSOA", strategy = "increment")
    @Column(length = 8, updatable = false)
    private Long IdPessoa;

    @Column(length = 100, nullable = false)
    @NotBlank(message = "O campo Nome deve ser informado.")
    private String nmPessoa;

    @Column(length = 50)
    private String dsFantasia;

    @Column(length = 10)
    private String rg;

    @CPF
    @Column(length = 14)
    private String cpf;

    @CNPJ
    @Column(length = 18)
    private String cnpj;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dtNascimento;
    
    @Column(length = 200)
    private String nomePais;

    @Column(length = 50)
    private String profissao;

    @Column(length = 100)
    private String logradouro;

    @Column(length = 5)
    private String numero;

    @Pattern(regexp = "\\d{5}-\\d{3}", message = "Formato do CEP inválido")
    private String cep = null;

    @Column(length = 30)
    private String bairro;
    
    @Column(length = 50)
    private String cidade;

    @Column(length = 50)
    private String fone;

    @Email
    @Column(length = 100)
    private String email;

    @Column(length = 1, columnDefinition = "char")
    private Character tpPessoa = 'F';
    
    /**
     * M - Masculino
     * F - Feminino
     */
    private Character sexo;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro = new Date();
    
    private String assuser;
    
    /**
     * Recuperado atraves da data de nascimento;
     */
    @Transient
    private Integer idade;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "idCidade")
//    private Cidade cidade;


    public String getCep() {
        if(cep == null)
            return null;
        else if (cep.trim().isEmpty())
            return null;
        else 
            return cep;
    }
    
    public void setCep(String cep){
        if(cep != null){
            if(cep.trim().isEmpty())
                this.cep = null;
            else
                this.cep = cep;
        } else {
            this.cep = null;
        }
    }

    public String getCpf(){
        if(cpf == null)
            return null;
        else if (cpf.trim().isEmpty())
            return null;
        else 
            return cpf;
    }
    
    public void setCpf(String cpf){
        if(cpf != null){
            if(cpf.trim().isEmpty())
                this.cpf = null;
            else
                this.cpf = cpf;
        } else {
            this.cpf = null;
        }
    }
    
    public Integer getIdade(){
        
        if(getDtNascimento() != null){
            Calendar c = Calendar.getInstance();
            c.setTime(getDtNascimento());
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            
            
            
            Calendar cHoje = Calendar.getInstance();
            int yearAtual = cHoje.get(Calendar.YEAR);
            int atualMonth = cHoje.get(Calendar.MONTH);
            int atualDate = cHoje.get(Calendar.DAY_OF_MONTH);
            
            int newyear = yearAtual - year;
            
            if(month > atualMonth){
                newyear = newyear -1;
            } else if (month == atualMonth){
                if(day > atualDate){
                    newyear = newyear -1;
                }
            }
            
            return newyear;
        }
        
        return null;
    }
}
