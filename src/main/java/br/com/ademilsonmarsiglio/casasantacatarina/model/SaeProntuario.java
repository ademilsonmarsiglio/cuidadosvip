package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 *
 * @author ademilsonmarsiglio
 */

@Data
@Entity
@Table(name = "sae_prontuario")
public class SaeProntuario implements Serializable {
    
    @Id
    @Generated(GenerationTime.INSERT)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;
    
    
    private Boolean marcado = Boolean.FALSE;
    
    /**
     * Guarda a descricao do diagnostico, para que se alterar, não venha dar problema..
     */
    @Column(length = 1000)
    private String descricao;
    
    @Column(length = 1000)
    private String justificativa;
    
    
    @ManyToOne(fetch = FetchType.EAGER) //é eager pq sempre q tem um tem o outro.
    @JoinColumn(name = "idDiagnosticoPaciente", referencedColumnName = "id", nullable = false)
    private DiagnosticoPaciente diagnosticoPaciente;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProntuario", referencedColumnName = "idProntuario", nullable = false)
    private Prontuario prontuario;
    
    
    @Transient
    private List<SaeProntuario> filhos;
    
    public void addFilho(SaeProntuario d) {
        if (filhos == null) {
            filhos = new ArrayList<>();
        }
        
        filhos.add(d);
    }
   
    
    public SaeProntuario setId(Object id) {
        if (id != null)
            if (id instanceof Long)
                this.id = (Long) id;
            else if (id instanceof BigInteger)
                this.id = ((BigInteger) id).longValue();
            else if (id instanceof Integer)
                this.id = ((Integer) id).longValue();
            else if (id instanceof String)
                this.id = Long.parseLong((String) id);
        else 
            this.id = null;
        
        return this;
    }
}
            
            
