package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "estoque")
@SequenceGenerator(allocationSize = 1, name = "SEQ_ESTOQUE", sequenceName = "SEQ_ESTOQUE")
@Data
@EqualsAndHashCode(exclude = {"produto"})
@Accessors(chain = true)
@DynamicUpdate
@SelectBeforeUpdate
public class Estoque implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ESTOQUE")
    private Long idEstoque;
    
    @Column(scale = 2, precision = 8, length = 10)
    @ColumnDefault("0.0")
    private Double qtd;
    
    /**
     * G - Geral;
     * P - Paciente
     */
    @ColumnDefault("'G'")
    private Character tpEstoque = 'G';
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false, name = "idProduto", referencedColumnName = "idProduto")
    private Produto produto;
    
    public Estoque setIdEstoque(Object idEstoque) {
        this.idEstoque = Util.convertObjToLong(idEstoque);
        return this;
    }
}
