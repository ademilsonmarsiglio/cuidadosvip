package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import lombok.Data;

/**
 *
 * @author ademilson
 */
@Data
public class Resumo implements Serializable{
    private String qtdDocumetadosHoje = "0";
    private String qtdNaoDocumetadosHoje = "0";
    private String qtdProdutosEstoqueBaixo = "0";
    
    private List<Paciente> pacientesDocumentados;
    private List<Paciente> pacientesNaoDocumentados;
    private List<ProdutoEstoqueBaixo> produtoEstoqueBaixo;
    
    @Data
    public static class ProdutoEstoqueBaixo implements Serializable {
        private BigInteger idEstoque;
        private String dsProduto;
        private String nmPessoa;
        private Double qtd;
    }
}
