package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 *
 * @author ademilsonmarsiglio
 */
@Data
@Entity
@Table(name = "anamnese")
@Accessors(chain = true)
public class AnamnesePaciente implements Serializable {
    
    @Id
    @Generated(GenerationTime.INSERT)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;
    
    
    
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    
    
    
    @Enumerated(EnumType.STRING)
    private EnumNivelConsciencia nivelConsciencia;
    
    
    
    @Enumerated(EnumType.STRING)
    private EnumOrientacao orientacao;
    
    
    // CABECA E PESCOCO:
    @Column(length = 1000)
    private String cabeca_dsCranio;
    
    
    
    @Column(length = 1000)
    private String cabeca_dsOlhos;
    
    
    
    @Column(length = 1000)
    private String cabeca_dsOuvidos;
    
    
    
    @Column(length = 1000)
    private String cabeca_dsNariz;
    
    
    
    @Column(length = 1000)
    private String cabeca_dsCavidadeOral;
    
    
    
    @Column(length = 1000)
    private String torax_dsInspecao;
    
    
    
    @Column(length = 1000)
    private String torax_dsPalpacao;
    
    
    
    @Column(length = 1000)
    private String torax_dsAusculta;
    
    
    
    @Column(length = 1000)
    private String abdome_dsInspecao;
    
    
    
    @Column(length = 1000)
    private String abdome_dsPalpacao;
    
    
    
    @Column(length = 1000)
    private String abdome_dsAusculta;
    
    
//    
//    @Column(length = 1000)
//    private String abdome_dsGastronomia;
    
    
    
    
    @Column(length = 1000)
    private String abdome_dsOstomia;
    
    
    
    
    @Column(length = 1000)
    private String membros_dsInspecao;
    
    
    
    
    @Column(length = 1000)
    private String membros_dsPalpacao;
    
    
    
    
    @Column(length = 1000)
    private String genitarioMasculino_dsInspecao;
    
    
    
    
    @Column(length = 1000)
    private String genitarioFeminino_dsInspecao;
    
    
    
    
    @Column(length = 1000)
    private String genitarioFeminino_dsPerianal;
    
    
    
    
    private EnumNivelIncontinencia eliminacaoFisiologicas_incontinenciaUrinaria;
    
    
    private EnumNivelIncontinencia eliminacaoFisiologicas_incontinenciaIntestinal;
    
    
    @Column(length = 1000)
    private String eliminacaoFisiologicas_dsEvacuacao;
    
    
    
    
    @Column(length = 1000)
    private String eliminacaoFisiologicas_dsDiurese;
    
    
    
    
    @Column(length = 1000)
    private String peleAnexos_dsInspecao;
    
    
    
    @Column(length = 1000)
    private EnumUlcerasPorPressao peleAnexos_ulcerasPorPressao;
    
    
    
    @Enumerated(EnumType.STRING)
    private EnumMobilidadeFisica mobilidadeFisica;
    
    
    @Column(length = 1000)
    private String mobilidadeFisica_observacao;
    
    private Boolean alimentacao_voSolido;
    private Boolean alimentacao_voPastoso;
    private Boolean alimentacao_voBrando;
    private Boolean alimentacao_sng;
    private Boolean alimentacao_gastrostromia;
    
    @Column(length = 1000)
    private String alimentacao_observacao;
    
    
    private Boolean hidratacao_voLiquido;
    private Boolean hidratacao_voLiquidoEspressado;
    private Boolean hidratacao_sng;
    private Boolean hidratacao_gastrostomia;
    
    @Column(length = 1000)
    private String hidratacao_observacao;
    
    
    private Boolean ventilacao_arAmbiente;
    private Boolean ventilacao_usoDeConcentrador;
    private Boolean ventilacao_usoDe02EmOculosNasal;
    private Boolean ventilacao_usoDe02EmMascaraFacial;
    private Boolean ventilacao_traqueostomia;
    
    @Column(length = 1000)
    private String  ventilacao_observacao;
    
    
    
    @Column(length = 1000)
    private String dsConduta;
    
    
    private String assuser;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    
    
    
    
    @Transient private String mobilidadeFisicaStr;
    @Transient private String alimentacao_opcaoStr;
    @Transient private String hidratacao_opcaoStr;
    @Transient private String ventilacao_opcaoStr;
    
    @Transient private String eliminacaoFisiologicas_incontinenciaUrinariaStr;
    @Transient private String eliminacaoFisiologicas_incontinenciaIntestinalStr;
    @Transient private String nivelConscienciaStr;
    @Transient private String orientacaoStr;
    @Transient private String ulcerasStr;
    @Transient private String dataStr;
    
    public String getEliminacaoFisiologicas_incontinenciaUrinariaStr() {
        return eliminacaoFisiologicas_incontinenciaUrinariaStr = eliminacaoFisiologicas_incontinenciaUrinaria == null ? "" : eliminacaoFisiologicas_incontinenciaUrinaria.toString();
    }
    
    public String getEliminacaoFisiologicas_incontinenciaIntestinalStr() {
        return eliminacaoFisiologicas_incontinenciaIntestinalStr = eliminacaoFisiologicas_incontinenciaIntestinal == null ? "" : eliminacaoFisiologicas_incontinenciaIntestinal.toString();
    }
    
    public String getAlimentacao_opcaoStr() {
        String retorno = "";
        
        if (Verify.equals(alimentacao_voSolido, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "VO Sólido";
        }

        if (Verify.equals(alimentacao_voPastoso, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "VO Pastoso";
        }
        if (Verify.equals(alimentacao_voBrando, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "VO Brando";
        }

        if (Verify.equals(alimentacao_sng, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "SNG";
        }

        if (Verify.equals(alimentacao_gastrostromia, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Gastrostomia";
        }
        
        return alimentacao_opcaoStr = retorno;
    }
    
    public String getHidratacao_opcaoStr() {
        String retorno = "";
        
        if (Verify.equals(hidratacao_voLiquido, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "VO Líquido";
        }

        if (Verify.equals(hidratacao_voLiquidoEspressado, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "VO Líquido Espressado";
        }

        if (Verify.equals(hidratacao_sng, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "SNG";
        }

        if (Verify.equals(hidratacao_gastrostomia, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Gastrostomia";
        }
        
        return hidratacao_opcaoStr = retorno;
    }
    
    public String getVentilacao_opcaoStr() {
        String retorno = "";
        
        if (Verify.equals(ventilacao_arAmbiente, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Ar Ambiente";
        }

        if (Verify.equals(ventilacao_usoDeConcentrador, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Uso de concentrador";
        }

        if (Verify.equals(ventilacao_usoDe02EmOculosNasal, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Uso de 02 em Mascara Nasal";
        }
        
        if (Verify.equals(ventilacao_usoDe02EmMascaraFacial, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Uso de 02 em Mascara Facial";
        }

        if (Verify.equals(ventilacao_traqueostomia, Boolean.TRUE)) {
            if (!retorno.isEmpty()) 
                retorno += ", ";
            retorno += "Traqueostomia";
        }
        
        return ventilacao_opcaoStr = retorno;
    }
    
    public String getMobilidadeFisicaStr() {
        return mobilidadeFisicaStr = mobilidadeFisica == null ? "" : mobilidadeFisica.toString();
    }
    
    public String getNivelConscienciaStr() {
        return nivelConscienciaStr = nivelConsciencia == null ? "" : nivelConsciencia.toString();
    }
    
    
    public String getOrientacaoStr() {
        return orientacaoStr = orientacao == null ? "" : orientacao.toString();
    }
    
    
    public String getUlcerasStr() {
        return ulcerasStr = peleAnexos_ulcerasPorPressao == null ? "" : peleAnexos_ulcerasPorPressao.toString();
    }
    
    
    public String getDataStr() {
        return dataStr = data == null ? "" : Util.formatTimestamp(data);
    }
    
    
    public static enum EnumNivelConsciencia {
        A("Alerta"), L("Letárgicos/sonolento"), E("Estupor e semicoma"), C("Coma");

        private EnumNivelConsciencia(String str) {
            this.str = str;
        }
        
        private String str;  
        
        @Override
        public String toString() {  
            return str;  
        }
    }
    
    
    
    public static enum EnumOrientacao {
        O("Orientado"),
        OP("Orientado em períodos"),
        D("Desorientado");

        private EnumOrientacao(String str) {
            this.str = str;
        }
        
        private String str;  
        
        @Override
        public String toString() {  
            return str;  
        }
    }
    
    
    
    public static enum EnumMobilidadeFisica {
        DEAMBULA_SEM_AUXILIO("Deambula sem auxilio"),
        DEAMBULA_COM_AUXILIO("Deambula com auxilio"),
        CADEIRANTE("Cadeirante"),
        ACAMADO("Acamado");

        private EnumMobilidadeFisica(String str) {
            this.str = str;
        }
        
        private String str;  
        
        @Override
        public String toString() {  
            return str;  
        }
    }
    
    
    
    
    
    public static enum EnumUlcerasPorPressao {
        E1("Estágio I"),
        E2("Estágio II"),
        E3("Estágio III"),
        E4("Estágio IV"),
        N_ESTIGAVEL("Não Estigável"),
        SUSP_LESAO("Suspeita de lesão tissular profunda");
        
        private EnumUlcerasPorPressao(String str) {
            this.str = str;
        }
        
        private String str;  
        
        @Override
        public String toString() {  
            return str;  
        }
    }
    
    
    
    
    
    public static enum EnumNivelIncontinencia {
        LEVE("Leve"),
        MODERADA("Moderada"),
        GRAVE("Grave");
        
        private EnumNivelIncontinencia(String str) {
            this.str = str;
        }
        
        private String str;  
        
        @Override
        public String toString() {  
            return str;  
        }
    }
    
    
    
    
    @Data
    @Accessors(chain = true)
    public static class ParamEdicao {
        private Long id;
        private Long idPaciente;
        private String nome;
        private AnamnesePaciente objRetornoForm;
        private String mensagem;
    }
}
            
            
