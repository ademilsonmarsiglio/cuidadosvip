package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 *
 * @author ademilsonmarsiglio
 */
@Data
@Entity
@Table(name = "diagnosticopaciente")
public class DiagnosticoPaciente implements Serializable {
 
    @Id
    @Generated(GenerationTime.INSERT)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;
    
    
    @Column(nullable = false, length = 1000)
    private String descricao;
    
    
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EnumTipoDiagnostico tipo;
    
    
    @ColumnDefault("true")
    private Boolean ativo = Boolean.TRUE;
    
    
    private int ordem = 1;
    
    /**
     * idParent é o DiagnosticoPaciente.id do nível superior.
    */
    private Integer idParent;
    
    private String assuser;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    
    @Transient
    private List<DiagnosticoPaciente> filhos;
    
   
    
    public void addFilho(DiagnosticoPaciente d) {
        if (filhos == null) {
            filhos = new ArrayList<>();
        }
        
        filhos.add(d);
    }
    
    public void setId(Object id) {
        if (id == null) {
            this.id = null;
        } else {
            
            if (id instanceof Integer)
                this.id = (Integer) id;
            else if (id instanceof BigInteger)
                this.id = ((BigInteger) id).intValue();
            else if (id instanceof Long)
                this.id = ((Long) id).intValue();
            else if (id instanceof String)
                this.id = Integer.parseInt((String) id);
            
        }
    }
    
    public void setTipo(Object tipo) {
        if (tipo == null) {
            this.tipo = null; 
        } else {
            if (tipo instanceof EnumTipoDiagnostico) {
                this.tipo = (EnumTipoDiagnostico) tipo;
            } else {
                this.tipo = EnumTipoDiagnostico.valueOf(tipo.toString());
            } 
        }
    }
}
