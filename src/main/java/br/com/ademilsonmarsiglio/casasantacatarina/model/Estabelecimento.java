package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "estabelecimento")
@SequenceGenerator(allocationSize = 1, name = "SEQ_ESTABELECIMENTO", sequenceName = "SEQ_ESTABELECIMENTO")
@Data
public class Estabelecimento implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ESTABELECIMENTO")
    @GenericGenerator(name = "SEQ_ESTABELECIMENTO", strategy = "increment")
    private Long idEstabelecimento;
    
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dtLicenca;
    
    private String localArquivos;
    
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dtCadastro = new Date();
    
    @Valid
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPessoa")
    private Pessoa pessoa;
}
