package br.com.ademilsonmarsiglio.casasantacatarina.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "controleepi")
@SequenceGenerator(allocationSize = 1, name = "SEQ_CONTROLEEPI", sequenceName = "SEQ_CONTROLEEPI")
@Data
@Accessors(chain = true)
public class ControleEPI implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONTROLEEPI")
    private Long id;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEntrega = new Date();
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtDevolucao;
    
    private String dsEpi;
    
    /**
     * Certificado de Aprovação
     */
    private String ca;

    private Integer qtd;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idLogin", referencedColumnName = "idLogin")
    private Login login;
}