package br.com.ademilsonmarsiglio.casasantacatarina.model.json;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import java.math.BigInteger;
import lombok.Data;

/**
 *
 * @author ademilson
 * @since 07/07/2018
 */
@Data
public class ProdutoJson {
    private Long codigo;
    private String descricao;
    private Double qtd;
    
    public ProdutoJson setCodigo(Object codigo) {
        if (codigo != null)
            if (codigo instanceof Long)
                this.codigo = (Long) codigo;
            else if (codigo instanceof BigInteger)
                this.codigo = ((BigInteger) codigo).longValue();
        else 
            this.codigo = null;
        
        return this;
    }
}
