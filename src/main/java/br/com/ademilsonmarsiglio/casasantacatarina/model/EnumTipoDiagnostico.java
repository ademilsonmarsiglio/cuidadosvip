
package br.com.ademilsonmarsiglio.casasantacatarina.model;

/**
 *
 * @author ademilsonmarsiglio
 */
public enum EnumTipoDiagnostico {
    MASTER, DETAIL;
}
