package br.com.ademilsonmarsiglio.casasantacatarina.model.json;

import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @since 29/06/2017
 * @author ademilson
 */
@Data
@Accessors(chain = true)
public class BasicJson implements Serializable{
    private Long codigo;
    private String descricao;
    private String aux;
    
    public BasicJson setCodigo(Number number) {
        this.codigo = number == null ? null : number.longValue();
        return this;
    }
}
