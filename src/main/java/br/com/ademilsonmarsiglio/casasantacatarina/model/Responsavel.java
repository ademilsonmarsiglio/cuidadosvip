package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "responsavel")
@SequenceGenerator(allocationSize = 1, name = "SEQ_RESPONSAVEL", sequenceName = "SEQ_RESPONSAVEL")
@Data
@EqualsAndHashCode(exclude = {"pessoa", "paciente", "estabelecimento"})
@Accessors(chain = true)
public class Responsavel implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_RESPONSAVEL")
    @GenericGenerator(name = "SEQ_RESPONSAVEL", strategy = "increment")
    private Long idResponsavel;
    
    private Boolean principal = Boolean.FALSE;
    
    @Valid
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPessoa", referencedColumnName = "idPessoa", nullable = false)
    private Pessoa pessoa;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
    private Estabelecimento estabelecimento;
}
