package br.com.ademilsonmarsiglio.casasantacatarina.model.transients;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Classe auxiliar para popular o relatório de qualidade de vida formulário.
 * @author ademilsonmarsiglio
 */
@Data
@Accessors(chain = true)
public class RelQualidadeVida {
    
    public RelQualidadeVida(Integer id, String questao, String r1, String r2, String r3, String r4, String r5) {
        this.id = id;
        this.questao = questao;
        this.r1 = r1;
        this.r2 = r2;
        this.r3 = r3;
        this.r4 = r4;
        this.r5 = r5;
    }
    
    private Integer id;
    private String descricaoBlocos;
    private String questao;
    private String r1;
    private String r2;
    private String r3;
    private String r4;
    private String r5;
}
