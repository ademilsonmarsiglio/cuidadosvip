package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 * @since 02/07/2018
 */
@Entity
@Table(name = "historicoEstoque")
@SequenceGenerator(allocationSize = 1, name = "SEQ_HISTORICOESTOQUE", sequenceName = "SEQ_HISTORICOESTOQUE")
@Data
@EqualsAndHashCode(of = "id")
@Accessors(chain = true)
public class HistoricoEstoque implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HISTORICOESTOQUE")
    private Long id;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data = new Date();
    
    @Column(scale = 2, precision = 8, length = 10)
    private Double qtd;
    
    private String observacao;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false, name = "idEstoque", referencedColumnName = "idEstoque")
    private Estoque estoque;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false, name = "idLogin", referencedColumnName = "idLogin")
    private Login login;
    
    @Transient
    private EnumTipoHistoricoEstoque tipo;
    
    @Transient
    private Double saldo;
    
    @Transient
    private String hora;
    
    @Transient
    private String dataFormatada;
    
    @Transient
    private Long idPaciente;
    
    
    public String getDataFormatada() {
        return dataFormatada = data == null ? "" : Util.formatTimestamp(data);
    }
    
    public String getHora() {
        return hora = (hora != null ? hora : (data == null ? "" : Util.formatTime(data)));
    }
    
    public void setTipo(Object tipo) {
        if (tipo != null) {
            
            if (tipo instanceof EnumTipoHistoricoEstoque) {
                this.tipo = (EnumTipoHistoricoEstoque) tipo;
            } else {
                this.tipo = EnumTipoHistoricoEstoque.valueOf(tipo.toString());
            }
            
        } else {
            this.tipo = null;
        }
    }
    
    public HistoricoEstoque setId(Object id) {
        if (id != null)
            if (id instanceof Long)
                this.id = (Long) id;
            else if (id instanceof BigInteger)
                this.id = ((BigInteger) id).longValue();
        else 
            this.id = null;
        
        return this;
    }
    
    public HistoricoEstoque setIdPaciente(Object id) {
        if (id != null)
            if (id instanceof Long)
                this.idPaciente = (Long) id;
            else if (id instanceof BigInteger)
                this.idPaciente = ((BigInteger) id).longValue();
        else 
            this.idPaciente = null;
        
        return this;
    }
    
    
    public static enum EnumTipoHistoricoEstoque {
        PRONTUARIO, MANUAL;
    }
    
    
    @Data
    public static class Filter {
        private Long idPaciente;
        private Long idEstoque;
        private String produto;
        private String paciente;
        private Long limit;
        private Long offset;
    }
}
