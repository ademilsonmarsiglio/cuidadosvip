package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "prontuario")
@SequenceGenerator(allocationSize = 1, name = "SEQ_PRONTUARIO", sequenceName = "SEQ_PRONTUARIO")
@Data
@EqualsAndHashCode(exclude = {"paciente", "login"})
@Accessors(chain = true)
@DynamicUpdate
public class Prontuario implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRONTUARIO")
    @GenericGenerator(name = "SEQ_PRONTUARIO", strategy = "increment")
    private Long idProntuario;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtProntuario;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date dtCriacao;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAlteracao;
    
    /**
     * M - Manha
     * T - Tarde
     * N - Noite
     */
    @Column(length = 1)
    private String turno;
    
    @Column(length = 10000)
    private String descricao;
    
    @NotBlank(message = "O responsável não pode estar vazio")
    @Column(length = 500)
    private String responsavel;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    private String assuser;
            
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idLogin", referencedColumnName = "idLogin", nullable = false)
    private Login login;
    
    @Transient
    private SinalVital sinalVital;
    
    @ManyToMany(targetEntity = ItemProntuario.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(joinColumns = @JoinColumn(name = "prontuario_idProntuario"), inverseJoinColumns = @JoinColumn(name = "item_idItemProntuario"))
    private List<ItemProntuario> itens;
    
    @Transient
    private List<SaeProntuario> saes;
    
    @Transient
    private String turnoStr;
    
    @Transient
    private String descricaoLimit;
    
    @Transient
    private Boolean podeAlterar;
    
    @Transient
    private String jsonItens;
    
    public String getDescricaoLimit(){
        
        if(getDescricao() != null && getDescricao().length() > 200){
            String str = getDescricao().substring(0, 199).concat("...");
            
            return quebraString(str);
        }
        
        return getDescricao();
    }
    
    private String quebraString(String str){
        
        String retorno = "";
        int length = str.length();
        int quebra = 0;
        
        for (int i = 0; i < length; i++) {
            retorno += str.charAt(i);
            
            if(quebra++ == 60){
                quebra = 0;
//                retorno += "\n";
            }
        }
        
        return retorno;
    }
    
    public String getTurnoStr(){
        if(getTurno() == null || getTurno().trim().isEmpty()){
            return "";
        }
        
        switch (getTurno()) {
            case "M":
                return "Manha";
            case "T":
                return "Tarde";
            case "N":
                return "Noite";
            default:
                return " ";
        }
    }

    public String getJsonItens() {
        if (!(itens instanceof HibernateProxy) && !Verify.nullsOrEmpty(itens)) {
            jsonItens = "[";
            for (int i = 0; i < itens.size(); i++) {
                ItemProntuario iten = itens.get(i);
                if (iten != null && iten.getEstoque() != null && iten.getEstoque().getProduto() != null && iten.getEstoque().getProduto().getDsProduto() != null){
                    jsonItens += "{\"medicamento\": \"" + iten.getEstoque().getProduto().getDsProduto() + "\", \"qtd\": " + iten.getQtd() + "}";
                }

                if(i != itens.size() -1){
                    jsonItens += ",";
                }
            }
            jsonItens += "]";
            return jsonItens;
        }
        
        return null;
    }
    
    /**
     * Nao deixa alterar depois de 5 minutos.
     * @return true se pode alterar
     */
    public Boolean getPodeAlterar(){
        if (podeAlterar == null) {
            if (getDtCriacao() == null) {
                return false;
            } else {
                Calendar cAtual = Calendar.getInstance();

                Calendar cCriacao = Calendar.getInstance();
                cCriacao.setTime(getDtCriacao());
                cCriacao.add(Calendar.MINUTE, 5);

                return podeAlterar = !cAtual.getTime().after(cCriacao.getTime());
            }
        } else {
            return podeAlterar;
        }
        
    }

    public void setPodeAlterar(Boolean podeAlterar) {
        this.podeAlterar = podeAlterar;
    }
    
    public Prontuario setIdProntuario(Object id) {
        if (id != null)
            if (id instanceof Long)
                this.idProntuario = (Long) id;
            else if (id instanceof BigInteger)
                this.idProntuario = ((BigInteger) id).longValue();
            else if (id instanceof Integer)
                this.idProntuario = ((Integer) id).longValue();
            else if (id instanceof String)
                this.idProntuario = Long.parseLong((String) id);
        else 
            this.idProntuario = null;
        
        return this;
    }
}
