package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "paciente")
@SequenceGenerator(allocationSize = 1, name = "SEQ_PACIENTE", sequenceName = "SEQ_PACIENTE")
@Data
@EqualsAndHashCode(exclude = {"pessoa", "estabelecimento"})
@Accessors(chain = true)
public class Paciente implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PACIENTE")
    @GenericGenerator(name = "SEQ_PACIENTE", strategy = "increment")
    private Long idPaciente;
    
    /**
     * A - Ativo
     * I - Inativo.
     */
    @Column(nullable = false)
    @ColumnDefault("'A'")
    private Character stPaciente;
    
    /**
     * C - Casado
     * D - Divorciado
     * S - Solteiro
     * V - Viuvo
     */
    private Character estadoCivil;
    
    @Column(length = 50)
    private String escolaridade;
    
    @Column(length = 3)
    private Integer nrFilhos;
    
    @Column(length = 500)
    private String motivoInternacao;
    
    
    @Column(length = 500)
    private String habitosVicios;
    
    /**
     * Preferencias culturais e do lazer.
     */
    @Column(length = 500)
    private String preferencias;
    
    @Column(length = 10)
    private String tipoSanguineo;
    
    @Column(length = 10)
    private String fatorRH = "+";
    
    
    /**
     * SUS;
     * Particular;
     * Etc.
     */
    @Column(length = 50)
    private String convenio;
    
    @Column(length = 500)
    private String dadosSus;
    
    @Column(length = 500)
    private String dadosPlanoSaude;
    
    @Column(length = 50)
    private String medicoAcompanhamento;
    
    @Column(length = 500)
    private String hda;
    
    @Column(length = 500)
    private String hdp;
    
    @Column(length = 500)
    private String observacoes;
    
    @Column(length = 4000)
    private String historico;
    
    @Column(length = 50)
    private String hospitalPreferencia;
    
    /**
     * 1 - Independete
     * 2 - 2º Grau
     * 3 - 3º Grau
     */
    private Character grauDependencia = '1';
    
    /**
     * Auxilio Medico - Alimentacao
     */
    @ColumnDefault(value = "false")
    private Boolean auxilioMedicoA;
    
    /**
     * Auxilio Medico - Mobilidade
     */
    @ColumnDefault(value = "false")
    private Boolean auxilioMedicoM;
    
    /**
     * Auxilio Medico - Higiene
     */
    @ColumnDefault(value = "false")
    private Boolean auxilioMedicoH;
    
    /**
     * Auxilio Medico - Vestir-se
     */
    @ColumnDefault(value = "false")
    private Boolean auxilioMedicoV;
    
    /**
     * Auxilio Medico - Outros
     */
    @ColumnDefault(value = "false")
    private Boolean auxilioMedicoO;
    
    @Column(length = 50)
    private String dsAuxilioMedicoOutro;
    
    /**
     * Condiçao de autonomia.
     * C - Com comprometimento cognitivo;
     * S - Sem comprometimento cognitivo;
     */
    private Character stComprometimentoCognitivo = 'C';
    
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dtInclusaoPaciente;
    
    private String assuser;
    
    @Valid
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPessoa", referencedColumnName = "idPessoa")
    private Pessoa pessoa = new Pessoa();
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
    private Estabelecimento estabelecimento;
    
    @Transient
    private List<Medicamento> listaMedicamento;
    
    @Transient
    private String medicamentosEmUso;
    
//    @Valid
    @Transient
    private Responsavel responsavelPrincipal;
    
    @Transient
    private String stPacienteAux;
    
    @Transient
    private Responsavel responsavel;
    
    public void addMedicamento(String dsMedicamento){
        if(medicamentosEmUso == null){
            medicamentosEmUso = dsMedicamento;
        } else {
            medicamentosEmUso += ", " + dsMedicamento;
        }
    }
    
    public String getStPacienteAux() {
        if (stPaciente != null && stPaciente.equals('A')) {
            return stPacienteAux = "Ativo";
        } else {
            return stPacienteAux = "Inativo";
        }
    }
    
    public Paciente setIdPaciente(Object id) {
        if (id != null)
            if (id instanceof Long)
                this.idPaciente = (Long) id;
            else if (id instanceof BigInteger)
                this.idPaciente = ((BigInteger) id).longValue();
            else if (id instanceof Integer)
                this.idPaciente = ((Integer) id).longValue();
            else if (id instanceof String)
                this.idPaciente = Long.parseLong((String) id);
        else 
            this.idPaciente = null;
        
        return this;
    }
}
