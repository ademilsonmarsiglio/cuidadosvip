package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "arquivo")
@SequenceGenerator(allocationSize = 1, name = "SEQ_ARQUIVO", sequenceName = "SEQ_ARQUIVO")
@Data
@EqualsAndHashCode(exclude = {"paciente", "login"})
@ToString(exclude = {"login", "paciente"})
@Accessors(chain = true)
public class Arquivo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ARQUIVO")
    @GenericGenerator(name = "SEQ_ARQUIVO", strategy = "increment")
    private Long idArquivo;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtModificacao;
    
    private String nmArquivo;
    
    private String localArquivo;
    
    private Long tamanho;
    
    @Transient
    private String tamanhoStr;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idLogin", referencedColumnName = "idLogin", nullable = false)
    private Login login;
    
    public String getTamanhoStr(){
        if(getTamanho() != null){
            if(tamanho > 1024){
                BigDecimal size = BigDecimal.valueOf(tamanho);
                BigDecimal kb = size.divide(BigDecimal.valueOf(1024D), 2, BigDecimal.ROUND_HALF_DOWN);
                
                if(kb.compareTo(BigDecimal.valueOf(1024D)) > 0){
                    BigDecimal mb = kb.divide(BigDecimal.valueOf(1024D), 2, BigDecimal.ROUND_HALF_DOWN);
                    return mb + " mb";
                } else {
                    return kb + " kb";
                }
            } else {
                return tamanho + " b";
            }
        }
        
        return "";
    }
}
