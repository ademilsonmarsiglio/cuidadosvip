package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

/**
 * WHOQOF - OLD (WHO | The World Health Organization Quality of Life)
 * 
 * Qualidade de Vida do Idoso
 * 
 * Calculos baseados neste site:
 * http://www.cefid.udesc.br/arquivos/id_submenu/1173/whoqol_old.pdf
 *
 * @author ademilsonmarsiglio
 */
@Data
@Entity
@Table(name = "qualidade_vida")
@Accessors(chain = true)
public class QualidadeVida implements Serializable{

    @Id
    @Generated(GenerationTime.INSERT)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;
    
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    /**
     * Q.1 Até que ponto as perdas nos seus sentidos (por exemplo, audição,
     * visão, paladar, olfato, tato), afetam a sua vida diária?
     * 
     * ivt = resultado da questão invertido.
     */
    @NotNull
    private Integer q1_ivt;
    /**
     * Q.2 Até que ponto a perda de, por exemplo, audição, visão, paladar,
     * olfato, tato, afeta a sua capacidade de participar em atividades?
     */
    @NotNull
    private Integer q2_ivt;
    /**
     * Q.3 Quanta liberdade você tem de tomar as suas próprias decisões?
     */
    @NotNull
    private Integer q3;
    /**
     * Q.4 Até que ponto você sente que controla o seu futuro?
     */
    @NotNull
    private Integer q4;
    /**
     * Q.5 O quanto você sente que as pessoas ao seu redor respeitam a sua
     * liberdade?
     */
    @NotNull
    private Integer q5;
    /**
     * Q.6 Quão preocupado você está com a maneira pela qual irá morrer?
     */
    @NotNull
    private Integer q6_ivt;
    /**
     * Q.7 O quanto você tem medo de não poder controlar a sua morte?
     */
    @NotNull
    private Integer q7_ivt;
    /**
     * Q.8 O quanto você tem medo de morrer?
     */
    @NotNull
    private Integer q8_ivt;
    /**
     * Q.9 O quanto você teme sofrer dor antes de morrer?
     */
    @NotNull
    private Integer q9_ivt;
    
    

    /* *************
        As seguintes questões perguntam sobre quão completamente você fez ou se sentiu apto a
        fazer algumas coisas nas duas últimas semanas. 
     */
    
    /**
     * Q.10 Até que ponto o funcionamento dos seus sentidos (por exemplo,
     * audição, visão, paladar, olfato, tato) afeta a sua capacidade de
     * interagir com outras pessoas?
     */@NotNull
    private Integer q10_ivt;

    /**
     * Q.11 Até que ponto você consegue fazer as coisas que gostaria de fazer?
     */@NotNull
    private Integer q11;

    /**
     * Q.12 Até que ponto você está satisfeito com as suas oportunidades para
     * continuar alcançando outras realizações na sua vida?
     */@NotNull
    private Integer q12;

    /**
     * Q.13 O quanto você sente que recebeu o reconhecimento que merece na sua
     * vida?
     */@NotNull
    private Integer q13;

    /**
     * Q.14 Até que ponto você sente que tem o suficiente para fazer em cada
     * dia?
     */@NotNull
    private Integer q14;
    

    /* ********************
        As seguintes questões pedem a você que diga o quanto você se sentiu satisfeito, feliz ou bem
        sobre vários aspectos de sua vida nas duas últimas semanas.
    
     */
    /**
     * Q.15 Quão satisfeito você está com aquilo que alcançou na sua vida?
     */@NotNull
    private Integer q15;

    /**
     * Q.16 Quão satisfeito você está com a maneira com a qual você usa o seu
     * tempo?
     */@NotNull
    private Integer q16;

    /**
     * Q.17 Quão satisfeito você está com o seu nível de atividade?
     */@NotNull
    private Integer q17;

    /**
     * Q.18 Quão satisfeito você está com as oportunidades que você tem para
     * participar de atividades da comunidade?
     */@NotNull
    private Integer q18;

    /**
     * Q.19 Quão feliz você está com as coisas que você pode esperar daqui para
     * frente?
     */@NotNull
    private Integer q19;

    /* ***********************************
     *  As seguintes questões se referem a qualquer relacionamento íntimo que você possa ter. Por
        favor, considere estas questões em relação a um companheiro ou uma pessoa próxima com a
        qual você pode compartilhar (dividir) sua intimidade mais do que com qualquer outra pessoa
        em sua vida
     */
    
    
    /**
     * Q.20 Como você avaliaria o funcionamento dos seus sentidos (por exemplo,
     * audição, visão, paladar, olfato, tato)?
     */@NotNull
    private Integer q20;

    /**
     * Q.21 Até que ponto você tem um sentimento de companheirismo em sua vida?
     */@NotNull
    private Integer q21;

    /**
     * Q.22 Até que ponto você sente amor em sua vida?
     */@NotNull
    private Integer q22;

    /**
     * Q.23 Até que ponto você tem oportunidades para amar?
     */@NotNull
    private Integer q23;
    /**
     * Q.24 Até que ponto você tem oportunidades para ser amado?
     */@NotNull
    private Integer q24;
    
    private String assuser;
    
    /* ************************
            RESULTADOS
    */
    
    
    private Integer ttFuncionamentoSensorio;
    private Double medFuncionamentoSensorio;
    private Double peFuncionamentoSensorio;
    
    
    private Integer ttAutonomia;
    private Double medAutonomia;
    private Double peAutonomia;
    
    
    private Integer ttAtividadesPPF;
    private Double medAtividadesPPF;
    private Double peAtividadesPPF;
    
    
    private Integer ttParticipacaoSocial;
    private Double medParticipacaoSocial;
    private Double peParticipacaoSocial;
    
    
    private Integer ttMorteMorrer;
    private Double medMorteMorrer;
    private Double peMorteMorrer;
    
    
    private Integer ttIntimidade;
    private Double medIntimidade;
    private Double peIntimidade;
    
    
    private Integer ttGeral;
    private Double medGeral;
    private Double peGeral;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    
    
    @Transient private String dataStr;
    @Transient private String qualitySensoriais;
    @Transient private String qualityAutonomia;
    @Transient private String qualityAtividadesPPF;
    @Transient private String qualityPartSocial;
    @Transient private String qualityMorteMorrer;
    @Transient private String qualityIntimidade;
    @Transient private String qualityGeral;
    
    
    
    
    
    public String getDataStr() {
        return dataStr = data == null ? "" : Util.formatTimestamp(data);
    }
    
    private String retornaDescricao(Double media) {
        return media == null || media < 2.9 ? "Precisa Melhorar" : media < 3.9 ? "Regular" : media < 4.9 ? "Boa" :"Excelente";
    }
    
    
    public String getQualitySensoriais() {
        return qualitySensoriais = retornaDescricao(medFuncionamentoSensorio);
    }
    
    
    public String getQualityAutonomia() {
        return qualityAutonomia = retornaDescricao(medAutonomia);
    }
    
    
    public String getQualityAtividadesPPF() {
        return qualityAtividadesPPF = retornaDescricao(medAtividadesPPF);
    }
    
    
    public String getQualityPartSocial() {
        return qualityPartSocial = retornaDescricao(medParticipacaoSocial);
    }
    
    
    public String getQualityMorteMorrer() {
        return qualityMorteMorrer = retornaDescricao(medMorteMorrer);
    }
    
    public String getQualityIntimidade() {
        return qualityIntimidade = retornaDescricao(medIntimidade);
    }
    
    public String getQualityGeral() {
        return qualityGeral = retornaDescricao(medGeral);
    }
    
    
   
    
    
    public void calculaResultadoTotal() {
        ttFuncionamentoSensorio = q1_ivt + q2_ivt + q10_ivt + q20;
        ttAutonomia = q3 + q4 + q5 + q11;
        ttAtividadesPPF = q12 + q13 + q15 + q19;
        ttParticipacaoSocial = q14 + q16 + q17 + q18;
        ttMorteMorrer = q6_ivt + q7_ivt + q8_ivt + q9_ivt;
        ttIntimidade = q21 + q22 + q23 + q24;
        ttGeral = ttFuncionamentoSensorio + ttAutonomia + ttAtividadesPPF + ttParticipacaoSocial + ttMorteMorrer + ttIntimidade;
    }
    
    public void calculaMedia() {
        medFuncionamentoSensorio = Util.round(ttFuncionamentoSensorio / 4D, 2);
        medAutonomia = Util.round(ttAutonomia / 4D, 2);
        medAtividadesPPF = Util.round(ttAtividadesPPF / 4D, 2);
        medParticipacaoSocial = Util.round(ttParticipacaoSocial / 4D, 2);
        medMorteMorrer = Util.round(ttMorteMorrer / 4D, 2);
        medIntimidade = Util.round(ttIntimidade / 4D, 2);
        medGeral = Util.round(ttGeral / 24D, 2);
    }
    
    public void calculaPercentual() {
        peFuncionamentoSensorio = Util.round( ((medFuncionamentoSensorio - 1) / 4) * 100, 2);
        peAutonomia = Util.round( ((medAutonomia - 1) / 4) * 100, 2);
        peAtividadesPPF = Util.round(((medAtividadesPPF - 1) / 4) * 100, 2);
        peParticipacaoSocial = Util.round(((medParticipacaoSocial - 1) / 4) * 100, 2);
        peMorteMorrer = Util.round(((medMorteMorrer - 1) / 4) * 100, 2);
        peIntimidade = Util.round(((medIntimidade - 1) / 4) * 100, 2);
        peGeral = Util.round(((medGeral - 1) / 4) * 100, 2);        
    }
    
    
    @Data
    @Accessors(chain = true)
    public static class ParamEdicao {
        private Long id;
        private Long idPaciente;
        private String nmPaciente;
        private QualidadeVida objRetornoForm;
        private String mensagem;
        private Integer offset;
        private Integer limit;
    }
}
