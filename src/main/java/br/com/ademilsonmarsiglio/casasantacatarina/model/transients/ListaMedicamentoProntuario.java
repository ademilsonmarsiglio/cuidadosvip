package br.com.ademilsonmarsiglio.casasantacatarina.model.transients;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author ademilson
 * @since 21/07/2018
 */
@Data
public class ListaMedicamentoProntuario implements Serializable{
    private List<Medicamento> listManha = new ArrayList();
    private List<Medicamento> listTarde = new ArrayList();
    private List<Medicamento> listNoite = new ArrayList();
    private List<Medicamento> listSeNecessario = new ArrayList();
    private List<Medicamento> listSemAplicacao = new ArrayList();
    private List<Medicamento> listGeral = new ArrayList();
}
