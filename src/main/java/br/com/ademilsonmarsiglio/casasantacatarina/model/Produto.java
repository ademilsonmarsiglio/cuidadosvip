package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "produto")
@SequenceGenerator(allocationSize = 1, name = "SEQ_PRODUTO", sequenceName = "SEQ_PRODUTO")
@Data
@EqualsAndHashCode(exclude = {"estabelecimento"})
@Accessors(chain = true)
@DynamicUpdate
@SelectBeforeUpdate
public class Produto implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PRODUTO")
    private Long idProduto;
    
    @NotEmpty(message = "O nome do produto deve ser informado.")
    @Column(length = 100, nullable = false)
    private String dsProduto;
    
    @Column(scale = 2, precision = 8, length = 10)
    @ColumnDefault("0.0")
    private Double preco;
    
    @Column(scale = 2, precision = 8, length = 10)
    @ColumnDefault("0.0")
    private Double custo;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
    private Estabelecimento estabelecimento;
    
    
    @Transient
    @Column(scale = 2, precision = 8, length = 10)
    private Double qtdEstoqueInicial;
    
    @Transient
    private Long idEstoqueGeral;
    
    @Transient
    @Column(scale = 2, precision = 8, length = 10)
    private Double qtdGeral = 0.0;
    
    @Transient
    @Column(scale = 2, precision = 8, length = 10)
    private Double qtdPacientes = 0.0;
    
    private String assuser;
    
    @ColumnDefault("true")
    private boolean status = true;
    
    /**
     * qtd estoque
     */
    @Transient
    private Double qtd;
    
    @Transient
    private String tpEstoque;
    
    public void setTpEstoque(Object o) {
        if (o != null) {
            this.tpEstoque = String.valueOf(o);
        } else {
            this.tpEstoque = null;
        }
    }
    
    public String getTpEstoque() {
        if (tpEstoque != null) {
            if (Verify.equals(tpEstoque, "G"))
                return tpEstoque = "Materiais";
            else 
                return tpEstoque = "Medicamentos";
        }
        
        return tpEstoque = "";
    }
    
    
    @Data
    public static class FilterRelatorio {
        
        private String produto;
        private EnumTipoEstoque tipoEstoque;
        private EnumSituacaoEstoque situacaoEstoque;
        private Integer offset;
        private Integer limit;
        
        public static enum EnumTipoEstoque {
            TODOS, MEDICAMENTOS, MATERIAIS;
        }
        public static enum EnumSituacaoEstoque {
            TODOS, POSITIVOS, NEGATIVOS;
        }
    }
}
