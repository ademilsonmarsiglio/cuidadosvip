package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.connector.StrArrayUserType;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "medicamento")
@SequenceGenerator(allocationSize = 1, name = "SEQ_MEDICAMENTO", sequenceName = "SEQ_MEDICAMENTO")
@Data
@EqualsAndHashCode(exclude = "paciente")
@ToString(of = "idMedicamento")
@Accessors(chain = true)
@TypeDef(name = "str-array", typeClass = StrArrayUserType.class)
public class Medicamento implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MEDICAMENTO")
    private Long idMedicamento;
    
    @Column(length = 500)
    private String observacao;
    
    /**
     * Situação de medicamento, que não deve ser administrado todo dia. Apenas quando possúi dor, ou insônia, 
     */
    @ColumnDefault("false")
    private Boolean seNecessario = Boolean.FALSE;
    
    /**
     * Dias em que deve ser aplicado este medicamento.
     * 
     * SEG - Segunda
     * TER - Terça
     * QUA - Quarta
     * QUI - Quinta
     * SEX - Sexta
     * SAB - Sabado
     * DOM - Domingo
     */
    @Type( type = "str-array" )
    @Column(columnDefinition = "varchar[]")
    private String[] dias;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idEstoque", referencedColumnName = "idEstoque", nullable = false)
    private Estoque estoque;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    @Transient
    private List<MedicamentoAplicacao> aplicacoes = new ArrayList();
    
    @Transient
    private List<MedicamentoAplicacao> aplicacoesManha = new ArrayList();
    @Transient
    private List<MedicamentoAplicacao> aplicacoesTarde = new ArrayList();
    @Transient
    private List<MedicamentoAplicacao> aplicacoesNoite = new ArrayList();
    @Transient
    private List<MedicamentoAplicacao> aplicacoesSemInfo = new ArrayList();
    
    @Transient
    private String str_dias = "";


    //CAMPOS DA APLICACAO
        @Transient
        private Double aplicacao;

        @Transient
        private String hora;

        @Transient
        private Long idAplicacao;

    @Transient
    private String uniqueID;

    
    public void setIdMedicamento(Object id) {
        this.idMedicamento = Util.convertObjToLong(id);
    }
    
    public void setIdAplicacao(Object id) {
        this.idAplicacao = Util.convertObjToLong(id);
    }
    
    public void addAplicacao(MedicamentoAplicacao medicamentoAplicacao) {
        if (aplicacoes == null){
            aplicacoes = new ArrayList();
        }
        
        aplicacoes.add(medicamentoAplicacao);
        
        
        String _hora = medicamentoAplicacao.getHora();
        
        if (!Verify.nullsOrEmpty(_hora)) {
            int minutos = Util.timeToMinute(_hora);

            //MANHA = Depois das 05:59 e antes da 13:00
            if (minutos > 359 && minutos < 780) {
                aplicacoesManha.add(medicamentoAplicacao);
            } else if (minutos > 780 && minutos < 1140) { //TARDE = Depois da 13:00 e antes da 19:00
                aplicacoesTarde.add(medicamentoAplicacao);
            } else {
                aplicacoesNoite.add(medicamentoAplicacao);
            }            
        } else {
            aplicacoesSemInfo.add(medicamentoAplicacao);
        }
    }
    
    public String getStr_dias() {
        if (Verify.nullsOrEmpty(str_dias)) {
            if (!Verify.nullsOrEmpty(dias)) {
                for (int i = 0; i < dias.length; i++) {
                    String dia = dias[i];

                    str_dias += dia;

                    if (i != dias.length - 1)
                        str_dias += ", ";
                }
            }
        }
        
        return str_dias == null ? "" : str_dias.replace("{", "").replace("}", "");
    }
    
    public String getUniqueID() {
        uniqueID = "";
        
        if (getEstoque() != null) {
            uniqueID += "_";
            uniqueID += getEstoque().getIdEstoque();
        }

        if (idMedicamento != null) {
            uniqueID += "_";
            uniqueID += idMedicamento;
        }

        if (idAplicacao != null) {
            uniqueID += "_";
            uniqueID += idAplicacao;
        }
        
        return uniqueID;
    }
    
    @Data
    public static class Filter {
        private Long idPaciente;
        private String paciente;
        private Long limit;
        private Long offset;
    }
}
