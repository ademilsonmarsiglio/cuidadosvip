package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @since 13/07/2017
 * @author ademilson
 */
@Entity
@Table(name = "itemprontuario")
@ToString(of = "idItemProntuario", callSuper = false)
@SequenceGenerator(allocationSize = 1, name = "SEQ_ITEMPRONTUARIO", sequenceName = "SEQ_ITEMPRONTUARIO")
@Data
//@EqualsAndHashCode(exclude = {"paciente", "login"})
@Accessors(chain = true)
@DynamicUpdate
public class ItemProntuario implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ITEMPRONTUARIO")
    private Long idItemProntuario;
    
    /**
     * E - Entrada;
     * S - Saída;
     */
    private Character tpOperacao = 'S';
    
    private Double valor;
    
    private Double qtd;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtMovimentacao;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "idEstoque", referencedColumnName = "idEstoque")
    private Estoque estoque;
}
