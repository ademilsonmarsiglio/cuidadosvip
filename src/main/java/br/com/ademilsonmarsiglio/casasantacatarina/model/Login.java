package br.com.ademilsonmarsiglio.casasantacatarina.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OrderBy;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "login")
@SequenceGenerator(allocationSize = 1, name = "SEQ_LOGIN", sequenceName = "SEQ_LOGIN")
@Data()
@EqualsAndHashCode(exclude = {"estabelecimento", "pessoa"})
@ToString(exclude = {"estabelecimento", "pessoa"})
public class Login implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LOGIN")
    @GenericGenerator(name = "SEQ_LOGIN", strategy = "increment")
    private Long idLogin;

    @OrderBy(clause = "login asc")
    @Column(unique = true, nullable = false, length = 200)
    @NotBlank(message = "O Login é Obrigatório.")
    private String login;
    
    @Column(nullable = false, length = 200)
    @NotBlank(message = "A Senha é Obrigatória.")
    private String senha;
    
    @Column(updatable = false)
    @ColumnDefault(value = "false")
    private Boolean superUser = false;
    
    @ColumnDefault(value = "false")
    private Boolean admin = false;
    
    /**
     * E - Enfermeiro
     * M - Medico
     * T - Técnico Enfermeiro
     * O - Outros
     */
    private String tpLogin = "O";
    
    private String identificador;
    
    @ColumnDefault(value = "0")
    private Integer qtdAcessos;
    
    @Valid
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idPessoa", referencedColumnName = "idPessoa")
    private Pessoa pessoa;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
    private Estabelecimento estabelecimento;
    
    @Transient
    private String tpLoginStr;
    
    @Transient
    private String tpIdentificadorStr;
    
    public String getTpIdentificadorStr(){
        if(getTpLogin() == null){
            return "";
        } else {
            switch (getTpLogin()) {
                case "M":
                    return "CRM";
                case "E":
                    return "COREN";
                case "T":
                    return "COREN";
                case "C":
                    return "Identificador";
                case "A":
                    return "CRESS";
                case "F":
                    return "CREFITO";
                case "N":
                    return "CRN";
                case "O":
                    return "Identificador";
                default:
                    return "Identificador";
            }
        }
    }
    
    public String getTpLoginStr(){
        if(getTpLogin() == null){
            return "";
        } else {
            switch (getTpLogin()) {
                case "M":
                    return "Médico";
                case "E":
                    return "Enfermeiro";
                case "T":
                    return "Téc. Enfermagem";
                case "C":
                    return "Cuidador";
                case "A":
                    return "Assistente Social";
                case "F":
                    return "Fisioterapeuta";
                case "N":
                    return "Nutricionista";
                case "O":
                    return "Outros";
                default:
                    return "";
            }
        }
    }
}
