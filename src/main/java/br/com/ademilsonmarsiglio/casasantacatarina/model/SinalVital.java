package br.com.ademilsonmarsiglio.casasantacatarina.model;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @since 01/08/2017
 * @author ademilson
 */
@Entity
@Table(name = "sinalvital")
@SequenceGenerator(allocationSize = 1, name = "SEQ_SINALVITAL", sequenceName = "SEQ_SINALVITAL")
@Data
@EqualsAndHashCode(of = "idSinalVital")
@Accessors(chain = true)
public class SinalVital implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SINALVITAL")
    private Long idSinalVital;
    
    @NotNull(message = "A data não pode ser vazia")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date data = new Date();
    
    private String pressaoArterial;
    
    private String temperatura;
    
    private String frequenciaCardiaca;
    
    private String frequenciaRespiratoria;
    
    private String pupilas;
    
    private String dor;
    
    private String saturacao;
    
    private String liquidoAdministrado_viaOral;
    
    // na verdade é via enteral, mas pra nao fazer o update.. deixei assim e so mudei o label
    private String liquidoAdministrado_viaParenteral;
    
    private String liquidoEliminado_urinaVolume;
    
    private String liquidoEliminado_fezes;
    
    private String liquidoEliminado_vomito;
    
    private String hgt;
    
    @Column(length = 1000)
    private String observacao;
    
    private String assuser;
    
    @Transient
    private String dataFormatada;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPaciente", referencedColumnName = "idPaciente", nullable = false)
    private Paciente paciente;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idLogin", referencedColumnName = "idLogin", nullable = false)
    private Login login;
    
    public String getDataFormatada() {
        if (data == null) {
            return null;
        }
        
        return Util.formatTimestamp(data);
    }
}
