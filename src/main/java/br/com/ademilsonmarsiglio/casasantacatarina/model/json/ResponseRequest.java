/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ademilsonmarsiglio.casasantacatarina.model.json;

import lombok.Data;

/**
 *
 * @author ademilsonmarsiglio
 */
@Data
public class ResponseRequest<T> {
    private String mensagem;
    private boolean ok;
    private T obj;

    public ResponseRequest(String mensagem, boolean ok, T obj) {
        this.mensagem = mensagem;
        this.ok = ok;
        this.obj = obj;
    }
    
    public static ResponseRequest ok(String msg) {
        return new ResponseRequest(msg, true, null);
    }
    
    public static ResponseRequest ok(String msg, Object obj) {
        return new ResponseRequest(msg, true, obj);
    }
    
    public static ResponseRequest noOk(String msg) {
        return new ResponseRequest(msg, false, null);
    }
}
