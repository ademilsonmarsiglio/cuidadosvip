package br.com.ademilsonmarsiglio.casasantacatarina.model.transients;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.Null;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ademilson
 */
public class Relatorios implements Serializable {
    @Data
    public static class ProdutoPorPaciente implements Serializable{
        private String dsProduto;
        private String tpEstoque;
        private String tpEstoqueStr;
        private Double qtd;
        
        public void setTpEstoque(Object ob) {
            tpEstoque = ob == null ? null : ob.toString();
        }
        
        public String getTpEstoqueStr(){
            if (tpEstoque != null) {
                if (tpEstoque.equals("P")){
                    return "Paciente";
                } else if (tpEstoque.equals("G")){
                    return "Geral";
                } else {
                    return "Outros";
                }
            } else {
                return null;
            }
        }
        
        @Data
        public static class Filter implements Serializable{ 
            private String descricao;
            private String tipoEstoque;
            private Long id;
            @DateTimeFormat(pattern = "dd/MM/yyyy")
            @Null
            private Date date1;
            @DateTimeFormat(pattern = "dd/MM/yyyy")
            @Null
            private Date date2;
        }
    }
}
