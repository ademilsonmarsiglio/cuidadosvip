package br.com.ademilsonmarsiglio.casasantacatarina.config;

import br.com.ademilsonmarsiglio.casasantacatarina.controller.Key;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.jasperreports.JasperReportsUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author ademilson
 */
public class InterceptorLogin extends HandlerInterceptorAdapter{

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String uri = request.getRequestURI();
        
        //ja tem sessao, mas tentou ir ao login
        if (session.getAttribute(Key.LOGIN_SESSION) != null 
                && "GET".equals(request.getMethod()) 
                && uri.endsWith("/login")) {
            response.sendRedirect(request.getContextPath() + "/index");
            return false;
        }

        if(session.getAttribute(Key.LOGIN_SESSION) == null && (!uri.endsWith("/login") && !uri.endsWith("/login/validar"))){
            response.sendRedirect(request.getContextPath() + "/login");
            return false;
        }
        
        return true;
    }
}
