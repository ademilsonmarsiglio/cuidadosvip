package br.com.ademilsonmarsiglio.casasantacatarina.connector;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Id;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.dialect.Dialect;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.SQLGrammarException;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;

/**
 *
 * @author Ademilson
 */
public class GenericDAO<T> {

    private String mensagem;

    private Object[] defaultCriterions = null;
    private Object[] criterions = null;
    private ProjectionParc[] projections = null;
    private Object[] join;
    private OrderBy[] orderByClause;
    private OrderBy[] defaultOrderBy;
    private ResultTransformer resultTransformer;

    //** Maximo de linhas retornadas na busca de uma Lista
    private int maxFetchSize = -1;
    private boolean execCommit = true;
    private boolean usePaginator = false;
    int qtdPaginator = 100;

    private Session session;

    private HashMap<String, Integer> hmAlias = new HashMap(); //conterá o alias e a quantidade de vezes que repetiu.

    /** Creates a new instance of DAO */
    public GenericDAO() {}

    /**
     * Inclui o objeto recebido por parametro no banco de dados.
     * @param obj
     * @return
     */
    public boolean incluir(List obj) {
            Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession();
        }

        try {
            for (Object item : obj)
                sessao.save(item);

            if (execCommit) {
                sessao.getTransaction().begin();// inicia uma transacao
                sessao.getTransaction().commit();
            }
            return true;
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }

    /**
     * Alterar o objeto recebido por parametro no banco de dados.
     * @param obj
     * @return
     */
    public boolean alterar(List obj) {
        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession();
        }

        try {
            for (Object item : obj)
                sessao.update(item);

            if (execCommit) {
                sessao.getTransaction().begin();
                sessao.getTransaction().commit();
            }
            return true;
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }


    public boolean incluirOuAlterar(ArrayList obj){
        
        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession();
        }

        try {
            for (Object item : obj) {
                sessao.saveOrUpdate(item);
            }

            if (execCommit) {
                sessao.beginTransaction();
                sessao.getTransaction().commit();
            }
            return true;
        } catch (Exception e) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            sessao.getTransaction().rollback();
            return false;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }
    
    
    /**
     * Exclui o objeto recebido por parametro do banco de dados
     * @param obj
     * @return
     */
    public boolean excluir(Object obj) {
        
        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession();
        }

        try {
            if (execCommit) {
                sessao.beginTransaction();
            }
            
            sessao.delete(obj);
            
            if (execCommit) {
                sessao.getTransaction().commit();
            }
            return true;
        } catch (Exception e) {
            mensagem = e.getMessage();
            sessao.getTransaction().rollback();
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }


    /**
     * Exclui os objetos que se enquadram nos criterios recebidos por parametro
     * em uma unica transacao com o banco de dados.
     *
     * @param type
     * @param criterions
     * @return
     */
    public boolean excluir(Class type, Criterion[] criterions) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession();
        }

        try {
            
            if (execCommit) {
                if(!sessao.isConnected()){
                    sessao.beginTransaction();
                }
            }
            
            //** seta as condicoes para recuperar os itens
            Criteria criteria = sessao.createCriteria(type);
            for (Object criterion : criterions) {
                criteria.add((Criterion)criterion);
            }
            //** percorre os itens retornados excluindo-os
            List list = criteria.list();
            for (Object item : list) {
                //** marcacao para exclusao no banco
                sessao.delete(item);
            }
            if (execCommit) {
                sessao.getTransaction().commit();
            }

            return true;
        } catch (HibernateException e) {
            sessao.getTransaction().rollback();
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }

    /**
     * Exclui os objetos recebidos por parametro do banco de dados
     * em uma unica transacao.
     *
     * @param obj
     * @return
     */
    public boolean excluir(Object[] objects) {

        if (objects == null || objects.length == 0){
            return true;
        }

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession();
        }

        try {
            
            if (execCommit) {
                sessao.beginTransaction();
            }
            
            //** percorre os objetos marcando para delecao
            for (Object obj : objects)
                if (obj != null)
                    sessao.delete(obj);

            //** confirmando exclusao dos objetos
            if (execCommit) {
                sessao.getTransaction().commit();
            }
            
            return true;
        } catch (Exception e) {
                mensagem = "Não foi possível excluir.";
            
            if (e instanceof ConstraintViolationException) {
                String sqlState = ((ConstraintViolationException) e).getSQLState();
                
                if (sqlState.equals("23503"));
                    mensagem = "Não é possível excluir. Existem outros registros referenciando o mesmo.";
            }
            
            sessao.getTransaction().rollback();
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }

    /**
     * Localiza o registro especifico e retorna o objeto.
     * @param type
     * @param id
     * @return
     */
    public T getRegistro(Class type, Serializable id) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            //** Join
            if (getJoin() != null) {

                Criteria criteria = sessao.createCriteria(type);
                String nmField = null;
                Set<Field> myFields = new HashSet<Field>(Arrays.asList(type.getDeclaredFields()));
                myFields.addAll(Arrays.asList(type.getSuperclass().getDeclaredFields()));
                for (Field field : myFields) {
                    if (field.isAnnotationPresent(Id.class)) {
                        nmField = field.getName();
                        break;
                    }
                }

                if (nmField != null) {
                    criteria.add(Restrictions.eq(nmField, id));
                } else {
                    return null;
                }

                for (Object novoJoin : getJoin())
                {
                    String associationPath = (String)((Object[])novoJoin)[0];
                    JoinType joinType = (JoinType)((Object[])novoJoin)[1];

                    /*criteria = */criteria.createCriteria(associationPath, joinType);
                }

                return (T) criteria.uniqueResult();
            }


            //** se não tem join carrega via método load
            T encontrado = (T) sessao.load(type, id);
            Hibernate.initialize(type.cast(encontrado));

            return encontrado;
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro", e);
            setMensagem(e.getMessage());
            return null;
        } finally {
            if (!isConnected())   //sessao.getTransaction().rollback();
                sessao.close();
        }
    }

    /**
     * Localiza um registro especifico cujo o indice é uma String ao inves de
     * um int.
     * @param id
     * @param classe
     * @return
     */
    public T getRegistro(Class type, Integer id, LockMode lockMode) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            //** se não tem join carrega via método load
            T encontrado = (T) sessao.load(type, id, new LockOptions(lockMode));
            Hibernate.initialize(type.cast(encontrado));
            return encontrado;
        } catch (Exception e) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return null;
        } finally {
            if (!isConnected())     
                sessao.close();
        }
    }

    public boolean hasRegistro(Class type, Integer id) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            Object encontrado = sessao.get(type, id);
            return (encontrado != null);
        } catch (Exception e) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())        //sessao.getTransaction().rollback();
                sessao.close();
        }
    }

    public boolean hasRegistro(Class type, String id) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            Object encontrado = sessao.get(type, id);
            return (encontrado != null);
        } catch (Exception e) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return false;
        } finally {
            if (!isConnected())        //sessao.getTransaction().rollback();
                sessao.close();
        }
    }

    public List<T> getLista(Class type, int inicio, int maximo) {
        
        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            HashMap criterias = new HashMap();
            hmAlias.clear();
            
            // Buscar uma lista de objetos do BD
            criterias.put("this", sessao.createCriteria(type));
            //** Join
            if (getJoin() != null) {
                for (Object novoJoin : getJoin())
                {
                    String associationPath = (String)((Object[])novoJoin)[0];
                    //JoinType joinType = (JoinType)((Object[])novoJoin)[1];

                    Object criteria = criterias.get(associationPath);
                    if (criteria == null) {

                        if (((Object[])novoJoin)[1] instanceof JoinType) {
                            criteria = ((Criteria)criterias.get("this")).createCriteria(associationPath, getAlias(associationPath), (JoinType)((Object[])novoJoin)[1]);
                        }
                        
                        criterias.put(associationPath, criteria);
                    }
                    if (((Object[])novoJoin)[1] instanceof FetchMode) {
                        ((Criteria)criterias.get("this")).setFetchMode(associationPath, (FetchMode)((Object[])novoJoin)[1]);
                    }
                }
            }
            //** Default Where Clause
            if (getDefaultCriterions() != null) {
                for (Object defaultClause : getDefaultCriterions())
                {
                    String associationPath = (String)((Object[])defaultClause)[0];
                    Criterion criterion = (Criterion)((Object[])defaultClause)[1];

                    Object defaultCriteria = criterias.get(associationPath);
                    if (defaultCriteria == null) {
                        defaultCriteria = ((Criteria)criterias.get("this")).createCriteria(associationPath, getAlias(associationPath));
                        criterias.put(associationPath, defaultCriteria);
                    }
                    ((Criteria)defaultCriteria).add(criterion);
                }
            }
            //** Clausura Where
            if (getCriterions() != null) {
                for (Object clause : getCriterions())
                {
                    String associationPath = (String)((Object[])clause)[0];
                    Criterion criterion = (Criterion)((Object[])clause)[1];

                    Object criteria = criterias.get(associationPath);
                    if (criteria == null) {
                        criteria = ((Criteria)criterias.get("this")).createCriteria(associationPath, getAlias(associationPath));
                        criterias.put(associationPath, criteria);
                    }
                    ((Criteria)criteria).add(criterion);
                }
            }
            //** Clausula Order by (DefaultOrderBy e OrderBy)
            if (getDefaultOrderBy() != null) {
                for (OrderBy dfltOrder :  getDefaultOrderBy()) {
                    Object criteria = criterias.get(dfltOrder.getAssociationPath());
                    if (criteria == null) {
                        criteria = ((Criteria)criterias.get("this")).createCriteria(dfltOrder.getAssociationPath(), getAlias(dfltOrder.getAssociationPath()));
                        criterias.put(dfltOrder.getAssociationPath(), criteria);
                    }
                    ((Criteria)criteria).addOrder(dfltOrder.getOrder());
                }
            }
            if (getOrderByClause() != null) {
                for (OrderBy order :  getOrderByClause()) {
                    Object criteria = criterias.get(order.getAssociationPath());
                    if (criteria == null) {
                        criteria = ((Criteria)criterias.get("this")).createCriteria(order.getAssociationPath(), getAlias(order.getAssociationPath()));
                        criterias.put(order.getAssociationPath(), criteria);
                    }
                    ((Criteria)criteria).addOrder(order.getOrder());
                }
            }
            
            configuraProjections(criterias);

            //** se foi setado um limite de linhas retornadas
            Criteria criteriaThis = ((Criteria)criterias.get("this"));
            if (getMaxFetchSize() >0) {
                criteriaThis = criteriaThis.setMaxResults(getMaxFetchSize());
                mensagem = "listagem limitada";
            } else if (getMaxFetchSize() == 0) {
                return new ArrayList();
            }

            if (inicio != -1 && maximo != -1) {
                criteriaThis = criteriaThis.setMaxResults(maximo).setFirstResult(inicio);
            }
            
            if(getResultTransformer() != null){
                criteriaThis.setResultTransformer(getResultTransformer());
            }
            
            return criteriaThis.list();

        } catch (Exception e) {
            sessao.getTransaction().rollback();
            mensagem = "nao localizou";
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return null;
        } finally {
            if (!isConnected()) {
                sessao.close();
            }
        }
    }
    
    public Long getRowCount(Class type) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            HashMap criterias = new HashMap();
            hmAlias.clear();
            
            // Buscar uma lista de objetos do BD
            criterias.put("this", sessao.createCriteria(type));
            //** Join
            if (getJoin() != null) {
                for (Object novoJoin : getJoin())
                {
                    String associationPath = (String)((Object[])novoJoin)[0];
                    //JoinType joinType = (JoinType)((Object[])novoJoin)[1];

                    Object criteria = criterias.get(associationPath);
                    if (criteria == null) {

                        if (((Object[])novoJoin)[1] instanceof JoinType) {
                            criteria = ((Criteria)criterias.get("this")).createCriteria(associationPath, getAlias(associationPath), (JoinType)((Object[])novoJoin)[1]);
                        }
                        
                        criterias.put(associationPath, criteria);
                    }
                    if (((Object[])novoJoin)[1] instanceof FetchMode) {
                        ((Criteria)criterias.get("this")).setFetchMode(associationPath, (FetchMode)((Object[])novoJoin)[1]);
                    }
                }
            }
            //** Default Where Clause
            if (getDefaultCriterions() != null) {
                for (Object defaultClause : getDefaultCriterions())
                {
                    String associationPath = (String)((Object[])defaultClause)[0];
                    Criterion criterion = (Criterion)((Object[])defaultClause)[1];

                    Object defaultCriteria = criterias.get(associationPath);
                    if (defaultCriteria == null) {
                        defaultCriteria = ((Criteria)criterias.get("this")).createCriteria(associationPath, getAlias(associationPath));
                        criterias.put(associationPath, defaultCriteria);
                    }
                    ((Criteria)defaultCriteria).add(criterion);
                }
            }
            //** Clausura Where
            if (getCriterions() != null) {
                for (Object clause : getCriterions())
                {
                    String associationPath = (String)((Object[])clause)[0];
                    Criterion criterion = (Criterion)((Object[])clause)[1];

                    Object criteria = criterias.get(associationPath);
                    if (criteria == null) {
                        criteria = ((Criteria)criterias.get("this")).createCriteria(associationPath, getAlias(associationPath));
                        criterias.put(associationPath, criteria);
                    }
                    ((Criteria)criteria).add(criterion);
                }
            }
            
            Criteria criteriaThis = ((Criteria)criterias.get("this"));
            criteriaThis.setProjection(Projections.rowCount());
            
            return (Long) criteriaThis.uniqueResult();
        } catch (Exception e) {
            sessao.getTransaction().rollback();
            mensagem = "Erro ao buscar o 'rowCount'";
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return null;
        } finally {
            if (!isConnected()) {
                sessao.close();
            }
        }
    }

    public List<T> getLista(Class type) {
        return getLista(type, -1, -1);
    }
    
    

    public List execQuerySQL (String querySQL, boolean exec) {

        Session sessao = HibernateUtil.openSession();

        try {
            Query select = sessao.createSQLQuery(querySQL); // executa query
            if (exec) {
                sessao.getTransaction().begin();
                int retorno = select.executeUpdate();
                sessao.getTransaction().commit();
                
                List lst = new ArrayList(); 
                lst.add(retorno);
                return lst;
            }
            
            if(getResultTransformer() != null){
                select.setResultTransformer(getResultTransformer());
            }
            
            return select.list();
       } catch (Exception e) {
            mensagem = "Erro ao executar query nomeada";
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return null;
        } finally {
            if (sessao != null && sessao.isOpen()) {
                sessao.close();
            }
        }
    }


    public List execQueryHQL (String queryHQL) {

        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            sessao.beginTransaction(); // inicia uma transacao
        }

        try {
            Query select = sessao.createQuery(queryHQL); // cria query HQL
            //select.setString("nome", "Jornalismo");
            if (maxFetchSize > 0) {
                select.setMaxResults(maxFetchSize);
                mensagem = "listagem limitada";
            }
            return select.list();
       } catch (Exception e) {
            mensagem = "Erro ao executar query nomeada";
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
            return null;
        } finally {
            if (!isConnected())
                sessao.close();
        }
    }
    
    private String getAlias(String associationPath){
        
        String apelido = associationPath;
        if(associationPath.contains(".")){
            int lastIndex = associationPath.lastIndexOf(".");
            apelido = associationPath.substring(lastIndex + 1);
        }
        
        //Verifica se o alias já foi usado, se foi usado adiciona + 1 no final do nome.
        if(hmAlias.containsKey(apelido)){
            Integer qtd = hmAlias.get(apelido);
            hmAlias.put(apelido, ++qtd);
            return apelido + qtd;
        } else {
            hmAlias.put(apelido, 0);
        }
        
        return apelido;
    }
    
    private void configuraProjections(HashMap criterias){
        if (getProjections() != null) {

            ProjectionList projectionList = Projections.projectionList();

            for (ProjectionParc projectionParc : getProjections()){

                Projection projection   = projectionParc.getProjection();
                String alias            = projectionParc.getAlias();

                if(alias != null){
                    projectionList.add(projection, alias);
                } else {
                    projectionList.add(projection);
                }
            }

            Object criteria = criterias.get("this");
            ((Criteria)criteria).setProjection(projectionList);
        }
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
  
    /**
     * @return the restrictions
     */
    public Object[] getCriterions() {
        return criterions;
    }

    /**
     * @param restrictions the restrictions to set
     */
    public void setCriterions(Object[] criterions) {
        this.criterions = criterions;
    }
    
    /**
     * @return the projections
     */
    public ProjectionParc[] getProjections() {
        return projections;
    }

    /**
     * @param projections the projections to set
     */
    public void setProjections(ProjectionParc[] projections) {
        this.projections = projections;
    }

    /**
//     * @return the orderByClause
//     */
//    public OrderBy[] getOrderByClause() {
//        return orderByClause;
//    }
//
//    /**
//     * @param orderByClause the orderByClause to set
//     */
//    public void setOrderByClause(OrderBy[] orderByClause) {
//        this.orderByClause = orderByClause;
//    }
//
//    /**
//     * @return the defaultOrderBy
//     */
//    public OrderBy[] getDefaultOrderBy() {
//        return defaultOrderBy;
//    }
//
//    /**
//     * @param defaultOrderBy the defaultOrderBy to set
//     */
//    public void setDefaultOrderBy(OrderBy[] defaultOrderBy) {
//        this.defaultOrderBy = defaultOrderBy;
//    }

    /**
     * Indica quantas linhas a busca no banco retornara
     * @param MaxFetchSize
     */
    public void setMaxFetchSize(int maxFetchSize) {
        this.maxFetchSize = maxFetchSize;
    }

    /**
     * Retorna o quantidade maxima de registro que a consulta retornara
     * @param MaxFetchSize
     */
    public int getMaxFetchSize() {
        return this.maxFetchSize;
    }

    /**
     * @return the fetchMode
     */
    public Object[] getJoin() {
        return join;
    }

    /**
     * @param fetchMode the fetchMode to set
     */
    public void setJoin(Object[] join) {
        this.join = join;
    }

    /**
     * @return the defaultCriterions
     */
    public Object[] getDefaultCriterions() {
        return defaultCriterions;
    }

    /**
     * @param defaultCriterions the defaultCriterions to set
     */
    public void setDefaultCriterions(Object[] defaultCriterions) {
        this.defaultCriterions = defaultCriterions;
    }

    public void setResultTransformer(ResultTransformer resultTransformer) {
        this.resultTransformer = resultTransformer;
    }

    public ResultTransformer getResultTransformer(){
        return resultTransformer;
    }


    public void openTransaction(boolean exec){
        
        this.execCommit = exec;
        
        if (session == null) {
            session = HibernateUtil.openSession(); // abre uma nova sessao
            session.beginTransaction(); // inicia uma transacao
        }
    }
    
    public boolean closeTransaction(boolean exec) {

        if (session != null) {

            if (!session.isConnected()) {
                session.beginTransaction();
            }
            
            if (exec) {
                try {
                    session.getTransaction().commit();
                    mensagem = " Alterações gravadas com sucesso!";
                    return true;
                } catch (Exception e) {
                    session.getTransaction().rollback();
                    mensagem = " Não foi possível gravar alterações.";
                    Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
                    return false;
                } finally {
                    session.close();
                    session = null;
                    execCommit = true;
                }
            } else {
                session.getTransaction().rollback();
                session.close();
            }
        }
        session = null;
        execCommit = true;
                
        return true;
    }


    private boolean isConnected(){
        return session != null && session.isConnected();
    }


    /**
     * Sugestão em:  http://www.guj.com.br/java/116968-obter-valor-da-sequence---hibernate
     *
     * @param nmSequence
     * @return Próximo valor da sequence.
     */
    public Integer getNextValueSequence(String nmSequence){
        try {
            //** Obtem a classe dialect que esta sendo utilizada
            String obj = HibernateUtil.getProperty("hibernate.dialect");

            //** Cria um objeto Class
            Class klass = Class.forName(obj);

            //** Nova instancia do Dialect
            Dialect hsql = (Dialect) klass.newInstance();

            //** Abrir sessao
            Session sessao = session;
            if (!isConnected()) {
                sessao = HibernateUtil.openSession(); // abre uma nova sessao
                sessao.beginTransaction(); // inicia uma transacao
            }
            
            try {
                //** Executa a native query de acordo com o select que o dialect retornou
                BigInteger val = (BigInteger)sessao.createSQLQuery(hsql.getSequenceNextValString( nmSequence) ).list().get(0);
                return val.intValue();

            } catch (SQLGrammarException ex) { 

                sessao.getTransaction().rollback();
                //** Sequence nao encontrada, entao cria
                sessao.createSQLQuery(hsql.getCreateSequenceStrings(nmSequence, 1, 1)[0]).executeUpdate();
                
                //** Executa a native query de acordo com o select que o dialect retornou
                BigInteger val = (BigInteger)sessao.createSQLQuery(hsql.getSequenceNextValString( nmSequence) ).list().get(0);
                return val.intValue();
                
            } catch (Exception e) {
                // MSG: (34013) 
                mensagem = " Erro ao recuperar próximo valor da sequence " + nmSequence;
                Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);

            } finally {
                if (!isConnected())
                    sessao.close();
            }


        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, "Erro ao setar o valor da proxima sequencia.", ex);
        }

        return null;
    }

    /**
     * Pode ser utilizado para atribuir uma sessão diferente da utilizada pelo 
     * HibernateUtil Default. Não esquecer de realizar o closeTransaction após 
     * o uso da sessão.
     * 
     * @param session 
     */
    public void setSession(Session session) {
        this.session = session;
    }
    
    /**
     * TESTAR MELHOR. NUM PRIMEIRO MOMENTO NÃO FUNCIONOU.
     * @param obj 
     */
    public void lock(Object obj) {
        Session sessao = session;
        
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
            //sessao.beginTransaction(); // inicia uma transacao
        }
        
        try {
            sessao.buildLockRequest( new LockOptions(LockMode.PESSIMISTIC_WRITE)).lock(obj);
        } finally {
            if (!isConnected())
             sessao.close();
        }
    }
    
    public void recarregar(Object obj) {
        Session sessao = session;
        if (!isConnected()) {
            sessao = HibernateUtil.openSession(); // abre uma nova sessao
        }

        try {
            sessao.refresh(obj);
        } catch (Exception e) {
            mensagem = "Não foi possível recarregar objeto.";//nao alterou
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, mensagem, e);
        } finally {
            if (!isConnected()){
                sessao.close();
            }
        }
    }
    
    /**
     * @return the orderByClause
     */
    public OrderBy[] getOrderByClause() {
        return orderByClause;
    }

    /**
     * @param orderByClause the orderByClause to set
     */
    public void setOrderByClause(OrderBy[] orderByClause) {
        this.orderByClause = orderByClause;
    }
    
    /**
     * @return the defaultOrderBy
     */
    public OrderBy[] getDefaultOrderBy() {
        return defaultOrderBy;
    }

    /**
     * @param defaultOrderBy the defaultOrderBy to set
     */
    public void setDefaultOrderBy(OrderBy[] defaultOrderBy) {
        this.defaultOrderBy = defaultOrderBy;
    }

    public void setUsePaginator(boolean usePaginator) {
        this.usePaginator = usePaginator;
    }

    public boolean isUsePaginator() {
        return usePaginator;
    }
}