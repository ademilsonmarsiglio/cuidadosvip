package br.com.ademilsonmarsiglio.casasantacatarina.connector;

import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.SequenceGenerator;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.transform.ResultTransformer;

/**
 * Responsável pela comunicacao com o modulo de persistencia.
 * @author jean
 * @param <T>
 */
public class GenericCON <T> {

    private ArrayList<T> obj = new ArrayList();
    private GenericDAO<T> genericDao;
    private String mensagem;
    private char acao;
    private final Class<T> classe;

    // Se true, utiliza método específico conforme a ação informada. 
    private boolean considerarAcaoAoGravar = false;

    public GenericCON(Class<T> classe) {
        this(classe, false);
    }

    public GenericCON(Class<T> classe, boolean considerarAcaoAoGravar) {
        this.classe = classe;
        this.acao = ' ';
        this.genericDao = new GenericDAO<T>();
        this.considerarAcaoAoGravar = considerarAcaoAoGravar;
    }

    public void setConsiderarAcaoAoGravar(boolean considerarAcaoAoGravar) {
        this.considerarAcaoAoGravar = considerarAcaoAoGravar;
    }
    
    
    /**
     * Marcacao da acao de INCLUIR
     * @return - retorna true para a acao de incluir
     */
    public boolean incluir() {
        
        try {
            if (getAcao() == ' ')
                obj = new ArrayList<T>();

            Constructor cons = classe.getConstructor();
            obj.add((T)cons.newInstance());

            setAcao('I');


            return true;
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(GenericCON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Marcacao da acao de ALTERAR.
     * @return true para confirmar que foi marcado a alteracao.
     */
    public boolean alterar() {
        setAcao('A');

        return true;
    }


    /**
     * Salva os dados.
     * @return - retorna true se salvou com sucesso
     */
    public boolean gravar() {
        
        if (acao != 'A' && acao != 'I'){
            
            return true;
        }

        boolean retorno = false;
        
        retorno = incluirOuAlterar();

        setAcao(' ');

        setMensagem(genericDao.getMensagem());

        return retorno;
    }
    

    protected boolean incluirOuAlterar() {
        
        if (considerarAcaoAoGravar) {
            boolean retorno = false;
            if (acao == 'I') {

                retorno = genericDao.incluir(obj);

            } else if (acao =='A') {

                retorno = genericDao.alterar(obj);
            } 
            return retorno;
            
        } else  {
            return  genericDao.incluirOuAlterar(obj);
        }
    }

    /**
     * Exclui o objeto.
     * @return true se a exclusao foi bem sucedida
     */
    public boolean excluir() {

        boolean retorno = false;
        retorno = genericDao.excluir(getObjeto());

        setMensagem(genericDao.getMensagem());
        return retorno;
    }


    /**
     * Exclui mais de um objeto.
     * @return true se a exclusao foi bem sucedida
     */
    public boolean excluir(Criterion[] criterions) {

        Object[] criterionsAnterior = getCriterions();
        Object[] defaultAnterior = getDefaultCriterions();
        
        ArrayList<Object[]> lstCriterions = new ArrayList<Object[]>();
        for (Criterion criterion : criterions) {
            
           lstCriterions.add(new Object[]{"this", criterion });
        }
        setCriterions(lstCriterions.toArray());
        setDefaultCriterions(null);
        
        openTransaction(false);
        boolean retorno = excluir(getLista().toArray());
        
        setCriterions(criterionsAnterior);
        setDefaultCriterions(defaultAnterior);
        
        closeTransaction(retorno);

        setMensagem(genericDao.getMensagem());
        return retorno;
        
//        boolean retorno = genericDao.excluir(classe, criterions);
//
//        setMensagem(genericDao.getMensagem());
//        return retorno;
    }


    /**
     * Exclui mais de um objeto.
     * @return true se a exclusao foi bem sucedida
     */
    public boolean excluir(Object[] objetos) {

        boolean retorno = false;
        retorno = genericDao.excluir(objetos);

        setMensagem(genericDao.getMensagem());
        return retorno;
    }

    /**
     * Exclui todos os registros da tabela cujo o nome é passado como parametro.
     * @param nomeTabela
     * @return
     */
    /* public boolean excluirTodos(String nomeTabela){
    if (dao.excluirTodos(nomeTabela)){
    return true;
    }
    else{
    setMensagem(dao.getMensagem());
    return false;
    }
    }*/
    /**
     * Executa query sem retornar uma list e sim um boolean se true
     * é sinal que executou corretamente e false se deu erro ao executar
     * a query
     *
     */
    /*public boolean executaQuerySemRetorno(String strQuery){
    return dao.executaQuerySemRetorno(strQuery);
    }*/

    /*public List execNamedNativeQueryWithoutParams(String namedNativeQuery){
        List list = genericDao.execNamedNativeQueryWithoutParams(namedNativeQuery);

        setMensagem(genericDao.getMensagem());
        return list;
    }*/

    public List execQuerySQL(String querySQL){
        return this.execQuerySQL(querySQL, false);
    }
    
    public List execQuerySQL(String querySQL, boolean exec){
        List list = genericDao.execQuerySQL(querySQL, exec);

        setMensagem(genericDao.getMensagem());
        return list;
    }

    public List execQueryHQL(String queryHQL){
        List list = genericDao.execQueryHQL(queryHQL);

        setMensagem(genericDao.getMensagem());
        return list;
    }

    /**
     * Localiza um objeto segundo o id passado como
     * argumento e armazena no objeto global .
     * @param id - codigo
     * @return - true se conseguiu localizar
     */
    public boolean localizar(Serializable id) {
        boolean retorno = false;
        // Buscar da persistencia
        if (id != null)
        {
            T object = genericDao.getRegistro(classe, id);

            // Localizou??
            if (object != null) {
                if (getAcao() == ' ')
                    obj = new ArrayList<T>();

                obj.add(object);

                retorno = true;
            }
            setMensagem(genericDao.getMensagem());

        } 

        return retorno;
    }
    
    /**
     * Faz uma busca no banco, que retorne um unico resultado.
     * 
     * @return 
     */
    public T getUniqueResult() {
        int max = getMaxFetchSize();
        try {
            setMaxFetchSize(1);
            List<T> lista = getLista();

            if (Verify.nullsOrEmpty(lista))
                return null;

            return lista.get(0);
        } finally {
            setMaxFetchSize(max);
        }
    }
    
    public T get(Serializable id) {
        if (id != null) {
            return genericDao.getRegistro(classe, id);
        } 

        return null;
    }
    
    /**
     * Localiza um objeto segundo o id passado como
     * argumento e armazena no objeto global .
     * @param id - código
     * @return - true se conseguiu localizar
     */
    public boolean localizarLock(Object id) {
        boolean retorno = false;
        // Buscar da persistência
        if (id != null)
        {
            T object = null;
            if (id instanceof Integer) {
                object = genericDao.getRegistro(classe, Integer.parseInt(id.toString()), LockMode.PESSIMISTIC_WRITE);
            } else {
                object = genericDao.getRegistro(classe, id.toString());
            }

            // Localizou??
            if (object != null) {
                if (getAcao() == ' ')
                    obj = new ArrayList();

                obj.add(object);

                retorno = true;
            }
            setMensagem(genericDao.getMensagem());

        } 

        return retorno;
    }

    /**
     * Localiza um objeto segundo o id passado como
     * argumento e armazena no objeto global .
     * @param id - codigo
     * @return - true se conseguiu localizar
     */
    public boolean existeRegistro(Object id) {
        boolean retorno = false;
        // Buscar da persistencia
        if (id != null)
        {
            if (id instanceof Integer)
                retorno = genericDao.hasRegistro(classe, (Integer)id );
            else
                retorno = genericDao.hasRegistro(classe, id.toString());

            setMensagem(genericDao.getMensagem());

        }

        return retorno;
    }


    /**
     * Retorna uma lista de objetos  contidos na tabela
     * no banco de dados.
     * @return
     */
    public List<T> getLista() {
        if (isUsePaginator()) {
            int ate = genericDao.qtdPaginator;
            List<T> listPaginator = new ArrayList();
            int qtdResult = 0;

            do {
                List lista = genericDao.getLista(classe, listPaginator.size(), ate);
                
                listPaginator.addAll(lista);
                qtdResult = lista.size();
            } while (qtdResult > 0);

            return listPaginator;
        }
        
        return this.getLista(-1, -1);
    }
    
    public List<T> getLista(int primeiro, int maximo) {
        List list = genericDao.getLista(classe, primeiro, maximo);

        setMensagem(genericDao.getMensagem());
        return list;
    }
    
    /*public List getLista(String queryHQL){
        List list = genericDao.getLista(classe, queryHQL);

        setMensagem(genericDao.getMensagem());
        return list;
    }*/

    /**
     * Retorna uma lista ordenada de objetos contidos na tabela de banco de dados.
     * Esta ordenacao se faze de acordo com o nome do campo recebido como parametro.
     * @param nomeCampo
     * @return
     */
    /*public List getListaOrdenada(String nomeCampo){
    return dao.getListaOrdenada(classe,nomeCampo);
    }*/
    /**
     * Retorna o valor atual da variavel mensagem.
     * @return
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Seta o valor atual da mensagem.
     * @param mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Atualiza o objeto atual.
     * @param obj
     *
     */
    public void setObjeto(T obj){
        if (getAcao() == ' ')
            this.obj = new ArrayList<T>();

        this.obj.add(obj);
    }

    /**
     * Retorna o Objecto atual.
     *
     * @return
     */
    public T getObjeto() {
        return (obj.size() > 0) ? obj.get(obj.size()-1) : null;
    }
    
    public void setObjetoCorrente(T object) {
        if(obj == null || obj.isEmpty()){
            return;
        }
        
        obj.set(obj.size() -1, object);
    }
    
    public void removeObjeto(Object object) {
        obj.remove(object);
    }

    /**
     *
     * @return A ultima acao executada. Se foi uma insercao retorna 'I'.
     * Se foi uma alteracao retorna 'A'. Caso contrario retorna ' '.
     */
    public char getAcao(){
        return acao;
    }

    /**
     * @param acao the acao to set
     */
    public void setAcao(char acao) {
        this.acao = acao;
    }

    /*public int getNumRegistros(String nomeTabela){


        List lst= dao.executaQuery("SELECT COUNT(*) FROM " + nomeTabela);

    if (lst.size() != 0){

    return Integer.parseInt(lst.get(0).toString());
    }
    return 0;
    }*/

    public Field[] getAttributeDefs(){
        return getClassDAO().getDeclaredFields();
    }
    
    public Class<?> getClassDAO()
    {
        return classe;
    }

    public void setCriterions(Object[] criterions){
       genericDao.setCriterions(criterions);
    }

    public Object[] getCriterions(){
        return genericDao.getCriterions();
    }

    public void setDefaultCriterions(Object[] defaultCriterions){
       genericDao.setDefaultCriterions(defaultCriterions);
    }

    public Object[] getDefaultCriterions(){
        return genericDao.getDefaultCriterions();
    }

    public void setProjections(ProjectionParc[] projections){
       genericDao.setProjections(projections);
    }

    public ProjectionParc[] getProjections(){
        return genericDao.getProjections();
    }

    /**
     * Indica quantas linhas a busca no banco retornará
     * @param MaxFetchSize
     */
    public void setMaxFetchSize(int maxFetchSize) {
        genericDao.setMaxFetchSize(maxFetchSize);
    }

    /**
     * Retorna o quantidade máxima de registro que a consulta retornará
     * @param MaxFetchSize
     */
    public int getMaxFetchSize() {
        return genericDao.getMaxFetchSize();
    }

    /**
     * @return the join
     */
    public Object[] getJoin() {
        return genericDao.getJoin();
    }

    /**
     * @param join
     */
    public void setJoin(Object[] join) {
        genericDao.setJoin(join);
    }

    public void setResultTransformer(ResultTransformer resultTransformer) {
        genericDao.setResultTransformer(resultTransformer);
    }
    
    public ResultTransformer getResultTransformer(){
        return genericDao.getResultTransformer();
    }

    protected List<T> getAllObjects(){
        return obj;
    }
    
    protected void setAllObjects(ArrayList allObjects){
        this.obj = allObjects;
    }


    public void openTransaction(){
        this.openTransaction(true);
    }

    public void closeTransaction(){
        this.closeTransaction(false);
    }


    protected void openTransaction(boolean exec){
        genericDao.openTransaction(exec);
    }

    protected boolean closeTransaction(boolean exec){
        return genericDao.closeTransaction(exec);
    }

    /**
     *
     * @param name Nome do atributo do DAO.
     * @return Tipo da Classe.
     */
    public Class<?> getFieldType(String name) {

        Class classeAux = classe;

        Method method = null;

        int fromIndex = 0;
        int endIndex = -1;
        try {

            while ((endIndex = name.indexOf(".", fromIndex)) != -1) {

                method = classeAux.getMethod("get" + name.substring(fromIndex, fromIndex + 1).toUpperCase() + name.substring(fromIndex + 1, endIndex));

                classeAux =  method.getReturnType();

                fromIndex = endIndex + 1;
            }
            return classeAux.getDeclaredField(name.substring(fromIndex)).getType();

        } catch (NoSuchFieldException ex) {
            try {
                //** se nao encontrou o field tenta na superclasse
                return classeAux.getSuperclass().getDeclaredField(name.substring(fromIndex)).getType();
            } catch (NoSuchFieldException ex1) {
                Logger.getLogger(GenericCON.class.getName()).log(Level.SEVERE, null, ex1);
            } 
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(GenericCON.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public Integer getNextValueSequence(){

        Annotation sequence = getClassDAO().getAnnotation(SequenceGenerator.class);
        if (sequence != null) {
            return genericDao.getNextValueSequence(((SequenceGenerator)sequence).sequenceName());
        }

        //** tenta na superclasse
        sequence = getClassDAO().getSuperclass().getAnnotation(SequenceGenerator.class);
        if (sequence != null) {
            return genericDao.getNextValueSequence(((SequenceGenerator)sequence).sequenceName());
        }
        
        return null;
    }


    protected GenericDAO getGenericDao(){
        return genericDao;
    }

    public void recarregar(Object obj) {
        genericDao.recarregar(obj);
    }
    
    public void setSession(Session session) {
        genericDao.setSession(session);
    }
    
    /**
     * @return the defaultOrderBy
     */
    public OrderBy[] getDefaultOrderBy() {
        return genericDao.getDefaultOrderBy();
    }

    /**
     * @param defaultOrderBy the defaultOrderBy to set
     */
    public void setDefaultOrderBy(OrderBy[] defaultOrderBy) {
        genericDao.setDefaultOrderBy(defaultOrderBy);
    }
    
    public OrderBy[] getOrderByClause() {
        return genericDao.getOrderByClause();
    }

    /**
     * @param defaultOrderByClause the defaultOrderByClause to set
     */
    public void setOrderByClause(OrderBy[] orderByClause) {
        genericDao.setOrderByClause(orderByClause);
    }
    
    public void setUsePaginator(boolean usePaginator) {
        genericDao.setUsePaginator(usePaginator);
    }

    public boolean isUsePaginator() {
        return genericDao.isUsePaginator();
    }
}