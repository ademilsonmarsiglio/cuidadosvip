package br.com.ademilsonmarsiglio.casasantacatarina.connector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author ademilson
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static Properties properties;

    static {

        Configuration annotationConfiguration = new Configuration().configure();
        properties = annotationConfiguration.getProperties();

//        String fileconfBD = System.getProperty("fileconf_bd");
//        if (fileconfBD != null) {
//            try {
//                Properties arquivoExterno = new Properties();
//                //** Carrega arquivos de properties.
//                arquivoExterno.load(new FileInputStream(new File(fileconfBD)));
//
//                Enumeration iterKeys = arquivoExterno.keys();
//                while (iterKeys.hasMoreElements()) {
//                    String key = iterKeys.nextElement().toString();
//                    String value = arquivoExterno.getProperty(key);
//
//                    properties.setProperty(key, value);
//                }
//
//                if (arquivoExterno.getProperty("hibernate.connection.ssl") == null
//                        || arquivoExterno.getProperty("hibernate.connection.ssl").equals("false")) {
//                    properties.remove("hibernate.connection.ssl");
//                    properties.remove("hibernate.connection.sslfactory");
//                }
//
//                if ((arquivoExterno.getProperty("hibernate.hbm2ddl.auto") == null
//                        || arquivoExterno.getProperty("hibernate.hbm2ddl.auto").equals("false"))
//                        && System.getProperty("verify_bd") == null) {
//
//                    properties.remove("hibernate.hbm2ddl.auto");
//                } else {
//                    properties.setProperty("hibernate.hbm2ddl.auto", "update");
//                }
//
//            } catch (IOException ex) {
//                System.err.println("Erro ao carregar arquivo de configuração do banco de dados.\n" + ex);
//            }
//        }

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure().applySettings(properties).build();

        Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

        sessionFactory = metadata.getSessionFactoryBuilder().build();

//        try {
//            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
//            Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
//            
//            sessionFactory = metaData.getSessionFactoryBuilder().build();
//        } catch (Throwable th) {
//            System.err.println("Enitial SessionFactory creation failed" + th);
//            throw new ExceptionInInitializerError(th);
//
//        }
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session openSession() {
        return getSessionFactory().openSession();
    }
}
