package br.com.ademilsonmarsiglio.casasantacatarina.connector;

import java.util.List;
import org.hibernate.transform.ResultTransformer;


/**
 *
 * @author Ademilsom
 * @param <T>
 */
public class GenericController<T> extends GenericCON<T>{
    
    private Object[] bkp_criterions, bkp_defCriterions, bkp_joins;
    private OrderBy[] bkp_order, bkp_defOrder;
    private ProjectionParc[] bkp_projections;
    private ResultTransformer bkp_resultTransformer;
    private boolean bkp_usePaginator;
    
    public GenericController(Class classe) {
        this(classe, false);
    }
    
    public GenericController(Class classe, boolean considera) {
        super(classe, considera);
    }
    
    public boolean save(T object){
        setObjeto(object);
        alterar();
        return gravar();
    }
    
    public boolean delete(Long id){
        if(localizar(id))
            if(excluir()){
//                setMensagem("Excluido com sucesso!");
                return true;
            }
//        else
//            setMensagem("Nao localizou objeto para exclusao.");
        
        return false;
    }
    
    public boolean delete(T object){
        setObjeto(object);
        return excluir();
    }
    
    public void guardaConfiguracoes() {
        bkp_joins = getJoin();
        bkp_criterions = getCriterions();
        bkp_defCriterions = getDefaultCriterions();
        bkp_order = getOrderByClause();
        bkp_defOrder = getDefaultOrderBy();
        bkp_projections = getProjections();
        bkp_resultTransformer = getResultTransformer();
        bkp_usePaginator = isUsePaginator();
        
        limpaConfiguracoes();
    }
    
    public void limpaConfiguracoes() {
        setJoin(null);
        setCriterions(null);
        setDefaultCriterions(null);
        setOrderByClause(null);
        setDefaultOrderBy(null);
        setProjections(null);
        setResultTransformer(null);
        setMaxFetchSize(-1);
    }
    
    public void restauraConfiguracoes() {
        setJoin(bkp_joins);
        setCriterions(bkp_criterions);
        setDefaultCriterions(bkp_defCriterions);
        setOrderByClause(bkp_order);
        setDefaultOrderBy(bkp_defOrder);
        setProjections(bkp_projections);
        setResultTransformer(bkp_resultTransformer);
        setUsePaginator(bkp_usePaginator);
    }
    
    public T getFirstResult() {
        int max = getMaxFetchSize();
        try {
            setMaxFetchSize(1);
            List<T> lista = getLista();
            
            if (lista == null || lista.isEmpty())
                return null;
            
            return lista.get(0);
        } finally {
            setMaxFetchSize(max);
        }
    }
}
