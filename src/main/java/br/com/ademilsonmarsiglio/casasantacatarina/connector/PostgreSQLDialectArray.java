package br.com.ademilsonmarsiglio.casasantacatarina.connector;

import java.sql.Types;

/**
 *
 * @author ademilson
 * @since 21/07/2018
 */
public class PostgreSQLDialectArray extends org.hibernate.dialect.PostgreSQL94Dialect {

    public PostgreSQLDialectArray() {
        super();
        registerHibernateType(Types.ARRAY, "array"); 
        registerColumnType(Types.ARRAY, "varchar[]" ); 
    }

}
