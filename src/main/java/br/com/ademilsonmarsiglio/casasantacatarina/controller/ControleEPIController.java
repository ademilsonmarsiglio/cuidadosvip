package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.ControleEPI;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.RecebimentoEpiService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ademilson
 */
@Controller()
@RequestMapping("/controleEpi")
public class ControleEPIController {
    private String CONTROLLER = "controleEpi";
    private String VIEW = CONTROLLER + "/main";
    
    @Autowired private RecebimentoEpiService service;
    
    @Autowired
    @Qualifier("report_controleEpi")
    private JasperReportsPdfView report_controleEpi;
    
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro) {
        
        if (filtro != null && filtro.getDate1() == null) {
            filtro.setDate1(Util.addDays(new Date(), -60));
        }
        
        return exibir(filtro, new ControleEPI());
    }
    
    @GetMapping("{codigo}")
    public ModelAndView editar(@ModelAttribute("filtro") Filter filtro, @PathVariable("codigo") Long codigo) {
        ControleEPI recebimentoEPI = service.get(codigo);
        
        String mensagem = null;
        if (recebimentoEPI == null){
            mensagem = "Objeto não encontrado.";
            recebimentoEPI = new ControleEPI();
        }
        
        ModelAndView mv = exibir(filtro, recebimentoEPI);
        mv.addObject("mensagemError", mensagem);
        
        return mv;
    }
    
    @GetMapping("getById/{codigo}")
    @ResponseBody
    public ControleEPI editar(@PathVariable("codigo") Long codigo) {
        ControleEPI recebimentoEPI = service.get(codigo);
        return recebimentoEPI;
    }
    
    @DeleteMapping("{codigo}")
    public ModelAndView delete(@ModelAttribute("filtro") Filter filtro, @PathVariable("codigo") Long codigo) {
        ModelAndView mv = null;
        if(service.delete(codigo)){
            mv = new ModelAndView();
            mv.setViewName("redirect:/controleEpi");
        } else {
            mv = listar(filtro);
            mv.addObject("mensagemError", "Erro ao excluir.");
        }
        
        return mv;
    }
    
    @PostMapping
    public ModelAndView gravar(@ModelAttribute("obj") @Validated ControleEPI recebimentoEPI, BindingResult result, RedirectAttributes attributes, HttpSession httpSession, @ModelAttribute("filtro") Filter filtro){
        if (result.hasErrors()) {
            return exibir(filtro, recebimentoEPI);
        }
        
        if (service.incluirOuEditar(recebimentoEPI, (Login) httpSession.getAttribute(Key.LOGIN_SESSION))){
            ModelAndView mv = new ModelAndView();
            mv.setViewName("redirect:/controleEpi");
            return mv;
        } else {
            return exibir(filtro, recebimentoEPI);
        }
    }
    
    public ModelAndView exibir(Filter filtro, ControleEPI recebimentoEPI) {
        ModelAndView mv = new ModelAndView(VIEW);
        mv.addObject("lista", service.list(filtro, false));
        mv.addObject("obj", recebimentoEPI);
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("filtro", filtro);

        return mv;
    }
    
    @GetMapping("/pdf")
    public ModelAndView getPdf(ModelAndView modelAndView, 
                               @ModelAttribute("filtro") Filter filtro, 
                               HttpSession httpSession) {
        
        Util.parametersToReport(report_controleEpi, httpSession);
    
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("datasource", new JRBeanCollectionDataSource(service.list(filtro, true)));
        
        modelAndView = new ModelAndView(report_controleEpi, parameterMap);
        return modelAndView;
    }
}
