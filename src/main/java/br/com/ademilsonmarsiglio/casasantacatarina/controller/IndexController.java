package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@RequestMapping({"/", "/index"})
@Controller
public class IndexController {
    
    @Autowired
    private IndexService index_service;

    @GetMapping
    public ModelAndView listar() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("obj", index_service.getDadosResumo());

        return mv;
    }
    
    
}
