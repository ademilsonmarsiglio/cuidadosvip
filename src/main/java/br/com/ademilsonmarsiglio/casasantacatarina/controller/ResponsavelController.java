package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.ResponsavelService;
import javax.servlet.http.HttpSession;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

@Controller()
@RequestMapping("/responsavel")
public class ResponsavelController {

    @Autowired
    private ResponsavelService responsavel_service;
    
    @GetMapping("{paciente}")
    public ModelAndView listar(@PathVariable("paciente") Long idPaciente, @ModelAttribute("filtro") Filter filtro) {
        ModelAndView modelAndView = new ModelAndView("responsavel/responsavel");
        modelAndView.addObject("lista", responsavel_service.filtrar(idPaciente, filtro));
        modelAndView.addObject("paciente", idPaciente);
        
        return modelAndView;
    }
    
    public ModelAndView showMessage(Long idPaciente, String mensagem) {

        ModelAndView modelAndView = new ModelAndView("responsavel/responsavel");
        modelAndView.addObject("lista", responsavel_service.filtrar(idPaciente, new Filter()));
        modelAndView.addObject("filtro", new Filter());
        modelAndView.addObject("paciente", idPaciente);
        
        if(mensagem != null){
            modelAndView.addObject("mensagem", mensagem);
        }

        return modelAndView;
    }

    @GetMapping("{paciente}/{codigo}")
    public ModelAndView edicao(@PathVariable("paciente") Long idPaciente, @PathVariable("codigo") Long codigo) {
        
        Responsavel responsavel = responsavel_service.getResponsavel(codigo);
        
        if(responsavel != null){
            return edicao(idPaciente, responsavel);
        } else {
            return showMessage(idPaciente, "Paciente nao encontrado!");
        }
    }
    
    public ModelAndView edicao(Long idPaciente, Responsavel responsavel) {
        ModelAndView mv = new ModelAndView("responsavel/manutencaoResponsavel");
        
        Paciente paciente = new Paciente();
        paciente.setIdPaciente(idPaciente);
        
        responsavel.setPaciente(paciente);
        
        mv.addObject("responsavel", responsavel);
        return mv;
    }

    @RequestMapping("{paciente}/novo")
    public ModelAndView novo(@PathVariable("paciente") Long idPaciente) {
        ModelAndView mv = new ModelAndView("responsavel/manutencaoResponsavel");
        
        Paciente paciente = new Paciente();
        paciente.setIdPaciente(idPaciente);
        
        Responsavel responsavel = new Responsavel();
        responsavel.setPaciente(paciente);
        responsavel.setPessoa(new Pessoa());
        
        mv.addObject("responsavel", responsavel);
        return mv;
    }

    @PostMapping()
    public String salvar(@Validated Responsavel responsavel, BindingResult result, RedirectAttributes attributes, HttpSession httpSession) {

        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView("responsavel/manutencaoResponsavel");
            mv.addObject("responsavel", responsavel);
            return mv.getViewName();
        }

        this.responsavel_service.salvar(responsavel, httpSession);
        return "redirect:/responsavel/" + responsavel.getPaciente().getIdPaciente();
    }

    @RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
    public String excluir(@PathVariable Long codigo, RedirectAttributes attributes) {

        Long idPaciente = null;
        Responsavel responsavel = responsavel_service.get(codigo);
        if(responsavel != null){
            idPaciente = responsavel.getPaciente().getIdPaciente();
        }
        
        responsavel_service.excluir(codigo);

        attributes.addFlashAttribute("mensagem", "Responsavel excluído com sucesso!");
        return "redirect:/responsavel/" + idPaciente;
    }
}
