package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.controller.validators.ProntuarioValidator;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SinalVital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.PacienteService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.ProntuarioService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.SaeProntuarioService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.SinalVitalService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.MonetaryPropertyEditor;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

@Controller()
@RequestMapping("/prontuario")
public class ProntuarioController {
    
    @Autowired
    private ProntuarioService prontuarioService;
    
    @Autowired
    private SinalVitalService sinalVitalService;
    
    @Autowired
    private ProntuarioValidator prontuarioValidator;
    
    @Autowired private  SaeProntuarioService saeService;
    
    @Autowired
    @Qualifier("report_prontuario")
    private JasperReportsPdfView report_prontuario;
    
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(Double.class, "itens.qtd", new MonetaryPropertyEditor());
    }
    
    @GetMapping("{paciente}")
    public ModelAndView listar(@PathVariable("paciente") Long idPaciente, @ModelAttribute("filtro") Filter filtro, HttpSession httpSession, @RequestHeader(value = "referer", required = false) String referer) {
        ModelAndView modelAndView = new ModelAndView("prontuario/prontuario");
        modelAndView.addObject("nmPaciente", PacienteService.getNomePaciente(idPaciente));
        modelAndView.addObject("paciente", idPaciente);
        
        String strFiltro = filtro != null && filtro.getDescricao() != null && !filtro.getDescricao().trim().isEmpty() ? "?descricao=" + filtro.getDescricao() : "";
        modelAndView.addObject("filtro_", strFiltro);
        
        return modelAndView;
    }
    
    
    @GetMapping("/getJsonItens/{prontuario}")
    public @ResponseBody String getJsonItens(@PathVariable("prontuario") String idProntuario){
        Prontuario p = prontuarioService.getItens(Long.parseLong(idProntuario));
        if (p != null){
            return p.getJsonItens();
        }
        
        return null;
    }
    
    @PostMapping("/{paciente}/getLista")
    public @ResponseBody String getLista(@PathVariable("paciente") Long idPaciente, @RequestParam(value = "filtro_consulta", required = false) String jsonFiltro, @RequestParam("base_url") String baseUrl, HttpSession httpSession){
        
        Filter filtro = null;
        
        if (jsonFiltro != null){
            filtro = new Gson().fromJson(jsonFiltro, new TypeToken<Filter>(){}.getType());
        }
        
        Boolean isAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
        
        List<Prontuario> lista = prontuarioService.filtrar(idPaciente, filtro);
        if (isAdmin){
            for (Prontuario prontuario : lista) {
                prontuario.setPodeAlterar(true);
            }
        }
        
        StringBuilder s = new StringBuilder();

        if (Verify.nullsOrEmpty(lista)){
            if (filtro != null && filtro.getOffset() != null && filtro.getOffset() > 0){
                return null;
            } else {
                addStr(s, "<tr><td colspan=\"6\">Nenhum Registro encontrado</td></tr>");
            }
        } else {
            for (Prontuario prontuario : lista) {
                addStr(s, "<tr>");
                    addStr(s, "<td class=\"col-sm-1\">");
                        addStr(s, "<input type=\"checkbox\" value=\"%s\"/>", prontuario.getIdProntuario());
                    addStr(s, "</td>");    
                    addStr(s, "<td class=\"col-sm-6\">%s</td>", prontuario.getDescricaoLimit().replace("\n", " ").replace("\r", " "));
                    addStr(s, "<td class=\"col-sm-1\">%s</td>", Util.formatDate(prontuario.getDtProntuario()));
                    addStr(s, "<td class=\"col-sm-1\">%s</td>", prontuario.getTurnoStr());
                    addStr(s, "<td title=\"%s\" class=\"col-sm-1\">%s</td>", prontuario.getLogin().getPessoa().getNmPessoa(), prontuario.getResponsavel());
                    addStr(s, "<td class=\"col-sm-1 text-right\">");
                        addStr(s, "<a class=\"btn btn-link btn-xs\" title=\"Visualizar\" rel=\"tooltip\" data-placement=\"top\"");
                        addStr(s, "data-toggle=\"modal\" data-target=\"#descricaoProntuario\"");
                        addStr(s, "data-descricao=\"%s\" data-prontuario='%s'>", prontuario.getDescricao(), prontuario.getIdProntuario());
//                        addStr(s, "data-descricao=\"%s\" data-itens='%s'>", prontuario.getDescricao(), prontuario.getJsonItens());
                        addStr(s, "<span class=\"glyphicon glyphicon-eye-open\"></span></a>");
                        addStr(s, "</a>");

                        if (prontuario.getPodeAlterar()){
                            //btn editar
                            addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%s%s/%s\"", baseUrl, prontuario.getPaciente().getIdPaciente(), prontuario.getIdProntuario());
                            addStr(s, "title=\"Editar\" rel=\"tooltip\" data-placement=\"top\">");
                            addStr(s, "<span class=\"glyphicon glyphicon-pencil\"></span>");
                            addStr(s, "</a>");

                            //btn excluir
                            addStr(s, "<a class=\"btn btn-link btn-xs\" data-toggle=\"modal\" data-target=\"#confirmacaoExclusaoModal\" ");
                            addStr(s, "data-codigo=\"%s\" data-descricao=\"o registro do dia %s\"", prontuario.getIdProntuario(), Util.formatDate(prontuario.getDtProntuario()));
                            addStr(s, "title=\"Excluir\" rel=\"tooltip\" data-placement=\"top\">");
                            addStr(s, "<span class=\"glyphicon glyphicon-remove\"></span>");
                            addStr(s, "</a>");
                        }
                    addStr(s, "</td>");    
                addStr(s, "</tr>");
            }
        }

        return s.toString();
    }
    
    private void addStr(StringBuilder s, String str, Object... params){
        s.append(String.format(str.concat(" \n"), params));
    }
    
    public ModelAndView showMessage(boolean aviso, Long idPaciente, String mensagem) {

        ModelAndView modelAndView = new ModelAndView("prontuario/prontuario");
        modelAndView.addObject("filtro", new Filter());
        modelAndView.addObject("paciente", idPaciente);
        modelAndView.addObject("nmPaciente", PacienteService.getNomePaciente(idPaciente));
        
        if(mensagem != null){
            if(aviso){
                modelAndView.addObject("mensagem", mensagem);
            } else{
                modelAndView.addObject("mensagemErr", mensagem);
            }
        }

        return modelAndView;
    }

    @GetMapping("{paciente}/{codigo}")
    public ModelAndView edicao(@PathVariable("paciente") Long idPaciente, @PathVariable("codigo") Long codigo, HttpSession httpSession) {
        Prontuario prontuario = prontuarioService.get(codigo);
        
        if(prontuario != null){
            Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
            Boolean isAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
            
            if (isAdmin){
                prontuario.setPodeAlterar(true);
            } else {
                if(login == null || !login.getIdLogin().equals(prontuario.getLogin().getIdLogin())){
                    return showMessage(false, idPaciente, "Atenção! Você não tem permissão de efetuar esta operação!");
                }
            }
            
            saeService.getListaByProntuario(new ArrayList(Arrays.asList(prontuario)));
            
            if(prontuario.getPodeAlterar()){
                ModelAndView mv = new ModelAndView("prontuario/manutencaoProntuario");
                mv.addObject("nmPaciente", PacienteService.getNomePaciente(idPaciente));
                mv.addObject("paciente", idPaciente);
                mv.addObject("prontuario", prontuario);
                mv.addObject("informa_paciente", Boolean.FALSE);
                return mv;
            } else 
                return showMessage(false, idPaciente, "O tempo de alteração expirou.");
        } else {
            return showMessage(false, idPaciente, "Prontuário não encontrado!");
        }
    }
    
    /**
     * Inserção rápida, apartir de um link da tela principal.
     * 
     * @param httpSession
     * @return 
     */
    @RequestMapping("novo")
    public ModelAndView novo(HttpSession httpSession){
        ModelAndView mv = novo(null, httpSession);
        mv.addObject("informa_paciente", Boolean.TRUE);
        mv.addObject(Key.CONTORLLER_NAME, "prontuario");
        return mv;
    }

    @RequestMapping("{paciente}/novo")
    public ModelAndView novo(@PathVariable("paciente") Long idPaciente, HttpSession httpSession) {
        ModelAndView mv = new ModelAndView("prontuario/manutencaoProntuario");
        
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        
        Prontuario prontuario = new Prontuario()
                .setLogin(login)
                .setResponsavel(login.getPessoa().getNmPessoa())
                .setDtProntuario(new Date())
                .setSinalVital(new SinalVital().setLogin(login));
        
        if(idPaciente != null){
            prontuario.setPaciente(new Paciente().setIdPaciente(idPaciente).setPessoa(new Pessoa().setNmPessoa(PacienteService.getNomePaciente(idPaciente))));
            prontuario.getSinalVital().setPaciente(prontuario.getPaciente());
            
            mv.addObject("nmPaciente", prontuario.getPaciente().getPessoa().getNmPessoa());
            mv.addObject("paciente", prontuario.getPaciente().getIdPaciente());
        }
        
        prontuario.setTurno(prontuarioService.getTurno());
        mv.addObject("prontuario", prontuario);
        mv.addObject("informa_paciente", Boolean.FALSE);
        
        return mv;
    }

    @PostMapping()
    public ModelAndView salvar(@Validated Prontuario prontuario, 
                               BindingResult result, 
                               RedirectAttributes attributes, 
                               @ModelAttribute("nmPaciente") String nmPaciente, 
                               @ModelAttribute("informa_paciente") Boolean informa_paciente, 
                               HttpSession httpSession) {

        prontuarioValidator.validate(prontuario, result);
        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView("prontuario/manutencaoProntuario");
            attributes.addFlashAttribute("informa_paciente", informa_paciente);
            mv.addObject("prontuario", prontuario);
            
            if(!Verify.nullsOrEmpty(nmPaciente)){
                mv.addObject("nmPaciente", nmPaciente);
                mv.addObject("paciente", prontuario.getPaciente().getIdPaciente());
            } else {
                mv.addObject("nmPaciente", null);
            }
            
            return mv;
        }
        
        if(prontuario.getPaciente() == null || prontuario.getPaciente().getIdPaciente() == null){
            result.addError(new ObjectError("Paciente", "Informe um paciente para gravar."));
            ModelAndView mv = new ModelAndView("prontuario/manutencaoProntuario");
            mv.addObject("prontuario", prontuario);
            mv.addObject(Key.CONTORLLER_NAME, "prontuario");
            attributes.addFlashAttribute("informa_paciente", Boolean.TRUE);
            
            if(!Verify.nullsOrEmpty(nmPaciente)){
                mv.addObject("nmPaciente", nmPaciente);
                mv.addObject("paciente", prontuario.getPaciente().getIdPaciente());
            } else {
                mv.addObject("nmPaciente", null);
            } 
            
            return mv;
        }
        
        prontuarioService.salvar(prontuario, httpSession);
        
        if (prontuario.getSinalVital() != null && prontuario.getSinalVital().getData() != null && prontuarioValidator.algoPreenchido(prontuario.getSinalVital())){
            sinalVitalService.salvar(prontuario.getSinalVital(), httpSession);
        }
        
        ModelAndView mv = new ModelAndView();
        if(prontuario.getPaciente().getPessoa() != null && !Verify.nullsOrEmpty(prontuario.getPaciente().getPessoa().getNmPessoa())){
            attributes.addFlashAttribute("idPacienteSucesso", prontuario.getPaciente().getIdPaciente());
            attributes.addFlashAttribute("nmPacienteSucesso", prontuario.getPaciente().getPessoa().getNmPessoa());
            mv.setViewName("redirect:/prontuario/novo");
        } else {
            mv.setViewName("redirect:/prontuario/" + prontuario.getPaciente().getIdPaciente());
        }
        
        return mv;
    }

    @RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
    public String excluir(@PathVariable Long codigo, RedirectAttributes attributes, HttpSession httpSession) {
        
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);

        Long idPaciente = null;
        Prontuario prontuario = prontuarioService.get(codigo);
        
        if(prontuario != null){
            idPaciente = prontuario.getPaciente().getIdPaciente();
            Boolean isAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
            
            if (isAdmin){
                prontuario.setPodeAlterar(true);
            } else {
                if(login == null || !login.getIdLogin().equals(prontuario.getLogin().getIdLogin())){
                    attributes.addFlashAttribute("mensagemErr", "Atenção! Você não tem permissão de efetuar esta operação.!");
                    return "redirect:/prontuario/" + idPaciente;
                }
            }
            
            if(prontuario.getPodeAlterar()){
                prontuarioService.excluir(codigo);
                attributes.addFlashAttribute("mensagem", "Excluído com sucesso!");
            } else {
                attributes.addFlashAttribute("mensagem", "O tempo para exclusão expirou.");
            }
            
        } else {
            attributes.addFlashAttribute("mensagemErr", "Prontuário não encontrado!");
        }
        
        
        return "redirect:/prontuario/" + idPaciente;
    }
    
    @PostMapping("{paciente}/pdf")
    public ModelAndView getPdf(@PathVariable("paciente") Long idPaciente, 
                               @RequestParam(value="arrCodigos[]") Long[] arrCodigos,
                               @RequestParam(value="enfermeiro", required = false, defaultValue = "false") boolean enfermeiro,
                               @RequestParam(value="medico", required = false, defaultValue = "false") boolean medico,
                               @RequestParam(value="tecEnfermeiro", required = false, defaultValue = "false") boolean tecEnfermeiro,
                               @RequestParam(value="cuidador", required = false, defaultValue = "false") boolean cuidador,
                               @RequestParam(value="assSocial", required = false, defaultValue = "false") boolean assSocial,
                               @RequestParam(value="fisioterapeuta", required = false, defaultValue = "false") boolean fisioterapeuta,
                               @RequestParam(value="nutricionista", required = false, defaultValue = "false") boolean nutricionista,
                               @RequestParam(value="outros", required = false, defaultValue = "false") boolean outros,
                               @RequestParam(required = false, defaultValue = "false") boolean imprimirDescricao,
                               @RequestParam(required = false, defaultValue = "false") boolean imprimirMedicamentos,
                               @RequestParam(required = false, defaultValue = "false") boolean imprimirSae,
                               @RequestParam(required = false) Long login,
                               @RequestParam(required = false) String descricao,
                               ModelAndView modelAndView, 
                               HttpSession httpSession) {
        
        Map<String, Object> parameterMap = new HashMap<>();
        
        Util.parametersToReport(report_prontuario, httpSession);
        
        parameterMap.put("datasource", new JRBeanCollectionDataSource( prontuarioService.relatorio(idPaciente, arrCodigos, null, enfermeiro, medico, tecEnfermeiro, cuidador, assSocial, fisioterapeuta, nutricionista, outros, null, null, descricao, login, imprimirMedicamentos, imprimirSae, imprimirDescricao)));
        modelAndView = new ModelAndView(report_prontuario, parameterMap);
        return modelAndView;
    }
}
