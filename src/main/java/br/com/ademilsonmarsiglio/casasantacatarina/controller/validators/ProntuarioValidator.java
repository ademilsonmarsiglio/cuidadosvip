package br.com.ademilsonmarsiglio.casasantacatarina.controller.validators;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SinalVital;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.Date;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author ademilson
 */
@Component
public class ProntuarioValidator implements Validator {

    /**
     * e
     *
     * @param clazz
     * @return
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return Prontuario.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Prontuario prontuario = (Prontuario) target;

        SinalVital sinalVital = prontuario.getSinalVital();
        if (sinalVital != null) {
            if (algoPreenchido(sinalVital)){
                Date data = sinalVital.getData();
                if (data == null) {
                    errors.rejectValue("sinalVital.data", "mandatory.sinalVital", "A data do Sinal Vital é obrigatoria.");
                }
            }
        }
    }
    
    public boolean algoPreenchido(SinalVital sinalVital){
        
        if(notIsEmpty(sinalVital.getFrequenciaCardiaca())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getFrequenciaRespiratoria())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getDor())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getHgt())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getLiquidoAdministrado_viaOral())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getLiquidoAdministrado_viaParenteral())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getLiquidoEliminado_fezes())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getLiquidoEliminado_urinaVolume())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getLiquidoEliminado_vomito())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getObservacao())){
            return true;
        }
        
        
        if(notIsEmpty(sinalVital.getPressaoArterial())){
            return true;
        }
        
        
        if(notIsEmpty(sinalVital.getPupilas())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getSaturacao())){
            return true;
        }
        
        if(notIsEmpty(sinalVital.getTemperatura())){
            return true;
        }
        
        return false;
    }
    
    private boolean notIsEmpty(String str){
        boolean nullsOrEmpty = Verify.nullsOrEmpty(str);
        return !nullsOrEmpty;
    }
}
