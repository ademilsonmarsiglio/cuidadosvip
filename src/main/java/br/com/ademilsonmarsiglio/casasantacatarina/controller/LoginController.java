package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.BasicJson;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.LoginService;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller()
@RequestMapping("/login")
public class LoginController {
    
    private final String CONTROLLER = "usuarios";

    @Autowired
    private LoginService loginService;

    @GetMapping()
    public ModelAndView login() {
        return login(new Login());
    }
    
    public ModelAndView login(Login login) {
        ModelAndView modelAndView = new ModelAndView("login/login");
        modelAndView.addObject("login", login);
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        return modelAndView;
    }
    
    @GetMapping("/lista")
    public ModelAndView lista(@ModelAttribute("filtro") Filter filtro, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("login/listaLogin");
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        
        Login sessionLogin = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);

        if (sessionLogin.getAdmin())    
            modelAndView.addObject("lista", loginService.filtrar(filtro.setSessionEstabelecimento(httpSession)));

        return modelAndView;
    }
    
    public ModelAndView showMessage(String mensagem, HttpSession httpSession) {

        ModelAndView modelAndView = new ModelAndView("login/listaLogin");
        modelAndView.addObject("lista", loginService.filtrar(new Filter().setSessionEstabelecimento(httpSession)));
        modelAndView.addObject("filtro", new Filter());
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        
        if(mensagem != null){
            modelAndView.addObject("mensagem", mensagem);
        }

        return modelAndView;
    }

    @GetMapping("{codigo}")
    public ModelAndView edicao(@PathVariable("codigo") Long codigo, HttpSession httpSession) {
        
        Login sessionLogin = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        Boolean sessionAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
        
        if(!sessionAdmin){
            if(sessionLogin != null && !sessionLogin.getIdLogin().equals(codigo)){
                return showMessage("Usuário sem permissão.", httpSession);
            }
        }
        
        
        Login login = loginService.getLogin(codigo);
        
        if(login.getSuperUser() && !sessionLogin.getSuperUser()){
            return showMessage("Usuário sem permissão.", httpSession);
        }
        
        if(login != null){
            return edicao(login);
        } else {
            return showMessage("Usuário nao encontrado!", httpSession);
        }
    }
    
    public ModelAndView edicao(Login login) {
        ModelAndView mv = new ModelAndView("login/manutencaoLogin");
        
        if(login == null){
            login.setPessoa(new Pessoa());
        }
        
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("login", login);
        return mv;
    }
    
    @RequestMapping("/novo")
    public ModelAndView novo(HttpSession httpSession) {
        Boolean sessionAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
        if(!sessionAdmin){
            return showMessage("Usuário sem permissão.", httpSession);
        }
        
        
        ModelAndView mv = new ModelAndView("login/manutencaoLogin");
        
        Login login = new Login();
        login.setPessoa(new Pessoa());
        
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject(login);
        return mv;
    }
    
    @PostMapping()
    public ModelAndView salvar(@Valid Login login, BindingResult result, RedirectAttributes attributes, HttpSession httpSession) {
        if (result.hasErrors()) {
            return edicao(login);
        }

        Login sessionLogin = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        
        this.loginService.salvar(login, httpSession);

        
        if(sessionLogin.getIdLogin().equals(login.getIdLogin())){
            return new ModelAndView("redirect:/index");
        } else {
            return showMessage(null, httpSession);
        }
    }
    
    @DeleteMapping("{codigo}")
    public String excluir(@PathVariable Long codigo, RedirectAttributes attributes, HttpSession httpSession) {
        
        Login sessionLogin = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        Boolean sessionAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
        
        if(!sessionAdmin){
            if(sessionLogin != null && !sessionLogin.getIdLogin().equals(codigo)){
                attributes.addFlashAttribute("mensagem", "Usuário sem permissão.");
                return "redirect:/login/lista";
            }
        }
        
        Login login = loginService.getLogin(codigo);
        
        if(login.getSuperUser() && !sessionLogin.getSuperUser()){
            attributes.addFlashAttribute("mensagem", "Usuário sem permissão.");
            return "redirect:/login/lista";
        }
        
        
        loginService.excluir(codigo);

        attributes.addFlashAttribute("mensagem", "Login excluído com sucesso!");
        if(sessionLogin.getIdLogin().equals(codigo)){
            return "redirect:/logout";
        } else {
            return "redirect:/login/lista";
        }
    }
    
    @PostMapping("/validar")
    public String validar(@Valid Login login, BindingResult result, HttpSession httpSession, Model model) {
        if (result.hasErrors()) {
            return login(login).getViewName();
        }
        
        Login login_ = loginService.validarLogin(login);

        if(login_ != null){
            login_.setSenha(null);
            httpSession.setAttribute(Key.LOGIN_SESSION, login_);
            httpSession.setAttribute(Key.LOGIN_SUPERUSER, login_.getSuperUser());
            httpSession.setAttribute(Key.LOGIN_ADMIN, login_.getSuperUser() ? true : login_.getAdmin());
            return "redirect:/index";
        } else {
            model.addAttribute("mensagem", "Usuario ou Senha invalido.");
            return login(login).getViewName();
        }
    }
    
    @GetMapping("/logout")
    public String logout(HttpSession httpSession){
        httpSession.setAttribute(Key.LOGIN_SESSION, null);
        httpSession.setAttribute(Key.LOGIN_ADMIN, null);
        httpSession.setAttribute(Key.LOGIN_SUPERUSER, null);
        return "redirect:/login";
    }
    
    @GetMapping("/getLista")
    public ResponseEntity<List<BasicJson>> getLista(HttpSession httpSession){
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        return ResponseEntity.ok(loginService.getListaResumida(login.getEstabelecimento()));
    }
}
