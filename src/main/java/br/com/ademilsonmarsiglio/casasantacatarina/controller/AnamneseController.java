package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.AnamnesePaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.ResponseRequest;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.AnamneseService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.PacienteService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

/**
 *
 * @author ademilsonmarsiglio
 */
@Controller
@RequestMapping("anamnese")
public class AnamneseController {
    
    private final String CONTROLLER = "anamnese";

    @Autowired private PacienteService pacienteService;
    @Autowired private AnamneseService service;
    
    @Autowired
    @Qualifier("report_anamnese_paciente")
    private JasperReportsPdfView report_anamnese_paciente;
    
    
    @GetMapping
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro, HttpServletRequest request, HttpServletResponse response) {
        
//        HttpSession session = request.getSession();
//        
//        Login sessionLogin = (Login) session.getAttribute(Key.LOGIN_SESSION);

//        if (sessionLogin.getAdmin()) {
            ModelAndView modelAndView = home(filtro);
            

            return modelAndView;
//        } else {
//            try {
//                response.sendRedirect(request.getContextPath() + "/index");
//            } catch (IOException ex) {
//                Logger.getLogger(DiagnosticoPacienteController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            return null;
//        }
    }
    
    private ModelAndView home(Filter filtro) {
        ModelAndView modelAndView = new ModelAndView(CONTROLLER + "/" + CONTROLLER);
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        
        if (filtro != null && filtro.getId() != null) {
            modelAndView.addObject("filtro", filtro);

            if(filtro.getId() != null && filtro.getDescricao() != null){
                modelAndView.addObject("idPaciente", filtro.getId());
                modelAndView.addObject("nmPaciente", filtro.getDescricao());
            }
            
        } else {
            modelAndView.addObject("mensagem", "Informe o paciente para filtrar");
        }
        
        return modelAndView;
    }
    
    
    
     
    @PostMapping(value = "getLista")
    public ResponseEntity<ResponseRequest<List<AnamnesePaciente>>> getLista(@RequestBody Filter filtro) {
        if (filtro == null || filtro.getId() == null) {
            return ResponseEntity.ok(ResponseRequest.noOk("Código do paciente não informado."));
        }
        
        List<AnamnesePaciente> lista = service.lista(filtro);
        
        if (lista.isEmpty()) {
            return ResponseEntity.ok(ResponseRequest.noOk("Nenhum resultado para o conteúdo filtrado."));
        }
        
        return ResponseEntity.ok(ResponseRequest.ok("ok", lista));
        
    }
    
    @PostMapping(value = "formulario")
    public ModelAndView formulario(AnamnesePaciente.ParamEdicao param) {
        
        ModelAndView modelAndView = null;
        if (param == null) {
            modelAndView =  home(null);
            modelAndView.addObject("mensagem", "Parametros obrigatórios não informados.");
            return modelAndView;
        }
        
        
        modelAndView = new ModelAndView(CONTROLLER + "/manutencaoAnamnese");
        AnamnesePaciente obj = null;
        
        
        if (param.getId() != null) { //Edicao
            obj = service.get(param.getId());
        } 
        
        
        if (obj == null) { //Insercao 
            Paciente paciente = null;
            if (param.getIdPaciente()!= null) {
                paciente = pacienteService.getPaciente(param.getIdPaciente());
            }
            
            obj = new AnamnesePaciente().setPaciente(paciente);
        }
        
        
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("obj", obj);
        modelAndView.addObject("idPaciente", param.getIdPaciente());
        modelAndView.addObject("nmPaciente", param.getNome());

        return modelAndView;
    }
    
    
    
    
    @PostMapping(value = "salvar")
    public ModelAndView salvar(@Validated AnamnesePaciente obj,  BindingResult result, RedirectAttributes attributes, HttpSession session) {
        
        if (obj == null) {
            return formulario(new AnamnesePaciente.ParamEdicao().setMensagem("Parametros não informados...."));
        }
        
        
        service.salvar(obj, session);
        
        Filter filtro = new Filter()
                        .setId(obj.getPaciente().getIdPaciente())
                        .setDescricao(obj.getPaciente().getPessoa().getNmPessoa());
        
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/anamnese");
        attributes.addFlashAttribute(Key.CONTORLLER_NAME, CONTROLLER);
        attributes.addFlashAttribute("filtro", filtro);
        modelAndView.addObject("id", filtro.getId());
        modelAndView.addObject("descricao", filtro.getDescricao());
        return modelAndView;
    }
    
    
    @DeleteMapping("{codigo}")
    public ModelAndView excluir(@PathVariable("codigo") Long codigo, RedirectAttributes attributes, HttpSession httpSession) {
        
        AnamnesePaciente anamnesePaciente = service.get(codigo);
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/anamnese");
        
        Filter filtro = new Filter()
                        .setId(anamnesePaciente.getPaciente().getIdPaciente())
                        .setDescricao(anamnesePaciente.getPaciente().getPessoa().getNmPessoa());
        attributes.addFlashAttribute("filtro", filtro);
        mv.addObject("idPaciente", anamnesePaciente.getPaciente().getIdPaciente());
        mv.addObject("nmPaciente", anamnesePaciente.getPaciente().getPessoa().getNmPessoa());
        
//        if(!permissaoExclusao){
//            attributes.addFlashAttribute("mensagemError", "Sem Permissão para excluir.");
//        } else {
            if(service.excluir(anamnesePaciente)){
                attributes.addFlashAttribute("mensagem", "Excluído com sucesso!");
            } else {
                attributes.addFlashAttribute("mensagemErr", "Erro ao excluir.");
            }
//        }
        
        return mv;
    }
    
    
    @PostMapping("/pdf")
    public ModelAndView getPdf(ModelAndView modelAndView, 
                               @RequestParam(value="codigos[]") Long[] arrCodigos, 
                               HttpSession httpSession) {
        
        Util.parametersToReport(report_anamnese_paciente, httpSession);
    
        Map<String, Object> parameterMap = new HashMap<>();
        
        parameterMap.put("datasource", new JRBeanCollectionDataSource( service.relatorio(arrCodigos)));
        modelAndView = new ModelAndView(report_anamnese_paciente, parameterMap);
        return modelAndView;
    }
}
