package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.BasicJson;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.ProdutoService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.MonetaryPropertyEditor;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller()
@RequestMapping("/produtos")
public class ProdutoController {

    private final String CONTROLLER = "produtos";
    private final String INDEX = "produtos/produtos";
    private final String FORM = "produtos/manutencaoProduto";

    @Autowired
    private ProdutoService produto_service;

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(Double.class, "preco", new MonetaryPropertyEditor());
        binder.registerCustomEditor(Double.class, "custo", new MonetaryPropertyEditor());
        binder.registerCustomEditor(Double.class, "qtdEstoqueInicial", new MonetaryPropertyEditor());
    }

//    @Autowired
//    @Qualifier("report_produto")
//    private JasperReportsPdfView report_produto;
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro, HttpSession httpSession) {
        ModelAndView mv = new ModelAndView(INDEX);
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("filtro", filtro);

        return mv;
    }

    private ModelAndView showMessage(String mensagem) {

        ModelAndView mv = new ModelAndView(INDEX);
        mv.addObject("filtro", new Filter());
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);

        if (mensagem != null) {
            mv.addObject("mensagem", mensagem);
        }

        return mv;
    }
    
    @PostMapping("/getLista")
    public @ResponseBody String getLista(@RequestParam(value = "filtro_consulta", required = false) String jsonFiltro, @RequestParam("base_url") String baseUrl, HttpSession httpSession) {
        
        Filter filtro = null;
        
        if (jsonFiltro != null){
            filtro = new Gson().fromJson(jsonFiltro, new TypeToken<Filter>(){}.getType());
        }
        
        List<Produto> lista = produto_service.filtrar(filtro, httpSession);
        
        StringBuilder s = new StringBuilder();
        
        if (Verify.nullsOrEmpty(lista)){
            if (filtro != null && filtro.getOffset() != null && filtro.getOffset() > 0){
                return null;
            } else {
                addStr(s, "<tr><td colspan=\"5\">Nenhum Registro encontrado</td></tr>");
            }
        } else {
            
            Boolean isAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
            if (isAdmin == null){
                isAdmin = false;
            }
            
            for (Produto produto : lista) {
                addStr(s, "<tr>");
                    addStr(s, "<td>%s</td>", produto.getDsProduto());
                    addStr(s, "<td>%s</td>", Util.formatNumber(produto.getQtdPacientes()));
                    
                    if (produto.getIdEstoqueGeral() != null && produto.getIdEstoqueGeral() > 0) {
                        addStr(s, "<td><a href=\"historicoEstoque?idEstoque=%s&produto=%s\">%s</a></td>", produto.getIdEstoqueGeral(), Util.encodeURL(produto.getDsProduto()), Util.formatNumber(produto.getQtdGeral()));
                    } else {
                        addStr(s, "<td>%s</td>", Util.formatNumber(produto.getQtdGeral()));
                    }
                    
                    
                    if (isAdmin) {
                        addStr(s, "<td>%s</td>", Util.ifNullEmpty(Util.formatNumber(produto.getCusto())));
                        addStr(s, "<td>%s</td>", Util.ifNullEmpty(Util.formatNumber(produto.getPreco())));
                    
                        addStr(s, "<td class=\"text-right\">");
                            //btn editar
                            addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%sprodutos/%s\"", baseUrl, produto.getIdProduto());
                            addStr(s, "title=\"Editar\" rel=\"tooltip\" data-placement=\"top\">");
                            addStr(s, "<span class=\"glyphicon glyphicon-pencil\"></span></a>");
                            //btn excluir
                            addStr(s, "<a class=\"btn btn-link btn-xs\" data-toggle=\"modal\" data-target=\"#confirmacaoExclusaoModal\" ");
                            addStr(s, "data-codigo=\"%s\" data-descricao=\"%s\"", produto.getIdProduto(), produto.getDsProduto());
                            addStr(s, "title=\"Excluir\" rel=\"tooltip\" data-placement=\"top\">");
                            addStr(s, "<span class=\"glyphicon glyphicon-remove\"></span></a>");
                        addStr(s, "</td>");
                    }
                addStr(s, "</tr>");
            }
        }
        
        return s.toString();
    }
    
    private void addStr(StringBuilder s, String str, Object... params){
        s.append(String.format(str.concat(" \n"), params));
    }

    @GetMapping("{codigo}")
    public ModelAndView edicao(@PathVariable("codigo") Long codigo, HttpSession httpSession) {

        Produto produto = produto_service.getProduto(codigo);
        
        if (produto != null) {
            Estoque estoque = produto_service.getEstoqueProduto(codigo);
            if(estoque == null){
                Login login = (Login)httpSession.getAttribute(Key.LOGIN_SESSION);
                estoque = produto_service.insereEstoqueProduto(produto, login);
            }
            produto.setQtdEstoqueInicial(estoque.getQtd());
            
            ModelAndView mv = new ModelAndView(FORM);
            mv.addObject("obj", produto);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        } else {
            return showMessage("Produto não encontrado!");
        }
    }

    @RequestMapping("/novo")
    public ModelAndView novo() {
        ModelAndView mv = new ModelAndView(FORM);

        mv.addObject("obj", new Produto());
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        return mv;
    }

    @PostMapping()
    public ModelAndView salvar(@ModelAttribute("obj") @Validated Produto produto, BindingResult result, RedirectAttributes attributes, HttpSession httpSession) {

        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView(FORM);
            mv.addObject("obj", produto);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }

        this.produto_service.salvar(produto, httpSession);

        return listar(new Filter(), httpSession);
    }

    @DeleteMapping("{codigo}")
    public String excluir(@PathVariable Long codigo, RedirectAttributes attributes) {
        if (produto_service.excluir(codigo)) {
            attributes.addFlashAttribute("mensagem", "Produto excluído com sucesso!");
        } else {
            attributes.addFlashAttribute("mensagemError", produto_service.getMessageJPA());
        }

        return "redirect:/" + CONTROLLER;
    }
    
    @GetMapping("/getProdutos")
    public @ResponseBody List<BasicJson> getNamePacientes(@RequestParam("phrase") String phrase){
        return produto_service.getProdutosJson(phrase);
    }

//    @GetMapping("/pdf")
//    public ModelAndView getPdf(ModelAndView modelAndView, @ModelAttribute("filtro") Filter filtro, HttpSession httpSession) {
//        Map<String, Object> parameterMap = new HashMap<>();
//        
//        parameterMap.put("datasource", new JRBeanCollectionDataSource( produto_service.filtrar(filtro, httpSession)));
//        modelAndView = new ModelAndView(report_produto, parameterMap);
//        return modelAndView;
//    }
}
