package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.SinalVital;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.SinalVitalService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

/**
 * @since 01/08/2017
 * @author ademilson
 */
@Controller()
@RequestMapping("/sinaisVitais")
public class SinalVitalController {

    private final String CONTROLLER = "sinaisVitais";

    @Autowired
    private SinalVitalService sinalVital_service;
    
    @Autowired
    @Qualifier("report_sinalVital")
    private JasperReportsPdfView report_sinalVital;
    
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro) {
        
        ModelAndView modelAndView = new ModelAndView("sinaisVitais/sinaisVitais");
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("filtro", filtro);
        
        if(filtro != null && filtro.getId() != null && filtro.getDescricao() != null){
//            modelAndView.addObject("lista", sinalVital_service.list(filtro));
            modelAndView.addObject("idPaciente", filtro.getId());
            modelAndView.addObject("nmPaciente", filtro.getDescricao());
            modelAndView.addObject("mensagem", null);
        } else {
            modelAndView.addObject("mensagem", "Informe o paciente e clique em filtrar");
        }
        
        return modelAndView;
    }
    
    @PostMapping("/getLista")
    public @ResponseBody List<SinalVital> getLista(@RequestParam(value = "filtro_consulta", required = false) String jsonFiltro) {
        
        Filter filtro = null;
        
        if (jsonFiltro != null){
            filtro = new Gson().fromJson(jsonFiltro, Filter.class);
        }
        
        return sinalVital_service.list(filtro);
    }
    
    @PostMapping("/novo")
    public ModelAndView novo(@RequestParam("codigo") Long idPaciente, @RequestParam("nome") String nome, HttpSession httpSession) {
        ModelAndView mv;
        
        if(idPaciente == null || nome == null || nome.trim().isEmpty()){
            mv = listar(new Filter());
            mv.addObject("mensagemError", "Selecione um paciente.");
            return mv;
        }
        
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);

        if(login == null || login.getIdLogin() == null){
            mv = listar(new Filter());
            mv.addObject("mensagemError", "Login está nulo.");
            return mv;
        }

        mv = new ModelAndView("sinaisVitais/manutencaoSinaisVitais");
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("obj", new SinalVital().setPaciente(new Paciente().setIdPaciente(idPaciente).setPessoa(new Pessoa().setNmPessoa(nome))).setLogin(login));
        mv.addObject("idPaciente", idPaciente);
        mv.addObject("nmPaciente", nome);
        
        return mv;
    }
    
    @GetMapping("/editar/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Long idSinalVital, HttpSession httpSession) {
        ModelAndView mv;
        
        SinalVital sinalVital = sinalVital_service.get(idSinalVital);
        
        if(sinalVital == null){
            return listar(new Filter()).addObject("mensagemError", "Sinal Vital não encontrado!");
        }
        
        mv = new ModelAndView("sinaisVitais/manutencaoSinaisVitais");
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("obj", sinalVital);
        
        mv.addObject("idPaciente", sinalVital.getPaciente().getIdPaciente());
        mv.addObject("nmPaciente", sinalVital.getPaciente().getPessoa().getNmPessoa());
        
        return mv;
    }
    
    @PostMapping()
    public ModelAndView salvar(@ModelAttribute("obj") @Valid SinalVital obj,
            BindingResult bindingResult, RedirectAttributes attributes,
            HttpSession httpSession) {
        
        if (bindingResult.hasErrors()) {
            ModelAndView mv = new ModelAndView("sinaisVitais/manutencaoSinaisVitais");
            mv.addObject("obj", obj);
            mv.addObject("idPaciente", obj.getPaciente().getIdPaciente());
            mv.addObject("nmPaciente", obj.getPaciente().getPessoa().getNmPessoa());
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }
        
        if(!sinalVital_service.salvar(obj, httpSession)){
            ModelAndView mv = new ModelAndView("sinaisVitais/manutencaoSinaisVitais");
            mv.addObject("obj", obj);
            mv.addObject("idPaciente", obj.getPaciente().getIdPaciente());
            mv.addObject("nmPaciente", obj.getPaciente().getPessoa().getNmPessoa());
            mv.addObject("mensagemError", sinalVital_service.getMensagem());
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }
            
        
        return listar(getFilterPaciente(obj));
    }
    
    @DeleteMapping("{codigo}")
    public ModelAndView excluir(@PathVariable("codigo") Long idSinalVital, RedirectAttributes attributes, HttpSession httpSession) {
        
        SinalVital sinalVital = sinalVital_service.get(idSinalVital);
        
        if(sinalVital == null){
            return listar(new Filter()).addObject("mensagemError", "Sinal Vital não encontrado!");
        }
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/sinaisVitais");
        mv.addObject("id", sinalVital.getPaciente().getIdPaciente());
        mv.addObject("descricao", sinalVital.getPaciente().getPessoa().getNmPessoa());
        
        if(sinalVital_service.excluir(sinalVital)){
            attributes.addFlashAttribute("mensagem", "Sinal Vital excluído com sucesso!");
        } else {
            attributes.addFlashAttribute("mensagemError", "Erro ao excluir.");
        }
        
        return mv;
    }
    
    private Filter getFilterPaciente(SinalVital sinalVital){
        return new Filter().setId(sinalVital.getPaciente().getIdPaciente()).setDescricao(sinalVital.getPaciente().getPessoa().getNmPessoa());
    }
    
    @GetMapping("/pdf")
    public ModelAndView getPdf(ModelAndView modelAndView, 
                               @ModelAttribute("filtro") Filter filtro, 
                               HttpSession httpSession) {
        
        Util.parametersToReport(report_sinalVital, httpSession);
    
        Map<String, Object> parameterMap = new HashMap<>();
        
        parameterMap.put("datasource", new JRBeanCollectionDataSource(sinalVital_service.list(filtro)));
        modelAndView = new ModelAndView(report_sinalVital, parameterMap);
        return modelAndView;
    }
}
