package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.QualidadeVida;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.ResponseRequest;
import br.com.ademilsonmarsiglio.casasantacatarina.model.transients.RelQualidadeVida;
import br.com.ademilsonmarsiglio.casasantacatarina.service.PacienteService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.QualidadeVidaService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

/**
 *
 * @author ademilsonmarsiglio
 */
@Controller
@RequestMapping("qualidadeVida")
public class QualidadeVidaController {
    
    private final String CONTROLLER = "qualidadeVida";

    @Autowired private PacienteService pacienteService;
    @Autowired private QualidadeVidaService service;
    
    @Autowired
    @Qualifier("report_resultadoqualidadevida_paciente")
    private JasperReportsPdfView report_resultadoqualidadevida_paciente;
    
    @Autowired
    @Qualifier("report_formularioqualidadevida_paciente")
    private JasperReportsPdfView report_formularioqualidadevida_paciente;
    
    
    @GetMapping
    public ModelAndView listar(@ModelAttribute("filtro") QualidadeVida.ParamEdicao param, HttpServletRequest request, HttpServletResponse response) {
        
//        HttpSession session = request.getSession();
//        
//        Login sessionLogin = (Login) session.getAttribute(Key.LOGIN_SESSION);

//        if (sessionLogin.getAdmin()) {
            ModelAndView modelAndView = home(param);
            

            return modelAndView;
//        } else {
//            try {
//                response.sendRedirect(request.getContextPath() + "/index");
//            } catch (IOException ex) {
//                Logger.getLogger(DiagnosticoPacienteController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            return null;
//        }
    }
    
    private ModelAndView home(QualidadeVida.ParamEdicao param) {
        ModelAndView modelAndView = new ModelAndView(CONTROLLER + "/" + CONTROLLER);
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        
        if (param != null && param.getIdPaciente()!= null) {
            modelAndView.addObject("idPaciente", param.getIdPaciente());
            modelAndView.addObject("nmPaciente", param.getNmPaciente());
            
        } else {
            modelAndView.addObject("mensagem", "Informe o paciente para filtrar");
        }
        
        return modelAndView;
    }
    
    
    
     
    @PostMapping(value = "getLista")
    public ResponseEntity<ResponseRequest<List<QualidadeVida>>> getLista(@RequestBody QualidadeVida.ParamEdicao filtro) {
        if (filtro == null || filtro.getIdPaciente()== null) {
            return ResponseEntity.ok(ResponseRequest.noOk("Código do paciente não informado."));
        }
        
        List<QualidadeVida> lista = service.lista(filtro);
        
        if (lista.isEmpty()) {
            return ResponseEntity.ok(ResponseRequest.noOk("Nenhum resultado para o conteúdo filtrado."));
        }
        
        return ResponseEntity.ok(ResponseRequest.ok("ok", lista));
        
    }
    
    @PostMapping(value = "formulario")
    public ModelAndView formulario(QualidadeVida.ParamEdicao param) {
        
        ModelAndView modelAndView = null;
        if (param == null) {
            modelAndView =  home(null);
            modelAndView.addObject("mensagem", "Parametros obrigatórios não informados.");
            return modelAndView;
        }
        
        
        modelAndView = new ModelAndView(CONTROLLER + "/manutencao");
        QualidadeVida obj = param.getObjRetornoForm();
        
        
        if (param.getId() != null) { //Edicao
            obj = service.get(param.getId());
        } 
        
        
        if (obj == null) { //Insercao 
            Paciente paciente = null;
            if (param.getIdPaciente()!= null) {
                paciente = pacienteService.getPaciente(param.getIdPaciente());
            }
            
            obj = new QualidadeVida().setPaciente(paciente);
        }
        
        
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("obj", obj);
        modelAndView.addObject("idPaciente", param.getIdPaciente());
        modelAndView.addObject("nmPaciente", param.getNmPaciente());
        modelAndView.addObject("mensagemErr", param.getMensagem());

        return modelAndView;
    }
    
    
    
    
    @PostMapping(value = "salvar")
    public ModelAndView salvar(@Validated QualidadeVida obj,  BindingResult result, RedirectAttributes attributes, HttpSession session) {
        
        if (obj == null) {
            return formulario(new QualidadeVida.ParamEdicao().setMensagem("Parametros não informados...."));
        }
        
        if (result.hasErrors()) {
            return formulario(new QualidadeVida.ParamEdicao()
                    .setIdPaciente(obj.getPaciente() == null ? null : obj.getPaciente().getIdPaciente())
                    .setNmPaciente(obj.getPaciente() == null ? null : obj.getPaciente().getPessoa().getNmPessoa())
                    .setMensagem("Responda todas as perguntas para gravar!")
                    .setObjRetornoForm(obj));
        }
        
        
        service.salvar(obj, session);
        
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/" + CONTROLLER);
        attributes.addFlashAttribute(Key.CONTORLLER_NAME, CONTROLLER);
//        attributes.addFlashAttribute("filtro", filtro);
        modelAndView.addObject("idPaciente", obj.getPaciente().getIdPaciente());
        modelAndView.addObject("nmPaciente", obj.getPaciente().getPessoa().getNmPessoa());
        return modelAndView;
    }
    
    
    @DeleteMapping("{codigo}")
    public ModelAndView excluir(@PathVariable("codigo") Long codigo, RedirectAttributes attributes, HttpSession httpSession) {
        
        QualidadeVida qualidadeVida = service.get(codigo);
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/" + CONTROLLER);
        
//        Filter filtro = new Filter()
//                        .setId(qualidadeVida.getPaciente().getIdPaciente())
//                        .setDescricao(qualidadeVida.getPaciente().getPessoa().getNmPessoa());
//        attributes.addFlashAttribute("filtro", filtro);
        mv.addObject("idPaciente", qualidadeVida.getPaciente().getIdPaciente());
        mv.addObject("nmPaciente", qualidadeVida.getPaciente().getPessoa().getNmPessoa());
        
//        if(!permissaoExclusao){
//            attributes.addFlashAttribute("mensagemError", "Sem Permissão para excluir.");
//        } else {
            if(service.excluir(qualidadeVida)){
                attributes.addFlashAttribute("mensagem", "Excluído com sucesso!");
            } else {
                attributes.addFlashAttribute("mensagemErr", "Erro ao excluir.");
            }
//        }
        
        return mv;
    }
    
    
    @GetMapping("/formulario-qualidade-de-vida")
    public ModelAndView getPdfFormulario(HttpSession httpSession) {
        Util.parametersToReport(report_formularioqualidadevida_paciente, httpSession);
    
         
        
        
        
        ArrayList arrList = new ArrayList();
        arrList.add(new RelQualidadeVida(1, "Q.1 Até que ponto as perdas nos seus sentidos (por exemplo, audição, visão, paladar, olfato, tato), afetam a sua vida diária?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente").setDescricaoBlocos("Por favor, tenha em mente os seus valores, esperanças, prazeres e preocupações. <br>Pedimos que pense na sua vida <b>nas duas últimas semanas.</b><br><br>As seguintes questões perguntam sobre o <b>quanto</b> você tem tido certos sentimentos nas últimas duas semanas."));
        arrList.add(new RelQualidadeVida(2, "Q.2 Até que ponto a perda de, por exemplo, audição, visão, paladar, olfato, tato, afeta a sua capacidade de participar em atividades?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(3, "Q.3 Quanta liberdade você tem de tomar as suas próprias decisões?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(4, "Q.4 Até que ponto você sente que controla o seu futuro?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(5, "Q.5 O quanto você sente que as pessoas ao seu redor respeitam a sua liberdade?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(6, "Q.6 Quão preocupado você está com a maneira pela qual irá morrer?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(7, "Q.7 O quanto você tem medo de não poder controlar a sua morte?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(8, "Q.8 O quanto você tem medo de morrer?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(9, "Q.9 O quanto você teme sofrer dor antes de morrer?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(10, "Q.10 Até que ponto o funcionamento dos seus sentidos (por exemplo, audição, visão, paladar, olfato, tato) afeta a sua capacidade de interagir com outras pessoas?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente").setDescricaoBlocos("As seguintes questões perguntam sobre quão completamente você fez ou se sentiu apto \na fazer algumas coisas nas duas últimas semanas."));
        arrList.add(new RelQualidadeVida(11, "Q.11 Até que ponto você consegue fazer as coisas que gostaria de fazer?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(12, "Q.12 Até que ponto você está satisfeito com as suas oportunidades para continuar alcançando outras realizações na sua vida?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(13, "Q.13 O quanto você sente que recebeu o reconhecimento que merece na sua vida?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(14, "Q.14 Até que ponto você sente que tem o suficiente para fazer em cada dia?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(15, "Q.15 Quão satisfeito você está com aquilo que alcançou na sua vida?", "Muito insatisfeito", "Instatisfeito", "Nem satisfeito nem insatisfeito", "Satisfeito", "Muito Satisfeito").setDescricaoBlocos("As seguintes questões pedem a você que diga o quanto você se sentiu satisfeito, feliz ou bem sobre vários aspectos de sua vida nas duas últimas semanas."));
        arrList.add(new RelQualidadeVida(16, "Q.16 Quão satisfeito você está com a maneira com a qual você usa o seu tempo?", "Muito insatisfeito", "Instatisfeito", "Nem satisfeito nem insatisfeito", "Satisfeito", "Muito Satisfeito"));
        arrList.add(new RelQualidadeVida(17, "Q.17 Quão satisfeito você está com o seu nível de atividade?", "Muito insatisfeito", "Instatisfeito", "Nem satisfeito nem insatisfeito", "Satisfeito", "Muito Satisfeito"));
        arrList.add(new RelQualidadeVida(18, "Q.18 Quão satisfeito você está com as oportunidades que você tem para participar de atividades da comunidade?", "Muito insatisfeito", "Instatisfeito", "Nem satisfeito nem insatisfeito", "Satisfeito", "Muito Satisfeito"));
        arrList.add(new RelQualidadeVida(19, "Q.19 Quão feliz você está com as coisas que você pode esperar daqui para frente?", "Muito infeliz", "Infeliz", "Nem feliz nem infeliz", "Feliz", "Muito feliz"));
        arrList.add(new RelQualidadeVida(20, "Q.20 Como você avaliaria o funcionamento dos seus sentidos (por exemplo, audição, visão, paladar, olfato, tato)?", "Muito ruim", "Ruim", "Nem ruim nem boa", "Boa", "Muito boa"));
        arrList.add(new RelQualidadeVida(21, "Q.21 Até que ponto você tem um sentimento de companheirismo em sua vida?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente").setDescricaoBlocos("As seguintes questões se referem a qualquer relacionamento íntimo que você possa ter. \nPor favor, considere estas questões em relação a um companheiro ou uma pessoa próxima com a qual você pode\n compartilhar (dividir) sua intimidade mais do que com qualquer outra pessoa em sua vida."));
        arrList.add(new RelQualidadeVida(22, "Q.22 Até que ponto você sente amor em sua vida?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(23, "Q.23 Até que ponto você tem oportunidades para amar?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        arrList.add(new RelQualidadeVida(24, "Q.24 Até que ponto você tem oportunidades para ser amado?", "Nada", "Muito Pouco", "Mais ou menos", "Bastente", "Extremamente"));
        
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("datasource", new JRBeanCollectionDataSource(arrList));
        ModelAndView modelAndView = new ModelAndView(report_formularioqualidadevida_paciente, parameterMap);
        return modelAndView;
    }

    
    @PostMapping("/pdf")
    public ModelAndView getPdf(ModelAndView modelAndView, 
                               @RequestParam(value="codigos[]") Long[] arrCodigos, 
                               HttpSession httpSession) {
        
        Util.parametersToReport(report_resultadoqualidadevida_paciente, httpSession);
    
        Map<String, Object> parameterMap = new HashMap<>();
        
        parameterMap.put("datasource", new JRBeanCollectionDataSource( service.relatorio(arrCodigos)));
        modelAndView = new ModelAndView(report_resultadoqualidadevida_paciente, parameterMap);
        return modelAndView;
    }
}
