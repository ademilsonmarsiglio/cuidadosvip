package br.com.ademilsonmarsiglio.casasantacatarina.controller.validators;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author ademilson
 */
@Component
public class PacienteValidator implements Validator {

    /**
     * e
     *
     * @param clazz
     * @return
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return Paciente.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Paciente paciente = (Paciente) target;

        Responsavel responsavel = paciente.getResponsavelPrincipal();
        if (responsavel != null) {
            String nmResponsavel = null;
            
            if (responsavel != null) {
                nmResponsavel = responsavel.getPessoa().getNmPessoa();
            }

            if (nmResponsavel == null || nmResponsavel.trim().isEmpty()) {
                errors.rejectValue("responsavelPrincipal.pessoa.nmPessoa", "mandatory.responsavel", "O Responsavel principal e obrigatorio.");
            }
        }
    }
}
