package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.DiagnosticoPaciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.ResponseRequest;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.DiagnosticoPacienteService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.PacienteService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import com.mchange.v2.beans.BeansUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

/**
 * @since 12/02/2019
 * @author ademilsonmarsiglio
 */
@Controller()
@RequestMapping("/diagnosticoPaciente")
public class DiagnosticoPacienteController {
    
    private final String CONTROLLER = "diagnosticoPaciente";
    
    @Autowired
    private DiagnosticoPacienteService service;
    
    @Autowired
    @Qualifier("report_diagnosticos_condutas_paciente")
    private JasperReportsPdfView report_diagnosticos_condutas_paciente;
    
    @GetMapping
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro, HttpServletRequest request, HttpServletResponse response) {
        
        HttpSession session = request.getSession();
        
        Login sessionLogin = (Login) session.getAttribute(Key.LOGIN_SESSION);

        if (sessionLogin.getAdmin()) {
            ModelAndView modelAndView = new ModelAndView(CONTROLLER + "/" + CONTROLLER);
            modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            modelAndView.addObject("filtro", filtro);

            if(filtro != null && filtro.getId() != null && filtro.getDescricao() != null){
                modelAndView.addObject("idPaciente", filtro.getId());
                modelAndView.addObject("nmPaciente", filtro.getDescricao());
                modelAndView.addObject("mensagem", null);
            } else {
                modelAndView.addObject("mensagem", "Informe o paciente para filtrar");
            }

            return modelAndView;
        } else {
            try {
                response.sendRedirect(request.getContextPath() + "/index");
            } catch (IOException ex) {
                Logger.getLogger(DiagnosticoPacienteController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        
        
    }
    
    
    
    @PostMapping(value = "getLista")
    public ResponseEntity<ResponseRequest<List<DiagnosticoPaciente>>> getLista(@RequestBody Filter filtro) {
        if (filtro == null || filtro.getId() == null) {
            return ResponseEntity.ok(ResponseRequest.noOk("Código do paciente não informado."));
        }
        
        List<DiagnosticoPaciente> lista = service.getLista(filtro.getId(), true);
        
        if (lista.isEmpty()) {
            return ResponseEntity.ok(ResponseRequest.noOk("Nenhum resultado para o conteúdo filtrado."));
        }
        
        return ResponseEntity.ok(ResponseRequest.ok("ok", lista));
        
    }
    
    
    
    
    @PostMapping(value = "gravar")
    public ResponseEntity<ResponseRequest> gravar(@RequestBody DiagnosticoPaciente diagnosticoPaciente, HttpSession session) {

        if (diagnosticoPaciente.getId() != null) { //alterando?
            DiagnosticoPaciente diagBD = service.get(diagnosticoPaciente.getId());

            if (diagBD == null)
                return ResponseEntity.notFound().build();

            diagBD.setDescricao(diagnosticoPaciente.getDescricao());
            diagnosticoPaciente = diagBD;
        }
        
        if (!service.salvar(diagnosticoPaciente, session)) {
            return ResponseEntity.ok(ResponseRequest.noOk("Erro ao salvar."));
        }
        
        return ResponseEntity.ok(ResponseRequest.ok("ok", true));  
    }
    
    
    
    
    @DeleteMapping("{id}")
    public @ResponseBody boolean excluirDiagnostico(@PathVariable Long id, HttpSession session) {
        return service.excluir(id, session);
    }
    
    
    
    
    @PutMapping(value = "ordenar/{id}/{pos}")
    public @ResponseBody boolean ordenarDiagnostico(@PathVariable Integer id, @PathVariable String pos) {
        return service.ordenar(id, pos);  
    }
    
    
    @GetMapping("/pdf/DiagnosticoPorPaciente/{idPaciente}")
    public ModelAndView getPdf(@PathVariable Long idPaciente, HttpSession httpSession) {
        
        Util.parametersToReport(report_diagnosticos_condutas_paciente, httpSession);
    
        Map<String, Object> parameterMap = new HashMap();
        
        if(idPaciente != null){
            List<DiagnosticoPaciente> lista = service.getLista(idPaciente, true);
            parameterMap.put("datasource", new JRBeanCollectionDataSource(lista));
            parameterMap.put("paciente", PacienteService.getNomePaciente(idPaciente));  
        }
        
        ModelAndView modelAndView = new ModelAndView(report_diagnosticos_condutas_paciente, parameterMap);
        return modelAndView;
    }
    
}
