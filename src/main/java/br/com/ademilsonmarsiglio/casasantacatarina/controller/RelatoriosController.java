package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.model.transients.Relatorios;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.RelatoriosService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import com.google.gson.Gson;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

/**
 *
 * @author ademilson
 */
@Controller()
@RequestMapping("/relatorios")
public class RelatoriosController {
    private final String CONTROLLER = "relatorios";
    
    @Autowired
    @Qualifier("report_produtoByPaciente")
    private JasperReportsPdfView report_produtoByPaciente;
    
    @Autowired
    @Qualifier("report_prontuario")
    private JasperReportsPdfView report_prontuario;
    
    @Autowired
    @Qualifier("report_produtos")
    private JasperReportsPdfView report_produtos = null;
    
    
    
    
    @Autowired
    private RelatoriosService.ProdutoPorPacienteSer produtoPacienteService;
    
            @GetMapping("/produtosPorPaciente")
            public ModelAndView produtoPorPaciente(@ModelAttribute("filtro") Relatorios.ProdutoPorPaciente.Filter filtro){
                ModelAndView modelAndView = new ModelAndView("relatorios/produtoPorPaciente");
                modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
                modelAndView.addObject("filtro", filtro);


                if(filtro != null && filtro.getId() != null && filtro.getDescricao() != null){
                    modelAndView.addObject("lista", produtoPacienteService.getLista(filtro));
                    modelAndView.addObject("idPaciente", filtro.getId());
                    modelAndView.addObject("nmPaciente", filtro.getDescricao());
                    modelAndView.addObject("mensagem", null);
                } else {
                    modelAndView.addObject("mensagem", "Informe o paciente e clique em filtrar");
                }

                return modelAndView;
            }

            @PostMapping("/produtosPorPaciente/pdf")
            public ModelAndView getPdfProdutosPorPaciente(ModelAndView modelAndView, 
                                       @ModelAttribute("filtro") Relatorios.ProdutoPorPaciente.Filter filtro, 
                                       HttpSession httpSession) {

                Util.parametersToReport(report_produtoByPaciente, httpSession);

                Map<String, Object> parameterMap = new HashMap<>();
                if(filtro != null && filtro.getId() != null && filtro.getDescricao() != null){
                    parameterMap.put("datasource", new JRBeanCollectionDataSource(produtoPacienteService.getLista(filtro)));
                    parameterMap.put("paciente", filtro.getDescricao());
                }

                modelAndView = new ModelAndView(report_produtoByPaciente, parameterMap);
                return modelAndView;
            }
            
            
            
            
            
    @Autowired
    private RelatoriosService.ProntuarioPorPaciente prontuarioPorPaciente;
            @GetMapping("/prontuarioPorPaciente")
            public ModelAndView prontuarioPorPaciente(@ModelAttribute("filtro") Filter filtro){
                ModelAndView modelAndView = new ModelAndView("relatorios/prontuarioPorPaciente");
                modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
                filtro.setDate1(Util.addDays(new Date(), -7));
                filtro.setDate2(new Date());
                modelAndView.addObject("filtro", filtro);
                return modelAndView;
            }
            
            @PostMapping("/prontuarioPorPaciente/getLista")
            public @ResponseBody List<Paciente> listaProntuarioPorPaciente(@RequestParam(value = "filtro_consulta", required = false) String jsonFiltro ){
                Filter filtro = null;
                if (jsonFiltro != null){
                    filtro = new Gson().fromJson(jsonFiltro, Filter.class);
                }
                
                return prontuarioPorPaciente.getLista(filtro);
            }


            @PostMapping("/prontuarioPorPaciente/pdf")
            public ModelAndView getPdfProntuarioPorPaciente(@RequestParam(value="arrCodigos[]") Long[] arrCodigos,
                               @RequestParam(value="enfermeiro", required = false, defaultValue = "false") boolean enfermeiro,
                               @RequestParam(value="medico", required = false, defaultValue = "false") boolean medico,
                               @RequestParam(value="tecEnfermeiro", required = false, defaultValue = "false") boolean tecEnfermeiro,
                               @RequestParam(value="cuidador", required = false, defaultValue = "false") boolean cuidador,
                               @RequestParam(value="assSocial", required = false, defaultValue = "false") boolean assSocial,
                               @RequestParam(value="fisioterapeuta", required = false, defaultValue = "false") boolean fisioterapeuta,
                               @RequestParam(value="nutricionista", required = false, defaultValue = "false") boolean nutricionista,
                               @RequestParam(value="outros", required = false, defaultValue = "false") boolean outros,
                               @RequestParam(value="imprimirDescricao", required = false, defaultValue = "false") boolean imprimirDescricao,
                               @RequestParam(value="imprimirMedicamentos", required = false, defaultValue = "false") boolean imprimirMedicamentos,
                               @RequestParam(value="imprimirSae", required = false, defaultValue = "false") boolean imprimirSae,
                               @RequestParam(required = false) String data1,
                               @RequestParam(required = false) String data2,
                               @RequestParam(required = false) Long login,
                               ModelAndView modelAndView,
                               HttpSession httpSession) {

                Util.parametersToReport(report_prontuario, httpSession);

                Map<String, Object> parameterMap = new HashMap<>();
                parameterMap.put("datasource", new JRBeanCollectionDataSource(prontuarioPorPaciente.relatorio(arrCodigos, enfermeiro, medico, tecEnfermeiro, cuidador, assSocial, fisioterapeuta, nutricionista, outros, imprimirDescricao, imprimirMedicamentos, imprimirSae, data1, data2, login)));

                System.gc();
                
                modelAndView = new ModelAndView(report_prontuario, parameterMap);
                return modelAndView;
            }
            
            
            
            
            
    @Autowired
    private RelatoriosService.Produtos produtos;
            @GetMapping("/porProdutos")
            public ModelAndView relatorioPorProdutos(){
                ModelAndView modelAndView = new ModelAndView("relatorios/produtos");
                modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
                return modelAndView;
            }
            
            @PostMapping("/porProdutos/lista")
            public ResponseEntity<List<Produto>> listaProntuarioPorPaciente(@RequestBody Produto.FilterRelatorio filtro){
                return ResponseEntity.ok(produtos.getLista(filtro));
            }

            @PostMapping("/porProdutos/pdf")
            public ModelAndView getPdfPorProdutos(Produto.FilterRelatorio filtro, ModelAndView modelAndView, HttpSession httpSession) {
                Util.parametersToReport(report_produtos, httpSession);

                Map<String, Object> parameterMap = new HashMap<>();
                parameterMap.put("datasource", new JRBeanCollectionDataSource(produtos.getLista(filtro)));

                System.gc();
                
                modelAndView = new ModelAndView(report_produtos, parameterMap);
                return modelAndView;
            }
}
