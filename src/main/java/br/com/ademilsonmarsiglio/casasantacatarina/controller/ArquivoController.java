package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Arquivo;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.ArquivoService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.PacienteService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller()
@RequestMapping("/arquivos")
public class ArquivoController {

    private final Long maxSizeFile = 20L * 1024L * 1024L;
    private final String CONTROLLER = "arquivos";
    private final String INDEX = CONTROLLER + "/arquivos";
    private final String MANUTENCAO = CONTROLLER + "/manutencaoArquivos";

    @Autowired
    private ArquivoService arquivoService;

    @GetMapping("{paciente}")
    public ModelAndView listar(@PathVariable("paciente") Long idPaciente, @ModelAttribute("filtro") Filter filtro) {

        ModelAndView modelAndView = new ModelAndView(INDEX);
        modelAndView.addObject("lista", arquivoService.filtrar(idPaciente, filtro));
        modelAndView.addObject("nmPaciente", PacienteService.getNomePaciente(idPaciente));
        modelAndView.addObject("paciente", idPaciente);

        return modelAndView;
    }

    public ModelAndView showMessage(boolean aviso, Long idPaciente, String mensagem) {

        ModelAndView modelAndView = new ModelAndView(INDEX);
        modelAndView.addObject("lista", arquivoService.filtrar(idPaciente, new Filter()));
        modelAndView.addObject("filtro", new Filter());
        modelAndView.addObject("paciente", idPaciente);
        modelAndView.addObject("nmPaciente", PacienteService.getNomePaciente(idPaciente));

        if (mensagem != null) {
            if (aviso) {
                modelAndView.addObject("mensagem", mensagem);
            } else {
                modelAndView.addObject("mensagemError", mensagem);
            }

        }

        return modelAndView;
    }

    @RequestMapping("{paciente}/novo")
    public ModelAndView novo(@PathVariable("paciente") Long idPaciente) {
        ModelAndView modelAndView = new ModelAndView(MANUTENCAO);

        modelAndView.addObject("nmPaciente", PacienteService.getNomePaciente(idPaciente));
        modelAndView.addObject("paciente", idPaciente);
        modelAndView.addObject("maxFileSize", maxSizeFile);

        return modelAndView;
    }

    @PostMapping()
    public String salvar(@RequestParam(value = "file") MultipartFile file,
            @RequestParam(value = "idPaciente", required = false, defaultValue = "-1") Long idPaciente,
            HttpSession httpSession,
            RedirectAttributes redirectAttributes) {

        redirectAttributes.addAttribute("maxFileSize", maxSizeFile);
        
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("mensagemError", "Selecione um arquivo para enviar");
            return "redirect:/arquivos/" + idPaciente + "/novo";
        }

        if(file.getSize() > (maxSizeFile)){
            redirectAttributes.addFlashAttribute("mensagemError", "O arquivo infomado é muito grande.");
            return "redirect:/arquivos/" + idPaciente + "/novo";
        }
        
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        Arquivo arquivo = arquivoService.gravar(file, login, idPaciente);
        
        if(arquivo != null){
            try {
                byte[] bytes = file.getBytes();
                System.out.println("Arquivo será salvo em: " + arquivo.getLocalArquivo());
                Path path = Paths.get(arquivo.getLocalArquivo());
                File pasta = path.toFile().getParentFile();

                if (!pasta.exists()) {
                    pasta.mkdirs();
                }

                Files.write(path, bytes);

                redirectAttributes.addFlashAttribute("mensagem", "Enviado com sucesso " + file.getOriginalFilename());

            } catch (IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Erro ao gravara arquivo.", e);
                redirectAttributes.addFlashAttribute("mensagemError", "Erro ao enviar arquivo: " + file.getOriginalFilename());
            }
        } else {
            redirectAttributes.addFlashAttribute("mensagemError", "Erro ao enviar arquivo: " + file.getOriginalFilename());
        }
        
        return "redirect:/arquivos/".concat(idPaciente.toString());
    }

    @GetMapping("baixar/{codigo}")
    public HttpEntity<byte[]> download(@PathVariable Long codigo, RedirectAttributes attributes) {
        
        Arquivo arquivo = arquivoService.get(codigo);

        if (arquivo != null) {
            String localFile = arquivo.getLocalArquivo();
            File file = new File(localFile);
            
            if(file.exists()){
                Path path = Paths.get(localFile);
                byte[] data = null;
                try {
                    data = Files.readAllBytes(path);
                } catch (IOException ex) {
                    Logger.getLogger(ArquivoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if(data != null){
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.add("Content-Disposition", "attachment; filename=\""+arquivo.getNmArquivo()+"\"");
                    HttpEntity<byte[]> entity = new HttpEntity<byte[]>(data,httpHeaders);
                    return entity;
                } 
            }
        } 
        
        return null;
    }
    
    @RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
    public String excluir(@PathVariable Long codigo, RedirectAttributes redirectAttributes) {

        Long idPaciente = null;
        Arquivo arquivo = arquivoService.get(codigo);
        
        redirectAttributes.addAttribute("maxFileSize", maxSizeFile);

        if (arquivo != null) {
            String localArquivo = arquivo.getLocalArquivo();
            idPaciente = arquivo.getPaciente().getIdPaciente();
            if(arquivoService.excluir(codigo)){
                
                if(localArquivo != null){
                    excluiArquivo(localArquivo);
                }
                
                redirectAttributes.addFlashAttribute("mensagem", "Excluído com sucesso!");
            }
            
        } else {
            redirectAttributes.addFlashAttribute("mensagemErr", "Arquivo não encontrado!");
        }

        return "redirect:/" + CONTROLLER + "/" + idPaciente;
    }
    
    @Async
    private void excluiArquivo(String localArquivo){
        File file = new File(localArquivo);
        if(file.exists()){
            file.delete();
        }
    }

}
