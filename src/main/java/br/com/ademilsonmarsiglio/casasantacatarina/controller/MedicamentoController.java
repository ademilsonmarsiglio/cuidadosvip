package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.MedicamentoAplicacao;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.ProdutoJson;
import br.com.ademilsonmarsiglio.casasantacatarina.model.transients.ListaMedicamentoProntuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.bind.annotation.PostMapping;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.MedicamentoService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.MonetaryPropertyEditor;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;


/**
 * @author ademilson
 */
@Controller()
@RequestMapping("/medicamentos")
public class MedicamentoController {
    
    private final String CONTROLLER = "medicamentos";

    @Autowired
    private MedicamentoService medicamento_service;
    
    @Autowired
    @Qualifier("report_medicamento")
    private JasperReportsPdfView report_medicamento;
    
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(Double.class, "estoque.qtd", new MonetaryPropertyEditor());
        binder.registerCustomEditor(Double.class, "aplicacoes.aplicacao", new MonetaryPropertyEditor());
    }
    
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro) {
        
        ModelAndView modelAndView = new ModelAndView("medicamentos/medicamentos");
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("filtro", filtro);
        
        if(filtro != null && filtro.getId() != null && filtro.getDescricao() != null){
            modelAndView.addObject("idPaciente", filtro.getId());
            modelAndView.addObject("nmPaciente", filtro.getDescricao());
            modelAndView.addObject("mensagem", null);
        } else {
            modelAndView.addObject("mensagem", "Informe o paciente e clique em filtrar");
        }
        
        return modelAndView;
    }
    
    @PostMapping("/novo")
    public ModelAndView novo(@RequestParam("codigo") Long idPaciente, @RequestParam("nome") String nome, HttpSession httpSession) {
        ModelAndView mv;
        boolean permissao = permiteAlterar(httpSession);
        
        if(!permissao){
            mv = listar(new Filter().setId(idPaciente).setDescricao(nome));
            mv.addObject("mensagemError", "Sem Permissão para Inserir.");
        } else {
            if(idPaciente == null || nome == null || nome.trim().isEmpty()){
                mv = listar(new Filter());
                mv.addObject("mensagemError", "Selecione um paciente.");
                return mv;
            }

            mv = new ModelAndView("medicamentos/manutencaoMedicamentos");
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            mv.addObject("medicamento", new Medicamento().setPaciente(new Paciente().setIdPaciente(idPaciente).setPessoa(new Pessoa().setNmPessoa(nome))));
        }
        
        mv.addObject("idPaciente", idPaciente);
        mv.addObject("nmPaciente", nome);
        
        return mv;
    }
    
    @GetMapping("/editar/{codigo}")
    public ModelAndView editar(@PathVariable("codigo") Long idMedicamento, HttpSession httpSession) {
        ModelAndView mv;
        boolean permissao = permiteAlterar(httpSession);
        
        Medicamento medicamento = medicamento_service.get(idMedicamento);
        
        if (medicamento != null) {
            List<MedicamentoAplicacao> aplicacoes = medicamento_service.getAplicacoesMedicamentos(Arrays.asList(medicamento.getIdMedicamento()));
            if (aplicacoes != null)
                medicamento.setAplicacoes(aplicacoes);
        }
        
        if(!permissao){
            mv = listar(getFilterPaciente(medicamento));
            mv.addObject("mensagemError", "Sem Permissão para Editar.");
        } else {
            mv = new ModelAndView("medicamentos/manutencaoMedicamentos");
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            mv.addObject("medicamento", medicamento);
        }
        
        mv.addObject("idPaciente", medicamento.getPaciente().getIdPaciente());
        mv.addObject("nmPaciente", medicamento.getPaciente().getPessoa().getNmPessoa());
        
        
        
        return mv;
    }
    
    @PostMapping()
    public ModelAndView salvar(@Validated Medicamento medicamento, 
            BindingResult result, 
            @RequestParam("paciente.idPaciente") Long idPaciente, 
            @RequestParam("paciente.pessoa.nmPessoa") String nome, 
            RedirectAttributes attributes, 
            HttpSession httpSession) {
        
        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView("medicamentos/manutencaoMedicamentos");
            mv.addObject("medicamento", medicamento);
            mv.addObject("idPaciente", idPaciente);
            mv.addObject("nmPaciente", nome);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }
        
        //Verifica se o nome é vazio.
        if(Verify.nullsOrEmpty(medicamento.getEstoque().getProduto().getDsProduto())){
            result.addError(new ObjectError("estoque.produto.dsProduto", "O Medicamento não pode ficar sem descrição."));
            ModelAndView mv = new ModelAndView("medicamentos/manutencaoMedicamentos");
            mv.addObject("medicamento", medicamento);
            mv.addObject("idPaciente", idPaciente);
            mv.addObject("nmPaciente", nome);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }

        if(!medicamento_service.salvar(medicamento, httpSession)){
            
            result.addError(new ObjectError("estoque.produto.dsProduto", medicamento_service.msgGravar));
            ModelAndView mv = new ModelAndView("medicamentos/manutencaoMedicamentos");
            mv.addObject("medicamento", medicamento);
            mv.addObject("idPaciente", idPaciente);
            mv.addObject("nmPaciente", nome);
            mv.addObject("mensagemError", medicamento_service.msgGravar);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }
            
        
        //Recarregado a pagina desta forma para q não corra o risco de ao recarregar a pagina enviar as informações novamente.
        Filter filtro = getFilterPaciente(medicamento);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:medicamentos");
        attributes.addFlashAttribute(Key.CONTORLLER_NAME, CONTROLLER);
        attributes.addFlashAttribute("filtro", filtro);
        modelAndView.addObject("id", filtro.getId());
        modelAndView.addObject("descricao", filtro.getDescricao());
        return modelAndView;
    }
    
    @DeleteMapping("{codigo}")
    public ModelAndView excluir(@PathVariable("codigo") Long idMedicamento, RedirectAttributes attributes, HttpSession httpSession) {
        
        
        boolean permissaoExclusao = permiteAlterar(httpSession);
        Medicamento medicamento = medicamento_service.get(idMedicamento);
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/medicamentos");
        mv.addObject("id", medicamento.getPaciente().getIdPaciente());
        mv.addObject("descricao", medicamento.getPaciente().getPessoa().getNmPessoa());
        
        if(!permissaoExclusao){
            attributes.addFlashAttribute("mensagemError", "Sem Permissão para excluir.");
        } else {
            if(medicamento_service.excluir(medicamento)){
                attributes.addFlashAttribute("mensagem", "Medicamento excluído com sucesso!");
            } else {
                attributes.addFlashAttribute("mensagemError", "Erro ao excluir.");
            }
        }
        
        return mv;
    }
    
    private boolean permiteAlterar(HttpSession httpSession){
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        boolean permissao = false;
        if(login != null){
            permissao = login.getAdmin() || login.getTpLogin().startsWith("M");
        }
        
        return permissao;
    }
    
    private Filter getFilterPaciente(Medicamento medicamento){
        return new Filter().setId(medicamento.getPaciente().getIdPaciente()).setDescricao(medicamento.getPaciente().getPessoa().getNmPessoa());
    }
    
    @GetMapping("/pdf")
    public ModelAndView getPdf(ModelAndView modelAndView, 
                               @ModelAttribute("filtro") Filter filtro, 
                               HttpSession httpSession) {
        
        Util.parametersToReport(report_medicamento, httpSession);
    
        Map<String, Object> parameterMap = new HashMap<>();
        
        if(filtro != null && filtro.getId() != null && filtro.getDescricao() != null){
            List<Medicamento> lista = medicamento_service.list(filtro.getId(), filtro.getDescricao());
            lista = medicamento_service.getListaMedicamentosComAplicacoes(lista);
            
            parameterMap.put("datasource", new JRBeanCollectionDataSource(lista));
            parameterMap.put("paciente", filtro.getDescricao());
        }
        
        modelAndView = new ModelAndView(report_medicamento, parameterMap);
        return modelAndView;
    }
    
    @GetMapping("/getMedicamentos")
    public @ResponseBody List<ProdutoJson> getMedicamentos(@RequestParam(required = false, value = "idPaciente") Long idPaciente, @RequestParam(required = false, value = "phrase") String phrase){
        return medicamento_service.getMedicamentos(idPaciente, phrase);
    }
    
    @GetMapping("/byPaciente")
    public ResponseEntity<ListaMedicamentoProntuario> getMedicamentosByPaciente(@RequestParam("idPaciente") Long idPaciente) {
        List<Medicamento> list = medicamento_service.listar(idPaciente);
        
        ListaMedicamentoProntuario listaRetorno = new ListaMedicamentoProntuario();
        
        List<Medicamento> listManha = listaRetorno.getListManha();
        List<Medicamento> listTarde = listaRetorno.getListTarde();
        List<Medicamento> listNoite = listaRetorno.getListNoite();
        List<Medicamento> listSeNecessario = listaRetorno.getListSeNecessario();
        List<Medicamento> listSemAplicacao = listaRetorno.getListSemAplicacao();
        List<Medicamento> listGeral = listaRetorno.getListGeral();
        
        for (Medicamento medicamento : list) {
            
            if (Verify.equals(medicamento.getSeNecessario(), Boolean.TRUE)) {
                listSeNecessario.add(medicamento);
            } else if (medicamento.getIdMedicamento() == null) {
                listGeral.add(medicamento);
            } else {
                String hora = medicamento.getHora();
                
                if (!Verify.nullsOrEmpty(hora)) {
                    
                    int minutos = Util.timeToMinute(hora);
                    
                    //MANHA = Depois das 05:59 e antes da 13:00
                    if (minutos > 359 && minutos < 780) {
                        listManha.add(medicamento);
                    } else if (minutos >= 780 && minutos < 1140) { //TARDE = Depois da 13:00 e antes da 19:00
                        listTarde.add(medicamento);
                    } else {
                        listNoite.add(medicamento);
                    }
                    
                } else {
                    listSemAplicacao.add(medicamento);
                }
            }
        }
        
        Comparator ordenaNome = (Comparator<Medicamento>) (Medicamento o1, Medicamento o2) -> o1.getEstoque().getProduto().getDsProduto().compareTo(o2.getEstoque().getProduto().getDsProduto());
        Comparator ordenaHora = (Comparator<Medicamento>) new Comparator<Medicamento>() {
            @Override
            public int compare(Medicamento o1, Medicamento o2) {
                if (o1.getHora() == null) {
                    return 1;
                }
                if (o2.getHora() == null) {
                    return -1;
                }
                return o1.getHora().compareTo(o2.getHora());
            }
        };
        
        listManha.sort(ordenaNome);
        listManha.sort(ordenaHora);
        
        listTarde.sort(ordenaNome);
        listTarde.sort(ordenaHora);
        
        listNoite.sort(ordenaNome);
        listNoite.sort(ordenaHora);
        
        listSeNecessario.sort(ordenaNome);
        listSeNecessario.sort(ordenaHora);
        
        listSemAplicacao.sort(ordenaNome);
        
        listGeral.sort(ordenaNome);
        
        
        return ResponseEntity.ok(listaRetorno);
    }
    
    @PostMapping("/getLista")
    public ResponseEntity<List<Medicamento>> getLista(@RequestBody Medicamento.Filter filter, HttpSession httpSession) {
        List<Medicamento> lista = null;
        
        if( filter != null && 
            filter.getIdPaciente()!= null 
            && filter.getPaciente()!= null){
            
            lista = medicamento_service.list(filter.getIdPaciente(), filter.getPaciente());
            lista = medicamento_service.getListaMedicamentosComAplicacoes(lista);
        }
        
        return ResponseEntity.ok(lista);
    }
}
