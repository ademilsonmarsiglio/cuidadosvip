package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Estoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.HistoricoEstoque;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Medicamento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Produto;
import br.com.ademilsonmarsiglio.casasantacatarina.service.HistoricoEstoqueService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.MedicamentoService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.ProdutoService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.MonetaryPropertyEditor;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author ademilson
 * @since 05/07/2018
 */
@Controller()
@RequestMapping("/historicoEstoque")
public class HistoricoEstoqueController {

    private final String CONTROLLER = "historicoEstoque";
    
    @Autowired private HistoricoEstoqueService service;
    @Autowired private MedicamentoService medicamentoService;
    @Autowired private ProdutoService produtoService;
    
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(Double.class, "qtd", new MonetaryPropertyEditor());
        binder.registerCustomEditor(Double.class, "estoque.qtd", new MonetaryPropertyEditor());
    }
    
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") HistoricoEstoque.Filter filtro, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("historicoEstoque/historicoEstoque");

        Double qtdEstoque = 0D;
        
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("lista", null);
        modelAndView.addObject("idEstoque", filtro.getIdEstoque());
        modelAndView.addObject("produto", filtro.getProduto());
        
        
        if (filtro.getIdPaciente() == null && filtro.getIdEstoque() != null) {
            Medicamento medicamento = medicamentoService.getMedicamentoByIdEstoque(filtro.getIdEstoque());
            
            if (medicamento != null) {
                filtro.setIdPaciente(medicamento.getPaciente().getIdPaciente());
                filtro.setPaciente(medicamento.getPaciente().getPessoa().getNmPessoa());
                
                qtdEstoque = medicamento.getEstoque().getQtd();
            }
        }
        
        if ((qtdEstoque == null || qtdEstoque == 0D) && filtro.getIdEstoque() != null) {
            Estoque estoque = produtoService.getEstoqueById(filtro.getIdEstoque());
            if (estoque != null) {
                qtdEstoque = estoque.getQtd();
            }
        }

        modelAndView.addObject("qtdEstoque", qtdEstoque);
        modelAndView.addObject("idPaciente", filtro.getIdPaciente());
        modelAndView.addObject("paciente", filtro.getPaciente());
        return modelAndView;
    }
    
    @PostMapping("/novo")
    public ModelAndView novo(@RequestParam Long idEstoque, @RequestParam String produto, RedirectAttributes attributes, HttpSession httpSession) {
        ModelAndView mv;
        
        if(idEstoque == null || Verify.nullsOrEmpty(produto)){
            attributes.addFlashAttribute("mensagemErro", "Informe o medicamento para inserir!");
            return new ModelAndView("redirect:/historicoEstoque");
        }
        
        
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);

        if(login == null || login.getIdLogin() == null){
            attributes.addFlashAttribute("mensagemErro", "Login está nulo!");
            return new ModelAndView("redirect:/historicoEstoque");
        }
        
        Estoque estoque = produtoService.getEstoqueById(idEstoque);

        mv = new ModelAndView("historicoEstoque/manutencaoHistoricoEstoque");
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("idEstoque", idEstoque);
        mv.addObject("produto", produto);
        mv.addObject("obj", new HistoricoEstoque()
                .setLogin(login)
                .setEstoque(estoque
                        .setIdEstoque(idEstoque)
                        .setProduto(new Produto().setDsProduto(produto))
                )
        );
        
        return mv;
    }
    
    
    
    @GetMapping("/editar/{codigo}")
    public ModelAndView edicao(@PathVariable("codigo") Long codigo, HttpSession httpSession) {
        ModelAndView mv = null;
        
        Boolean isAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
        if (isAdmin) {
            
            HistoricoEstoque h = service.get(codigo);
            
            Double qtdEstoque = h.getEstoque().getQtd();
            if (qtdEstoque == null)
                qtdEstoque = 0D;
            
            Double qtd = h.getQtd();
            if (qtd == null)
                qtd = 0D;
            
            h.getEstoque().setQtd(qtdEstoque - qtd);
            
            mv = new ModelAndView("historicoEstoque/manutencaoHistoricoEstoque");
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            mv.addObject("obj", h);
            mv.addObject("idEstoque", h.getEstoque().getIdEstoque());
            mv.addObject("produto", h.getEstoque().getProduto().getDsProduto());
        } else {
            mv = new ModelAndView("redirect:/historicoEstoque");
        }
        
            
        return mv;
    }
    
    
    
    @PostMapping()
    public ModelAndView salvar(@ModelAttribute("obj") @Validated HistoricoEstoque historicoEstoque, BindingResult result, RedirectAttributes attributes, HttpSession httpSession, HttpServletRequest request) {
        
        System.out.println(request.getHeader("referer"));

        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView("historicoEstoque/manutencaoHistoricoEstoque");
            mv.addObject("obj", historicoEstoque);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv;
        }

        if (historicoEstoque != null && historicoEstoque.getData() != null) {
            if (!Verify.nullsOrEmpty(historicoEstoque.getHora())) {
                historicoEstoque.setData(Util.dateSetTime(historicoEstoque.getData(), historicoEstoque.getHora()));
            }
        }
        
        service.gravar(historicoEstoque);
        
        
        String params = "?idEstoque=" + historicoEstoque.getEstoque().getIdEstoque() + "&produto=";
        try {
            params += URLEncoder.encode(historicoEstoque.getEstoque().getProduto().getDsProduto(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HistoricoEstoqueController.class.getName()).log(Level.SEVERE, "erro ao encodar query", ex);
        }

        return new ModelAndView("redirect:/historicoEstoque" + params) ;
    }
    
    
    
    
    @DeleteMapping("{codigo}")
    public ModelAndView excluir(@PathVariable Long codigo, RedirectAttributes attributes, HttpSession httpSession) {

        HistoricoEstoque h = service.get(codigo);
        
        String params = "?idEstoque=" + h.getEstoque().getIdEstoque() + "&produto=";
        String produto = h.getEstoque().getProduto().getDsProduto();
        
        Boolean isAdmin = (Boolean) httpSession.getAttribute(Key.LOGIN_ADMIN);
        if (isAdmin) {
            service.excluir(codigo);
        }
        
        
        ModelAndView mv = new ModelAndView();
        try {
            params += URLEncoder.encode(produto, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HistoricoEstoqueController.class.getName()).log(Level.SEVERE, "erro ao encodar query", ex);
        }
        
        mv.setViewName("redirect:/historicoEstoque" + params);
        
        return mv;
    }
    
    
    @PostMapping("/getLista")
    public ResponseEntity<List<HistoricoEstoque>> getLista(@RequestBody HistoricoEstoque.Filter filter, HttpSession httpSession) {
        return ResponseEntity.ok(service.getLista(filter));
    }
}
