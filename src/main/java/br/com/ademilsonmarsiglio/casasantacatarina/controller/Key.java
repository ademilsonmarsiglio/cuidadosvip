package br.com.ademilsonmarsiglio.casasantacatarina.controller;

/**
 *
 * @author ademilson
 */
public class Key {
    public static final String LOGIN_SESSION = "user_id";
    public static final String LOGIN_SUPERUSER = "user_id_su"; //retorna um boolean
    public static final String LOGIN_ADMIN = "user_id_admin"; //retorna um boolean
    public static final String CONTORLLER_NAME = "controller_name";
    public static final String FILTRO_PRONTUARIO_DATA1 = "filtro.pront.dat1"; //date
    public static final String FILTRO_PRONTUARIO_DATA2 = "filtro.pront.dat2"; //date
    public static final String FILTRO_PRONTUARIO_MUDOU_DATA1 = "filtro.pront.changedat1"; //boolean
    public static final String FILTRO_PRONTUARIO_MUDOU_DATA2 = "filtro.pront.changedat2"; //boolean
}
