package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.controller.validators.PacienteValidator;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Login;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Paciente;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Responsavel;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.BasicJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.MedicamentoService;
import br.com.ademilsonmarsiglio.casasantacatarina.service.PacienteService;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Util;
import br.com.ademilsonmarsiglio.casasantacatarina.util.Verify;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;

@Controller()
@RequestMapping("/pacientes")
public class PacienteController {
    
    private final String CONTROLLER = "pacientes";

    @Autowired
    private PacienteService paciente_service;
    
    @Autowired
    private MedicamentoService medicamento_service;
    
    @Autowired
    private PacienteValidator pacienteValidator;
    
    @Autowired
    @Qualifier("report_paciente")
    private JasperReportsPdfView report_paciente;
    
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("pacientes/pacientes");
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        
        return modelAndView;
    }
    
    private ModelAndView showMessage(String mensagem) {

        ModelAndView modelAndView = new ModelAndView("pacientes/pacientes");
//        modelAndView.addObject("pacientes", paciente_service.filtrar(new Filter(), httpSession));
        modelAndView.addObject("filtro", new Filter());
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        
        if(mensagem != null){
            modelAndView.addObject("mensagem", mensagem);
        }

        return modelAndView;
    }
    
    @PostMapping("/getLista")
    public @ResponseBody String getLista(@RequestParam(value = "filtro_consulta", required = false) String jsonFiltro, @RequestParam("base_url") String baseUrl, HttpSession httpSession) {
        
        Filter filtro = null;
        
        if (jsonFiltro != null){
            filtro = new Gson().fromJson(jsonFiltro, new TypeToken<Filter>(){}.getType());
        }
        
        List<Paciente> lista = paciente_service.filtrar(filtro, httpSession);
        
        StringBuilder s = new StringBuilder();
        
        if (Verify.nullsOrEmpty(lista)){
            if (filtro != null && filtro.getOffset() != null && filtro.getOffset() > 0){
                return null;
            } else {
                addStr(s, "<tr><td colspan=\"5\">Nenhum Registro encontrado</td></tr>");
            }
        } else {
            
            for (Paciente paciente : lista) {
                addStr(s, "<tr ondblclick=\"'javascript:rowClicked('%s');'\">", paciente.getIdPaciente());
                    addStr(s, "<td>");
                        addStr(s, "<input type=\"checkbox\" value=\"%s\"/>", paciente.getIdPaciente());
                    addStr(s, "</td>");
                    addStr(s, "<td>");
                        addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%sprontuario/%s\">", baseUrl, paciente.getIdPaciente());
                        addStr(s, "%s", paciente.getPessoa().getNmPessoa());
                        addStr(s, "</a>");
                    addStr(s, "</td>");    
                    addStr(s, "<td>%s</td>", Util.formatDate(paciente.getPessoa().getDtNascimento()));
                    addStr(s, "<td>%s</td>", paciente.getPessoa().getCpf());
                    addStr(s, "<td>%s</td>", paciente.getPessoa().getFone());
                    addStr(s, "<td class=\"text-right\">");
                        //btn prontuario
                        addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%sprontuario/%s\"", baseUrl, paciente.getIdPaciente());
                        addStr(s, "title=\"Prontuario\" rel=\"tooltip\" data-placement=\"top\">");
                        addStr(s, "<span class=\"fa fa-list-alt fa-fw\"></span></a>");
                        
                        //btn responsavel
                        addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%sresponsavel/%s\"", baseUrl, paciente.getIdPaciente());
                        addStr(s, "title=\"Responsaveis\" rel=\"tooltip\" data-placement=\"top\">");
                        addStr(s, "<span class=\"fa fa-user fa-fw\"></span></a>");
                        
                        //btn arquivos
                        addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%sarquivos/%s\"", baseUrl, paciente.getIdPaciente());
                        addStr(s, "title=\"Arquivos\" rel=\"tooltip\" data-placement=\"top\">");
                        addStr(s, "<span class=\"fa fa-file-archive-o fa-fw\"></span></a>");
                        
                        //btn editar
                        addStr(s, "<a class=\"btn btn-link btn-xs\" href=\"%spacientes/%s\"", baseUrl, paciente.getIdPaciente());
                        addStr(s, "title=\"Editar\" rel=\"tooltip\" data-placement=\"top\">");
                        addStr(s, "<span class=\"glyphicon glyphicon-pencil\"></span></a>");
                        //btn excluir
                        addStr(s, "<a class=\"btn btn-link btn-xs\" data-toggle=\"modal\" data-target=\"#confirmacaoExclusaoModal\" ");
                        addStr(s, "data-codigo=\"%s\" data-descricao=\"%s\"", paciente.getIdPaciente(), paciente.getPessoa().getNmPessoa());
                        addStr(s, "title=\"Excluir\" rel=\"tooltip\" data-placement=\"top\">");
                        addStr(s, "<span class=\"glyphicon glyphicon-remove\"></span></a>");
                    addStr(s, "</td>");
                addStr(s, "</tr>");
            }
        }
        
        return s.toString();
    }
    
    private void addStr(StringBuilder s, String str, Object... params){
        s.append(String.format(str.concat(" \n"), params));
    }

    @GetMapping("{codigo}")
    public ModelAndView edicao(@PathVariable("codigo") Long codigo, HttpSession httpSession) {
        
        Paciente paciente = paciente_service.getPaciente(codigo);
        
        if(paciente != null){
            ModelAndView mv = new ModelAndView("pacientes/manutencaoPaciente");
            mv.addObject("paciente", paciente);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
//            mv.addObject("permissaoMedicamentos", permiteAlterarMedicamento(httpSession));
            return mv;
        } else {
            return showMessage("Paciente não encontrado!");
        }
    }
    
    @RequestMapping("/novo")
    public ModelAndView novo(HttpSession httpSession) {
        ModelAndView mv = new ModelAndView("pacientes/manutencaoPaciente");
        
        Paciente paciente = new Paciente()
                .setResponsavelPrincipal(new Responsavel().setPrincipal(Boolean.TRUE).setPessoa(new Pessoa()))
                .setResponsavel(new Responsavel().setPessoa(new Pessoa()))
                .setDtInclusaoPaciente(new Date());
        
        mv.addObject("isInsert", true);
        mv.addObject("paciente", paciente);
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
//        mv.addObject("permissaoMedicamentos", permiteAlterarMedicamento(httpSession));
        return mv;
    }

    @PostMapping()
    public String salvar(@Validated Paciente paciente, BindingResult result, Model model, RedirectAttributes attributes, HttpSession httpSession) {
        
        pacienteValidator.validate(paciente, result);
        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView("pacientes/manutencaoPaciente");
            mv.addObject("paciente", paciente);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            model.addAttribute("isInsert", paciente.getIdPaciente() == null);
            return mv.getViewName();
        }
        
        if(paciente_service.salvar(paciente, httpSession)){
//            if(permiteAlterarMedicamento(httpSession)){
//                if(!medicamento_service.salvar(paciente.getListaMedicamento(), paciente, httpSession)){
//                    attributes.addFlashAttribute("mensagemError", "Erro ao gravar alguns medicamentos. " + medicamento_service.msgGravar);
//                }
//            }
        }
        

        return "redirect:/pacientes";
    }

    @RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
    public String excluir(@PathVariable Long codigo, RedirectAttributes attributes) {
        if(paciente_service.excluir(codigo)){
            attributes.addFlashAttribute("mensagem", "Paciente excluído com sucesso!");
        } else {
            attributes.addFlashAttribute("mensagemError", "Nao foi possivel excluir.");
        }

        
        return "redirect:/pacientes";
    }
    
    @PostMapping("/pdf")
    public ModelAndView getPdf(ModelAndView modelAndView, 
                               @RequestParam(value="arrCodigos[]") Long[] arrCodigos, 
                               HttpSession httpSession) {
        
        Util.parametersToReport(report_paciente, httpSession);
    
        Map<String, Object> parameterMap = new HashMap<>();
        
        parameterMap.put("datasource", new JRBeanCollectionDataSource( paciente_service.relatorio(arrCodigos, httpSession)));
        modelAndView = new ModelAndView(report_paciente, parameterMap);
        return modelAndView;
    }
    
    @GetMapping("/getPacientes")
    public @ResponseBody List<BasicJson> getNamePacientes(@RequestParam("phrase") String phrase){
        return paciente_service.getPacienteJson(phrase);
    }
    
    
    @GetMapping("/getPaciente/{idPaciente}")
    public @ResponseBody Paciente getNamePacientes(@PathVariable Long idPaciente){
        return paciente_service.getOnlyPropertiesPaciente(idPaciente);
    }
    
    
    
    private boolean permiteAlterarMedicamento(HttpSession httpSession){
        Login login = (Login) httpSession.getAttribute(Key.LOGIN_SESSION);
        boolean permissao = false;
        if(login != null){
            permissao = login.getAdmin() || login.getTpLogin().startsWith("M");
        }
        
        return permissao;
    }
}
