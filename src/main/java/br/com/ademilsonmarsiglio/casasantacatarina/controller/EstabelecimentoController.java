package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Estabelecimento;
import br.com.ademilsonmarsiglio.casasantacatarina.model.Pessoa;
import br.com.ademilsonmarsiglio.casasantacatarina.repository.filter.Filter;
import br.com.ademilsonmarsiglio.casasantacatarina.service.EstabelecimentoService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author ademilson
 */
@Controller()
@RequestMapping("/estabelecimentos")
public class EstabelecimentoController {
    
    private final String CONTROLLER = "estabelecimentos";

    @Autowired
    private EstabelecimentoService estabelecimento_service;
    
    @GetMapping()
    public ModelAndView listar(@ModelAttribute("filtro") Filter filtro, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("estabelecimento/estabelecimentos");
        
        Boolean superUser = (Boolean) httpSession.getAttribute(Key.LOGIN_SUPERUSER);
        List lista = new ArrayList();
        if(superUser){
            lista = estabelecimento_service.filtrar(filtro);
        }
        
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("estabelecimentos", lista);
        
        return modelAndView;
    }
    

    @GetMapping("{codigo}")
    public ModelAndView edicao(@PathVariable("codigo") Long codigo, HttpSession httpSession) {
        Boolean superUser = (Boolean) httpSession.getAttribute(Key.LOGIN_SUPERUSER);
        Estabelecimento estabelecimento = estabelecimento_service.getEstabelecimento(codigo);
        
        if(superUser && estabelecimento != null){
            return edicao(estabelecimento);
        } else {
            return showMessage("Estabelecimento nao encontrado!", httpSession);
        }
    }
    
    public ModelAndView showMessage(String mensagem, HttpSession httpSession) {

        Boolean superUser = (Boolean) httpSession.getAttribute(Key.LOGIN_SUPERUSER);
        List lista = new ArrayList();
        if(superUser){
            lista = estabelecimento_service.filtrar(new Filter());
        }
        
        ModelAndView modelAndView = new ModelAndView("estabelecimento/estabelecimentos");
        modelAndView.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        modelAndView.addObject("estabelecimentos", lista);
        modelAndView.addObject("filtro", new Filter());
        
        if(mensagem != null){
            modelAndView.addObject("mensagem", mensagem);
        }

        return modelAndView;
    }
    
    public ModelAndView edicao(Estabelecimento estabelecimento) {
        ModelAndView mv = new ModelAndView("estabelecimento/manutencaoEstabelecimento");
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        mv.addObject("estabelecimento", estabelecimento);
        return mv;
    }

    @RequestMapping("/novo")
    public ModelAndView novo(HttpSession httpSession) {
        Boolean superUser = (Boolean) httpSession.getAttribute(Key.LOGIN_SUPERUSER);
        
        if(!superUser)
            return showMessage("Usuário sem permissão.", httpSession);
        
        ModelAndView mv = new ModelAndView("estabelecimento/manutencaoEstabelecimento");
        Estabelecimento estabelecimento = new Estabelecimento();
        estabelecimento.setPessoa(new Pessoa());
        mv.addObject("estabelecimento", estabelecimento);
        mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
        return mv;
    }

    @PostMapping()
    public String salvar(@Validated Estabelecimento estabelecimento, BindingResult result, RedirectAttributes attributes) {

        if (result.hasErrors()) {
            ModelAndView mv = new ModelAndView("estabelecimento/manutencaoEstabelecimento");
            mv.addObject("estabelecimento", estabelecimento);
            mv.addObject(Key.CONTORLLER_NAME, CONTROLLER);
            return mv.getViewName();
        }
        
        this.estabelecimento_service.salvar(estabelecimento);

        return "redirect:/estabelecimentos";
    }

    @RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
    public String excluir(@PathVariable Long codigo, RedirectAttributes attributes) {
        estabelecimento_service.excluir(codigo);

        attributes.addFlashAttribute("mensagem", "Estabelecimento excluído com sucesso!");
        return "redirect:/estabelecimentos";
    }
}
