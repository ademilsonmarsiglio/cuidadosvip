package br.com.ademilsonmarsiglio.casasantacatarina.controller;

import br.com.ademilsonmarsiglio.casasantacatarina.model.Prontuario;
import br.com.ademilsonmarsiglio.casasantacatarina.model.json.ResponseRequest;
import br.com.ademilsonmarsiglio.casasantacatarina.service.SaeProntuarioService;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author ademilsonmarsiglio
 */

@Controller()
@RequestMapping("/sae")
public class SaeProntuarioController {
    
    @Autowired private SaeProntuarioService service;
    
    
    
    @GetMapping("newListByPaciente/{idPaciente}")
    public ResponseEntity<ResponseRequest<List>> listaAdd(@PathVariable Long idPaciente) {
        return ResponseEntity.ok(ResponseRequest.ok("ok", service.getListaNovoProntuario(idPaciente)));
        
    }
    
    @GetMapping("getSaeByProntuario/{idProntuario}")
    public ResponseEntity<ResponseRequest<List>> getListaByProntuario(@PathVariable Long idProntuario) {
        
        Prontuario prontuario = new Prontuario().setIdProntuario(idProntuario);
        service.getListaByProntuario(Arrays.asList(prontuario));
        
        return ResponseEntity.ok(ResponseRequest.ok("ok", prontuario.getSaes()));
        
    }
    
}
