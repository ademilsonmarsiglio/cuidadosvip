CREATE TABLE bas_historicobean
(
  idhistoricobean serial NOT NULL,
  actionhistorico character varying NOT NULL,
  dtalteracao timestamp without time zone,
  historico json,
  idtable character varying(255) NOT NULL,
  nametable character varying(255) NOT NULL,
  CONSTRAINT bas_historicobean_pkey PRIMARY KEY (idhistoricobean)
);


CREATE OR REPLACE FUNCTION public.trg_fct_registra_historicobean()
  RETURNS trigger AS
$BODY$
DECLARE
    _row record;
    _json json;
    _action VARCHAR;
    _idTable VARCHAR;
BEGIN
    IF (TG_OP = 'UPDATE') THEN
        _row := NEW;
        _action := 'UPDATE';
        _json := jsonb_diff_val(row_to_json(NEW)::jsonb, row_to_json(OLD)::jsonb)::json;
    ELSIF (TG_OP = 'INSERT') THEN
        _row := NEW;
        _action := 'INSERT';
        _json := row_to_json(_row);
    ELSE 
        _row := OLD;
        _action := 'DELETE';
        _json := row_to_json(_row);
    END IF;

    _idTable := to_json(_row)->>TG_ARGV[0];

    if (_json::text <> '{}'::text) THEN
        INSERT INTO bas_historicobean (actionhistorico, dtalteracao, idtable, nametable, historico) VALUES (_action, now(), _idTable, TG_TABLE_NAME, _json);
    END IF;

    RETURN _row;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.trg_fct_registra_historicobean()
  OWNER TO postgres;
  
  
  
  

CREATE OR REPLACE FUNCTION public.jsonb_diff_val(
    val1 jsonb,
    val2 jsonb)
  RETURNS jsonb AS
$BODY$
DECLARE
  result JSONB;
  v RECORD;
BEGIN
   result = val1;
   FOR v IN SELECT * FROM jsonb_each(val2) LOOP
     IF result @> jsonb_build_object(v.key,v.value)
        THEN result = result - v.key;
     ELSIF result ? v.key THEN CONTINUE;
     ELSE
        result = result || jsonb_build_object(v.key,'null');
     END IF;
   END LOOP;
   RETURN result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.jsonb_diff_val(jsonb, jsonb)
  OWNER TO postgres;

  
  

CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.paciente
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idpaciente');
  
  

CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.pessoa
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idpessoa');


CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.diagnosticopaciente
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('id');
  
  
CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.anamnese
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('id');




CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.qualidade_vida
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('id');

CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.prontuario
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idprontuario');


CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.estoque
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idestoque');

CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.produto
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idproduto');

CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.sinalvital
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idsinalvital');


CREATE TRIGGER trg_historicobean
  AFTER INSERT OR UPDATE OR DELETE
  ON public.login
  FOR EACH ROW
  EXECUTE PROCEDURE public.trg_fct_registra_historicobean('idlogin');




