/**
 * Author:  ademilson
 * Created: 13/07/2018
 */

ALTER TABLE medicamento ADD COLUMN aplicacaomanha DOUBLE PRECISION;
ALTER TABLE medicamento ADD COLUMN aplicacaotarde DOUBLE PRECISION;
ALTER TABLE medicamento ADD COLUMN aplicacaonoite DOUBLE PRECISION;

UPDATE medicamento SET 
aplicacaomanha = (CASE WHEN hrmanha is not null AND hrmanha <> '' THEN (CASE WHEN regexp_replace(posologia, '\D', '', 'g') IS NOT NULL AND regexp_replace(posologia, '\D', '', 'g') <> '' THEN CAST(regexp_replace(posologia, '\D', '', 'g') AS DOUBLE PRECISION) END) ELSE null END),
aplicacaotarde = (CASE WHEN hrtarde is not null AND hrtarde <> '' THEN (CASE WHEN regexp_replace(posologia, '\D', '', 'g') IS NOT NULL AND regexp_replace(posologia, '\D', '', 'g') <> '' THEN CAST(regexp_replace(posologia, '\D', '', 'g') AS DOUBLE PRECISION) END) ELSE null END),
aplicacaonoite = (CASE WHEN hrnoite is not null AND hrnoite<> '' THEN (CASE WHEN regexp_replace(posologia, '\D', '', 'g') IS NOT NULL AND regexp_replace(posologia, '\D', '', 'g') <> '' THEN CAST(regexp_replace(posologia, '\D', '', 'g') AS DOUBLE PRECISION) END) ELSE null END);
