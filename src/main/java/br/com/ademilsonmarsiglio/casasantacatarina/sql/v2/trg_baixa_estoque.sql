/**
 * Author:  ademilson
 * Created: 09/07/2018
 */

CREATE OR REPLACE FUNCTION trg_baixa_estoque_prontuario() RETURNS trigger AS $BODY$
BEGIN
  IF (TG_OP = 'INSERT') THEN
        IF (NEW.qtd IS NOT NULL) THEN
            UPDATE estoque SET qtd = qtd - NEW.qtd WHERE idEstoque = NEW.idEstoque;
        END IF;
  RETURN NEW;

  
  ELSIF (TG_OP = 'UPDATE') THEN
        IF (NEW.qtd IS NULL) THEN 
            NEW.qtd = 0;
        END IF;

        IF (OLD.qtd IS NULL) THEN 
            OLD.qtd = 0;
        END IF;

	IF (NEW.qtd != OLD.qtd) THEN
		UPDATE estoque SET qtd = qtd - (NEW.qtd - OLD.qtd) WHERE idEstoque = NEW.idEstoque;
	END IF;  
  RETURN NEW;
  
  
  ELSIF (TG_OP = 'DELETE') THEN
        IF (OLD.qtd IS NOT NULL) THEN
            UPDATE estoque SET qtd = qtd + OLD.qtd WHERE idEstoque = OLD.idEstoque;
        END IF;
	RETURN OLD;
  END IF;
  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;







CREATE OR REPLACE FUNCTION trg_baixa_estoque_historicoestoque() RETURNS trigger AS $BODY$
BEGIN
  IF (TG_OP = 'INSERT') THEN
    IF (NEW.qtd IS NOT NULL) THEN
	UPDATE estoque SET qtd = qtd + NEW.qtd WHERE idEstoque = NEW.idEstoque;
    END IF;
  RETURN NEW;

  
  ELSIF (TG_OP = 'UPDATE') THEN
        
        IF (NEW.qtd IS NULL) THEN 
            NEW.qtd = 0;
        END IF;

        IF (OLD.qtd IS NULL) THEN 
            OLD.qtd = 0;
        END IF;

	IF (NEW.qtd != OLD.qtd) THEN
		UPDATE estoque SET qtd = qtd + (NEW.qtd - OLD.qtd) WHERE idEstoque = NEW.idEstoque;
	END IF;  
  RETURN NEW;
  
  
  ELSIF (TG_OP = 'DELETE') THEN
        IF (OLD.qtd IS NOT NULL) THEN
            UPDATE estoque SET qtd = qtd - OLD.qtd WHERE idEstoque = OLD.idEstoque;
        END IF;
	RETURN OLD;
  END IF;
  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;









CREATE TRIGGER trigger_baixa_estoque_prontuario
  AFTER INSERT OR UPDATE OR DELETE
  ON itemprontuario
  FOR EACH ROW
  EXECUTE PROCEDURE trg_baixa_estoque_prontuario();












CREATE TRIGGER trigger_baixa_estoque_historicoestoque
  AFTER INSERT OR UPDATE OR DELETE
  ON historicoestoque
  FOR EACH ROW
  EXECUTE PROCEDURE trg_baixa_estoque_historicoestoque();