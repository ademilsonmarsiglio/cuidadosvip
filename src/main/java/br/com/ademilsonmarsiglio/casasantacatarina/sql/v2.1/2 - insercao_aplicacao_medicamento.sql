/**
 * Author:  ademilson
 * Created: 20/07/2018
 */

--Inserções de aplicação de medicamentos.
insert into medicamentoaplicacao (aplicacao, hora, idmedicamento)
select aplicacaomanha, hrmanha, idmedicamento from medicamento where aplicacaomanha is not null;

insert into medicamentoaplicacao (aplicacao, hora, idmedicamento)
select aplicacaotarde, hrtarde, idmedicamento from medicamento where aplicacaotarde is not null;

insert into medicamentoaplicacao (aplicacao, hora, idmedicamento)
select aplicacaonoite, hrnoite, idmedicamento from medicamento where aplicacaonoite is not null;