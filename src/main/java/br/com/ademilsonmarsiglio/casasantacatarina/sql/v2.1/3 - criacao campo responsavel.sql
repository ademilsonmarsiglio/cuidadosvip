ALTER TABLE prontuario  ADD COLUMN responsavel varchar(500);

UPDATE prontuario set responsavel = pessoa.nmPessoa 
FROM login 
inner join pessoa USING (idpessoa)
WHERE login.idlogin = prontuario.idlogin
AND prontuario.responsavel is null;

ALTER TABLE prontuario ALTER COLUMN responsavel SET NOT NULL;
ALTER TABLE prontuario ALTER COLUMN responsavel SET DEFAULT 'N/I';
