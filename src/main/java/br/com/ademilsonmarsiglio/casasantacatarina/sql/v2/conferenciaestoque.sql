
/**
 * Author:  ademilson
 * Created: 17/07/2018
 */


with _itemprontuario as (
	select idestoque, sum(qtd) as qtd from itemprontuario group by idestoque
), _historicoestoque as (
	select idestoque, sum(qtd) as qtd from historicoestoque group by idestoque
)
select 
estoque.idEstoque, 
_historicoestoque.qtd - _itemprontuario.qtd as _teste,
estoque.qtd as _estoque
from 
estoque, _itemprontuario, _historicoestoque
where estoque.idestoque = _itemprontuario.idestoque
and estoque.idestoque = _historicoestoque.idestoque
group by estoque.idEstoque, _historicoestoque.qtd, _itemprontuario.qtd
having (_historicoestoque.qtd - _itemprontuario.qtd) <> estoque.qtd;