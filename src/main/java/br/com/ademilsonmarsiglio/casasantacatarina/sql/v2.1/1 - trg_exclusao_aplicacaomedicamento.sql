CREATE OR REPLACE FUNCTION trg_excluir_aplicacaomedicamento() RETURNS trigger AS $BODY$
BEGIN
  IF (TG_OP = 'DELETE') THEN
    DELETE FROM medicamentoaplicacao WHERE idMedicamento = OLD.idMedicamento;
	RETURN OLD;
  END IF;
  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;

CREATE TRIGGER trigger_aplicacaomedicamento
  BEFORE DELETE
  ON medicamento
  FOR EACH ROW
  EXECUTE PROCEDURE trg_excluir_aplicacaomedicamento();
