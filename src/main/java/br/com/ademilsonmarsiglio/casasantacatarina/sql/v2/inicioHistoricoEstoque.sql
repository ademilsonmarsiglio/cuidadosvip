/**
 * Author:  ademilson
 * Created: 09/07/2018
 */

select * from historicoestoque;
delete from historicoestoque;
UPDATE estoque set qtd = 0 where qtd is null;

CREATE OR REPLACE FUNCTION inicioHistoricoEstoque() RETURNS void AS
$BODY$

	DECLARE 
		CURSOR_ESTOQUE CURSOR FOR select * from estoque;
		SOMA_QTD_ITEM_PRONTUARIO DOUBLE PRECISION;
	BEGIN
		
		--LOOP 
		FOR ESTOQUE IN CURSOR_ESTOQUE LOOP

			SOMA_QTD_ITEM_PRONTUARIO := 0;
			SELECT SUM(qtd) into SOMA_QTD_ITEM_PRONTUARIO FROM itemprontuario WHERE idEstoque = ESTOQUE.idEstoque;

			IF (SOMA_QTD_ITEM_PRONTUARIO IS NULL) THEN 
				SOMA_QTD_ITEM_PRONTUARIO := 0.0;
			END IF;

			
			RAISE notice '   E: % S: % D: %', SOMA_QTD_ITEM_PRONTUARIO + ESTOQUE.qtd, SOMA_QTD_ITEM_PRONTUARIO, ESTOQUE.qtd;

			INSERT INTO historicoEstoque (id, data, qtd, idestoque, idLogin) VALUES (nextval('SEQ_HISTORICOESTOQUE'), '2016-01-01', (SOMA_QTD_ITEM_PRONTUARIO + ESTOQUE.qtd), ESTOQUE.idEstoque, 1);
			
		END LOOP; 
		
		
		
	END;
	
$BODY$
LANGUAGE plpgsql VOLATILE;

select inicioHistoricoEstoque();