/**
 * Author:  ademilson
 * Created: 13/07/2018
 */

INSERT INTO estoque (idestoque, qtd, tpestoque, idproduto) select NEXTVAL('seq_estoque'), 0, 'G', idProduto from produto where idProduto NOT IN (select distinct idproduto from estoque WHERE tpestoque = 'G');