var options = {
    url: function (phrase) {
        var url = $("#base-url").val();
        return url + "getProdutos?phrase=" + phrase;
    },
    getValue: "descricao",
    list: {
        onChooseEvent: function () {
            var codigo = $("#filter-produto").getSelectedItemData().codigo;
            $("#idProduto").val(codigo);
        }
    }
};


var insereNovoProduto = false;
var validaNovoProduto = true;

function validaSubmit(){

    if(validaNovoProduto){
        var idProduto = $("#idProduto").val();

        if((idProduto === null || idProduto.toString() === '') && !insereNovoProduto){
            var nomeProduto = $("#filter-produto").val();

            swal({
              title: "Você tem certeza?",
              text: "O produto " + nomeProduto + " não foi encontrado no cadastro, deseja cadastrar?",
              icon: "warning",
              buttons: true,
              buttons: [ "Não", "Sim, Gravar!"],
              dangerMode: true,
            })
            .then((confirmou) => {
              if (confirmou) {
                validaNovoProduto = false;  
                insereNovoProduto = true;
                $("#form-manutencao").submit();
              } else {
                validaNovoProduto = true;  
                insereNovoProduto = false;
              }
            });
            return false;
        }
    }
    
    
    var aplicacoes = $(".js-aplicacoes").length;
    
    if (typeof aplicacoes !== "undefined" && aplicacoes !== null) {
        for (var i = 0; i < aplicacoes; i++) {
            var el_id = $(".js-aplicacao-id")[i];
            var el_hora = $(".js-aplicacao-hora")[i];
            var el_aplicacao = $(".js-aplicacao-aplicacao")[i];
            
            if (typeof el_id !== "undefined") {
                el_id.setAttribute("name", "aplicacoes[" + i + "].id");
            }
            
            if (typeof el_hora !== "undefined") {
                el_hora.setAttribute("name", "aplicacoes[" + i + "].hora");
            }
            
            if (typeof el_aplicacao !== "undefined") {
                el_aplicacao.setAttribute("name", "aplicacoes[" + i + "].aplicacao");
            }
        }
    }

    return true;
}

function zeraVariavelProduto() {
    insereNovoProduto = false;
}

function habilitaPesquisa(){
    document.getElementById("filter-produto").disabled = false;
}

function addAplicacao() {
    
    // Criar div principal
    var divPrincipal = document.createElement('div');
    divPrincipal.className = 'row js-aplicacoes';
    
    
    // Now create and append to iDiv
    var divHora = document.createElement('div');
    var divAplicacao = document.createElement('div');
    var divBotao = document.createElement('div');
    
    
    divHora.className = 'form-group col-xs-6';
    divAplicacao.className = 'form-group col-xs-6';
    
    var labelHora = document.createElement('label');
    var inputHora = document.createElement('input');
    
    var labelAplicacao = document.createElement('label');
    var inputAplicacao = document.createElement('input');
    
    var aExcluir = document.createElement('a');
    var spanIconeExcluir = document.createElement('span');
    
    
    //HORA
        labelHora.className = "control-label" ;
        labelHora.innerHTML = "Hora" ;
        inputHora.type = "tel";
        inputHora.className = "form-control js-aplicacao-hora js-time";
        inputHora.autocomplete = "off";
        inputHora.maxlength = "5";
        inputHora.name = "aplicacoes[x].hora";

        divHora.appendChild(labelHora);
        divHora.appendChild(inputHora);
    
    
    
    //APLICAÇÃO
        labelAplicacao.className = "control-label" ;
        labelAplicacao.innerHTML = "Aplicação" ;
        inputAplicacao.type = "tel";
        inputAplicacao.className = "form-control js-double js-aplicacao-aplicacao";
        inputAplicacao.autocomplete = "off";
        inputAplicacao.name = "aplicacoes[x].aplicacao";

        aExcluir.className = "btn btn-link btn-xs pull-right";
        aExcluir.onclick = function () {removeAplicacao(this);};
        aExcluir.title = "Excluir";
        aExcluir.rel = "tooltip";
        aExcluir.setAttribute("data-placement", "top");

        spanIconeExcluir.className = "glyphicon glyphicon-remove";
        aExcluir.appendChild(spanIconeExcluir);
        
        divBotao.appendChild(labelAplicacao);
        divBotao.appendChild(aExcluir);
    
        divAplicacao.appendChild(divBotao);
        divAplicacao.appendChild(inputAplicacao);
        
    
    

    // The variable iDiv is still good... Just append to it.
    divPrincipal.appendChild(divHora);
    divPrincipal.appendChild(divAplicacao);
    
    // Adiciona a div principal ao lugar q ela pertence...
    document.getElementsByClassName('js-div-aplicacoes')[0].appendChild(divPrincipal);
    
    mascararTime();
    
}

function removeAplicacao(botaoExcluir) {
    var div = botaoExcluir.parentNode;
    var divAplicacao = div.parentNode;
    var divPrincipal = divAplicacao.parentNode;
    divPrincipal.parentNode.removeChild(divPrincipal);
}













window.onload = function () {
    $("#filter-produto").easyAutocomplete(options);
    document.getElementById("filter-produto").disabled = $('#id').val().toString().length !== 0;
    
    if (typeof listaAplicacoes === "undefined" || listaAplicacoes == null || listaAplicacoes.length == 0) {
        addAplicacao();
    }
};