var relatorioproduto = {
                    
    init: function () {
        $("#filter-produto").keypress(function(e) {
            if(e.which === 13) {
                relatorioproduto.refresh();
            }
        });


        $("#filter-tipoEstoque").change(function(e) {
                relatorioproduto.refresh();
        });

        $("#filter-situacaoEstoque").change(function(e) {
                relatorioproduto.refresh();
        });
    },

    refresh: function() {
        document.getElementById('body_table').innerHTML = "";
        relatorioproduto.buscarLista();
    },

    buscarLista: function () {

        var url = $("#base-url").val() + "relatorios/porProdutos/lista";

        var filter = {
            produto: $("#filter-produto").val(),
            tipoEstoque: $("#filter-tipoEstoque").val(),
            situacaoEstoque: $("#filter-situacaoEstoque").val(),
            limit: 50,
            offset: $("#body_table tr").size()
        };

        $.ajax({
            method: "POST",
            contentType: "application/json",
            url: url,
            data: JSON.stringify(filter),
            dataType: 'json',
            success: function (data) {
                relatorioproduto.atualizaLista(data);
            }, 
            error: function (jqXHR, textStatus, errorThrown) {
                swal("Erro ao obter resultados da lista.");
                console.log(textStatus, errorThrown);
            }, 
            beforeSend: function (jqXHR, settings) {
                $("#loader").removeClass("loader-hidden");
                $("#img-carregar").addClass("loader-hidden");
            },
            complete: function (jqXHR, textStatus){
                $("#img-carregar").removeClass("loader-hidden");
                $("#loader").addClass("loader-hidden");
            }
        });
    },



    atualizaLista: function (dados) {
        var tbody = document.getElementById('body_table');
        var ultimaLinha = tbody.rows.length;

        for (var i in dados) {
            var c = 0; //coluna
            var row = tbody.insertRow(ultimaLinha++);

            row.insertCell(c++).innerHTML = dados[i].dsProduto;
            row.insertCell(c++).innerHTML = dados[i].tpEstoque;
            row.insertCell(c++).innerHTML = typeof dados[i].qtd !== "undefined" ? dados[i].qtd.formatMoney(2, "", ".", ",") : "";
        }

        if (dados.length <= 0) {
            $("#carregar_mais").addClass("hidden");
        } else {
            $("#carregar_mais").removeClass("hidden");
        }
    },

    validaRelatorio: function(){
        $("#rel-produto").val($("#filter-produto").val());
        $("#rel-tipoEstoque").val($("#filter-tipoEstoque").val());
        $("#rel-situacaoEstoque").val($("#filter-situacaoEstoque").val());

        $("#form-relatorio").submit();
    }
};


window.onload = function () {
    relatorioproduto.init();
    relatorioproduto.refresh();
};