var execEventAge = false;
$('#datepickerPaciente').on('changeDate', function () {
    if (!execEventAge) {
        execEventAge = true;
        return;
    }

    var selectDate = $('#datepickerPaciente').datepicker('getDate');
    var age = getAge(selectDate);

    $('#idade').val(age);
});

function getAge(birthDate) {
    var now = new Date();

    function isLeap(year) {
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    }

    // days since the birthdate    
    var days = Math.floor((now.getTime() - birthDate.getTime()) / 1000 / 60 / 60 / 24);
    var age = 0;
    // iterate the years
    for (var y = birthDate.getFullYear(); y <= now.getFullYear(); y++) {
        var daysInYear = isLeap(y) ? 366 : 365;
        if (days >= daysInYear) {
            days -= daysInYear;
            age++;  // increment the age only if there are available enough days for the year.
        }
    }

    return age;
}

function calculaIdade(_idade, _datePicker) {
    var idade = document.getElementById(_idade).value;
    if (idade !== null) {

        var date = $('#' + _datePicker).datepicker('getDate');
        if (date == null) {
            date = new Date();
        }

        var day = date.getDate();
        var month = date.getMonth();

        var atual = new Date();
        var atualYear = atual.getFullYear();
        var atualMonth = atual.getMonth();
        var atualDate = atual.getDate();

        var newyear = atualYear - idade;

        //Verifica se o mes ou o dia é maior que o atual, se for diminui um ano.
        if (month > atualMonth) {
            newyear = newyear - 1;
        } else if (month === atualMonth) {
            if (day > atualDate) {
                newyear = newyear - 1;
            }
        }

        var birthday = new Date(newyear, month, day);

        execEventAge = false;
        $('#' + _datePicker).datepicker('setDates', birthday);
    }
}