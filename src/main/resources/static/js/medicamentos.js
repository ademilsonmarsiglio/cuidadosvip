var options = {
    url: function (phrase) {
        var url = $("#base-url").val();
        return url + "pacientes/getPacientes?phrase=" + phrase;
    },

    getValue: "descricao",

    list: {
        onChooseEvent: function () {
            var selecionado = $("#filter-paciente").getSelectedItemData();
            
            $("#codigo").val(selecionado.codigo);

            //campos para inserção.
            $("#novo-nome").val(selecionado.descricao);
            $("#novo-codigo").val(selecionado.codigo);

            limpaTBody(document.getElementById('body_table'));
            maisResultados();
            
            $("#msgInfo").remove();
        }
    }
};

function validaInsercao() {
    var nome = $("#novo-nome").val();
    var codigo = $("#novo-codigo").val();

    if (nome !== null && codigo !== null && nome.length > 0 && codigo.length > 0) {
        $("#form-novo").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para inserir.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}
function validaRelatorio(){
    var codigo = $("#codigo").val();
    var descricao = $("#filter-paciente").val();

    $("#rel-codigo").val(codigo);
    $("#rel-descricao").val(descricao);

    if (descricao !== null && codigo !== null && descricao.length > 0 && codigo.length > 0) {
        $("#form-relatorio").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para continuar.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}




function maisResultados() {
    var _urlBuscaLista = $("#base-url").val() + "medicamentos/getLista";

    var filtro = {
        idPaciente: $("#codigo").val(),
        paciente: $("#form-filtro").val(),
        limit: 15,
        offset: $("#table-historicoestoque tbody tr").size()
    };

    $.ajax({
        method: "POST",
        contentType: "application/json",
        url: _urlBuscaLista,
        data: JSON.stringify(filtro),
        dataType: 'json',
        success: function (data) {
            insertRow(data);

            if (data == null || data.length == 0) {
                if (!$("#carregar_mais").hasClass("hidden"))
                    $("#carregar_mais").addClass("hidden");
            } else {
                $("#carregar_mais").removeClass("hidden");
            }

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Erro ao obter resultados da lista.");
            console.log(textStatus, errorThrown);

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        },
        beforeSend: function (jqXHR, settings) {
            $("#loader").removeClass("loader-hidden");
            $("#img-carregar").addClass("loader-hidden");
        }
    });
}

function insertRow(dados) {

    
    $('.js-col-horario').remove(); //remove as colunas de horarios
    $('.js-col-top-turno').remove(); //remove as colunas de turno
    
    var novasColunas = [];
    var indiceColunas = 0;
    
    
    for (var i in dados) {
        var medicamento = dados[i];
        
        if (typeof medicamento.aplicacoes !== "undefined" && medicamento.aplicacoes !== null) {
            
            for (j = 0; j < medicamento.aplicacoes.length; j++) {
                var aplicacao = medicamento.aplicacoes[j];

                //Verifica se a coluna nao existe
                if (!novasColunas.includes(aplicacao.hora)) {
                    novasColunas[indiceColunas++] = aplicacao.hora;
                }
            }
            
        }
    }
    
    
    
    //Verifica se tem alguma coluna a ser adicionada:
    if (novasColunas.length > 0) {
        //Ordena por turnos
        novasColunas.sort(dividirTurnos);
        
        var lastColumn = document.getElementById("th_observacao");
        var tHead = document.getElementById("table-medicamentos").tHead;
        
        var colsManha = 0, colsTarde = 0, colsNoite = 0;
        
        for (var i = novasColunas.length -1; i >= 0 ; i--) {
            var newTH = document.createElement('th');
            tHead.rows[1].insertBefore(newTH, lastColumn);
            newTH.innerHTML = novasColunas[i];
            newTH.classList.add("js-col-horario");
            newTH.id = "th_" + novasColunas[i].replace(":", "");

            lastColumn = newTH;
            
            var minutos = timeToMinute(novasColunas[i]);

            //MANHA = Depois das 05:59 e antes da 13:00
            if (minutos > 359 && minutos < 780) {
                colsManha++;
            } else if (minutos >= 780 && minutos < 1140) { //TARDE = Depois da 13:00 e antes da 19:00
                colsTarde++;
            } else {
                colsNoite++;
            }
        }
        
        
        ///////////////////////////
        //
        // Adiciona theader de turno
        //
        //////////////////////////
        lastColumn = document.getElementById("th_top_end");
        
        if (colsNoite > 0) {
            var newTH = document.createElement('th');
            tHead.rows[0].insertBefore(newTH, lastColumn);
          
            newTH.innerHTML = "Noite";
            newTH.classList.add("js-col-top-turno");
            newTH.id = "th_col_top_noite";
            newTH.colSpan = colsNoite;
            newTH.style.textAlign = "center";

            lastColumn = newTH;
        } 
        
        if (colsTarde > 0){
            var newTH = document.createElement('th');
            tHead.rows[0].insertBefore(newTH, lastColumn);
          
            newTH.innerHTML = "Tarde";
            newTH.classList.add("js-col-top-turno");
            newTH.id = "th_col_top_tarde";
            newTH.colSpan = colsTarde;
            newTH.style.textAlign = "center";

            lastColumn = newTH;
        }
        
        if (colsManha > 0) {
            var newTH = document.createElement('th');
            tHead.rows[0].insertBefore(newTH, lastColumn);
          
            newTH.innerHTML = "Manha";
            newTH.classList.add("js-col-top-turno");
            newTH.id = "th_col_top_manha";
            newTH.colSpan = colsManha;
            newTH.style.textAlign = "center";

            lastColumn = newTH;
        }
        
    }
    
    
    
    
    
    var b_table = document.getElementById('body_table');
    var urlBase = $("#base-url").val();

    var ultimaLinha = b_table.rows.length;
    
    var isAdmin = $("#session-is-admin").val();

    for (var i in dados) {
        var row = b_table.insertRow(ultimaLinha++);
        
        var medicamento = dados[i];

        row.insertCell(0).innerHTML = medicamento.estoque.produto.dsProduto;
        var colDias = row.insertCell(1);
        colDias.innerHTML = medicamento.dias;
        colDias.classList.add("col-medicamento-dias");
        
        row.insertCell(2).innerHTML = typeof medicamento.seNecessario !== null && medicamento.seNecessario ? "Sim" : "";
        
        //Adiciona as colunas dinamicamente
        for (var j = 0; j < novasColunas.length; j++) {
            row.insertCell(getColumnIndexesById("th_" + novasColunas[j].replace(":", "")));
        }
        
        row.insertCell(getColumnIndexesById("th_observacao")).innerHTML = medicamento.observacao;
        var colEstoque = row.insertCell(getColumnIndexesById("th_estoque"));
                
        if (typeof medicamento.estoque.qtd !== "undefined") {
            colEstoque.innerHTML = 
                    "<a href=\"" + urlBase + "historicoEstoque?idEstoque=" + medicamento.estoque.idEstoque + "&amp;produto=" + encodeURI(medicamento.estoque.produto.dsProduto) + "\">"
                  + medicamento.estoque.qtd.formatMoney(2, "", ".", ",")
                  + "</a>";
        } else {
            colEstoque.innerHTML = ""
        }
        
        //insere as aplicações do medicamentos.
        if (typeof medicamento.aplicacoes !== "undefined" && medicamento.aplicacoes !== null) {
            for (j = 0; j < medicamento.aplicacoes.length; j++) {
                var aplicacao = medicamento.aplicacoes[j];
                var index = getColumnIndexesById("th_" + aplicacao.hora.replace(":", ""));
        
                if (typeof aplicacao.aplicacao !== "undefined" && aplicacao.aplicacao !== null) {
                    row.cells[parseInt(index[0])].innerHTML = aplicacao.aplicacao.formatMoney(2, "", ".", ",").replace(",00", "");
                } else {
                    row.cells[parseInt(index[0])].innerHTML = "";
                }
            }
        }
        
        if (isAdmin) {
            var colButtons = row.insertCell(getColumnIndexesById("th_buttons"));
            colButtons.className = "text-right";
            colButtons.style.minWidth = "70px";
            colButtons.innerHTML =
                        '<a class="btn btn-link btn-xs" title="Editar" rel="tooltip" data-placement="top" href="' + urlBase + 'medicamentos/editar/' + medicamento.idMedicamento + '">'
                        + '<span class="glyphicon glyphicon-pencil"></span>'
                        + '</a>'
                        + '<a class="btn btn-link btn-xs" data-toggle="modal" data-target="#confirmacaoExclusaoModal"'
                        +    'data-codigo="'+ medicamento.idMedicamento + '" data-descricao="' + medicamento.estoque.produto.dsProduto + '"'
                        +    ' title="Excluir" rel="tooltip" data-placement="top">'
                        +    ' <span class="glyphicon glyphicon-remove"></span>'
                        + '</a>'
            ;
        }
        
    }
}

/**
 * Comparador
 * 
 * @param {type} a
 * @param {type} b
 * @returns {Number}
 */
function dividirTurnos(a, b) {
    
    var _a = timeToMinute(a);
    var _b = timeToMinute(b);
    
    //MANHA = Depois das 07:00 e antes da 13:00
    //TARDE = Depois da 13:00 e antes da 19:00
    //NOITE = Depois das 19:00 Até 07:00
    
    
    
    //////////////////////////////////////////////
    //
    //          MANHA
    //
    //////////////////////////////////////////////
    
    var isTurnoManha_A = (_a > 359 && _a < 780);
    var isTurnoManha_B = (_b > 359 && _b < 780);
    
    //SE OS DOIS SE ENQUADRAM NA MANHA, COMPARA ENTRE ELES:
    if (isTurnoManha_A && isTurnoManha_B) { 
        return comparar(_a, _b);
    } 
    
    //SE HORARIO A FOR DA MANHA E B NAO RETORNA -1 para A vir antes.
    if (isTurnoManha_A && !isTurnoManha_B) {
        return -1;
    }
    
    //SE HORARIO B FOR DA MANHA E A NAO RETORNA 1 para A vir depois.
    if (isTurnoManha_B && !isTurnoManha_A) {
        return 1;
    }
    
    
    //////////////////////////////////////////////
    //
    //          TARDE
    //
    //////////////////////////////////////////////
    
    var isTurnoTarde_A = _a >= 780 && _a < 1200;
    var isTurnoTarde_B = _b >= 780 && _b < 1200;
    
    //SE OS DOIS SE ENQUADRAM NA TARDE, COMPARA ENTRE ELES:
    if (isTurnoTarde_A && isTurnoTarde_B) { 
        return comparar(_a, _b);
    } 
    
    //SE HORARIO A FOR DA TARDE E B NAO RETORNA -1 para A vir antes.
    if (isTurnoTarde_A && !isTurnoTarde_B) {
        return -1;
    }
    
    //SE HORARIO B FOR DA TARDE E A NAO RETORNA 1 para A vir depois.
    if (isTurnoTarde_B && !isTurnoTarde_A) {
        return 1;
    }
    
    
    
    

    //////////////////////////////////////////////
    //
    //          NOITE
    //
    //////////////////////////////////////////////
    
    var isTurnoNoite_A = _a >= 1140 || _a < 359;
    var isTurnoNoite_B = _b >= 1140 || _b < 359;
    
    //SE OS DOIS SE ENQUADRAM NA TARDE, COMPARA ENTRE ELES:
    if (isTurnoNoite_A && isTurnoNoite_B) { 
        return comparar(_a, _b);
    } 
    
    //SE HORARIO A FOR DA TARDE E B NAO RETORNA -1 para A vir antes.
    if (isTurnoNoite_A && !isTurnoNoite_B) {
        return -1;
    }
    
    //SE HORARIO B FOR DA TARDE E A NAO RETORNA 1 para A vir depois.
    if (isTurnoNoite_B && !isTurnoNoite_A) {
        return 1;
    }
}

function comparar(a, b) {
   if (a > b) {
       return 1;
   }
   
   if (b > a) {
       return -1;
   }
   
   return 0;
}




function getColumnIndexesById(id) {
    return $("#" + id).map(function() {
        return $(this).index();
    }).get();
}

window.onload = function () {
    $("#filter-paciente").easyAutocomplete(options);
    
    var codigo = $("#codigo").val();
    var descricao = $("#filter-paciente").val();
    
    if (descricao !== null && codigo !== null && descricao.length > 0 && codigo.length > 0) {
        limpaTBody(document.getElementById('body_table'));
        maisResultados();
    }
};