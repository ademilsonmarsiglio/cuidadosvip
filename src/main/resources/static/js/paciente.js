function habilitaOutro(checkboxElem) {
    document.getElementById("dsAuxilioMedicoOutro").disabled = !checkboxElem.checked;
    if (!checkboxElem.checked) {
        $('#dsAuxilioMedicoOutro').val(null);
    }
}

function removeMedicamento(botaoExcluir) {
    var coluna = botaoExcluir.parentNode;
    var linha = coluna.parentNode;
    var bodyTable = linha.parentNode;
    bodyTable.removeChild(linha);
}

function editarMedicamento(botaoEditar) {
    var coluna = botaoEditar.parentNode;
    var linha = coluna.parentNode;
    var i = linha.id;

    var el_idMedicamento = document.getElementById("listaMedicamento" + i + ".idMedicamento");
    var el_posologia = document.getElementById("listaMedicamento" + i + ".posologia");
    var el_hrManha = document.getElementById("listaMedicamento" + i + ".hrManha");
    var el_hrTarde = document.getElementById("listaMedicamento" + i + ".hrTarde");
    var el_hrNoite = document.getElementById("listaMedicamento" + i + ".hrNoite");
    var el_observacao = document.getElementById("listaMedicamento" + i + ".observacao");
    var el_qtdEstoque = document.getElementById("listaMedicamento" + i + ".estoque.qtd");
    var el_tpEstoque = document.getElementById("listaMedicamento" + i + ".estoque.tpEstoque");
    var el_idEstoque = document.getElementById("listaMedicamento" + i + ".estoque.idEstoque");
    var el_idProduto = document.getElementById("listaMedicamento" + i + ".estoque.produto.idProduto");
    var el_dsProduto = document.getElementById("listaMedicamento" + i + ".estoque.produto.dsProduto");

    $("#med-indexRow").val(i);


    if(el_idMedicamento !== null){
        $("#med-idMedicamento").val(el_idMedicamento.value);
    }

    if(el_dsProduto !== null){
        $("#filter-produto").val(el_dsProduto.value);
    }

    if(el_idProduto !== null){
        $("#med-idProduto").val(el_idProduto.value);
    }

    if(el_posologia !== null){
        $("#med-posologia").val(el_posologia.value);
    }

    if(el_hrManha !== null){
        $("#med-hrManha").val(el_hrManha.value);
    }

    if(el_hrTarde !== null){
        $("#med-hrTarde").val(el_hrTarde.value);
    }

    if(el_hrNoite !== null){
        $("#med-hrNoite").val(el_hrNoite.value);
    }

    if(el_observacao !== null){
        $("#med-observacao").val(el_observacao.value);
    }

    if(el_idEstoque !== null){
        $("#med-idEstoque").val(el_idEstoque.value);
    }

    if(el_qtdEstoque !== null){
        $("#med-qtdEstoque").val(converteFloatMoeda(el_qtdEstoque.value));
    }

    if(el_tpEstoque !== null){
        $("#med-tpEstoque").val(el_tpEstoque.value);
    }

    document.getElementById("filter-produto").disabled = true;
}

var options = {
    url: function (phrase) {
            var url = $("#base-url").val();
            return url + "getProdutos?phrase=" + phrase;
    },
    adjustWidth: false,
    getValue: "descricao",

    list: {
        onChooseEvent: function () {
            var codigo = $("#filter-produto").getSelectedItemData().codigo;
            $("#med-idProduto").val(codigo);
        }
    }
};

$("#filter-produto").easyAutocomplete(options);

function cancelaInsercao(){
    if(event.keyCode === 27){
        limpaCamposInsercaoMedicamento();
    }
}

function limpaCamposInsercaoMedicamento(){
    $("#med-idMedicamento").val(null);
    $("#med-indexRow").val(null);
    $("#filter-produto").val(null);
    $("#med-idEstoque").val(null);
    $("#med-idProduto").val(null);
    $("#med-posologia").val(null);
    $("#med-hrManha").val(null);
    $("#med-hrTarde").val(null);
    $("#med-hrNoite").val(null);
    $("#med-observacao").val(null);
    $("#med-qtdEstoque").val(null);
    $("#med-tpEstoque").val(null);
    $("#filter-produto").focus();
}

function insereMedicamento(){

    if (event.keyCode === 13) {

        document.getElementById("filter-produto").disabled = false;

        var indexRow = $("#med-indexRow").val();
        var idMedicamento = $("#med-idMedicamento").val();
        var idEstoque = $("#med-idEstoque").val();
        var idProduto = $("#med-idProduto").val();
        var dsProduto = $("#filter-produto").val();
        var posologia = $("#med-posologia").val();
        var hrManha = $("#med-hrManha").val();
        var hrTarde = $("#med-hrTarde").val();
        var hrNoite = $("#med-hrNoite").val();
        var observacao = $("#med-observacao").val();
        var qtdEstoque = $("#med-qtdEstoque").val();
        var tpEstoque = $("#med-tpEstoque").val();

        var col0;
        var col1;
        var col2;
        var col3;
        var col4;
        var col5;
        var col6;
        var col7;

        var table = document.getElementById("tab-medicamentos");

        if(dsProduto === null || dsProduto.toString().trim().length === 0){
            swal("É obrigatório informar a descrição do medicamento.");
            return;
        }

        var index = 0;

        var row;

        if(idMedicamento === null || idMedicamento.toString().trim().length === 0){ //Inserindo

            var temAlgumItem = false;
            for (var i = 0, tableRow; tableRow = table.rows[i]; i++) {
                if(tableRow.id !== null && tableRow.id.length > 0){
                    temAlgumItem = true;
                    index = tableRow.id;
                }

                var el_idProduto = document.getElementById("listaMedicamento" + i + ".estoque.produto.idProduto");
                var el_dsProduto = document.getElementById("listaMedicamento" + i + ".estoque.produto.dsProduto");

                if(el_idProduto !== null && el_idProduto.value === idProduto){
                    swal("O medicamento informado já consta na tabela, informe outro.");
                    return;
                }

                if(el_dsProduto !== null && el_dsProduto.value === dsProduto){
                    swal("Na tabela já consta um medicamento com este nome, informe outro.");
                    return;
                }
            }

            row = table.insertRow();

            if(temAlgumItem === true){
                try {
                    index = parseInt(index) + 1;
                } catch (e) {
                    index = 0;
                }
            }

            col0 = row.insertCell(0);
            col1 = row.insertCell(1);
            col2 = row.insertCell(2);
            col3 = row.insertCell(3);
            col4 = row.insertCell(4);
            col5 = row.insertCell(5);
            col6 = row.insertCell(6);
            col7 = row.insertCell(7);
            col7.className = "text-right";
        } else { //Editando
            index = parseInt(indexRow);
            row = table.rows[index + 2];

            col0 = row.cells[0];
            col1 = row.cells[1];
            col2 = row.cells[2];
            col3 = row.cells[3];
            col4 = row.cells[4];
            col5 = row.cells[5];
            col6 = row.cells[6];
            col7 = row.cells[7];
        }

        row.id = index;
        col0.innerHTML = dsProduto;
        col1.innerHTML = posologia;
        col2.innerHTML = hrManha;
        col3.innerHTML = hrTarde;
        col4.innerHTML = hrNoite;
        col5.innerHTML = observacao;
        col6.innerHTML = qtdEstoque;
        col7.innerHTML =   "<a class=\"btn btn-link btn-xs\" title=\"Editar\" rel=\"tooltip\" data-placement=\"top\" onclick=\"editarMedicamento(this)\"> <span class=\"glyphicon glyphicon-pencil\"></span></a>"
                                      + "<a class=\"btn btn-link btn-xs\" onclick=\"removeMedicamento(this);\" title=\"Excluir\" rel=\"tooltip\" data-placement=\"top\"><span class=\"glyphicon glyphicon-remove\"></span></a>"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".idMedicamento\" name=\"listaMedicamento[" + index + "].idMedicamento\" value=\"" + idMedicamento + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".hrManha\" name=\"listaMedicamento[" + index + "].hrManha\" value=\"" + hrManha + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".hrTarde\" name=\"listaMedicamento[" + index + "].hrTarde\" value=\"" + hrTarde + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".hrNoite\" name=\"listaMedicamento[" + index + "].hrNoite\" value=\"" + hrNoite + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".posologia\" name=\"listaMedicamento[" + index + "].posologia\" value=\"" + posologia + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".observacao\" name=\"listaMedicamento[" + index + "].observacao\" value=\"" + observacao + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".estoque.qtd\" name=\"listaMedicamento[" + index + "].estoque.qtd\" value=\"" + convertMonetary2Double(qtdEstoque) + "\" class=\"decimal\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".estoque.tpEstoque\" name=\"listaMedicamento[" + index + "].estoque.tpEstoque\" value=\"" + tpEstoque + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".estoque.idEstoque\" name=\"listaMedicamento[" + index + "].estoque.idEstoque\" value=\"" + idEstoque + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".estoque.produto.idProduto\" name=\"listaMedicamento[" + index + "].estoque.produto.idProduto\" value=\"" + idProduto + "\" />"
                                      + "<input type=\"hidden\" id=\"listaMedicamento" + index + ".estoque.produto.dsProduto\" name=\"listaMedicamento[" + index + "].estoque.produto.dsProduto\" value=\"" + dsProduto + "\" />";


        limpaCamposInsercaoMedicamento();
    } 

    if (event.keyCode === 27) {
        document.getElementById("filter-produto").disabled = false;
        limpaCamposInsercaoMedicamento();
    }
}

function validaSubmit() {
    var dsProduto = $("#filter-produto").val();
    var posologia = $("#med-posologia").val();
    var hrManha = $("#med-hrManha").val();
    var hrTarde = $("#med-hrTarde").val();
    var hrNoite = $("#med-hrNoite").val();
    var observacao = $("#med-observacao").val();
    
    if(!perguntaMedicamentoEmInsercao(dsProduto)){
        return false;
    }
    
    if(!perguntaMedicamentoEmInsercao(posologia)){
        return false;
    }
    
    if(!perguntaMedicamentoEmInsercao(hrManha)){
        return false;
    }
    
    if(!perguntaMedicamentoEmInsercao(hrTarde)){
        return false;
    }
    
    if(!perguntaMedicamentoEmInsercao(hrNoite)){
        return false;
    }
    
    if(!perguntaMedicamentoEmInsercao(observacao)){
        return false;
    }
    

    return true;
}

function perguntaMedicamentoEmInsercao(teste) {

    if (teste !== null && teste.toString().length > 0) {
        swal({
            title: "Você tem certeza?",
            text: "Falta confirmar a inserção do medicamento, deseja gravar o paciente sem ele?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#81BEF7",
            confirmButtonText: "Sim, Gravar!",
            cancelButtonText: "Não",
            closeOnConfirm: true
        },
                function () {
                    limpaCamposInsercaoMedicamento();
                    $("#form-manutencao").submit();
                });
                
        return false;
    }

    return true;
}