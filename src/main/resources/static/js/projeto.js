$('#confirmacaoExclusaoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var codigo = button.data('codigo');
    var descricao = button.data('descricao');

    var modal = $(this);
    var form = modal.find('form');
    var action = form.data('url-base');
    if (!action.endsWith('/')) {
        action += '/';
    }
    form.attr('action', action + codigo);

    modal.find('.modal-body span').html('Tem certeza que deseja excluir <strong>' + descricao + '</strong>?');
});

$('#confirmacaoExclusaoMedicamentoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var codigo    = button.data('codigo');
    var descricao = button.data('descricao');
    var nome      = button.data('nome');
    
    $('#exc_descricao').val(descricao);
    $('#exc_codigo').val(codigo);
    $('#exc_nome').val(nome);

    $(this).find('.modal-body span').html('Tem certeza que deseja excluir <strong>' + descricao + '</strong>?');
});

$('#selectAll').click(function(e){
    var table= $(e.target).closest('table');
    $('td input:checkbox',table).prop('checked',this.checked);
});

$('#descricaoProntuario').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var descricao = button.data('descricao');
    var prontuario = button.data('prontuario');
    
    var modal = $(this);
    var table = document.getElementById("table-itens");
    var base_url = $("#base-url").val();
    
    try {
        limpaTabela(table);
        
        var jsonItens;
        $.getJSON( base_url + 'prontuario/getJsonItens/' + prontuario, function(data){
            jsonItens = data;
            
            if (jsonItens !== null){
                for (i = 0; i < jsonItens.length; i++) {
                    var item = jsonItens[i];
                    var row = table.insertRow();
                    var medicamento = item['medicamento'];
                    var qtd =  item['qtd'];
                    var qtdFormatadada = '';

                    if(qtd !== null){
                        qtdFormatadada = qtd;

                        try {
                            qtdFormatadada = qtd.formatMoney(2, "", ".", ",");
                        } catch(ex){}
                    }
                    row.insertCell(0).innerHTML = medicamento;
                    var cellQtd = row.insertCell(1);
                    cellQtd.style.textAlign = 'right';
                    cellQtd.innerHTML = qtdFormatadada;
                }
            }
	});
        
        
    } catch (e){
        console.log("erro ao popular a tabela" + e);
    }
    
    
    try {
        
        getRequest(`${base_url}sae/getSaeByProntuario/${prontuario}`)
                .then((data => {
                    console.log('ó retorno é:', data)
                    populaSAE_Viewer(data.obj);
                }))
                .catch (error => {
                    console.log('erro ao buscar saes do prontuario.');
                });
          
        
    } catch (e) {
        console.log("erro ao buscar saes", e);
    }
    
    modal.find('.modal-body textarea').html(descricao);
});



function populaSAE_Viewer(lista) {
    
    limpaLinhas();
    
    if (!lista) {
        return;
    }

    for (var i = 0; i < lista.length; i++) {
        const row = addSAE_Viewer(lista[i], i);

        $(row).appendTo("#dados");
    }
    
}


function limpaLinhas() {
    $("#dados").html("");
}



function addSAE_Viewer(sae, idx) {
    
    const header = (`
        <div class="panel-heading">
            <div class="diagnostico-row">
                <p class="diagnostico-descricao">${sae.descricao}</p>
            </div>
        </div>
    `);
    
    
    let rows = "";
    
    if (sae.filhos) {
        
        for (var i = 0; i < sae.filhos.length; i++) {
            const filho = sae.filhos[i];
            
            let valueJustificativa = "";
            let checkboxMarcado = "";
            
            if (filho.justificativa) {
                valueJustificativa = `value="${filho.justificativa}"`;
            }
            
            if (filho.marcado) {
                checkboxMarcado = `checked`;
            }
            
            rows += (`
                <div class="diagnostico-row line-row-bottom">
                    <label class="checkbox-inline col-xs-6">
                        <input disabled type="checkbox" class="js-sae-marcado" ${checkboxMarcado} name="saes[${idx}].filhos[${i}].marcado" sae-idx="${idx}_${i}"/>
                        ${filho.descricao}
                    </label>

                    <input readonly name="saes[${idx}].filhos[${i}].justificativa" type="text" maxlength="255" class="form-control col-xs-6 js-sae-justificativa" placeholder="se não marcar, justifique: ${filho.descricao}." ${valueJustificativa}" sae-idx="${idx}_${i}" />
                </div>
            `);
            
        }
        
    }
    
    return (`
    <div class="panel panel-default diagnostico-card">      
        ${header}
        
        <div class="panel-body diagnostico-body">
            <div class="form-group">
                ${rows}
            </div>
        </div>        
    </div>

    `);
    
    
    return (``);
}


$('#confirmacaoRelatorioPaciente').on('show.bs.modal', function (event) {
    var arrCodigos = getSelectedCheckbox();

    var algumSelecionado = arrCodigos === null || arrCodigos.length === 0;
    $('#selecionados').attr('disabled', algumSelecionado);

    if (algumSelecionado) {
        $('#todos').attr('checked', true);
    } else {
        $('#selecionados').attr('checked', true);
    }
});

function limpaTabela(table){
    var rowCount = table.rows.length;
    for (var x=rowCount-1; x>0; x--) {
       table.deleteRow(x);
    }
}

function limpaTBody(tbody){
    tbody.innerHTML = "";
}


function abreRelatorioPaciente() {
    var todos = document.getElementById('todos').checked;
    var arrCodigos;

    if (todos) {
        arrCodigos = null;

    } else {
        arrCodigos = getSelectedCheckbox();
    }

    $('#confirmacaoRelatorioPaciente').modal('hide');
    document.getElementById('arrCodigos').value = arrCodigos;
    document.dialog.submit();
}

function getSelectedCheckbox() {
    var selected = new Array();
    $('table td input[type="checkbox"]:checked').each(function () {
        selected.push($(this).attr('value'));
    });

    return selected;
}

function populaInfoCEP(cep) {
    id = document.getElementById('id').value;
    isEditando = id !== null && id.length > 0;
    if (isEditando || cep.value === null || cep.value.length <= 8) {
        return;
    }

    _url = "https://viacep.com.br/ws/" + cep.value + "/json";

    $.ajax({
        method: "GET",
        url: _url,
        success: function (data) {
            if (data !== null) {
                $('#logradouro').val(data.logradouro);
                $('#bairro').val(data.bairro);
                $('#cidade').val(data.localidade);
            }
        }
    });
}

function limpaCodigo(id) {
    if (event.keyCode !== 13) {
        $("#" + id).val(null);
    }
}

function convertMonetary2Double(valor){
    if(typeof valor === "undefined" || valor === ""){
       valor =  0;
    }else{
       valor = valor.replace(".","");
       valor = valor.replace(",",".");
       valor = parseFloat(valor);
    }
    return valor;

 }
 
function numberToReal(numero) {
    var numero = parseFloat(numero).toFixed(2).split('.');
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function converteFloatMoeda(valor){
    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();
    valor = ""+valor;
    c = valor.indexOf(".",0);
    //encontrou o ponto na string
    if(c > 0){
       //separa as partes em inteiro e decimal
       inteiro = valor.substring(0,c);
       decimal = valor.substring(c+1,valor.length);
    }else{
       inteiro = valor;
    }

    //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j-=3, c++){
       aux[c]=inteiro.substring(j-3,j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for(c = aux.length-1; c >= 0; c--){
       inteiro += aux[c]+'.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro

    inteiro = inteiro.substring(0,inteiro.length-1);

    decimal = parseInt(decimal);
    if(isNaN(decimal)){
       decimal = "00";
    }else{
       decimal = ""+decimal;
       if(decimal.length === 1){
          decimal = decimal+"0";
       }
    }

    valor = inteiro+","+decimal;
    
    return valor;
 }
 
 /**
  * Converte String para date.
  * Formato dd/MM/yyyy
  * @param {type} str
  * @param {type} separator
  * @returns {Date}
  */
 function strToDate(str, separator) {
    var parts = str.split(separator);
    return new Date(parseInt(parts[2], 10),
                      parseInt(parts[1], 10) - 1,
                      parseInt(parts[0], 10));
}

function sortTable(table, col, reverse, isDate) {
    var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
        tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
        i;
    reverse = -((+reverse) || -1);
    tr = tr.sort(function (a, b) { // sort rows
        var text1 = a.cells[col].textContent.trim();
        var text2 = b.cells[col].textContent.trim();

        if (isDate) {
            text1 = strToDate(text1, "/").toISOString();
            text2 = strToDate(text2, "/").toISOString();
        } 

        return reverse // `-1 *` if want opposite order
                * (text1.localeCompare(text2));
    });
    for(i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
}

function emptyUndefined(valor) {
    if (isNull(valor)) {
        return "";
    } else {
        return valor;
    }
}

function isNull(valor) {
    return typeof valor === 'undefined' || valor === null || valor.length === 0;
}

function mascararTime() {
    var timeFields = $('.js-time');
    
    if (typeof timeFields !== "undefined") {
        timeFields.mask('00:00');
    }
}

function mascararDouble() {
    var doubleFields = $('.js-double');
    
    if (typeof doubleFields !== "undefined") {
        doubleFields.mask('#.###,00', {reverse: true});
    }
}


function mascararInteger() {
    var intFields = $('.js-int');

    if (typeof intFields !== "undefined") {
        intFields.mask('#########');
    }
}

function mascararDoubleNegative() {
    var doubleFields = $('.js-double-negative');

    if (typeof doubleFields !== "undefined") {
        doubleFields.mask('#.###,##', {
            reverse: true,
            translation: {
                '#': {
                    pattern: /-|\d/,
                    recursive: true
                }
            },
            onChange: function (value, e) {
                e.target.value = value.replace(/(?!^)-/g, '').replace(/^,/, '').replace(/^-,/, '-');
            }
        });
    }
}

function timeToMinute(time) {
    if (!isNull(time)) {
        var splitTime1= time.split(':');

        var hour=0;
        var minute=0;

        hour = parseInt(parseInt(splitTime1[0]));
        minute = parseInt(splitTime1[1]);

        return minute + (hour * 60);
    } 
    
    return 0;
}

function minuteToTime(minute) {
    var hour = minute / 60;
    hour =parseInt(hour);
    minute = minute % 60;
    return doisDigitos(abs(hour)) + ":" + doisDigitos(abs(minute));
}

/**
 * Minimo Dois digitos.
 * 
 * Se menor igual a 9 fica 09
 * Se o tamanho (lengt) menor que 2 adiciona 0 depois.
 * 
 * @param {type} valor
 * @returns {String}
 */
function doisDigitos(valor) {
    if (valor !== null) {
        if (valor <= 9) {
            return "0" + valor;
        } else if (valor.toString().length < 2) {
            return valor + "0";
        }
    }

    return valor;
}

/**
 * 
 * Como usar: 
 * 
 * var a = 3211000.354
 * a.formatMoney(2, "R$ ", ".", ",");
 * 
 * @param {type} places
 * @param {type} symbol
 * @param {type} thousand
 * @param {type} decimal
 * @returns {@var;symbol|@var;thousand|@var;decimal|String}
 */
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var number = this, 
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};


function postRequest(url, data) {
    return new Promise(
        function (resolve, reject) {
            $.ajax({
                method: "POST",
                url: url,
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: 'json',
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(jqXHR, textStatus, errorThrown);
                },
            });
        } 
    );
} 



function getRequest(url) {
    return new Promise(
        function (resolve, reject) {
            $.ajax({
                method: "GET",
                url: url,
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(jqXHR, textStatus, errorThrown);
                },
            });
        } 
    );
} 

function putRequest(url) {
    return new Promise(
        function (resolve, reject) {
            $.ajax({
                method: "PUT",
                url: url,
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(jqXHR, textStatus, errorThrown);
                },
            });
        } 
    );
} 

function deleteRequest(url) {
    return new Promise(
        function (resolve, reject) {
            $.ajax({
                method: "DELETE",
                url: url,
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(jqXHR, textStatus, errorThrown);
                },
            });
        } 
    );
} 
