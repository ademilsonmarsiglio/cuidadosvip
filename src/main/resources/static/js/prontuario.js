$(function () {
    $('#datetimepicker').datetimepicker({format: 'dd/mm/yyyy hh:ii', language: 'pt-BR', todayBtn: true});

    var optionsPaciente = {
        url: function (phrase) {
            var url = $("#base-url").val();
            return url + "pacientes/getPacientes?phrase=" + phrase;
        },

        getValue: "descricao",
        adjustWidth: false,

        list: {
            onChooseEvent: function () {
                var codigo = $("#filter-paciente").getSelectedItemData().codigo;
                $("#idPaciente").val(codigo);
                $("#sinalVital_idPaciente").val(codigo);

                var table = document.getElementById("tab-medicamentos");
                limpaTabelaMenosPrimLinha(table);
                $("#med-idEstoque").val(null);

                buscaSAE(codigo);
            }
        }
    };
    
    $("#filter-paciente").easyAutocomplete(optionsPaciente);
});

function limpaTabelaMenosPrimLinha(table) {
    var rowCount = table.rows.length;
    for (var x = rowCount - 1; x > 1; x--) {
        table.deleteRow(x);
    }
}


function getDateTimeStr() {
    var d = new Date();
    return d.getDate() + "/" + (parseInt(d.getMonth()) + 1) + "/" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
}


function removeMedicamento(botaoExcluir) {
    var coluna = botaoExcluir.parentNode;
    var linha = coluna.parentNode;
    var bodyTable = linha.parentNode;
    bodyTable.removeChild(linha);
    limpaCamposInsercaoMedicamento();
}

function validaSelecaoPaciente() {
    var idPaciente = $("#idPaciente").val();

    if (idPaciente === null || idPaciente.toString().length === 0) {
        swal("Primeiro informe o paciente.");
    }
}


function carregaMedicamentosPaciente() {
    var idPaciente = $("#idPaciente").val();

    if (idPaciente === null || idPaciente.toString().length === 0) {
        swal("Primeiro informe o paciente.");
        
        return;
    }
    
    $('#dialogMedicamentos').modal('show');
    
    
    var url = $("#base-url").val() + "medicamentos/byPaciente?idPaciente=" + idPaciente;

    //LIMPA TABELAS
    document.getElementById('body_table_medicamento').innerHTML = "";
    document.getElementById('body_table_estoquesGerais').innerHTML = "";
    
    $.ajax({
        method: "GET",
        url: url,
        success: function (dados) {
            atualizaListaMedicamentos(dados);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Erro ao obter resultados da lista.");
            console.log(textStatus, errorThrown);
        }
    });
}

var medicamentos;

function atualizaListaMedicamentos(dados) {
    
    if (typeof dados === "undefined" || dados === null) {
        return;
    }
    
    medicamentos = [];
    medicamentos.push.apply(medicamentos, dados.listManha);
    medicamentos.push.apply(medicamentos, dados.listTarde);
    medicamentos.push.apply(medicamentos, dados.listNoite);
    medicamentos.push.apply(medicamentos, dados.listSeNecessario);
    medicamentos.push.apply(medicamentos, dados.listSemAplicacao);
    medicamentos.push.apply(medicamentos, dados.listGeral);
    
    var tbody_geral = document.getElementById('body_table_estoquesGerais');
    var ultimaLinha_geral = tbody_geral.rows.length;


    if (dados.listManha !== null && dados.listManha.length > 0) {
        insereRegistrosTabelaPaciente(dados.listManha, "Turno MANHA");
    }
    
    if (dados.listTarde !== null && dados.listTarde.length > 0) {
        insereRegistrosTabelaPaciente(dados.listTarde, "Turno TARDE");
    }
    
    if (dados.listNoite !== null && dados.listNoite.length > 0) {
        insereRegistrosTabelaPaciente(dados.listNoite, "Turno NOITE");
    }
    
    if (dados.listSeNecessario !== null && dados.listSeNecessario.length > 0) {
        insereRegistrosTabelaPaciente(dados.listSeNecessario, "Se Necessário");
    }
    
    
    if (dados.listSemAplicacao !== null && dados.listSemAplicacao.length > 0) {
        insereRegistrosTabelaPaciente(dados.listSemAplicacao, "Sem Hora de Aplicação Informada");
    }
    
    
    
    //ESTOQUE GERAL
    for (var i in dados.listGeral) {
        var medicamento = dados.listGeral[i];
        
        var c = 0; //coluna
        var row = tbody_geral.insertRow(ultimaLinha_geral++);
        
        var col1, col2, col3;
        
        row.id = medicamento.uniqueID;
        var str = "<div class=\"input-group\">"
                            + "<span class=\"input-group-addon\" id=\"sp_qtd_" + row.id + "\" onclick=\"incrementQtd('" + row.id + "');\" style=\"padding: 3px 6px; cursor: pointer;\"> + </span>"
                            + "<input type=\"tel\" id=\"in_qtd_" + row.id + "\" onkeyup=\"binding('qtdInserir', '" + row.id + "', this);\" onkeypress=\"if (event.keyCode == 13) {return false;}\" class=\"form-control js-double\" aria-describedby=\"sp_qtd_ " + row.id + "\">"
                    + "</div>";
            
        (col1 = row.insertCell(c++)).innerHTML = str; 
        (col2 = row.insertCell(c++)).innerHTML = medicamento.estoque.produto.dsProduto; 
        col2.colSpan  = "4";
        (col3 = row.insertCell(c++)).innerHTML = typeof medicamento.estoque.qtd === "undefined" ? "" : converteFloatMoeda(medicamento.estoque.qtd);
    }
    
    
    
    mascararDouble();
}

function insereRegistrosTabelaPaciente(lista, mensagem) {
    
    var tbody_medicamento = document.getElementById('body_table_medicamento');
    var ultimaLinha_medicamentos = tbody_medicamento.rows.length;
    
    if (mensagem !== null) {
        var row = tbody_medicamento.insertRow(ultimaLinha_medicamentos++);
        var cell = row.insertCell(c++);
        cell.innerHTML = "<h3>" + mensagem + "</h3>";
        cell.colSpan = 7;
    }
    
    for (var i in lista) {
        var c = 0; //coluna
        
        //var col1, col2, col3, col4, col5, col6;
        var row = tbody_medicamento.insertRow(ultimaLinha_medicamentos++);

        row.id = lista[i].uniqueID;
        var str = "<div class=\"input-group\">"
                            + "<span class=\"input-group-addon\" id=\"sp_qtd_" + row.id + "\" onclick=\"incrementQtd('" + row.id + "');\" style=\"padding: 3px 6px; cursor: pointer;\"> + </span>"
                            + "<input type=\"tel\" id=\"in_qtd_" + row.id + "\" onkeyup=\"binding('qtdInserir', '" + row.id + "', this);\" onkeypress=\"if (event.keyCode == 13) {return false;}\" class=\"form-control js-double\" aria-describedby=\"sp_qtd_ " + row.id + "\">"
                    + "</div>";

        row.insertCell(c++).innerHTML = str; 
        row.insertCell(c++).innerHTML = lista[i].estoque.produto.dsProduto; 
        row.insertCell(c++).innerHTML = typeof lista[i].hora === "undefined" ? "" : lista[i].hora;
        row.insertCell(c++).innerHTML = typeof lista[i].aplicacao === "undefined" ? "" : converteFloatMoeda(lista[i].aplicacao);
        row.insertCell(c++).innerHTML = typeof lista[i].str_dias === "undefined" ? "" : lista[i].str_dias;
        row.insertCell(c++).innerHTML = typeof lista[i].observacao === "undefined" ? "" : lista[i].observacao;
        row.insertCell(c++).innerHTML = typeof lista[i].estoque.qtd === "undefined" ? "" : converteFloatMoeda(lista[i].estoque.qtd);
    }
    
    
}

function binding(name, id, element) {
    var medicamento = getMedicamentoByID(id);
    
    if (medicamento !== null) {
        medicamento[name]=convertMonetary2Double(element.value);
    }
}

function incrementQtd(id) {
    var medicamento = getMedicamentoByID(id);
    
    if (medicamento !== null) {
        
        var qtd = medicamento.qtdInserir;

        if (typeof qtd === 'undefined' || qtd === null) {
            qtd = 0;
        }

        if (typeof medicamento.aplicacao === "undefined" || medicamento.aplicacao <= 0) {
            medicamento.aplicacao = 1;
        }

        medicamento.qtdInserir = qtd + medicamento.aplicacao;

        var idInput = 'in_qtd_' + id;
        $("#" + idInput).val(converteFloatMoeda(medicamento.qtdInserir));
    }
}

function getMedicamentoByID(id) {
    
    var ate = medicamentos.length > 0 ? medicamentos.length -1 : 0;
    for (var i = ate; i >= 0; i--) {
        if (medicamentos[i].uniqueID === id) {
            return medicamentos[i];
        }        
    }
    
    return null;
}



function concluirInsercaoMedicamento() {
    var ate = medicamentos.length > 0 ? medicamentos.length -1 : 0;
    for (var i = ate; i >= 0; i--) {
        if (typeof medicamentos[i].qtdInserir !== "undefined" && medicamentos[i].qtdInserir > 0) {
            incluirMedicamento(medicamentos[i]);
        }
    }
    
    mascararDouble();
    
    $('#dialogMedicamentos').modal('hide');
}

function incluirMedicamento(medicamento) {
                                
    var idEstoque = medicamento.estoque.idEstoque;
    var dsProduto = medicamento.estoque.produto.dsProduto;
    var qtd = medicamento.qtdInserir;

    var idItemProntuario = null;
    var dtMovimentacao = null;
    var valor = null;
    var tpOperacao = null;

    if (dsProduto === null || dsProduto.toString().trim().length === 0) {
        swal("É obrigatório informar um medicamento.");
        return;
    }

    if (idEstoque === null || idEstoque.toString().trim().length === 0) {
        swal("Atenção", "Não é possível inserir um medicamento que não está cadastrado no sistema.", "warning");
        return;
    }

    var table = document.getElementById("tab-medicamentos");

    var col0;
    var col1;
    var col2;

    var index = 0;
    var row;


    var temAlgumItem = false;
    for (var i = 0, tableRow; tableRow = table.rows[i]; i++) {
        if (tableRow.id !== null && tableRow.id.length > 0) {
            temAlgumItem = true;
            index = tableRow.id;
        }
    }

    row = table.insertRow();

    if (temAlgumItem === true) {
        try {
            index = parseInt(index) + 1;
        } catch (e) {
            index = 0;
        }
    }

    col0 = row.insertCell(0);
    col1 = row.insertCell(1);
    col2 = row.insertCell(2);
    col2.className = "text-right";

    idItemProntuario = null;
    dtMovimentacao = getDateTimeStr();

    valor = null;
    tpOperacao = "S";

    var html_itemProntuario = "";

    if (idItemProntuario !== null) {
        html_itemProntuario = "<input type=\"hidden\" id=\"itens" + index + ".idItemProntuario\" name=\"itens[" + index + "].idItemProntuario\" value=\"" + idItemProntuario + "\" />";
    }
    
    var html_qtd = "<input type=\"tel\" class=\"form-control js-double text-right\" id=\"itens" + index + ".qtd\" name=\"itens[" + index + "].qtd\" value=\"" + converteFloatMoeda(qtd) + "\" />";
    
    row.id = index;
    col0.innerHTML = dsProduto;
    col1.innerHTML = html_qtd;
    col2.innerHTML = 
             "<a class=\"btn btn-link btn-xs\" onclick=\"removeMedicamento(this);\" title=\"Excluir\" rel=\"tooltip\" data-placement=\"top\"><span class=\"glyphicon glyphicon-remove\"></span></a>"
            + html_itemProntuario
            + "<input type=\"hidden\" id=\"itens" + index + ".dtMovimentacao\" name=\"itens[" + index + "].dtMovimentacao\" value=\"" + dtMovimentacao + "\" />"
            + "<input type=\"hidden\" id=\"itens" + index + ".tpOperacao\" name=\"itens[" + index + "].tpOperacao\" value=\"" + tpOperacao + "\" />"
            + "<input type=\"hidden\" id=\"itens" + index + ".estoque.produto.dsProduto\" name=\"itens[" + index + "].estoque.produto.dsProduto\" value=\"" + dsProduto + "\" />"
            + "<input type=\"hidden\" id=\"itens" + index + ".estoque.idEstoque\" name=\"itens[" + index + "].estoque.idEstoque\" value=\"" + idEstoque + "\" />";
}


function buscaSAE(idPaciente) {
    
    var url = $("#base-url").val() + "sae/newListByPaciente/" + idPaciente;
    
    getRequest(url)
        .then(data => {
            if (data && data.obj) {
                populaSAE(data.obj);
            } else {
                console.log(data);
            }
        })
        .catch(() => {
            console.log("caiu no catch");
        }) 
}

function populaSAE(lista) {
    
    limpaLinhas();
    
    if (!lista) {
        return;
    }

    for (var i = 0; i < lista.length; i++) {
        const row = addSAE(lista[i], i);

        $(row).appendTo("#dados");
    }
    
}

function addHiddenInput(name, value) {
    let valueAttr = "";
    if (value) {
        valueAttr = `value="${value}"` ;
    }
    
    return (`<input type="hidden" name="${name}" ${valueAttr} />`);
}

function addSAE(sae, idx) {
    
    const header = (`
        <div class="panel-heading">
            <div class="diagnostico-row">
                <p class="diagnostico-descricao">${sae.descricao}</p>
                ${addHiddenInput(`saes[${idx}].id`, sae.id)}
                ${addHiddenInput(`saes[${idx}].descricao`, sae.descricao)}
                ${addHiddenInput(`saes[${idx}].diagnosticoPaciente.id`, sae.diagnosticoPaciente.id)}
            </div>
        </div>
    `);
    
    
    let rows = "";
    
    if (sae.filhos) {
        
        for (var i = 0; i < sae.filhos.length; i++) {
            const filho = sae.filhos[i];
            
            let valueJustificativa = "";
            let checkboxMarcado = "";
            
            if (filho.justificativa) {
                valueJustificativa = `value="${filho.justificativa}"`;
            }
            
            if (filho.marcado) {
                checkboxMarcado = `checked`;
            }
            
            rows += (`
                <div class="diagnostico-row line-row-bottom">
                    <label class="checkbox-inline col-xs-6">
                        <input type="checkbox" class="js-sae-marcado" ${checkboxMarcado} name="saes[${idx}].filhos[${i}].marcado" sae-idx="${idx}_${i}"/>
                        ${filho.descricao}
                    </label>

                    <input name="saes[${idx}].filhos[${i}].justificativa" type="text" maxlength="255" class="form-control col-xs-6 js-sae-justificativa" placeholder="se não marcar, justifique: ${filho.descricao}." ${valueJustificativa}" sae-idx="${idx}_${i}" />
            
                    ${addHiddenInput(`saes[${idx}].filhos[${i}].id`, filho.id)}
                    ${addHiddenInput(`saes[${idx}].filhos[${i}].descricao`, filho.descricao)}
                    ${addHiddenInput(`saes[${idx}].filhos[${i}].diagnosticoPaciente.id`, filho.diagnosticoPaciente.id)}
                </div>
            `);
            
        }
        
    }
    
    return (`
    <div class="panel panel-default diagnostico-card">      
        ${header}
        
        <div class="panel-body diagnostico-body">
            <div class="form-group">
                ${rows}
            </div>
        </div>        
    </div>

    `);
    
    
    return (``);
}


function limpaLinhas() {
    $("#dados").html("");
}

function visualizarInformacaoPaciante() {
    
    const idPaciente = $("#idPaciente").val();
    
    if (!idPaciente) {
        swal("Selecione o paciente para continuar");
        return false;
    }
    
    const url = $("#base-url").val() + "pacientes/getPaciente/" + idPaciente;
    
    getRequest(url)
            .then(data => {

                if (data) {
                    
                    if (data.historico)
                        $("#paciente-historico").html(data.historico);
                    else 
                        $("#paciente-historico").html("Sem Informação");
                        
                    $("#paciente-is-alimentacao").css("visibility", data.auxilioMedicoA ? 'visible' : 'hidden');
                    $("#paciente-is-mobilidade").css("visibility", data.auxilioMedicoM ? 'visible' : 'hidden');
                    $("#paciente-is-higiene").css("visibility", data.auxilioMedicoH ? 'visible' : 'hidden');
                    $("#paciente-is-vestir").css("visibility", data.auxilioMedicoV ? 'visible' : 'hidden');
                        
                    if (data.dsAuxilioMedicoOutro)
                        $("#paciente-outro-auxilio").html(data.dsAuxilioMedicoOutro);
                    else 
                        $("#paciente-outro-auxilio").html("");
                    
                }

                $("#modalInfoPaciente").modal("show")
            })
            .catch(() => {
                swal("Erro ao buscar informações do paciente.");
            })
    
}


function validaSubmit() {
    // verifica se selecionou alguma conduta de enfermagem no sae. se selecionou uma, tem q justificar as outras.
    const qtdCheckboexesMarcado = $(".js-sae-marcado:checked").length;
    const qtdCheckboexes = $(".js-sae-marcado").length;
    
    if (qtdCheckboexesMarcado > 0) { //marcou ao menos um
        
        if (qtdCheckboexes !== qtdCheckboexesMarcado) { //esqueceu de marcar algum...
        
            const chkNaoMarcados = $(".js-sae-marcado:not(:checked)");
            for (var i = 0; i < chkNaoMarcados.length; i++) {
                const chk = chkNaoMarcados[i];

                const sae_id = chk.getAttribute("sae-idx");

                const justificativa = $(`.js-sae-justificativa[sae-idx  =${sae_id}]`);

                if (justificativa && !justificativa[0].value) {
                    swal({
                        title: 'Justifique porque não foi marcado.',
                        icon: "warning",
                    })
                    .then(() => {
                        setTimeout(() => {
                            $(justificativa).focus();
                        }, 250);
                    });

                    return false;
                }
            }
        }
    } else { //nao marcou nenhum, deve justificar todos
        
        var arrJustificativas = [];
        
        //testa as justificativas
        $(".js-sae-justificativa").each(function (i, el) {
            if (el.value) {
                arrJustificativas.push(el);
            }
        });
        
        if (arrJustificativas) {
            if (arrJustificativas.length > 0 && qtdCheckboexes != arrJustificativas.length) {
                swal({
                    title: 'Justifique porque não foi marcado.',
                    icon: "warning",
                })
                .then(() => {
                    setTimeout(() => {
                        //seta o foco no primeiro componente nao marcado.
                        const chkNaoMarcados = $(".js-sae-marcado:not(:checked)");
                        
                        if (chkNaoMarcados && chkNaoMarcados.length > 0) {
                            for (let i = 0; i < chkNaoMarcados.length; i++) {
                                // pega o chk que nao ta marcado, e verifica se a justificativa ta marcado
                                const chk = chkNaoMarcados[i]; 
                                const sae_id = chk.getAttribute("sae-idx");
                                const justificativa = $(`.js-sae-justificativa[sae-idx  =${sae_id}]`);

                                if (!$(justificativa).val()) {
                                    $(justificativa).focus(); //e seta o foco no componente q nao justificou e nao marcou
                                    break;
                                }
                            }
                        }
                    }, 250);
                });
                
                return false;
            }
        }        
    }

    return true;
}



window.onload = function () {
    const idProntuario = $("#idProntuario").val();
    
    if (!idProntuario) { //se true é uma edicao.
        const idPaciente = $("#idPaciente").val();

        if (idPaciente) {
            buscaSAE(idPaciente);
        }
    }    
    
    
    const informaPaciente = $("#informa_paciente").val();
    
    if (informaPaciente) {
        setTimeout(() => {
            $("#filter-paciente").focus();
        }, 300);
    }
    
    const tpLogin = $("#tpLogin").val();
    if (tpLogin) {
        if (tpLogin === "M" || tpLogin === "O" || tpLogin === "E") {
            //onpaste="return false" ondrop="return false"
            document.getElementById('descricaoProntuario').onpaste="";
            document.getElementById('descricaoProntuario').ondrop="";
        }
    }
};

