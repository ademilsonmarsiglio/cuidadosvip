var options = {
    url: function (phrase) {
        var url = $("#base-url").val() + "pacientes/";
        return url + "getPacientes?phrase=" + phrase;
    },
    getValue: "descricao",
    adjustWidth: false,
    list: {
        onChooseEvent: function () {
            var dados = $("#filter-paciente").getSelectedItemData();
            $("#codigo").val(dados.codigo);
            $("#novo-codigo").val(dados.codigo);
            $("#novo-nome").val(dados.descricao);
            $("#filter-paciente").focus();
        }
    }
};
$("#filter-paciente").easyAutocomplete(options);

function validaInsercao() {
    var nome = $("#novo-nome").val();
    var codigo = $("#novo-codigo").val();

    if (nome !== null && codigo !== null && nome.length > 0 && codigo.length > 0) {
        $("#form-novo").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para inserir.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}

function validaRelatorio() {
    var codigo = $("#codigo").val();
    var descricao = $("#filter-paciente").val();
    var date1 = $("#filter-date1").val();
    var date2 = $("#filter-date2").val();

    $("#rel-codigo").val(codigo);
    $("#rel-descricao").val(descricao);
    $("#rel-date1").val(date1);
    $("#rel-date2").val(date2);

    if (descricao !== null && codigo !== null && descricao.length > 0 && codigo.length > 0) {
        $("#form-relatorio").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para continuar.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}

function buscarLista(offset, limit) {
    var _urlBuscaLista = $("#base-url").val() + "sinaisVitais/getLista";

    var _codigo = $("#codigo").val();
    var _data1 = $("#filter-date1").val();
    var _data2 = $("#filter-date2").val();
    var _json = "";

    if (_codigo !== null && _codigo.length > 0) {
        _json += " \"id\": " + _codigo + ", ";
    } else {
        swal("Informe o paciente para continuar.");
        return;
    }

    if (_data1 !== null && _data1.length > 0) {
        _json += " \"dateStr1\": \"" + _data1 + "\", ";
    }

    if (_data2 !== null && _data2.length > 0) {
        _json += " \"dateStr2\": \"" + _data2 + "\", ";
    }
//
    _json += "\"offset\": " + offset + ", ";
    _json += "\"limit\": " + limit + " ";
    _json = "{" + _json + "}";

    $.ajax({
        method: "POST",
        url: _urlBuscaLista,
        data: {filtro_consulta: _json},
        success: function (data) {
            insertRow(data);

            if (data == null || data.length == 0) {
                if (!$("#carregar_mais").hasClass("hidden"))
                    $("#carregar_mais").addClass("hidden");
            } else {
                $("#carregar_mais").removeClass("hidden");
            }

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Erro ao obter resultados da lista.");
            console.log(textStatus, errorThrown);

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        },
        beforeSend: function (jqXHR, settings) {
            $("#loader").removeClass("loader-hidden");
            $("#img-carregar").addClass("loader-hidden");
        }
    });
}

function maisResultados() {
    var rowCont = $("#table-sinaisvitais tbody tr").size();
    buscarLista(rowCont, rowCont + 10);
}

var dirSorteble = 1;
function addOrdenacao() {
    var table = document.getElementById('table-sinaisvitais');
    var th = table.tHead;

    var c = 0;
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 0, (dirSorteble = 1 - dirSorteble), true);});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 1, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 2, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 3, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 4, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 5, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 6, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 7, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 8, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 9, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 10, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 11, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 12, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 13, (dirSorteble = 1 - dirSorteble));});
    th.rows[1].cells[c++].addEventListener('click', function () {sortTable(table, 14, (dirSorteble = 1 - dirSorteble));});
}

function changePaciente() {
    limpaCodigo('codigo');
    limpaTBody(document.getElementById('body_table'));
    if (!$("#carregar_mais").hasClass("hidden"))
        $("#carregar_mais").addClass("hidden");
}

window.onload = function () {
    addOrdenacao();

    if (!isNull($("#codigo").val())) {
        maisResultados();
    }
};

function insertRow(dados) {
    var table = document.getElementById('body_table');
    var urlBase = $("#base-url").val();

    var ultimaLinha = table.rows.length;

    for (var i in dados) {
        var row = table.insertRow(ultimaLinha++);

        var r = 0;

        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].dataFormatada);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].pressaoArterial);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].temperatura);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].frequenciaCardiaca);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].frequenciaRespiratoria);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].pupilas);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].dor);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].saturacao);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].liquidoAdministrado_viaOral);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].liquidoAdministrado_viaParenteral);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].liquidoEliminado_urinaVolume);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].liquidoEliminado_fezes);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].liquidoEliminado_vomito);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].observacao);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].hgt);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].login.pessoa.nmPessoa);

        var colButtons = row.insertCell(r++);
        colButtons.className = "text-right";
        colButtons.innerHTML =
                '<a class="btn btn-link btn-xs" title="Editar" rel="tooltip" data-placement="top" href="' + urlBase + 'sinaisVitais/editar/' + dados[i].idSinalVital + '">'
                + '<span class="glyphicon glyphicon-pencil"></span>'
                + '</a>'
                + '<a class="btn btn-link btn-xs" data-toggle="modal" data-target="#confirmacaoExclusaoModal" data-codigo=' + dados[i].idSinalVital + ' data-descricao="o registro selecionado"'
                + 'title="Excluir" rel="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>';
    }
}