const options = {
    url: function (phrase) {
        var url = $("#base-url").val() + "pacientes/";
        return url + "getPacientes?phrase=" + phrase;
    },
    getValue: "descricao",
    adjustWidth: false,
    list: {
        onChooseEvent: function () {
            changePaciente();
            
            const dados = $("#filter-paciente").getSelectedItemData();
            $("#codigo").val(dados.codigo);
            $("#novo-codigo").val(dados.codigo);
            $("#novo-nome").val(dados.descricao);
            $("#filter-paciente").focus();
            
            $("#closeNotification").click();
            
            buscarLista();
        }
    }
};

$("#filter-paciente").easyAutocomplete(options);

let lista = [];

function buscarLista() {
    const url = $("#base-url").val() + "diagnosticoPaciente/getLista";
    
    let filter = {};
    
    const idPaciente = $("#codigo").val();
    const nomePaciente = $("#novo-nome").val();
    
    if (!idPaciente) {
        swal("Informe o paciente para continuar.");
        return;
    }
    
    filter.id = parseInt(idPaciente);
    filter.descricao = nomePaciente;
    
    postRequest(url, filter)
        .then(data => {
    
            limpaLinhas();
    
            console.log(data);
            if (data && data.obj) {
                
                const diagnosticos = data.obj;
                
                this.lista = data.obj;
                
                if (!diagnosticos) {
                    return;
                }

                for (var i = 0; i < diagnosticos.length; i++) {
                    const row = insereDiagnostico(diagnosticos[i]);
                    
                    $(row).appendTo("#dados");
                }
            } else {
                
                $(`<div class="line-row-bottom diagnostico-row">
                    <p class="diagnostico-descricao">${data.mensagem}</p>
                </div>
                `).appendTo("#dados");
            }
        })
}

function insereDiagnostico(diagnostico) {
    
    const header = (`
        <div class="panel-heading">
            <div class="diagnostico-row">
                <p class="diagnostico-descricao">${diagnostico.descricao}</p>
                <div class="diagnostico-buttons">
                    <button type="button" onclick="moveParaCima(${diagnostico.id});" class="btn btn-link btn-xs" title="Ordenar a cima">
                        <span class="glyphicon glyphicon-arrow-up"></span>
                    </button>

                    <button type="button" onclick="moveParaBaixo(${diagnostico.id});" class="btn btn-link btn-xs" title="Ordenar a Baixo">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </button>

                    <button type="button" onclick="editarDiagnostico(${diagnostico.id}, 'MASTER');" class="btn btn-link btn-xs" title="Editar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>

                    <button type="button" onclick="excluirDiagnostico(${diagnostico.id});" class="btn btn-link btn-xs" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </div>
            </div>
        </div>`);
    
    
    
    
    let rows = "";
    
    if (diagnostico.filhos) {
        
        for (var i = 0; i < diagnostico.filhos.length; i++) {
            const filho = diagnostico.filhos[i];
            
            rows += (`
                <div class="line-row-bottom diagnostico-row">
                    <p class="diagnostico-descricao">${filho.descricao}</p>
                    <div class="diagnostico-buttons">
                        <button type="button" onclick="moveParaCima(${filho.id});" class="btn btn-link btn-xs" title="Ordenar a cima">
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        </button>

                        <button type="button" onclick="moveParaBaixo(${filho.id});" class="btn btn-link btn-xs" title="Ordenar a Baixo">
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        </button>

                        <button type="button" onclick="editarDiagnostico(${filho.id}, 'DATAIL');" class="btn btn-link btn-xs" title="Editar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>

                        <button type="button" onclick="excluirDiagnostico(${filho.id});" class="btn btn-link btn-xs" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </div>
                </div>
            `);
            
        }
        
    }
    
    return (`
    <div class="panel panel-default diagnostico-card">      
        ${header}

        <div class="panel-body diagnostico-body">
            ${rows}
        </div>

        <div class="container-fluid panel-footer">
            <div class="col-xs-9">
                <input placeholder="Insira a intervenção de enfermagem" class="form-control js-input-conduta" parent="${diagnostico.id}" onkeydown="enterGravarConduta(event, ${diagnostico.id})" maxlength="1000" type="text"/>
                <input class="js-input-conduta-id" parent="${diagnostico.id}" type="hidden"/>
            </div>
            <div class="col-xs-3">
                <button class="form-control btn btn-default" type="button" onclick="gravarConduta(${diagnostico.id})">Gravar</button>
            </div>
        </div>
    </div>

    `);
    
}

function limpaLinhas() {
    $("#dados").html("");
}





function changePaciente() {
    limpaCodigo('codigo');
}


function abreModalInsercao() {
    $("#modalInclusao").modal('show');
    $("#dsDiagnostico").val('');
    $("#idDiagnostico").val('');
    
    setTimeout(() => {
        $("#dsDiagnostico").focus();
    }, 300);
}

function gravarDiagnostico() {
    const descricao = $("#dsDiagnostico").val();
    const id = $("#idDiagnostico").val();
    
    gravar(id, descricao, "MASTER", null);
}

function enterGravarDiagnostico(event) {
    if (event.which == 13 || event.keyCode == 13) {
        //code to execute here
        gravarDiagnostico();
        return false;
    }
    
    return true;
}

function enterGravarConduta(event, idParent) {
    if (event.which == 13 || event.keyCode == 13) {
        //code to execute here
        gravarConduta(idParent);
        return false;
    }
    
    return true;
}

function gravarConduta(idParent) {
    const descricao = $(`.js-input-conduta[parent=${idParent}]`).val();
    const id = $(`.js-input-conduta-id[parent=${idParent}]`).val();
    
    gravar(id, descricao, "DETAIL", idParent);
}



function gravar(id, descricao, tipo, idParent) {
    
    if (!descricao) {
        swal("Informe a descrição para inserir!");
        return false;
    }
    
    let idPaciente = $("#codigo").val();
    
    if (!idPaciente) {
        swal("Informe o paciente para inserir!");
        return false;
    }
    
    idPaciente = parseInt(idPaciente);
       
    let dados = {
        descricao,
        tipo,
        paciente: {
            idPaciente
        },
        idParent
    };
    
    if (id) {
        dados.id = parseInt(id);
    }
    
    const url = $("#base-url").val() + "diagnosticoPaciente/gravar";

    postRequest(url, dados)
        .then(data => {
            console.log(data);
            if (data && data.ok) {
                $("#modalInclusao").modal('hide');
                buscarLista();
            }
        })
        .catch(() => {
            console.log("Erro ao processar a requisicao");
        });
}



function moveParaCima(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/ordenar/${id}/UP`;
    
    putRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}


function moveParaBaixo(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/ordenar/${id}/DOWN`;
    
    putRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}

function excluirDiagnostico(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/${id}`;
    
    deleteRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}

function editarDiagnostico(id, tipo) {
    const d = getObjectById(id);


    if (!d) {
        swal("Ops! Registro não encontrado.");
    }


    if (tipo === 'MASTER') {
        $("#modalInclusao").modal('show');
        $("#dsDiagnostico").val(d.descricao);
        $("#idDiagnostico").val(d.id);
    } else {
        $(`.js-input-conduta[parent=${d.idParent}]`).val(d.descricao);
        $(`.js-input-conduta-id[parent=${d.idParent}]`).val(d.id);
    }
}


function getObjectById(id) {
    if (this.lista) {
        for (i=0; i < this.lista.length; i++) {
            const d = this.lista[i];
            
            if (d.id === id) {
                return d;
            }
            
            // busca nos filhos tbm
            if (d.filhos) {
                for (f = 0; f < d.filhos.length; f++) {
                    const filho = d.filhos[f];
                    
                    if (filho.id === id) {
                        return filho;
                    }
                }
            }
        }
    }
    
    return null;
}


function exibeRelatorio() {
    let id = $("#codigo").val();
    const url = $("#base-url").val() + `diagnosticoPaciente/pdf/DiagnosticoPorPaciente/${id}`;
//    window.location.href = url;
    window.open(url,'_blank');
}

window.onload = function () {
    if (!isNull($("#codigo").val())) {
        buscarLista();
    }
};

