const options = {
    url: function (phrase) {
        var url = $("#base-url").val() + "pacientes/";
        return url + "getPacientes?phrase=" + phrase;
    },
    getValue: "descricao",
    adjustWidth: false,
    list: {
        onChooseEvent: function () {
            changePaciente();
            
            const dados = $("#filter-paciente").getSelectedItemData();
            $("#codigo").val(dados.codigo);
            $("#novo-codigo").val(dados.codigo);
            $("#novo-nome").val(dados.descricao);
            $("#edicao-codigo").val(dados.codigo);
            $("#edicao-nome").val(dados.descricao);
            $("#rel-codigo").val(dados.codigo);
            $("#rel-nome").val(dados.descricao);
            $("#filter-paciente").focus();
            
            $("#closeNotification").click();
            
            buscarLista();
        }
    }
};

$("#filter-paciente").easyAutocomplete(options);

let lista = [];

function buscarLista() {
    const url = $("#base-url").val() + "anamnese/getLista";
    
    let filter = {};
    
    const idPaciente = $("#codigo").val();
    const nomePaciente = $("#novo-nome").val();
    
    if (!idPaciente) {
        swal("Informe o paciente para continuar.");
        return;
    }
    
    filter.id = parseInt(idPaciente);
    filter.descricao = nomePaciente;
    filter.limit = 20;
    filter.offset = 0;
    
    postRequest(url, filter)
        .then(data => {
    
            $("#body_table").html("");
    
            console.log(data);
            if (data && data.obj) {
                
                const anamnese = data.obj;
                
                this.lista = data.obj;
                
                if (!anamnese) {
                    return;
                }

                for (var i = 0; i < anamnese.length; i++) {
                    const row = insereLinha(anamnese[i]);
                    
                    $(row).appendTo("#body_table");
                }
            } else {
                $(`<tr><td colspan="100%">
                    ${data.mensagem}
                </td></tr>
                `).appendTo("#body_table");
            }
        })
}

function insereLinha(obj) {
    const url = $("#base-url").val() + "anamnese/";
    
    
    return (`
<tr> 
    <td> 
        <input type="checkbox" value="${obj.id}"/> 
    </td>
    <td>${obj.dataStr}</td> 
    <td>${obj.nivelConscienciaStr}</td> 
    <td>${obj.orientacaoStr}</td> 
    
    <td>${limitaText(obj.mobilidadeFisicaStr)}</td> 
    <td>${obj.mobilidadeFisica_observacao ? limitaText(obj.mobilidadeFisica_observacao): ""}</td> 
    
    <td>${limitaText(obj.alimentacao_opcaoStr)}</td> 
    <td>${obj.alimentacao_observacao ? limitaText(obj.alimentacao_observacao): ""}</td> 
    
    <td>${limitaText(obj.hidratacao_opcaoStr)}</td> 
    <td>${obj.hidratacao_observacao ? limitaText(obj.hidratacao_observacao): ""}</td> 
    
    <td>${limitaText(obj.ventilacao_opcaoStr)}</td> 
    <td>${obj.ventilacao_observacao ? limitaText(obj.ventilacao_observacao): ""}</td> 
    
    <td>${limitaText(obj.cabeca_dsCranio)}</td> 
    <td>${limitaText(obj.cabeca_dsOlhos)}</td> 
    <td>${limitaText(obj.cabeca_dsOuvidos)}</td> 
    <td>${limitaText(obj.cabeca_dsNariz)}</td> 
    <td>${limitaText(obj.cabeca_dsCavidadeOral)}</td> 
    <td>${limitaText(obj.torax_dsInspecao)}</td> 
    <td>${limitaText(obj.torax_dsPalpacao)}</td> 
    <td>${limitaText(obj.torax_dsAusculta)}</td> 
    <td>${limitaText(obj.abdome_dsInspecao)}</td> 
    <td>${limitaText(obj.abdome_dsPalpacao)}</td> 
    <td>${limitaText(obj.abdome_dsAusculta)}</td> 
    <td>${limitaText(obj.abdome_dsOstomia)}</td> 
    <td>${limitaText(obj.membros_dsInspecao)}</td> 
    <td>${limitaText(obj.membros_dsPalpacao)}</td> 
    <td>${obj.genitarioMasculino_dsInspecao ? limitaText(obj.genitarioMasculino_dsInspecao) : ""}</td> 
    <td>${obj.genitarioFeminino_dsInspecao ? limitaText(obj.genitarioFeminino_dsInspecao) : ""}</td> 
    <td>${obj.genitarioFeminino_dsPerianal ? limitaText(obj.genitarioFeminino_dsPerianal) : ""}</td> 
    <td>${obj.eliminacaoFisiologicas_incontinenciaUrinariaStr}</td> 
    <td>${obj.eliminacaoFisiologicas_incontinenciaIntestinalStr}</td> 
    <td>${limitaText(obj.eliminacaoFisiologicas_dsEvacuacao)}</td> 
    <td>${limitaText(obj.eliminacaoFisiologicas_dsDiurese)}</td> 
    <td>${limitaText(obj.peleAnexos_dsInspecao)}</td> 
    <td>${limitaText(obj.ulcerasStr)}</td> 
    
    <td>${limitaText(obj.dsConduta)}</td>
    <td class="text-right" style="min-width: 80px;">
        <button type="button" form="form-edicao" onclick="abreEdicao(${obj.id});" class="btn-link">
            <span class="glyphicon glyphicon-pencil"></span>
        </button>
        <a class="btn btn-link btn-xs" data-toggle="modal" data-target="#confirmacaoExclusaoModal" data-codigo="${obj.id}" data-descricao="Registro do dia ${obj.dataStr}" title="Excluir" rel="tooltip" data-placement="top">
            <span class="glyphicon glyphicon-remove"></span>
        </a>
    </td>
</tr>`);
}



function limitaText(text) {
    if (text && text.length > 50) {
        return text.substring(0, 47) + "...";
    } 
    
    return text;
}



function changePaciente() {
    limpaCodigo('codigo');
}


function abreInsercao() {
    var nome = $("#novo-nome").val();
    var codigo = $("#novo-codigo").val();

    if (nome !== null && codigo !== null && nome.length > 0 && codigo.length > 0) {
        $("#form-novo").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para inserir.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}

function abreEdicao(id) {
    $("#edicao-id").val(id);
    $("#form-edicao").submit();
}

function gravarDiagnostico() {
    const descricao = $("#dsDiagnostico").val();
    const id = $("#idDiagnostico").val();
    
    gravar(id, descricao, "MASTER", null);
}

function enterGravarDiagnostico(event) {
    if (event.which == 13 || event.keyCode == 13) {
        //code to execute here
        gravarDiagnostico();
        return false;
    }
    
    return true;
}

function enterGravarConduta(event, idParent) {
    if (event.which == 13 || event.keyCode == 13) {
        //code to execute here
        gravarConduta(idParent);
        return false;
    }
    
    return true;
}

function gravarConduta(idParent) {
    const descricao = $(`.js-input-conduta[parent=${idParent}]`).val();
    const id = $(`.js-input-conduta-id[parent=${idParent}]`).val();
    
    gravar(id, descricao, "DETAIL", idParent);
}



function gravar(id, descricao, tipo, idParent) {
    
    if (!descricao) {
        swal("Informe a descrição para inserir!");
        return false;
    }
    
    let idPaciente = $("#codigo").val();
    
    if (!idPaciente) {
        swal("Informe o paciente para inserir!");
        return false;
    }
    
    idPaciente = parseInt(idPaciente);
       
    let dados = {
        descricao,
        tipo,
        paciente: {
            idPaciente
        },
        idParent
    };
    
    if (id) {
        dados.id = parseInt(id);
    }
    
    const url = $("#base-url").val() + "diagnosticoPaciente/gravar";

    postRequest(url, dados)
        .then(data => {
            console.log(data);
            if (data && data.ok) {
                $("#modalInclusao").modal('hide');
                buscarLista();
            }
        })
        .catch(() => {
            console.log("Erro ao processar a requisicao");
        });
}



function moveParaCima(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/ordenar/${id}/UP`;
    
    putRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}


function moveParaBaixo(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/ordenar/${id}/DOWN`;
    
    putRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}

function excluirDiagnostico(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/${id}`;
    
    deleteRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}

function editarDiagnostico(id, tipo) {
    const d = getObjectById(id);


    if (!d) {
        swal("Ops! Registro não encontrado.");
    }


    if (tipo === 'MASTER') {
        $("#modalInclusao").modal('show');
        $("#dsDiagnostico").val(d.descricao);
        $("#idDiagnostico").val(d.id);
    } else {
        $(`.js-input-conduta[parent=${d.idParent}]`).val(d.descricao);
        $(`.js-input-conduta-id[parent=${d.idParent}]`).val(d.id);
    }
}


function getObjectById(id) {
    if (this.lista) {
        for (i=0; i < this.lista.length; i++) {
            const d = this.lista[i];
            
            if (d.id === id) {
                return d;
            }
            
            // busca nos filhos tbm
            if (d.filhos) {
                for (f = 0; f < d.filhos.length; f++) {
                    const filho = d.filhos[f];
                    
                    if (filho.id === id) {
                        return filho;
                    }
                }
            }
        }
    }
        
    return null;
}


function exibeRelatorio() {
    
    const linhasSelecionadas = getSelectedCheckbox();
    
   
    if (!linhasSelecionadas || linhasSelecionadas.length <= 0) {
        swal("Selecione ao menos uma linha para continuar");
        return;
    }
    
    document.getElementById('rel-codigos').value = linhasSelecionadas;
    $("#form-relatorio").submit();
}

window.onload = function () {
    if (!isNull($("#codigo").val())) {
        buscarLista();
    }
};

