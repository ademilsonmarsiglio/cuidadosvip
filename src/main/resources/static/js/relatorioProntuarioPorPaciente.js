function buscarLista(offset, limit){
    var _urlBuscaLista = $("#base-url").val() + "relatorios/prontuarioPorPaciente/getLista";

    var _data1 = $("#filter-date1").val();
    var _data2 = $("#filter-date2").val();

    var _json = "";

    //monta json
    if (_data1 !== null && _data1.length > 0) {
        _json += " \"dateStr1\": \"" + _data1 + "\", ";
    }

    if (_data2 !== null && _data2.length > 0) {
        _json += " \"dateStr2\": \"" + _data2 + "\", ";
    }

    _json += "\"offset\": " + offset + ", ";
    _json += "\"limit\": " + limit +" ";
    _json = "{" + _json + "}";

    $.ajax({
        method: "POST",
        url: _urlBuscaLista,
        data: {filtro_consulta: _json},
        success: function (data) {
            if (data !== null && data.length > 0){
                insertRow(data);
            }

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        }, 
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Erro ao obter resultados da lista.");
            console.log(textStatus, errorThrown);

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        }, 
        beforeSend: function (jqXHR, settings) {
            $("#loader").removeClass("loader-hidden");
            $("#img-carregar").addClass("loader-hidden");
        }
    });
}

function maisResultados(){
    var rowCont = $("#table-paciente tbody tr").size();
    buscarLista(rowCont, rowCont + 20);
}

var dirSorteble = 1;
function addOrdenacao(){
    var table = document.getElementById('table-paciente');
    var th = table.tHead;
    th.rows[0].cells[1].addEventListener('click', function () {sortTable(table, 1, (dirSorteble = 1 - dirSorteble));});
}

window.onload = function () {
    addOrdenacao();
    buscarLista(0, 10);
};

 $('#selectAll').click(function(e){
    var table= $(e.target).closest('table');
    $('td input:checkbox',table).prop('checked',this.checked);
});

function insertRow(dados) {
    var table = document.getElementById('body_table');
    var ultimaLinha = table.rows.length;

    for (var i in dados) {
        var row = table.insertRow(ultimaLinha++);
        var c = 0;

        row.insertCell(c++).innerHTML = "<input type=\"checkbox\" value=\"" + dados[i].idPaciente + "\"/>";
        row.insertCell(c++).innerHTML = dados[i].pessoa.nmPessoa;
    }
}

$('#confirmacaoRelatorioProntuario').on('show.bs.modal', function (event) {
    $('#data1').val($('#filter-date1').val());
    $('#data2').val($('#filter-date2').val());
});