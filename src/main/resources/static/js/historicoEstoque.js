var optionsPaciente = {
    url: function (phrase) {
        var url = $("#base-url").val() + "pacientes/";
        return url + "getPacientes?phrase=" + phrase;
    },
    getValue: "descricao",
    adjustWidth: false,
    list: {
        onChooseEvent: function () {
            var dados = $("#filter-paciente").getSelectedItemData();
            $("#idPaciente").val(dados.codigo);
            
            $("#idEstoque").val(null);
            $("#filter-produto").val(null);
            $("#filter-produto").focus();
            
            $("#link_medicamentos").attr("href", $("#base-url").val() + "medicamentos?id=" + dados.codigo + "&descricao=" + dados.descricao);
        }
    }
};

var optionsEstoque = {
    url: function (phrase) {
        var url = $("#base-url").val();
        var idPaciente = $("#idPaciente").val();
        
        if (!isNull(idPaciente)) {
            idPaciente = "&idPaciente="+idPaciente;
        }
        
        return url + "medicamentos/getMedicamentos?phrase=" + phrase + idPaciente;
    },
    adjustWidth: false,
    getValue: "descricao",
    list: {
        onChooseEvent: function () {
            var obj = $("#filter-produto").getSelectedItemData();
            $("#idEstoque").val(obj.codigo);
            
            $("#novo-idEstoque").val(obj.codigo);
            $("#novo-produto").val(obj.descricao);

            if (typeof obj.qtd !== "undefined") {
                $("#qtdEstoque").html(obj.qtd.formatMoney(2, "", ".", ","));
            } else {
                $("#qtdEstoque").html("0");
            }
            
            limpaTBody(document.getElementById('body_table'));
            maisResultados();
        }
    }
};

$("#filter-produto").easyAutocomplete(optionsEstoque);
$("#filter-paciente").easyAutocomplete(optionsPaciente);

function validaInsercao() {
    var codigo = $("#filter-produto").val();

    if (codigo !== null && codigo.length > 0) {
        $("#form-novo").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe um produto para inserir.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}

//function validaRelatorio() {
//    var codigo = $("#filter-produto").val();
//
//    $("#rel-codigo").val(codigo);
//    $("#rel-descricao").val(descricao);
//    $("#rel-date1").val(date1);
//    $("#rel-date2").val(date2);
//
//    if (descricao !== null && codigo !== null && descricao.length > 0 && codigo.length > 0) {
//        $("#form-relatorio").submit();
//    } else {
//        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para continuar.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
//    }
//}

function maisResultados() {
    var _urlBuscaLista = $("#base-url").val() + "historicoEstoque/getLista";

    var filtro = {
        idPaciente: $("#idPaciente").val(),
        idEstoque: $("#idEstoque").val(),
        limit: 15,
        offset: $("#table-historicoestoque tbody tr").size()
    };

    $.ajax({
        method: "POST",
        contentType: "application/json",
        url: _urlBuscaLista,
        data: JSON.stringify(filtro),
        dataType: 'json',
        success: function (data) {
            insertRow(data);

            if (data == null || data.length == 0) {
                if (!$("#carregar_mais").hasClass("hidden"))
                    $("#carregar_mais").addClass("hidden");
            } else {
                $("#carregar_mais").removeClass("hidden");
            }

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Erro ao obter resultados da lista.");
            console.log(textStatus, errorThrown);

            $("#img-carregar").removeClass("loader-hidden");
            $("#loader").addClass("loader-hidden");
        },
        beforeSend: function (jqXHR, settings) {
            $("#loader").removeClass("loader-hidden");
            $("#img-carregar").addClass("loader-hidden");
        }
    });
}

function insertRow(dados) {
    var table = document.getElementById('body_table');
    var urlBase = $("#base-url").val();

    var ultimaLinha = table.rows.length;
    
    var isAdmin = $("#session-is-admin").val();

    for (var i in dados) {
        var row = table.insertRow(ultimaLinha++);
        var r = 0;

        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].dataFormatada);
        row.insertCell(r++).innerHTML = typeof dados[i].qtd !== "undefined" ? dados[i].qtd.formatMoney(2, "", ".", ",") : "";
        row.insertCell(r++).innerHTML = typeof dados[i].saldo !== "undefined" ? dados[i].saldo.formatMoney(2, "", ".", ",") : "";
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].login.pessoa.nmPessoa);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].tipo);
        row.insertCell(r++).innerHTML = emptyUndefined(dados[i].observacao);

        var colButtons = row.insertCell(r++);
        colButtons.className = "text-right";
        
        if ("true" === isAdmin) {
            if ('PRONTUARIO' === dados[i].tipo) {
                colButtons.innerHTML =
                    '<a class="btn btn-link btn-xs" title="Editar" rel="tooltip" data-placement="top" href="' + urlBase + 'prontuario/' + dados[i].idPaciente + '/' + dados[i].id + '">'
                    + '<span class="glyphicon glyphicon-pencil"></span>'
                    + '</a>'
                    + '';
            } else {
                colButtons.innerHTML =
                        '<a class="btn btn-link btn-xs" title="Editar" rel="tooltip" data-placement="top" href="' + urlBase + 'historicoEstoque/editar/' + dados[i].id + '">'
                        + '<span class="glyphicon glyphicon-pencil"></span>'
                        + '</a>'
                        + '<a class="btn btn-link btn-xs" data-toggle="modal" data-target="#confirmacaoExclusaoModal" data-codigo=' + dados[i].id + ' data-descricao="o registro selecionado"'
                        + 'title="Excluir" rel="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>';
            }
        } else {
            colButtons.innerHTML = "";
        }
    }
}

window.onload = function () {
    if (!isNull($("#filter-produto").val())) {
        limpaTBody(document.getElementById('body_table'));
        maisResultados();
    }
};