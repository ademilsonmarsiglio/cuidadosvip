const options = {
    url: function (phrase) {
        var url = $("#base-url").val() + "pacientes/";
        return url + "getPacientes?phrase=" + phrase;
    },
    getValue: "descricao",
    adjustWidth: false,
    list: {
        onChooseEvent: function () {
            changePaciente();
            
            const dados = $("#filter-paciente").getSelectedItemData();
            $("#codigo").val(dados.codigo);
            $("#novo-codigo").val(dados.codigo);
            $("#novo-nome").val(dados.descricao);
            $("#edicao-codigo").val(dados.codigo);
            $("#edicao-nome").val(dados.descricao);
            $("#rel-codigo").val(dados.codigo);
            $("#rel-nome").val(dados.descricao);
            $("#filter-paciente").focus();
            
            $("#closeNotification").click();
            
            buscarLista();
        }
    }
};

$("#filter-paciente").easyAutocomplete(options);

let lista = [];

function buscarLista() {
    const url = $("#base-url").val() + "qualidadeVida/getLista";
    
    let filter = {};
    
    const idPaciente = $("#codigo").val();
    const nomePaciente = $("#novo-nome").val();
    
    if (!idPaciente) {
        swal("Informe o paciente para continuar.");
        return;
    }
    
    filter.idPaciente = parseInt(idPaciente);
    filter.nmPaciente = nomePaciente;
    filter.limit = 10;
    filter.offset = 0;
    
    postRequest(url, filter)
        .then(data => {
    
            $("#body_table").html("");
    
            console.log(data);
            if (data && data.obj) {
                
                const qualidadeVida = data.obj;
                
                this.lista = data.obj;
                
                if (!qualidadeVida) {
                    return;
                }

                for (var i = 0; i < qualidadeVida.length; i++) {
                    const row = insereLinha(qualidadeVida[i]);
                    
                    $(row).appendTo("#body_table");
                }
            } else {
                $(`<tr><td colspan="100%">
                    ${data.mensagem}
                </td></tr>
                `).appendTo("#body_table");
            }
        })
}

function insereLinha(obj) {
    const url = $("#base-url").val() + "qualidadeVida/";
    
    
    return (`
<tr> 
    <td rowspan="4"> 
        <input type="checkbox" value="${obj.id}"/> 
    </td>
    <td rowspan="4">${obj.dataStr}</td> 
    <td class="cell-bg-realce">Total</td> 
    <td class="text-right">${obj.ttFuncionamentoSensorio}</td>
    <td class="text-right">${obj.ttAutonomia}</td>
    <td class="text-right">${obj.ttAtividadesPPF}</td>
    <td class="text-right">${obj.ttParticipacaoSocial}</td>
    <td class="text-right">${obj.ttMorteMorrer}</td>
    <td class="text-right">${obj.ttIntimidade}</td>
    <td class="cell-bg-realce text-right">${obj.ttGeral}</td>
    <td rowspan="4" class="text-right">
        <button type="button" form="form-edicao" onclick="abreEdicao(${obj.id});" class="btn-link">
            <span class="glyphicon glyphicon-pencil"></span>
        </button>
        <a class="btn btn-link btn-xs" data-toggle="modal" data-target="#confirmacaoExclusaoModal" data-codigo="${obj.id}" data-descricao="Registro do dia ${obj.dataStr}" title="Excluir" rel="tooltip" data-placement="top">
            <span class="glyphicon glyphicon-remove"></span>
        </a>
    </td>
</tr>
<tr> 
    <td class="borderless-top cell-bg-realce">Média</td> 
    <td class="borderless-top text-right">${obj.medFuncionamentoSensorio}</td>
    <td class="borderless-top text-right">${obj.medAutonomia}</td>
    <td class="borderless-top text-right">${obj.medAtividadesPPF}</td>
    <td class="borderless-top text-right">${obj.medParticipacaoSocial}</td>
    <td class="borderless-top text-right">${obj.medMorteMorrer}</td>
    <td class="borderless-top text-right">${obj.medIntimidade}</td>
    <td class="borderless-top cell-bg-realce text-right">${obj.medGeral}</td>
</tr>
<tr>  
    <td class="borderless-top cell-bg-realce">Percentual</td> 
    <td class="borderless-top text-right">${obj.peFuncionamentoSensorio}</td>
    <td class="borderless-top text-right">${obj.peAutonomia}</td>
    <td class="borderless-top text-right">${obj.peAtividadesPPF}</td>
    <td class="borderless-top text-right">${obj.peParticipacaoSocial}</td>
    <td class="borderless-top text-right">${obj.peMorteMorrer}</td>
    <td class="borderless-top text-right">${obj.peIntimidade}</td>
    <td class="borderless-top cell-bg-realce text-right">${obj.peGeral}</td>
</tr>
<tr>  
    <td class="borderless-top cell-bg-realce">Qualidade</td> 
    <td class="borderless-top cell-bg-realce text-center">${obj.qualitySensoriais}</td>
    <td class="borderless-top cell-bg-realce text-center">${obj.qualityAutonomia}</td>
    <td class="borderless-top cell-bg-realce text-center">${obj.qualityAtividadesPPF}</td>
    <td class="borderless-top cell-bg-realce text-center">${obj.qualityPartSocial}</td>
    <td class="borderless-top cell-bg-realce text-center">${obj.qualityMorteMorrer}</td>
    <td class="borderless-top cell-bg-realce text-center">${obj.qualityIntimidade}</td>
    <td class="borderless-top cell-bg-realce text-center">${obj.qualityGeral}</td>
</tr>`);
}

function changePaciente() {
    limpaCodigo('codigo');
}


function abreInsercao() {
    var nome = $("#novo-nome").val();
    var codigo = $("#novo-codigo").val();

    if (nome !== null && codigo !== null && nome.length > 0 && codigo.length > 0) {
        $("#form-novo").submit();
    } else {
        $("#js-replace").html('<div id="js-replace" class="alert alert-danger alert-dismissable fade in"> <label>Informe o paciente para inserir.</label><a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>');
    }
}

function abreEdicao(id) {
    $("#edicao-id").val(id);
    $("#form-edicao").submit();
}

function gravarDiagnostico() {
    const descricao = $("#dsDiagnostico").val();
    const id = $("#idDiagnostico").val();
    
    gravar(id, descricao, "MASTER", null);
}

function enterGravarDiagnostico(event) {
    if (event.which == 13 || event.keyCode == 13) {
        //code to execute here
        gravarDiagnostico();
        return false;
    }
    
    return true;
}

function enterGravarConduta(event, idParent) {
    if (event.which == 13 || event.keyCode == 13) {
        //code to execute here
        gravarConduta(idParent);
        return false;
    }
    
    return true;
}

function gravarConduta(idParent) {
    const descricao = $(`.js-input-conduta[parent=${idParent}]`).val();
    const id = $(`.js-input-conduta-id[parent=${idParent}]`).val();
    
    gravar(id, descricao, "DETAIL", idParent);
}



function gravar(id, descricao, tipo, idParent) {
    
    if (!descricao) {
        swal("Informe a descrição para inserir!");
        return false;
    }
    
    let idPaciente = $("#codigo").val();
    
    if (!idPaciente) {
        swal("Informe o paciente para inserir!");
        return false;
    }
    
    idPaciente = parseInt(idPaciente);
       
    let dados = {
        descricao,
        tipo,
        paciente: {
            idPaciente
        },
        idParent
    };
    
    if (id) {
        dados.id = parseInt(id);
    }
    
    const url = $("#base-url").val() + "diagnosticoPaciente/gravar";

    postRequest(url, dados)
        .then(data => {
            console.log(data);
            if (data && data.ok) {
                $("#modalInclusao").modal('hide');
                buscarLista();
            }
        })
        .catch(() => {
            console.log("Erro ao processar a requisicao");
        });
}



function moveParaCima(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/ordenar/${id}/UP`;
    
    putRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}


function moveParaBaixo(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/ordenar/${id}/DOWN`;
    
    putRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}

function excluirDiagnostico(id) {
    const url = $("#base-url").val() + `diagnosticoPaciente/${id}`;
    
    deleteRequest(url)
        .then(data => {
            if (data) {
                buscarLista();
            }
        });
}

function editarDiagnostico(id, tipo) {
    const d = getObjectById(id);


    if (!d) {
        swal("Ops! Registro não encontrado.");
    }


    if (tipo === 'MASTER') {
        $("#modalInclusao").modal('show');
        $("#dsDiagnostico").val(d.descricao);
        $("#idDiagnostico").val(d.id);
    } else {
        $(`.js-input-conduta[parent=${d.idParent}]`).val(d.descricao);
        $(`.js-input-conduta-id[parent=${d.idParent}]`).val(d.id);
    }
}


function getObjectById(id) {
    if (this.lista) {
        for (i=0; i < this.lista.length; i++) {
            const d = this.lista[i];
            
            if (d.id === id) {
                return d;
            }
            
            // busca nos filhos tbm
            if (d.filhos) {
                for (f = 0; f < d.filhos.length; f++) {
                    const filho = d.filhos[f];
                    
                    if (filho.id === id) {
                        return filho;
                    }
                }
            }
        }
    }
        
    return null;
}


function exibeRelatorio() {
     
    const linhasSelecionadas = getSelectedCheckbox();
    
   
    if (!linhasSelecionadas || linhasSelecionadas.length <= 0) {
        swal("Selecione ao menos uma linha para continuar");
        return;
    }
    
    document.getElementById('rel-codigos').value = linhasSelecionadas;
    $("#form-relatorio").submit();
}

window.onload = function () {
    const id = $("#codigo").val();
    if (id) {
        buscarLista();
    }
};

